﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace EA.WPF.Controls.Views.EAToolBar
{


    /// <summary>
    /// Interaction logic for EAToolBar.xaml
    /// </summary>
    public partial class EAToolBar : UserControl, INotifyPropertyChanged
    {


        public delegate void FileNewDelegate(object sender, EventArgs e);
        public delegate void FileOpenDelegate(object sender, EventArgs e);
        public delegate void FileDeleteDelegate(object sender, EventArgs e);
        public delegate void FileSaveDelegate(object sender, EventArgs e);
        public delegate void FilePrintDelegate(object sender, EventArgs e);
        public delegate void FilePrintPreviewDelegate(object sender, EventArgs e);
        public delegate void FilePreferncesDelegate(object sender, EventArgs e);

        public event FileNewDelegate FileNew;
        public event FileOpenDelegate FileOpen;
        public event FileDeleteDelegate FileDelete;
        public event FileSaveDelegate FileSave;
        public event FilePrintDelegate FilePrint;
        //   public event FilePrintPreviewDelegate FilePrintPreview;
        public event FilePreferncesDelegate FilePrefernces;

        public delegate void EditCutDelegate(object sender, EventArgs e);
        public delegate void EditCopyDelegate(object sender, EventArgs e);
        public delegate void EditPasteDelegate(object sender, EventArgs e);

        public event EditCutDelegate EditCut;
        public event EditCopyDelegate EditCopy;
        public event EditPasteDelegate EditPaste;

        
        public delegate void HelpDelegate(object sender, EventArgs e);
        public event HelpDelegate Help;

        public delegate void ViewReportDelegate(object sender, ReportEventArgs e);
        public event ViewReportDelegate ViewReport;

        public List<string> Reports
        {
            get { return (List<string>)GetValue(ReportsProperty); }
            set
            {
                SetValue(ReportsProperty, value);
            }
        }

        private void SetReportMenuItems()
        {
            var reports = (List<string>)GetValue(ReportsProperty);
            if (reports != null && reports.Count > 0)
            {
                mnuReports.Items.Clear();
                foreach (var report in reports)
                {
                    var mi = new RadMenuItem();
                    mi.Header = report;
                    mi.Click += MnuReport_OnClick;
                    mnuReports.Items.Add(mi);
                }
            }
        }

        public static readonly DependencyProperty ReportsProperty = DependencyProperty.Register("Reports", typeof(List<string>), typeof(EAToolBar), new FrameworkPropertyMetadata(new List<string>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ReportListPropertyChangedCallback));

        private static void ReportListPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((EAToolBar)dependencyObject).SetReportMenuItems();
        }
        public EAToolBar()
        {
            InitializeComponent();
            IsEditMode = false;
        }

        private void CmdNew_OnClick(object sender, RoutedEventArgs e)
        {
            if (FileNew != null) FileNew(sender, e);
        }

        private void CmdOpen_OnClick(object sender, RoutedEventArgs e)
        {
            if (FileOpen != null) FileOpen(sender, e);
        }

        private void CmdDelete_OnClick(object sender, RoutedEventArgs e)
        {
            if (FileDelete != null) FileDelete(sender, e);
        }

        private void CmdSave_OnClick(object sender, RoutedEventArgs e)
        {
            if (FileSave != null) FileSave(sender, e);
        }

        private void CmdPrint_OnClick(object sender, RoutedEventArgs e)
        {
            if (FilePrint != null) FilePrint(sender, e);
        }

        private void CmdPrintPreview_OnClick(object sender, RoutedEventArgs e)
        {
            if (FilePrint != null) FilePrint(sender, e);
        }

        private void CmdSettings_OnClick(object sender, RoutedEventArgs e)
        {
            if (FilePrefernces != null) FilePrefernces(sender, e);
        }

        private void CmdCut_OnClick(object sender, RoutedEventArgs e)
        {
            if (EditCut != null) EditCut(sender, e);
        }

        private void CmdCopy_OnClick(object sender, RoutedEventArgs e)
        {
            if (EditCopy != null) EditCopy(sender, e);
        }

        private void CmdPaste_OnClick(object sender, RoutedEventArgs e)
        {
            if (EditPaste != null) EditPaste(sender, e);
        }

        public bool HideMenu
        {
            get { return mnuMain.Visibility != Visibility.Visible; }
            set
            {
                mnuMain.Visibility = value ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public Visibility FileNewVisibility
        {
            get { return cmdNew.Visibility; }
            set
            {
                cmdNew.Visibility = value;
                mnuNew.Visibility = value;
            }
        }

        public Visibility FileOpenVisibility
        {
            get { return cmdOpen.Visibility; }
            set
            {
                cmdOpen.Visibility = value;
                mnuOpen.Visibility = value;
            }
        }

        public Visibility FileDeleteVisibility
        {
            get { return cmdDelete.Visibility; }
            set
            {
                cmdDelete.Visibility = value;
                mnuDelete.Visibility = value;
            }
        }

        public Visibility FileSaveVisibility
        {
            get { return cmdSave.Visibility; }
            set
            {
                cmdSave.Visibility = value;
                mnuSave.Visibility = value;
            }
        }

        public Visibility FilePrintVisibility
        {
            get { return cmdPrint.Visibility; }
            set
            {
                cmdPrint.Visibility = value;
                mnuPrint.Visibility = value;
            }
        }

        public Visibility FileLockVisibility
        {
            get
            {
                if (imgEditLocked.Visibility == Visibility.Collapsed &&
                    imgEditUnLocked.Visibility == Visibility.Collapsed)
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
            set
            {
                if (value == Visibility.Collapsed)
                {
                    imgEditLocked.Visibility = Visibility.Collapsed;
                    imgEditLockedTB.Visibility = Visibility.Collapsed;
                    imgEditUnLocked.Visibility = Visibility.Collapsed;
                    imgEditUnLockedTB.Visibility = Visibility.Collapsed;
                    mnuEdit.Visibility = Visibility.Collapsed;
                    cmdEdit.Visibility = Visibility.Collapsed;
                }
            }
        }

        public Visibility FilePrintPreviewVisibility
        {
            get { return cmdPrintPreview.Visibility; }
            set
            {
                cmdPrintPreview.Visibility = value;
                mnuPrintPreview.Visibility = value;
            }
        }

        public Visibility EditCutVisibility
        {
            get { return cmdCut.Visibility; }
            set
            {
                cmdCut.Visibility = value;
                mnuCut.Visibility = value;
            }
        }

        public Visibility EditCopyVisibility
        {
            get { return cmdCopy.Visibility; }
            set
            {
                cmdCopy.Visibility = value;
                mnuCopy.Visibility = value;
            }
        }

        public Visibility EditPasteVisibility
        {
            get { return cmdPaste.Visibility; }
            set
            {
                cmdPaste.Visibility = value;
                mnuPaste.Visibility = value;
            }
        }


     
        public Visibility FilePreferencesVisibility
        {
            get { return cmdSettings.Visibility; }
            set
            {
                cmdSettings.Visibility = value;
                mnuSettings.Visibility = value;
            }
        }

        public Visibility FileVisibility
        {
            get { return mnuFile.Visibility; }
            set
            {
                mnuFile.Visibility = value;
                sepFile.Visibility = value;
            }
        }


        public Visibility EditVisibility
        {
            get { return mnuModify.Visibility; }
            set
            {
                mnuModify.Visibility = value;
                sepCopyPaste.Visibility = value;
            }
        }

        public Visibility ReportsVisibility
        {
            get { return mnuReports.Visibility; }
            set { mnuReports.Visibility = value; }
        }

        public Visibility HelpVisibility
        {
            get { return mnuHelp.Visibility; }
            set
            {
                mnuHelp.Visibility = value;
                cmdHelp.Visibility = value;
            }
        }

        public bool IsEditMode
        {
            get { return (bool)GetValue(IsEditModeProperty); }
            set { SetValue(IsEditModeProperty, value); }
        }

        public static readonly DependencyProperty IsEditModeProperty =
            DependencyProperty.Register("IsEditMode", typeof(Boolean),
            typeof(EAToolBar),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((EAToolBar)dependencyObject).imgEditLockedTB.Visibility = (bool)dependencyPropertyChangedEventArgs.NewValue ? Visibility.Collapsed : Visibility.Visible;
            ((EAToolBar)dependencyObject).imgEditLocked.Visibility = (bool)dependencyPropertyChangedEventArgs.NewValue ? Visibility.Collapsed : Visibility.Visible;
            ((EAToolBar)dependencyObject).imgEditUnLockedTB.Visibility = (bool)dependencyPropertyChangedEventArgs.NewValue ? Visibility.Visible : Visibility.Collapsed;
            ((EAToolBar)dependencyObject).imgEditUnLocked.Visibility = (bool)dependencyPropertyChangedEventArgs.NewValue ? Visibility.Visible : Visibility.Collapsed;
        }



        private void CmdEdit_OnClick(object sender, RoutedEventArgs e)
        {
            IsEditMode = !(bool)GetValue(IsEditModeProperty);
        }

        private void ChkEditMode_OnClick(object sender, RoutedEventArgs e)
        {
            IsEditMode = !(bool)GetValue(IsEditModeProperty);
        }

        private void MnuHelp_OnClick(object sender, RadRoutedEventArgs e)
        {
            if (Help != null) Help(sender, e);
        }


        private void CmdHelp_OnClick(object sender, RoutedEventArgs e)
        {
            if (Help != null) Help(sender, e);
        }

        private void MnuReport_OnClick(object sender, RadRoutedEventArgs e)
        {
            if (ViewReport != null) ViewReport(sender, new ReportEventArgs { ReportName = ((RadMenuItem)sender).Header.ToString() });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ReportEventArgs : EventArgs
    {
        public string ReportName { get; set; }
    }
}

