﻿using EA.Common.Helpers.Database;
using EA.Common.Helpers.Database.DataContexts;
using EA.Common.Helpers.Util;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace EA.Common.Helpers.Database
{
    public class DatabaseContextFactory : ObservableObject
    {
        /*
         * Install-Package Microsoft.EntityFrameworkCore
         * Install-Package Microsoft.EntityFrameworkCore.SqlServer
         * Install-Package Microsoft.EntityFrameworkCore.Tools
         * 
         * Scaffold-DbContext 'Data Source=.;Initial Catalog=PCE_Cache;Integrated Security=true' -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -ContextDir Data -OutputDir Models -Context UserContext -Tables tbl_users,tbl_groups,tbl_rights,rel_users_rights,rel_users_groups,rel_groups_rights
         * 
         */

        //public string PLMLineName { get; }
        //public string RLMLineName { get; }

        public string MQAKioskName { get; set; }

        //public DbContextOptions PLMGlobalDbContextOptions { get; }
        //public DbContextOptions PLMLocalDbContextOptions { get; }
        //public DbContextOptions RLMGlobalDbContextOptions { get; }
        //public DbContextOptions RLMLocalDbContextOptions { get; }

        public DbContextOptions MQAKioskDbContextOptions { get; }

        //public DatabaseState PLMGlobalState { get; } = new DatabaseState();
        //public DatabaseState PLMLocalState { get; } = new DatabaseState();
        //public DatabaseState RLMGlobalState { get; } = new DatabaseState();
        //public DatabaseState RLMLocalState { get; } = new DatabaseState();
        public DatabaseState MQAKioskState { get; } = new DatabaseState();

        public List<DatabaseMonitor> DatabaseMonitors { get; }
        //public DatabaseMonitor PLMGlobalMonitor { get; }
        //public DatabaseMonitor PLMLocalMonitor { get; }
        //public DatabaseMonitor RLMGlobalMonitor { get; }
        //public DatabaseMonitor RLMLocalMonitor { get; }

        public DatabaseMonitor MQAKioskMonitor { get; }

        public void ConfigureSurvices(IServiceCollection services)
        {
            services.AddDbContextPool<MQAKioskLoginContext>(options => options.UseSqlServer(@"Data Source = 127.0.0.1 ;Database=MQA_Kiosk; User ID = MQAKiosk ;password = mqakiosk"));
        }

        public DatabaseContextFactory()
        {
            DatabaseMonitors = new List<DatabaseMonitor>();

            //Todo Add json. file or app config to handle user name and passoerd

            //RegistryLoader registryLoader = new RegistryLoader();
            ConnectionStrings connectionStrings = new ConnectionStrings()
            {
                MQAKiosk = new Microsoft.Data.SqlClient.SqlConnectionStringBuilder()
                {ConnectionString = @"Data Source = 127.0.0.1 ;Database=MQA_Kiosk; User ID = MQAKiosk ;password = mqakiosk",InitialCatalog = "MQA_Kiosk" },
                MQAKioskName = "MQA_KIOSK"

                 
            };  // registryLoader.ReadConnectionStrings();

            //            PLMLineName = connectionStrings.PlmLineName ?? string.Empty;
            //            RLMLineName = connectionStrings.RlmLineName ?? string.Empty;
            if (connectionStrings.MQAKiosk != null)
            {
                MQAKioskDbContextOptions = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionStrings.MQAKiosk.ConnectionString).Options;
                DatabaseMonitors.Add(MQAKioskMonitor = new DatabaseMonitor(MQAKioskState, () => new DbContext(MQAKioskDbContextOptions)));
                


            }

            
            //Data Source = DESKTOP - T46QMIK\SQLEXPRESS; Encrypt = False; Integrated Security = True; User ID = EA\dandrus



            //if (connectionStrings.PlmGlobal != null)
            //{
            //    PLMGlobalDbContextOptions = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionStrings.PlmGlobal.ConnectionString).Options;
            //    DatabaseMonitors.Add(PLMGlobalMonitor = new DatabaseMonitor(PLMGlobalState, () => new DbContext(PLMGlobalDbContextOptions)));
            //}
            //if (connectionStrings.PlmLocal != null)
            //{
            //    PLMLocalDbContextOptions = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionStrings.PlmLocal.ConnectionString).Options;
            //    DatabaseMonitors.Add(PLMLocalMonitor = new DatabaseMonitor(PLMLocalState, () => new DbContext(PLMLocalDbContextOptions)));
            //}
            //if (connectionStrings.RlmGlobal != null)
            //{
            //    RLMGlobalDbContextOptions = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionStrings.RlmGlobal.ConnectionString).Options;
            //    DatabaseMonitors.Add(RLMGlobalMonitor = new DatabaseMonitor(RLMGlobalState, () => new DbContext(RLMGlobalDbContextOptions)));
            //}
            //if (connectionStrings.RlmLocal != null)
            //{
            //    RLMLocalDbContextOptions = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionStrings.RlmLocal.ConnectionString).Options;
            //    DatabaseMonitors.Add(RLMLocalMonitor = new DatabaseMonitor(RLMLocalState, () => new DbContext(RLMLocalDbContextOptions)));
            //}

            //if (!PLMGlobalState.IsAvailable)
            //    throw new Exception("PLM Database Not Available");
        }
        
        public MQAKioskLoginContext GetMQAKioskLoginContext()
        {
            return new MQAKioskLoginContext(MQAKioskDbContextOptions);
        }
        //public PLMLoginContext GetPLMLocalLoginContext()
        //{
        //    return new PLMLoginContext(PLMLocalDbContextOptions);
        //}

        //public PLMOrderContext GetPLMLocalOrderContext()
        //{
        //    return new PLMOrderContext(PLMLocalDbContextOptions);
        //}

        //public RLMGlobalContext GetRLMGlobalContext()
        //{
        //    return new RLMGlobalContext(RLMGlobalDbContextOptions);
        //}

        //public RLMLocalContext GetRLMLocalContext()
        //{
        //    return new RLMLocalContext(RLMLocalDbContextOptions);
       


        //needed to create RLM global database if it doesn't exist
        public MQAKioskLoginContext GetMQAKioskLoginContext_MQAKioskCreate()
        {

            if (MQAKioskState.IsAvailable)
                return new MQAKioskLoginContext(MQAKioskDbContextOptions);
            else
                return null;

       
        }

        //needed to create RLM local database if it doesn't exist
        //public PLMLoginContext GetPLMLocalLoginContext_RLMCreate()
        //{
        //    if (PLMLocalState.IsAvailable)
        //        return new PLMLoginContext(PLMLocalDbContextOptions);
        //    else
        //        return null;
        //}

        //public PLMLogContext GetPLMLogContext()
        //{
        //    return new PLMLogContext(PLMLocalDbContextOptions);
        //}

        //public PLMSettingsContext GetPLMSettingsContext()
        //{
        //    return new PLMSettingsContext(PLMLocalDbContextOptions);
        //}
    }
}
