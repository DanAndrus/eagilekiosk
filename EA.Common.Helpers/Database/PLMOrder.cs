﻿using EA.Common.Helpers.Util;
using System;

namespace EA.Common.Helpers.Database
{
    public class RLMOrder : PLMOrder, IEquatable<RLMOrder>
    {
        public string Gtin { get => _gtin; set => SetProperty(ref _gtin, value); }
        private string _gtin;

        public override void ResetOrder()
        {
            base.ResetOrder();
            Gtin = null;
        }
        public override bool IsValid()
        {
            return base.IsValid() && !string.IsNullOrWhiteSpace(Gtin);
        }

        public void CopyTo(RLMOrder destination)
        {
            destination.OrderName = OrderName;
            destination.ProductName = ProductName;
            destination.Gtin = Gtin;
            destination.Lot = Lot;
            destination.Expiry = Expiry;
        }

        #region IEquatable

        public override bool Equals(object obj)
        {
            if (obj is PLMOrder plmOrder)
                return Equals(plmOrder);
            else
                return false;
        }

        public bool Equals(RLMOrder obj)
        {
            return string.Equals(OrderName, obj.OrderName)
                && string.Equals(ProductName, obj.ProductName)
                && string.Equals(Gtin, obj.Gtin)
                && string.Equals(Lot, obj.Lot)
                && string.Equals(Expiry, obj.Expiry);
        }

        public static bool operator ==(RLMOrder obj1, RLMOrder obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (ReferenceEquals(obj1, null))
            {
                return false;
            }
            if (ReferenceEquals(obj2, null))
            {
                return false;
            }

            return obj1.Equals(obj2);
        }

        // this is second one '!='
        public static bool operator !=(RLMOrder obj1, RLMOrder obj2)
        {
            return !(obj1 == obj2);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                // Choose large primes to avoid hashing collisions
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, OrderName) ? OrderName.GetHashCode() : 0);
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, ProductName) ? ProductName.GetHashCode() : 0);
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, Gtin) ? Gtin.GetHashCode() : 0);
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, Lot) ? Lot.GetHashCode() : 0);
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, Expiry) ? Expiry.GetHashCode() : 0);
                return hash;
            }
        }

        #endregion IEquatable
    }

    public class PLMOrder : ObservableObject, IEquatable<PLMOrder>
    {
        public string OrderName { get => _orderName; set => SetProperty(ref _orderName, value); }
        private string _orderName;

        public string ProductName { get => _productName; set => SetProperty(ref _productName, value); }
        private string _productName;

        public string Lot { get => _lot; set => SetProperty(ref _lot, value); }
        private string _lot;

        public string Expiry { get => _expiry; set => SetProperty(ref _expiry, value); }
        private string _expiry;

        public virtual void ResetOrder()
        {
            OrderName = null;
            ProductName = null;
            Lot = null;
            Expiry = null;
        }

        public virtual bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(OrderName)
                    || string.IsNullOrWhiteSpace(ProductName)
                    || string.IsNullOrWhiteSpace(Lot)
                    || string.IsNullOrWhiteSpace(Expiry))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void CopyTo(PLMOrder destination)
        {
            destination.OrderName = OrderName;
            destination.ProductName = ProductName;
            destination.Lot = Lot;
            destination.Expiry = Expiry;
        }

        #region IEquatable

        public override bool Equals(object obj)
        {
            if (obj is PLMOrder plmOrder)
                return Equals(plmOrder);
            else
                return false;
        }

        public bool Equals(PLMOrder obj)
        {
            return string.Equals(OrderName, obj.OrderName)
                && string.Equals(ProductName, obj.ProductName)
                //&& string.Equals(Gtin, obj.Gtin)
                && string.Equals(Lot, obj.Lot)
                && string.Equals(Expiry, obj.Expiry);
        }

        public static bool operator ==(PLMOrder obj1, PLMOrder obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (ReferenceEquals(obj1, null))
            {
                return false;
            }
            if (ReferenceEquals(obj2, null))
            {
                return false;
            }

            return obj1.Equals(obj2);
        }

        // this is second one '!='
        public static bool operator !=(PLMOrder obj1, PLMOrder obj2)
        {
            return !(obj1 == obj2);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                // Choose large primes to avoid hashing collisions
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, OrderName) ? OrderName.GetHashCode() : 0);
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, ProductName) ? ProductName.GetHashCode() : 0);
                //hash = (hash * HashingMultiplier) ^ (!Object.ReferenceEquals(null, Gtin) ? Gtin.GetHashCode() : 0);
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, Lot) ? Lot.GetHashCode() : 0);
                hash = hash * HashingMultiplier ^ (!ReferenceEquals(null, Expiry) ? Expiry.GetHashCode() : 0);
                return hash;
            }
        }

        #endregion IEquatable
    }
}
