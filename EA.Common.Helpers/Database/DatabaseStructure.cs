﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading.Tasks;
using EA.Common.Helpers.Database.DataContexts;

namespace EA.Common.Helpers.Database
{
    public class DatabaseStructure
    {
        //RLMGlobalContext:
            //Scaffold-DbContext "Server=192.168.23.30;Database=PCE-RFID-Pilot;User Id=FKCache;
            //Password=cache;" Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -OutputDir Models -Tables "fkData","logs","eaDevices","devicesparameters" -ContextDir DataContexts -Context RLMGlobalContext -f

        //RLMLocalContext:
            //Scaffold-DbContext "Server=EA-8DWJ262\SQLDEV2017;Database=PCE-RFID-Cache;User Id=FKCache;
            //Password=cache;" Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -OutputDir Models -Tables "fkData","debugValidationData","indexerData","writerData","logs", "eaDevices", "devicesparameters", "validationData" -ContextDir DataContexts -Context RLMLocalContext -f

        public Func<MQAKioskLoginContext> GetMQAKioskContext { get; set; }
       // public Func<RLMLocalContext> GetRLMLocalContext { get; set; }

        public Func<MQAKioskLoginContext> GetMQAKioskLoginContext_MQAKioskCreate { get; set; }
        //public Func<MQAKioskLoginContext> GetPLMLocalLoginContext_RLMCreate { get; set; }

        //public LineManagerLib.Logging.Log log { get; } = new LineManagerLib.Logging.Log();

        public string[] globalTables =
            {
                "fkData",
                "logs",
                "eaDevices",
                "devicesparameters"
            };


        public string[] cacheTables =
        {
                "fkData",
                "debugValidationData",
                "indexerData",
                "writerData",
                "logs",
                "eaDevices",
                "devicesparameters"
            };


        public async Task<bool> EnsureRLMLocalAsync()
        {
            bool x = false;
            using (var ctx = GetMQAKioskContext())
            {
                x = await ctx.Database.EnsureCreatedAsync().ConfigureAwait(false);
            }
            return x;
        }

        public async Task<bool> EnsureRLMGlobalAsync()
        {
            bool x = false;
            using (var ctx = GetMQAKioskContext())
            {
                x = await ctx.Database.EnsureCreatedAsync().ConfigureAwait(false);
            }
            return x;
        }

        //public void EnsureRLMLocal()
        //{
        //    using (var ctx = GetRLMLocalContext())
        //    {
        //        ctx.Database.EnsureCreated();
        //    }
        //}

        //public void EnsureDatabaseExists(string location)
        //{
        //    if (DatabaseExists(location) == false)
        //    {
        //        CreateDatabase(location);

        //        int milliseconds = 5000;
        //        Thread.Sleep(milliseconds);
        //    }
        //}

        public void EnsureTableStructure(string location)
        {
            List<string> lstTables = new List<string>();

            switch (location)
            {
                case "local":
                    lstTables.AddRange(cacheTables);
                    break;
                case "global":
                    lstTables.AddRange(globalTables);
                    break;
            }

            foreach (string table in lstTables)
            {
                if (TabelsExist(table, location) == false)
                    CreateTables(table, location);
            }
        }

        //public bool DatabaseExists(string location)
        //{
        //    bool dbExists = false;

        //    if (location == "local")
        //    {
        //        try
        //        {

        //            using (var ctx = GetRLMLocalContext())
        //            {
        //                dbExists = (ctx.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists();
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            dbExists = false;
        //        }
        //    }
        //    else if (location == "global")
        //    {
        //        using (var ctx = GetRLMGlobalContext())
        //        {
        //            try
        //            {
        //                dbExists = (ctx.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists();
        //            }
        //            catch (Exception)
        //            {
        //                dbExists = false;
        //            }
        //        }
        //    }

        //    return dbExists;
        //}

        //public void CreateDatabase(string location)
        //{
        //    //GET THESE SETTINGS FROM REGISTRY ???
        //    string dbName,
        //        sqlScript;

        //    if (location == "global")
        //        dbName = "PCE-RFID-Pilot";
        //    else if (location == "local")
        //        dbName = "PCE-RFID-Cache";
        //    else
        //        dbName = "";

        //    sqlScript =
        //        "CREATE DATABASE [" + dbName + "] ";

        //    if (location == "local")
        //    {
        //        using (var ctx = GetPLMLocalLoginContext_RLMCreate())
        //        {
        //            try
        //            {
        //                //EXEC master..sp_addsrvrolemember @loginame = N'FKCache', @rolename = N'dbcreator'
        //                ctx.Database.ExecuteSqlRaw(sqlScript);
        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //        }
        //    }
        //    else if (location == "global") 
        //    {
        //        using (var ctx = GetPLMGlobalLoginContext_RLMCreate())
        //        {
        //            try
        //            {
        //                ctx.Database.ExecuteSqlRaw(sqlScript);
        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //        }
        //    }
        //}

        public bool TabelsExist(string tableName, string location)
        {
            bool tblExists = false;

            if (location == "local")
            {
                using (var ctx = GetMQAKioskContext()) 
                {
                    try
                    {
                        using (var command = ctx.Database.GetDbConnection().CreateCommand())
                        {
                            command.CommandText = "SELECT COUNT(*) FROM " + tableName;
                            ctx.Database.OpenConnection();
                            using (var result = command.ExecuteReader())
                                tblExists = result.HasRows;
                        }
                    }
                    catch (Exception)
                    {
                        tblExists = false;
                    }
                }
            }
            else if (location == "global")
            {
                using (var ctx = GetMQAKioskContext())
                {
                    try
                    {
                        using (var command = ctx.Database.GetDbConnection().CreateCommand())
                        {
                            command.CommandText = "SELECT COUNT(*) FROM " + tableName;
                            ctx.Database.OpenConnection();
                            using (var result = command.ExecuteReader())
                                tblExists = result.HasRows;
                        }
                    }
                    catch (Exception)
                    {
                        tblExists = false;
                    }
                }
            }

            return tblExists;
        }

        public void CreateTables(string tableName, string location)
        {

            // TABLE CREATION SQL SCRIPTS
            string
                fkDataCreate,
                debugValidationDataCreate,
                indexerDataCreate,
                writerDataCreate,
                rlmLogCreate,
                devicesCreate,
                devicesParametersCreate;

            fkDataCreate =
                "CREATE TABLE [dbo].[fkData](" +
                            "[id] [UNIQUEIDENTIFIER] NOT NULL," +
                            "[timestamp][datetime] NOT NULL," +
                            "[lineName][varchar](50) NOT NULL," +
                            "[workOrderName][varchar](5) NOT NULL," +
                            "[TID][varchar](50) NOT NULL," +
                            "[rawEPC][varchar](50) NOT NULL," +
                            "[decodedGTIN][varchar](50) NOT NULL," +
                            "[decodedSerial][varchar](50) NOT NULL," +
                            "[rawUser][varchar](50) NOT NULL," +
                            "[decodedLot][varchar](50) NOT NULL," +
                            "[decodedExpiry][varchar](50) NOT NULL," +
                            "[submittedTime][datetime] NOT NULL," +
                            "[syncStatus][int] NOT NULL," +
                            "CONSTRAINT[PK_FKData] PRIMARY KEY CLUSTERED" +
                            "(" +
                            "   [id] ASC" +
                            ")WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]" +
                            ") ON[PRIMARY]  " +
                            "ALTER TABLE [dbo].[fkData] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [id] " +
                            "ALTER TABLE [dbo].[fkData] ADD CONSTRAINT [DF_fkData] DEFAULT ((0)) FOR [syncStatus]";

            debugValidationDataCreate =
                "CREATE TABLE [dbo].[debugValidationData](" +
                            "[id] [UNIQUEIDENTIFIER] NOT NULL," +
                            "[triggerTimestamp][DATETIME] NOT NULL," +
                            "[validationTimestamp][DATETIME] NOT NULL," +
                            "[readTimestamp][DATETIME] NOT NULL," +
                            "[RSSI][VARCHAR](50) NOT NULL," +
                            "[readCount][INT] NOT NULL," +
                            "CONSTRAINT[PK_debugValidationData] PRIMARY KEY CLUSTERED" +
                            "(" +
                            "   [id] ASC" +
                            ")WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]" +
                            ") ON[PRIMARY] " +
                            "ALTER TABLE [dbo].[debugValidationData] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [id] ";

            indexerDataCreate =
                "CREATE TABLE [dbo].[indexerData](" +
                            "[TID][VARCHAR](50) NOT NULL," +
                            "[EPC][VARCHAR](50) NULL," +
                            "[timestamp][DATETIME] NOT NULL," +
                            "[RSSI][VARCHAR](50) NOT NULL," +
                            "[readCount][INT] NOT NULL," +
                            "CONSTRAINT[PK_indexerData] PRIMARY KEY CLUSTERED" +
                            "(" +
                            "   [TID] ASC" +
                            ")WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]" +
                            ") ON[PRIMARY] ";

            writerDataCreate =
                "CREATE TABLE [dbo].[writerData](" +
                            "[TID][VARCHAR](50) NOT NULL," +
                            "[EPC] [VARCHAR] (50) NULL," +
                            "[userData] [VARCHAR] (50) NULL," +
                            "[timestamp] [DATETIME] NULL," +
                            "[RSSI] [VARCHAR] (50) NULL," +
                            "[writeResult] [VARCHAR] (50) NULL," +
                            "[lockResult] [VARCHAR] (50) NULL," +
                            "CONSTRAINT[PK_writerData] PRIMARY KEY CLUSTERED" +
                            "(" +
                            "   [TID] ASC" +
                            ")WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]" +
                            ") ON[PRIMARY] ";

            rlmLogCreate =
                "CREATE TABLE [dbo].[logs](" +
                            "[id] [UNIQUEIDENTIFIER] NOT NULL," +
                            "[logTime] [DATETIME] NOT NULL," +
                            "[user] [VARCHAR](64) NULL," +
                            "[line] [VARCHAR](64) NOT NULL," +
                            "[message] [NVARCHAR](2000) NOT NULL," +
                            "[code] [INT] NOT NULL," +
                            "[logLevel] [INT] NOT NULL," +
                            "[syncStatus][int] NOT NULL," +
                            "CONSTRAINT [PK_logs] PRIMARY KEY CLUSTERED " +
                            "(" +
                            "   [id] ASC" +
                            ")WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]" +
                            ") ON[PRIMARY] " +
                            "ALTER TABLE [dbo].[logs] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [id] " +
                            "ALTER TABLE [dbo].[logs] ADD  CONSTRAINT [DF_logs]  DEFAULT ((0)) FOR [syncStatus]";

            devicesCreate =
                "CREATE TABLE [dbo].[eaDevices](" +
                            "[id] [UNIQUEIDENTIFIER] NOT NULL," +
                            "[id_line] [BIGINT] NULL," +
                            "[id_owner] [BIGINT] NULL," +
                            "[str_name] [NVARCHAR] (64) NOT NULL," +
                            "[int_sort] [BIGINT] NULL," +
                            "[str_connectdata] [NVARCHAR] (255) NULL," +
                            "[str_classname] [NVARCHAR] (128) NULL," +
                            "[bit_valid] [BIT] NOT NULL," +
                            "[syncStatus][int] NOT NULL," +
                            "CONSTRAINT[devices_pk] PRIMARY KEY CLUSTERED" +
                            "(" +
                            "   [id] ASC" +
                            ")WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]" +
                            ") ON[PRIMARY] " +
                            "ALTER TABLE [dbo].[eaDevices] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [id] " +
                            "ALTER TABLE [dbo].[eaDevices] ADD  CONSTRAINT [DF_eaDevices]  DEFAULT ((0)) FOR [syncStatus]";

            devicesParametersCreate =
                "CREATE TABLE [dbo].[devicesparameters](" +
                            "[id] [UNIQUEIDENTIFIER] NOT NULL," +
                            "[id_device] [BIGINT] NOT NULL," +
                            "[str_name] [NVARCHAR] (64) NOT NULL," +
                            "[str_type] [NVARCHAR] (32) NULL," +
                            "[str_value] [NVARCHAR] (255) NULL," +
                            "[syncStatus][int] NOT NULL," +
                            "CONSTRAINT[devicesparameters_pk] PRIMARY KEY CLUSTERED" +
                            "(" +
                            "   [id] ASC" +
                            ")WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]" +
                            ") ON[PRIMARY] " +
                            "ALTER TABLE [dbo].[devicesparameters] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [id] " +
                            "ALTER TABLE [dbo].[devicesparameters] ADD  CONSTRAINT [DF_devicesparameters]  DEFAULT ((0)) FOR [syncStatus]";

            if (location == "local")
            {
                using (var ctx = GetMQAKioskContext())
                {
                    try
                    {
                        switch (tableName)
                        {
                            case "fkData":
                                ctx.Database.ExecuteSqlRaw(fkDataCreate);
                                break;
                            case "debugValidationData":
                                ctx.Database.ExecuteSqlRaw(debugValidationDataCreate);
                                break;
                            case "indexerData":
                                ctx.Database.ExecuteSqlRaw(indexerDataCreate);
                                break;
                            case "writerData":
                                ctx.Database.ExecuteSqlRaw(writerDataCreate);
                                break;
                            case "logs":
                                ctx.Database.ExecuteSqlRaw(rlmLogCreate);
                                break;
                            case "eaDevices":
                                ctx.Database.ExecuteSqlRaw(devicesCreate);
                                break;
                            case "devicesparameters":
                                ctx.Database.ExecuteSqlRaw(devicesParametersCreate);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        //log.WriteToAppErrorLog(1, 1, "Failed to create local RLM database or tables", 1107, 0, 0, 0);
                    }
                }
            }
            else if (location == "global")
            {
                using (var ctx = GetMQAKioskContext())
                {
                    try
                    {
                        switch (tableName)
                        {
                            case "fkData":
                                ctx.Database.ExecuteSqlRaw(fkDataCreate);
                                break;
                            case "logs":
                                ctx.Database.ExecuteSqlRaw(rlmLogCreate);
                                break;
                            case "eaDevices":
                                ctx.Database.ExecuteSqlRaw(devicesCreate);
                                break;
                            case "devicesparameters":
                                ctx.Database.ExecuteSqlRaw(devicesParametersCreate);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        //log.WriteToAppErrorLog(1, 1, "Failed to create global RLM database or tables", 1107, 0, 0, 0);
                    }
                }
            }
        }
    }
}
