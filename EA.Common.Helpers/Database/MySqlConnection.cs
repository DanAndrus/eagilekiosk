﻿

namespace EA.Common.Helpers.Database
{
    public class DBSqlConnection
    {
        string ProductionConnectionString= "server=192.168.23.100;user=dandrus;database=econtrol;port=3306;password=pfmq@eA1";
        string TestConnectionString = "server=192.168.23.99;user=dandrus;database=econtrol_test;port=3306;password=pfmq@eA1";        

        public enum DBType
        {
            Test,
            Production
        }


        public MySql.Data.MySqlClient.MySqlConnection DB_Connection { get; set; }
        public DBType CurrentConnectionType { get; set; }

        public DBSqlConnection(DBType DBconnect)
        {
            switch (DBconnect)
            {
                case DBType.Test:
                    CurrentConnectionType = DBType.Test;
                    ConnectToTestDB();
                    break;
                case DBType.Production:
                    CurrentConnectionType = DBType.Production;
                    ConnectToProductionDB();
                    break;
            }
        }
        public MySql.Data.MySqlClient.MySqlConnection ConnectToProductionDB()
        {
            try
            {
                 DB_Connection = new MySql.Data.MySqlClient.MySqlConnection(ProductionConnectionString);
            }
            catch(MySql.Data.MySqlClient.MySqlException ex)
            {
                

            }
            

            return DB_Connection;
        }


        public MySql.Data.MySqlClient.MySqlConnection ConnectToTestDB()
        {
            try
            {
                DB_Connection = new MySql.Data.MySqlClient.MySqlConnection(TestConnectionString);
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {


            }


            return DB_Connection;
        }


    }
}
