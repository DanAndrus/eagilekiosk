﻿using EA.Common.Helpers.Util;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace EA.Common.Helpers.Database
{
    public class DatabaseState : ObservableObject
    {
        public bool IsAvailable { get => _isAvailable; set => SetProperty(ref _isAvailable, value); }
        private bool _isAvailable;
    }

    public enum DatabaseRole
    {
        Undefined,
        PLMGlobal,
        PLMLocal,
        RLMGlobal,
        RLMLocal,
    }

    public class DatabaseMonitor : ObservableObject
    {
        public DatabaseRole Role { get => _role; set => SetProperty(ref _role, value); }
        private DatabaseRole _role;

        public TimeSpan DatabaseCheckInterval { get => _databaseCheckInterval; set => SetProperty(ref _databaseCheckInterval, value); }
        private TimeSpan _databaseCheckInterval;

        public DatabaseState CurrentState { get; }
        protected Func<DbContext> GetDbContext { get; set; }
        
        private SemaphoreSlim _semaphore { get; } = new SemaphoreSlim(1, 1);
        private object _lock { get; } = new object();
        public bool IsRunning { get; private set; } = false;

        private Task _monitorTask { get; set; }
        public CancellationTokenSource CTS { get; set; } = new CancellationTokenSource();

        private TaskCompletionSource<bool> _initializedTcs { get; } = new TaskCompletionSource<bool>();
        public Task Initialized => _initializedTcs.Task;


        public DatabaseMonitor(DatabaseState databaseState, Func<DbContext> getDbContext)
        {
            CurrentState = databaseState;
            GetDbContext = getDbContext;
        }

        public void Start()
        {
            lock (_lock)
            {
                if (IsRunning)
                    return;

                IsRunning = true;

                CTS.Cancel();
                CTS = new CancellationTokenSource();
                _monitorTask = MonitorTask(CTS.Token);
            }
        }

        public void Stop()
        {
            lock (_lock)
            {
                IsRunning = false;
                CTS.Cancel();
            }
        }

        private async Task MonitorTask(CancellationToken ct)
        {
            try
            {
                while (!ct.IsCancellationRequested)
                {
                    try
                    {
                        try
                        {
                            await CheckDatabase(ct).ConfigureAwait(false);
                        }
                        finally
                        {
                            _initializedTcs.TrySetResult(true);
                        }
                        await Task.Delay(DatabaseCheckInterval, ct).ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.ToString();
                    }
                }
            }
            catch { }
        }

        private async Task CheckDatabase(CancellationToken ct)
        {
            bool serverAvailable = false;

            try
            {
                try
                {
                    using (DbContext db = GetDbContext())
                    {
                        if (db.Database.GetDbConnection().State == ConnectionState.Open)
                        {
                            serverAvailable = true;
                        }
                        else
                        {
                            try
                            {
                                await db.Database.OpenConnectionAsync(ct).ConfigureAwait(false);
                                serverAvailable = true;
                            }
                            catch
                            {
                                serverAvailable = false;
                            }
                            finally
                            {
                                await db.Database.CloseConnectionAsync().ConfigureAwait(false); ;
                            }
                        }
                    }
                }
                catch
                {
                    serverAvailable = false;
                }

                if (serverAvailable != CurrentState.IsAvailable)
                {
                    CurrentState.IsAvailable = serverAvailable;
                    ct.ThrowIfCancellationRequested();
                    OnDatabaseAvailabilityChanged(Role, CurrentState);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.ToString();
            }
        }

        public event EventHandler<DatabaseAvailabilityEventArgs> DatabaseAvailabilityChanged;
        protected virtual void OnDatabaseAvailabilityChanged(DatabaseRole role, DatabaseState state)
        {
            try { DatabaseAvailabilityChanged?.Invoke(this, new DatabaseAvailabilityEventArgs(role, state)); } catch { }
        }
    }

    public class DatabaseAvailabilityEventArgs : EventArgs
    {
        public DatabaseRole Role { get; }
        public DatabaseState State { get; }

        public DatabaseAvailabilityEventArgs(DatabaseRole role, DatabaseState state)
        {
            Role = role;
            State = state;
        }
    }
}
