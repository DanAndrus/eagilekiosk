﻿using System;
using EA.Common.Helpers.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EA.Common.Helpers.Database.DataContexts
{
    public partial class MQAKioskSystemContext : DbContext
    {
        public MQAKioskSystemContext(DbContextOptions options) : base(options) {}

        
        public virtual DbSet<Lines> KioskLines { get; set; }
        public virtual DbSet<Devices> KioskDevices { get; set; }
        public virtual DbSet<Logs> KioskLogs { get; set; }
        public virtual DbSet<Skus> KioskSKUs { get; set; }
        public virtual DbSet<ValidationTagData> KioskValidationData { get; set; }
        public virtual DbSet<Settings> KioskSettings{ get; set; }

    }
}
