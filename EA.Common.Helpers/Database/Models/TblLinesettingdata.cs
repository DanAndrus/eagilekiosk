﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("tbl_linesettingdata")]
    public partial class TblLineSettingData
    {
        [Key]
        [Column("id_linesetting")]
        public long IdLinesetting { get; set; }
        [Key]
        [Column("id_line")]
        public long IdLine { get; set; }
        [Column("str_value")]
        [StringLength(512)]
        public string StrValue { get; set; }

        //[ForeignKey(nameof(IdLine))]
        //[InverseProperty(nameof(TblLines.TblLinesettingdata))]
        //public virtual TblLines IdLineNavigation { get; set; }
        //[ForeignKey(nameof(IdLinesetting))]
        //[InverseProperty(nameof(TblLineSettings.TblLinesettingdata))]
        //public virtual TblLineSettings IdLinesettingNavigation { get; set; }
    }
}
