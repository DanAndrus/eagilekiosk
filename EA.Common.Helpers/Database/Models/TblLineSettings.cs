﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("tbl_linesettings")]
    public partial class TblLineSettings
    {
        public TblLineSettings()
        {
            TblLinesettingdata = new HashSet<TblLineSettingData>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Required]
        [Column("str_name")]
        [StringLength(64)]
        public string StrName { get; set; }
        [Column("str_type")]
        [StringLength(32)]
        public string StrType { get; set; }
        [Column("str_description")]
        [StringLength(255)]
        public string StrDescription { get; set; }

        [InverseProperty("IdLinesettingNavigation")]
        public virtual ICollection<TblLineSettingData> TblLinesettingdata { get; set; }
    }
}
