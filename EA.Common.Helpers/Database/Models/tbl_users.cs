﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EA.Common.Helpers.Database.Models
{
	public class tbl_users
	{
		private long _id;
		public long id
		{
			get { return _id; }
			set { _id = value; }
		}

		private string _str_name;
		public string str_name
		{
			get { return _str_name; }
			set { _str_name = value; }
		}

		private string _str_description;
		public string str_description
		{
			get { return _str_description; }
			set { _str_description = value; }
		}

		private string _str_password;
		public string str_password
		{
			get { return _str_password; }
			set { _str_password = value; }
		}

		private int _i16_type;
		public int i16_type
		{
			get { return _i16_type; }
			set { _i16_type = value; }
		}

		private string _str_langid;
		public string str_langid
		{
			get { return _str_langid; }
			set { _str_langid = value; }
		}

		private DateTime? _dte_expires;
		public DateTime? dte_expires
		{
			get { return _dte_expires; }
			set { _dte_expires = value; }
		}

		private DateTime _dte_lastpwdchange;
		public DateTime dte_lastpwdchange
		{
			get { return _dte_lastpwdchange; }
			set { _dte_lastpwdchange = value; }
		}

		private DateTime? _dte_lastfailedlogin;
		public DateTime? dte_lastfailedlogin
		{
			get { return _dte_lastfailedlogin; }
			set { _dte_lastfailedlogin = value; }
		}

		private int _int_failedlogincount;
		public int int_failedlogincount
		{
			get { return _int_failedlogincount; }
			set { _int_failedlogincount = value; }
		}

		private bool _bit_locked;
		public bool bit_locked
		{
			get { return _bit_locked; }
			set { _bit_locked = value; }
		}

		private bool _bit_valid;
		public bool bit_valid
		{
			get { return _bit_valid; }
			set { _bit_valid = value; }
		}


		public tbl_users(long id_, string str_name_, string str_description_, string str_password_, int i16_type_, string str_langid_, DateTime? dte_expires_, DateTime dte_lastpwdchange_, DateTime? dte_lastfailedlogin_, int int_failedlogincount_, bool bit_locked_, bool bit_valid_)
		{
			this.id = id_;
			this.str_name = str_name_;
			this.str_description = str_description_;
			this.str_password = str_password_;
			this.i16_type = i16_type_;
			this.str_langid = str_langid_;
			this.dte_expires = dte_expires_;
			this.dte_lastpwdchange = dte_lastpwdchange_;
			this.dte_lastfailedlogin = dte_lastfailedlogin_;
			this.int_failedlogincount = int_failedlogincount_;
			this.bit_locked = bit_locked_;
			this.bit_valid = bit_valid_;
		}
	}
}
