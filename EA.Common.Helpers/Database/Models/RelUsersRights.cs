﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("rel_users_rights")]
    public partial class RelUsersRights
    {
        [Key]
        [Column("id_user")]
        public long IdUser { get; set; }
        [Key]
        [Column("id_right")]
        public long IdRight { get; set; }

        [ForeignKey(nameof(IdRight))]
        [InverseProperty(nameof(TblRights.RelUsersRights))]
        //public virtual TblRights IdRightNavigation { get; set; }
        //[ForeignKey(nameof(IdUser))]
        //[InverseProperty(nameof(TblUsers.RelUsersRights))]
        public virtual TblUsers IdUserNavigation { get; set; }
    }
}
