﻿using EA.Common.Helpers.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("tbl_groups")]
    public partial class TblGroups
    {
        public TblGroups()
        {
            RelGroupsRights = new HashSet<RelGroupsRights>();
            RelUsersGroups = new HashSet<RelUsersGroups>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Required]
        [Column("str_name")]
        [StringLength(64)]
        public string StrName { get; set; }
        [Column("str_description")]
        [StringLength(255)]
        public string StrDescription { get; set; }
        [Column("i16_level")]
        public int I16Level { get; set; }
        [Required]
        [Column("bit_valid")]
        public bool? BitValid { get; set; }

        [InverseProperty("IdGroupNavigation")]
        public virtual ICollection<RelGroupsRights> RelGroupsRights { get; set; }
        [InverseProperty("IdGroupNavigation")]
        public virtual ICollection<RelUsersGroups> RelUsersGroups { get; set; }
    }
}
