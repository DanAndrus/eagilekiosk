﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("tbl_users")]
    public partial class TblUsers
    {
        //public TblUsers()
        //{
        //    RelUsersGroups = new HashSet<RelUsersGroups>();
        //    RelUsersRights = new HashSet<RelUsersRights>();
        //}

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Required]
        [Column("str_name")]
        [StringLength(64)]
        public string StrName { get; set; }
        [Column("str_description")]
        [StringLength(255)]
        public string StrDescription { get; set; }
        [Column("str_password")]
        [StringLength(128)]
        public string StrPassword { get; set; }
        [Column("i16_type")]
        public int I16Type { get; set; }
        [Column("str_langid")]
        [StringLength(5)]
        public string StrLangid { get; set; }
        [Column("dte_expires", TypeName = "datetime")]
        public DateTime? DteExpires { get; set; }
        [Column("dte_lastpwdchange", TypeName = "datetime")]
        public DateTime DteLastpwdchange { get; set; }
        [Column("dte_lastfailedlogin", TypeName = "datetime")]
        public DateTime? DteLastfailedlogin { get; set; }
        [Column("int_failedlogincount")]
        public int IntFailedlogincount { get; set; }
        [Column("bit_locked")]
        public bool BitLocked { get; set; }
        [Required]
        [Column("bit_valid")]
        public bool? BitValid { get; set; }

        //[InverseProperty("IdUserNavigation")]
        //public virtual ICollection<RelUsersGroups> RelUsersGroups { get; set; }
        //[InverseProperty("IdUserNavigation")]
        //public virtual ICollection<RelUsersRights> RelUsersRights { get; set; }
    }
}
