﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("validationTagData")]
    public partial class ValidationTagData
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("hostTime")]
        public DateTime HostTime { get; set; }

        [Column("readerTime")]
        public DateTime ReaderTime { get; set; }

        [Required]
        [Column("tid")]
        [StringLength(64)]
        public string TID { get; set; }

        [Required]
        [Column("epc")]
        [StringLength(64)]
        public string EPC { get; set; }

        [Column("rawUser")]
        [StringLength(128)]
        public string RawUser { get; set; }

        [Column("lockState")]
        public int LockState { get; set; }

        [Column("rssi")]
        public double RSSI { get; set; }

        [Column("antenna")]
        public double Antenna { get; set; }

        [Column("readFlags")]
        public int ReadFlags { get; set; }
    }
}
