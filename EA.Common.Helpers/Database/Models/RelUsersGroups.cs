﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("rel_users_groups")]
    public partial class RelUsersGroups
    {
        [Key]
        [Column("id_user")]
        public long IdUser { get; set; }
        [Key]
        [Column("id_group")]
        public long IdGroup { get; set; }

        [ForeignKey(nameof(IdGroup))]
        [InverseProperty(nameof(TblGroups.RelUsersGroups))]
        public virtual TblGroups IdGroupNavigation { get; set; }
    //    [ForeignKey(nameof(IdUser))]
    //    [InverseProperty(nameof(TblUsers.RelUsersGroups))]
        public virtual TblUsers IdUserNavigation { get; set; }
    }
}
