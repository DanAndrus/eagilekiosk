﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("settings")]
    public partial class Settings
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("device_id")]
        public Guid DeviceId { get; set; }

        [Column("sku_id")]
        public Guid SkuId { get; set; }

        [Required]
        [Column("name")]
        [StringLength(64)]
        public string Name { get; set; }

        [Required]
        [Column("type")]
        [StringLength(64)]
        public string Type { get; set; }

        [Required]
        [Column("value")]
        [StringLength(255)]
        public string Value { get; set; }

        [Column("active")]
        public bool Active { get; set; }

        [Column("syncStatus")]
        public int SyncStatus { get; set; }

        [Column("sortby")]
        public int SortBy { get; set; } = 0;
    }
}
