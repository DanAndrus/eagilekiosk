﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("logs")]
    public partial class Logs
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }
        [Column("logTime", TypeName = "datetime")]
        public DateTime LogTime { get; set; }
        [Column("user")]
        [StringLength(64)]
        public string User { get; set; }
        [Required]
        [Column("line")]
        [StringLength(64)]
        public string Line { get; set; }
        [Column("orderName")]
        [StringLength(64)]
        public string OrderName { get; set; }
        [Column("message")]
        [StringLength(2000)]
        public string Message { get; set; }
        [Column("code")]
        public int Code { get; set; }
        [Required]
        [Column("logLevel")]
        [StringLength(10)]
        public string LogLevel { get; set; }
        [Column("syncStatus")]
        public int SyncStatus { get; set; }
    }
}
