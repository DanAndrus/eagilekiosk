﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("skus")]
    public partial class Skus
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }
        [Required]
        [Column("name")]
        [StringLength(64)]
        public string Name { get; set; }
        [Column("active")]
        public bool Active { get; set; }
        [Column("syncStatus")]
        public int SyncStatus { get; set; }
    }
}
