﻿using EA.Common.Helpers.Database.DataContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Common.Helpers.Database.Models
{
    public class UserRepositoryController : ITblUsersRepository
    {
        public MQAKioskLoginContext Context { get; }

        public UserRepositoryController(MQAKioskLoginContext context)
        {
            Context = context;
        }

        
        public TblUsers AddUser(TblUsers newUser)
        {
            Context.KioskUsers.Add(newUser);
            Context.SaveChanges();
            return newUser;

        }

        public TblUsers DeleteUser(int DeleteUserId)
        {
            TblUsers deleteuser =Context.KioskUsers.Find(DeleteUserId);
            if(deleteuser != null)
            {
                Context.KioskUsers.Remove(deleteuser);
            }
            return deleteuser;
        }

        public IEnumerable<TblUsers> GetAllUsers()
        {
            return Context.KioskUsers;
        }

        public TblUsers GetUser(int findId)
        {
            TblUsers GetUser = Context.KioskUsers.FirstOrDefault(d => d.Id == findId);

            return GetUser;

        }

        public TblUsers Update(TblUsers userChanges)
        {
           var newupdate = Context.KioskUsers.Attach(userChanges);
           newupdate.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            Context.SaveChanges();
          
            return userChanges;
        }
    }
}
