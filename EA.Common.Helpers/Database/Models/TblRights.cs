﻿using EA.Common.Helpers.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("tbl_rights")]
    public partial class TblRights
    {
        public TblRights()
        {
            RelGroupsRights = new HashSet<RelGroupsRights>();
            RelUsersRights = new HashSet<RelUsersRights>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Required]
        [Column("str_name")]
        [StringLength(64)]
        public string StrName { get; set; }
        [Column("str_description")]
        [StringLength(255)]
        public string StrDescription { get; set; }
        [Required]
        [Column("bit_valid")]
        public bool? BitValid { get; set; }

        [InverseProperty("IdRightNavigation")]
        public virtual ICollection<RelGroupsRights> RelGroupsRights { get; set; }
        [InverseProperty("IdRightNavigation")]
        public virtual ICollection<RelUsersRights> RelUsersRights { get; set; }
    }
}
