﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("rel_groups_rights")]
    public partial class RelGroupsRights
    {
        [Key]
        [Column("id_group")]
        public long IdGroup { get; set; }
        [Key]
        [Column("id_right")]
        public long IdRight { get; set; }

        //[ForeignKey(nameof(IdGroup))]
        //[InverseProperty(nameof(TblGroups.RelGroupsRights))]
        //public virtual TblGroups IdGroupNavigation { get; set; }
        //[ForeignKey(nameof(IdRight))]
        //[InverseProperty(nameof(TblRights.RelGroupsRights))]
        //public virtual TblRights IdRightNavigation { get; set; }
    }
}
