﻿using EA.Common.Helpers.Database.DataContexts;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace EA.Common.Helpers.Database.Models
{
    public class SystemRepositoryController
    {
        private readonly MQAKioskSystemContext Context;

        public SystemRepositoryController(MQAKioskSystemContext context)
        {
            this.Context = context;
        }

        public IEnumerable<Settings> GetKoiskSettings()
        {
            return Context.KioskSettings;
        }
        public IEnumerable<Devices> GetKoiskDevices()
        {
            return Context.KioskDevices;
        }
        public IEnumerable<Lines> GetKoiskLines()
        {
            return Context.KioskLines;
        }

        public IEnumerable<Logs> GetKioskLogs()
        {
            return Context.KioskLogs;
        }
        public IEnumerable<Skus> GetKioskSKUs()
        {
            return Context.KioskSKUs;
        }
        public IEnumerable<ValidationTagData> GetKioskValidationData()
        {
            return Context.KioskValidationData;
        }
    }

}
