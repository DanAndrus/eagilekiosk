﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("tbl_lines")]
    public partial class TblLines
    {
        //public TblLines()
        //{
        //    TblLinesettingdata = new HashSet<TblLineSettingData>();
        //}

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Required]
        [Column("str_name")]
        [StringLength(64)]
        public string StrName { get; set; }
        [Column("str_description")]
        [StringLength(255)]
        public string StrDescription { get; set; }
        [Column("i16_type")]
        public int I16Type { get; set; }
        [Column("str_license")]
        [StringLength(64)]
        public string StrLicense { get; set; }

        //[InverseProperty("IdLineNavigation")]
        //public virtual ICollection<TblLineSettingData> TblLinesettingdata { get; set; }
    }
}
