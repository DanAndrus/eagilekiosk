﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EA.Common.Helpers.Database.Models
{
    public interface ITblUsersRepository
    {
        IEnumerable<TblUsers> GetAllUsers();
        TblUsers GetUser(int Id);
        TblUsers AddUser(TblUsers newUser);
        TblUsers Update(TblUsers userChanges);
        TblUsers DeleteUser(int Id);
    }
}
