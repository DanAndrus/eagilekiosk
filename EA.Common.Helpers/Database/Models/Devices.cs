﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EA.Common.Helpers.Database.Models
{
    [Table("devices")]
    public partial class Devices
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("line_id")]
        public Guid LineId { get; set; }

        [Required]
        [Column("name")]
        [StringLength(64)]
        public string Name { get; set; }

        [Required]
        [Column("type")]
        [StringLength(64)]
        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                if (SortBy == 0)
                {
                    SortBy = value switch
                    {
                        "indexer" => 10,
                        "writer" => 20,
                        "validator" => 30,
                        "ocs" => 40,
                        "rlm" => 50,
                        "plc" => 60,
                        _ => 0,
                    };
                }
                _type = value;
            }
        }
        private string _type;

        [Column("active")]
        public bool Active { get; set; }

        [Column("syncStatus")]
        public int SyncStatus { get; set; }

        [Column("sortby")]
        public int SortBy { get; set; } = 0;
    }
}
