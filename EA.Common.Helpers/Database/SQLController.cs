﻿using EA.Common.Helpers.Database.DataContexts;
using EA.Common.Helpers.Database.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EA.Common.Helpers.Database
{
    
    public class SQLController
    {

        public SqlConnection DB_Connection { get; set; }

        public string[] MqaTables =
        {
                "tbl_users",
                "ValidationData",
                "skus",
                "settings",
                "logs",
                "devices",
                "lines"
        };

        public List<TblUsers> TblUsers { get; set; }
        public List<Settings> SystemSettings { get; set; }
        public Lines ActiveLine { get; set; }


        public static MQAKioskLoginContext DB_Login { get; set; }
        public static MQAKioskSystemContext DB_System { get; set; }

        public UserRepositoryController UserController{ get; set; }
        public SystemRepositoryController SystemController { get; set; }
        public IServiceCollection services = new ServiceCollection();

        public DbContextOptions UserDbContextOptions { get; }
      
        public SQLController()
        {
            UserDbContextOptions = SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), @"Data Source = 127.0.0.1 ;Database=MQA_Kiosk; User ID = MQAKiosk ;password = mqakiosk").Options;

            DB_Login = new MQAKioskLoginContext(UserDbContextOptions);
            DB_System = new MQAKioskSystemContext(UserDbContextOptions);

            UserController = new UserRepositoryController(DB_Login);
            SystemController = new SystemRepositoryController(DB_System);

            // establish user list for managing login
            TblUsers = UserController.GetAllUsers().ToList();
            SystemSettings = SystemController.GetKoiskSettings().ToList();

            ActiveLine = SystemController.GetKoiskLines().ToList().FirstOrDefault();

            try
            {


                string connetionString;
               
               // SqlConnection cnn;

                connetionString = @"Data Source = 127.0.0.1 ;Database=MQA_Kiosk; User ID = MQAKiosk ;password = mqakiosk";

                DB_Connection = new SqlConnection(connetionString);

                foreach (string table in MqaTables)
                {

                    if (!AllTablesExist(table))
                    {

                    };

                }

                //UserList = new List<tbl_users>();




                //using (var command = DB_Connection.CreateCommand())
                //{
                //    command.CommandText = GetTablestring(tablename);
                //    command.ExecuteReader();
                //}

                //ModelBuilder modelBuilder;

                //modelBuilder.Entity<TblUsers>(entity =>
                //{
                //    entity.HasIndex(e => e.StrName)
                //        .HasName("tbl_users_uq")
                //        .IsUnique();

                //    entity.Property(e => e.BitValid).HasDefaultValueSql("((1))");
                //});


            }
            catch (Exception ex)
            {
                var eeaa = ex.Message;

            }

        }
        #region CreateDatabase
        private string GetTablestring(string tablename)
        {
            switch (tablename)
            {
                case "tbl_users":
                    return @$"CREATE TABLE MQA_Kiosk.dbo.tbl_users (
                          id bigint IDENTITY,
                          str_name nvarchar(64) NOT NULL,
                          str_description nvarchar(255) NULL,
                          str_password nvarchar(128) NULL,
                          i16_type int NOT NULL DEFAULT (0),
                          str_langid nvarchar(5) NULL,
                          dte_expires datetime NULL,
                          dte_lastpwdchange datetime NOT NULL,
                          dte_lastfailedlogin datetime NULL,
                          int_failedlogincount int NOT NULL DEFAULT (0),
                          bit_locked bit NOT NULL DEFAULT (0),
                          bit_valid bit NOT NULL DEFAULT (1),
                          CONSTRAINT tbl_users_pk PRIMARY KEY CLUSTERED (id),
                          CONSTRAINT tbl_users_uq UNIQUE (str_name)
                        )
                        ON [PRIMARY]";
                    
                case "ValidationData":

                    return @$"CREATE TABLE MQA_Kiosk.dbo.validationData (
                          id uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
                          triggerTimestamp datetime NOT NULL,
                          resultTimestamp datetime NOT NULL,
                          TID varchar(64) NULL,
                          rawEPC varchar(64) NULL,
                          decodedGTIN varchar(64) NULL,
                          decodedSerial varchar(64) NULL,
                          rawUser varchar(128) NULL,
                          decodedLot varchar(64) NULL,
                          decodedExpiry varchar(64) NULL,
                          result int NOT NULL,
                          firstRead datetime NULL,
                          lastRead datetime NULL,
                          readCount int NOT NULL,
                          rssi float NOT NULL,
                          lockState int NOT NULL,
                          CONSTRAINT PK_validationData PRIMARY KEY CLUSTERED (id)
                        )
                        ON [PRIMARY]";
                case "skus":
                    return @$"CREATE TABLE MQA_Kiosk.dbo.skus (
                              id uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
                              name nvarchar(64) NOT NULL,
                              active bit NOT NULL,
                              syncStatus int NOT NULL,
                              CONSTRAINT PK_skus PRIMARY KEY CLUSTERED (id)
                            )
                            ON [PRIMARY]";
                    
                case "settings":

                    return @$"CREATE TABLE MQA_Kiosk.dbo.settings(
                      id uniqueidentifier NOT NULL DEFAULT(newsequentialid()),
                      device_id uniqueidentifier NOT NULL,
                      sku_id uniqueidentifier NOT NULL,
                      name nvarchar(64) NOT NULL,
                      type nvarchar(64) NOT NULL,
                      value nvarchar(255) NOT NULL,
                      active bit NOT NULL,
                      syncStatus int NOT NULL,
                      sortby int NOT NULL,
                      CONSTRAINT PK_settings PRIMARY KEY CLUSTERED(id)
                    )
                    ON[PRIMARY]";
            
                case "logs":
                    return @$"CREATE TABLE MQA_Kiosk.logs (
                          id uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
                          logTime datetime NOT NULL,
                          [user] varchar(64) NULL,
                          line varchar(64) NOT NULL,
                          orderName varchar(64) NULL,
                          message nvarchar(2000) NULL,
                          code int NOT NULL,
                          logLevel varchar(10) NOT NULL,
                          syncStatus int NOT NULL,
                          CONSTRAINT PK_logs PRIMARY KEY CLUSTERED (id)
                        )
                        ON [PRIMARY]
                        GO  ";
                    
                case "devices":
                    return @$"CREATE TABLE MQA_Kiosk.dbo.devices (
                          id uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
                          line_id uniqueidentifier NOT NULL,
                          name nvarchar(64) NOT NULL,
                          type nvarchar(64) NOT NULL,
                          active bit NOT NULL,
                          syncStatus int NOT NULL,
                          sortby int NOT NULL,
                          CONSTRAINT PK_devices PRIMARY KEY CLUSTERED (id)
                        )
                        ON [PRIMARY]";
                case "lines":
                    return @$"CREATE TABLE MQA_Kiosk.dbo.lines (
                            id uniqueidentifier NOT NULL DEFAULT(newsequentialid()),
                            name nvarchar(64) NOT NULL,
                            active bit NOT NULL,
                            syncStatus int NOT NULL,
                            CONSTRAINT PK_lines PRIMARY KEY CLUSTERED(id)
                            )
                            ON[PRIMARY]";

            }
            return string.Empty;
        }
        private bool CreateTables(string tablename)
        {
            
            try
            {
                using (var command = DB_Connection.CreateCommand())
                {
                    command.CommandText = GetTablestring(tablename);
                    command.ExecuteReader();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }
        private bool AllTablesExist(string tablename)
        {

            string cmdText = @"IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES 
                       WHERE TABLE_NAME='" + tablename + "') SELECT 1 ELSE SELECT 0";
            DB_Connection.Open();
            SqlCommand DateCheck = new SqlCommand(cmdText, DB_Connection);
            int x = Convert.ToInt32(DateCheck.ExecuteScalar());
            if (x == 1)
            {
                //Table exists
            }
            else
            {
                //Table doesn't exist 
                if (!CreateTables(tablename))
                {
                    //Failed to create
                    return false;
                }
            }

            DB_Connection.Close();

            return true;
        }
        #endregion

    }


}
