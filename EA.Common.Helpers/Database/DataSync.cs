﻿using EA.Common.Helpers.Database.DataContexts;

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace EA.Common.Helpers.Database
{
    //public enum SyncStatus : int
    //{
    //    Unsynced = 0,
    //    Syncing = 1,
    //    Synced = 2,
    //}

    //public enum SyncLocation : int
    //{
    //    Global = 1,
    //    Local = 2,
    //}

    //public class DataSync
    //{
    //    //public Func<MQAKioskContext> GetRLMGlobalContext { get; set; }
    //    //public Func<MQAKioskContext> GetRLMLocalContext { get; set; }
    //    ///public Func<MQAKioskContext> GetMQAKioskContext { get; set; }
    //    ////public Func<PLMSettingsContext> GetPLMSettingsContext { get; set; }

    //    //public static List<FkData> lstFkData = new List<FkData>();
    //    //public static List<Devices> lstDevices = new List<Devices>();
    //    ////public static List<TblDevices> lstDevices = new List<TblDevices>();
    //    ////public static List<Devicesparameters> lstDevicesParameters = new List<Devicesparameters>();
    //    ////public static List<TblDevicesparameters> lstTblDevicesParameters = new List<TblDevicesparameters>();
    //    //public static List<Logs> lstLogs = new List<Logs>();
    //    ////public static List<TblLogs> lstTblLogs = new List<TblLogs>();

    //    #region ***** FKDATA SYNC *****

    //    private static SemaphoreSlim _syncDataLock { get; } = new SemaphoreSlim(1, 1);
    //    private static Guid RLMLineId { get; set; }

    //    //public static async Task WriteDataInsert(WriteTagData writeTagData, Func<MQAKioskContext> getMQAKioskContextext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);
    //    //    try
    //    //    {
    //    //        using (var ctx = getMQAKioskContext())
    //    //        {
    //    //            WriteTagData existing = await ctx.WriteTagData.SingleOrDefaultAsync(p => p.Indexed == writeTagData.Indexed && p.TID == writeTagData.TID).ConfigureAwait(false);
    //    //            if (existing == null)
    //    //            {
    //    //                ctx.WriteTagData.Add(writeTagData);
    //    //            }
    //    //            else
    //    //            {
    //    //                // TODO: pre-populate Id to avoid existence check?
    //    //                existing.FirstWrite = writeTagData.FirstWrite;
    //    //                existing.LastWrite = writeTagData.LastWrite;
    //    //                existing.RawUser = writeTagData.RawUser;
    //    //                existing.LockState = writeTagData.LockState;
    //    //                existing.IndexCount = writeTagData.IndexCount;
    //    //                existing.IndexMaxRSSI = writeTagData.IndexMaxRSSI;
    //    //                existing.WriteCount = writeTagData.WriteCount;
    //    //                existing.WriteMaxRSSI = writeTagData.WriteMaxRSSI;
    //    //                existing.WriteResult = writeTagData.WriteResult;
    //    //                existing.LockResult = writeTagData.LockResult;
    //    //            }
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task ValidationTagDataInsert(ValidationTagData validationTagData, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalLogContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);
    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            ctx.ValidationTagData.Add(validationTagData);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task ValidationTriggerDataInsert(ValidationTriggerData validationTriggerData, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalLogContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);
    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            ctx.ValidationTriggerData.Add(validationTriggerData);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task FKDataInsert(FkData fkData, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalLogContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);
    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            ctx.FkData.Add(fkData);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task LinesInsertUpdateAsync(IList<Lines> lines, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);

    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            var update =
    //    //                from l in lines
    //    //                join c in ctx.Lines on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = (int)SyncStatus.Syncing;
    //    //            }

    //    //            var insert = from l in lines
    //    //                         join c in ctx.Lines on l.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select l;

    //    //            ctx.Lines.AddRange(insert);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            await SyncLinesAsync(lines, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global);
    //    //            await SetSyncLinesAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task SkusInsertUpdateAsync(IList<Skus> skus, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);

    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            var update =
    //    //                from l in skus
    //    //                join c in ctx.Skus on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = (int)SyncStatus.Syncing;
    //    //            }

    //    //            var insert = from s in skus
    //    //                         join c in ctx.Skus on s.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select s;

    //    //            ctx.Skus.AddRange(insert);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            await SyncSkusAsync(skus, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global);
    //    //            await SetSyncSkusAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task DevicesInsertUpdateAsync(IList<Devices> devices, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);

    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            var update =
    //    //                from l in devices
    //    //                join c in ctx.Devices on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.LineId = u.l.LineId;
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Type = u.l.Type;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = ( int ) SyncStatus.Syncing;
    //    //                u.c.SortBy = u.l.SortBy;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from d in devices
    //    //                         join c in ctx.Devices on d.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select d;

    //    //            ctx.Devices.AddRange(insert);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            await SyncDevicesAsync(devices, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global);
    //    //            await SetSyncDevicesAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task SettingsInsertUpdateAsync(IList<Settings> settings, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);

    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            var update =
    //    //                from l in settings
    //    //                join c in ctx.Settings on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.DeviceId = u.l.DeviceId;
    //    //                u.c.SkuId = u.l.SkuId;
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Type = u.l.Type;
    //    //                u.c.Value = u.l.Value;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = (int)SyncStatus.Syncing;
    //    //                u.c.SortBy = u.l.SortBy;
    //    //            }


    //    //            var insert = from s in settings
    //    //                         join c in ctx.Settings on s.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select s;

    //    //            ctx.Settings.AddRange(insert);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            await SyncSettingsAsync(settings, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global);
    //    //            await SetSyncSettingsAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}


    //    //public static async Task SyncFkDataAsync(int batchSize, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    await _syncDataLock.WaitAsync(ct).ConfigureAwait(false);
    //    //    try
    //    //    {
    //    //        // CLEAR PREVIOUSLY SYNCED FKDATA RECORDS
    //    //        await DataSync.ClearSyncedFkDataAsync(getRlmLocalContext, ct).ConfigureAwait(false);

    //    //        if (!(await GetLocalFkDataHasUnsyncedRecordsAsync(getRlmLocalContext, ct).ConfigureAwait(false)))
    //    //            return;

    //    //        // CATCH ANY RECORDS SET AS SYNC STATUS "1" AND SYNC IF THEY DON'T EXIST IN GLOAL
    //    //        List<FkData> fkData = await DataSync.GetLocalFkDataToSyncAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    //        if (fkData.Count > 0)
    //    //        {
    //    //            await DataSync.SyncFkDataToGlobalWithCheckAsync(fkData, getRlmGlobalContext, ct).ConfigureAwait(false);
    //    //            await DataSync.SetSyncFkDataAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, ct).ConfigureAwait(false);
    //    //            await DataSync.ClearSyncedFkDataAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    //        }

    //    //        // SYNC FKDATA RECORDS FROM CACHE TO GLOBAL
    //    //        int countToSync = await DataSync.SetSyncFkDataAsync(SyncStatus.Unsynced, SyncStatus.Syncing, batchSize, getRlmLocalContext, ct).ConfigureAwait(false);
    //    //        while (countToSync > 0)
    //    //        {
    //    //            fkData = await DataSync.GetLocalFkDataToSyncAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    //            if (fkData.Count > 0)
    //    //            {
    //    //                await DataSync.SyncFkDataToGlobalAsync(fkData, getRlmGlobalContext, ct).ConfigureAwait(false);
    //    //                await DataSync.SetSyncFkDataAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, ct).ConfigureAwait(false);
    //    //                await DataSync.ClearSyncedFkDataAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    //            }
    //    //            countToSync = await DataSync.SetSyncFkDataAsync(SyncStatus.Unsynced, SyncStatus.Syncing, batchSize, getRlmLocalContext, ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncDataLock.Release();
    //    //    }
    //    //}

    //    ////public static async Task SyncSettingsAsync(int batchSize, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    ////{
    //    ////    await _syncDataLock.WaitAsync(ct).ConfigureAwait(false);
    //    ////    try
    //    ////    {
    //    ////        // CLEAR PREVIOUSLY SYNCED FKDATA RECORDS
    //    ////        await DataSync.ClearSyncedSettingsAsync(getRlmLocalContext, ct).ConfigureAwait(false);

    //    ////        if (!(await GetGlobalSettingsHasUnsyncedRecordsAsync(getRlmGlobalContext, ct).ConfigureAwait(false)))
    //    ////            return;

    //    ////        // CATCH ANY RECORDS SET AS SYNC STATUS "1" AND SYNC IF THEY DON'T EXIST IN GLOAL
    //    ////        List<Lines> lines = await DataSync.GetGlobalSettingsToSyncAsync(getRlmGlobalContext, ct).ConfigureAwait(false);
    //    ////        if (lines.Count > 0)
    //    ////        {
    //    ////            await DataSync.SyncSettingsToLocalWithCheckAsync(lines, getRlmLocalContext, ct).ConfigureAwait(false);
    //    ////            await DataSync.SetSyncSettingsAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmGlobalContext, ct).ConfigureAwait(false);
    //    ////            //await DataSync.ClearSyncedSettingsAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    ////        }

    //    ////        // SYNC FKDATA RECORDS FROM CACHE TO GLOBAL
    //    ////        int countToSync = await DataSync.SetSyncFkDataAsync(SyncStatus.Unsynced, SyncStatus.Syncing, batchSize, getRlmLocalContext, ct).ConfigureAwait(false);
    //    ////        while (countToSync > 0)
    //    ////        {
    //    ////            lines = await DataSync.GetGlobalFkDataToSyncAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    ////            if (lines.Count > 0)
    //    ////            {
    //    ////                await DataSync.SyncSettingsToLocalAsync(lines, getRlmLocalContext, ct).ConfigureAwait(false);
    //    ////                await DataSync.SetSyncFkDataAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, ct).ConfigureAwait(false);
    //    ////                await DataSync.ClearSyncedFkDataAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    ////            }
    //    ////            countToSync = await DataSync.SetSyncSettingsAsync(SyncStatus.Unsynced, SyncStatus.Syncing, batchSize, getRlmGlobalContext, ct).ConfigureAwait(false);
    //    ////        }
    //    ////    }
    //    ////    finally
    //    ////    {
    //    ////        _syncDataLock.Release();
    //    ////    }
    //    ////}




    //    //private static async Task<bool> GetLocalFkDataHasUnsyncedRecordsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        return await ctx.FkData.AnyAsync(p => p.SyncStatus == (int)SyncStatus.Unsynced || p.SyncStatus == (int)SyncStatus.Syncing).ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<bool> GetLocalLinesHasUnsyncedRecordsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        return await ctx.Lines.AnyAsync(p => p.SyncStatus == (int)SyncStatus.Syncing).ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<bool> GetLocalSkusHasUnsyncedRecordsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        return await ctx.Skus.AnyAsync(p => p.SyncStatus == (int)SyncStatus.Syncing).ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<bool> GetLocalDevicesHasUnsyncedRecordsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        return await ctx.Devices.AnyAsync(p => p.SyncStatus == (int)SyncStatus.Syncing).ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<bool> GetLocalSettingsHasUnsyncedRecordsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        return await ctx.Settings.AnyAsync(p => p.SyncStatus == (int)SyncStatus.Syncing).ConfigureAwait(false);
    //    //    }
    //    //}


    //    //private static async Task<List<FkData>> GetLocalFkDataToSyncAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        List<FkData> fkData = await (from s in ctx.FkData
    //    //                                     where s.SyncStatus == (int)SyncStatus.Syncing
    //    //                                     select new FkData
    //    //                                     {
    //    //                                         Id = s.Id,
    //    //                                         Timestamp = s.Timestamp,
    //    //                                         LineName = s.LineName,
    //    //                                         WorkOrderName = s.WorkOrderName,
    //    //                                         ProductName = s.ProductName,
    //    //                                         Tid = s.Tid,
    //    //                                         RawEpc = s.RawEpc,
    //    //                                         DecodedGtin = s.DecodedGtin,
    //    //                                         DecodedSerial = s.DecodedSerial,
    //    //                                         RawUser = s.RawUser,
    //    //                                         DecodedLot = s.DecodedLot,
    //    //                                         DecodedExpiry = s.DecodedExpiry,
    //    //                                         SubmittedTime = s.SubmittedTime,
    //    //                                         TriggerTime = s.TriggerTime,
    //    //                                         SyncStatus = (int)SyncStatus.Synced
    //    //                                     }).ToListAsync(ct).ConfigureAwait(false);
    //    //        return fkData;
    //    //    }
    //    //}

    //    //private static async Task<List<Lines>> GetGlobalLinesToSyncAsync(Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmGlobalContext())
    //    //    {
    //    //        List<Lines> lines = await (from s in ctx.Lines
    //    //                                   where s.SyncStatus == (int)SyncStatus.Syncing
    //    //                                   select new Lines
    //    //                                   {
    //    //                                       Id = s.Id,
    //    //                                       Name = s.Name,
    //    //                                       Active = s.Active,
    //    //                                       SyncStatus = (int)SyncStatus.Synced
    //    //                                   }).ToListAsync(ct).ConfigureAwait(false);
    //    //        return lines;
    //    //    }
    //    //}

    //    //private static async Task<List<Skus>> GetGlobalSkusToSyncAsync(Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmGlobalContext())
    //    //    {
    //    //        List<Skus> skus = await (from s in ctx.Skus
    //    //                                   where s.SyncStatus == (int)SyncStatus.Syncing
    //    //                                   select new Skus
    //    //                                   {
    //    //                                       Id = s.Id,
    //    //                                       Name = s.Name,
    //    //                                       Active = s.Active,
    //    //                                       SyncStatus = (int)SyncStatus.Synced
    //    //                                   }).ToListAsync(ct).ConfigureAwait(false);
    //    //        return skus;
    //    //    }
    //    //}

    //    //private static async Task<List<Devices>> GetGlobalDevicesToSyncAsync(Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmGlobalContext())
    //    //    {
    //    //        List<Devices> devices = await (from s in ctx.Devices
    //    //                                 where s.SyncStatus == (int)SyncStatus.Syncing
    //    //                                 select new Devices
    //    //                                 {
    //    //                                     Id = s.Id,
    //    //                                     LineId = s.LineId,
    //    //                                     Name = s.Name,
    //    //                                     Type = s.Type,
    //    //                                     Active = s.Active,
    //    //                                     SyncStatus = (int)SyncStatus.Synced,
    //    //                                     SortBy = s.SortBy
    //    //                                 }).ToListAsync(ct).ConfigureAwait(false);
    //    //        return devices;
    //    //    }
    //    //}

    //    //private static async Task<List<Settings>> GetGlobalSettingsToSyncAsync(Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmGlobalContext())
    //    //    {
    //    //        List<Settings> settings = await (from s in ctx.Settings
    //    //                                       where s.SyncStatus == (int)SyncStatus.Syncing
    //    //                                       select new Settings
    //    //                                       {
    //    //                                           Id = s.Id,
    //    //                                           DeviceId = s.DeviceId,
    //    //                                           SkuId = s.SkuId,
    //    //                                           Name = s.Name,
    //    //                                           Type = s.Type,
    //    //                                           Value = s.Value,
    //    //                                           Active = s.Active,
    //    //                                           SyncStatus = (int)SyncStatus.Synced,
    //    //                                           SortBy = s.SortBy
    //    //                                       }).ToListAsync(ct).ConfigureAwait(false);
    //    //        return settings;
    //    //    }
    //    //}

    //    //public static async Task<List<WriteTagData>> GetWriterTagDataAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        List<WriteTagData> writerData = await (from w in ctx.WriteTagData.OrderByDescending(t => t.Indexed)
    //    //                                               select new WriteTagData
    //    //                                               {
    //    //                                                   Id = w.Id,
    //    //                                                   Indexed = w.Indexed,
    //    //                                                   FirstWrite = w.FirstWrite,
    //    //                                                   LastWrite = w.LastWrite,
    //    //                                                   TID = w.TID,
    //    //                                                   EPC = w.EPC,
    //    //                                                   RawUser = w.RawUser,
    //    //                                                   LockState = w.LockState,
    //    //                                                   IndexCount = w.IndexCount,
    //    //                                                   IndexMaxRSSI = w.IndexMaxRSSI,
    //    //                                                   WriteCount = w.WriteCount,
    //    //                                                   WriteMaxRSSI = w.WriteMaxRSSI,
    //    //                                                   WriteResult = w.WriteResult,
    //    //                                                   LockResult = w.LockResult,
    //    //                                               }).ToListAsync(ct).ConfigureAwait(false);

    //    //        return writerData;
    //    //    }
    //    //}

    //    //public static async Task<List<BatchOrderReport>> GetBatchOrderReportAsync(Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    List<BatchOrderReport> orders = null;

    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            orders = await ctx.BatchOrderReport.ToListAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    catch
    //    //    {

    //    //    }

    //    //    return orders;
    //    //}

    //    //public static async Task<List<Lines>> GetLinesAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    List<Lines> lines = null;

    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            lines = await (from l in ctx.Lines 
    //    //                           where l.Active
    //    //                           select new Lines
    //    //                           {
    //    //                               Id = l.Id,
    //    //                               Name = l.Name,
    //    //                               Active = l.Active
    //    //                           }).ToListAsync(ct).ConfigureAwait(false);

                    
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            lines = await (from l in ctx.Lines
    //    //                           where l.Active
    //    //                           select new Lines
    //    //                           {
    //    //                               Id = l.Id,
    //    //                               Name = l.Name,
    //    //                               Active = l.Active
    //    //                           }).ToListAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }

    //    //    return lines;
    //    //}

    //    //public static async Task<List<Skus>> GetSkusAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    List<Skus> skus = null;

    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            skus = await (from l in ctx.Skus
    //    //                          where l.Active
    //    //                          orderby l.Name
    //    //                           select new Skus
    //    //                           {
    //    //                               Id = l.Id,
    //    //                               Name = l.Name,
    //    //                               Active = l.Active
    //    //                           }).ToListAsync(ct).ConfigureAwait(false);


    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            skus = await (from l in ctx.Skus
    //    //                          where l.Active
    //    //                          orderby l.Name
    //    //                          select new Skus
    //    //                           {
    //    //                               Id = l.Id,
    //    //                               Name = l.Name,
    //    //                               Active = l.Active
    //    //                           }).ToListAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }

    //    //    return skus;
    //    //}

    //    //public static async Task<List<Devices>> GetDevicesAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    List<Devices> devices = null;

    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            devices = await (from l in ctx.Devices
    //    //                             where l.Active
    //    //                             orderby l.SortBy ascending
    //    //                          select new Devices
    //    //                          {
    //    //                              Id = l.Id,
    //    //                              LineId = l.LineId,
    //    //                              Name = l.Name,
    //    //                              Type = l.Type,
    //    //                              Active = l.Active,
    //    //                              SortBy = l.SortBy
    //    //                          }).ToListAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            devices = await (from l in ctx.Devices 
    //    //                             where l.Active 
    //    //                             orderby l.SortBy ascending
    //    //                          select new Devices
    //    //                          {
    //    //                              Id = l.Id,
    //    //                              LineId = l.LineId,
    //    //                              Name = l.Name,
    //    //                              Type = l.Type,
    //    //                              Active = l.Active,
    //    //                              SortBy = l.SortBy
    //    //                          }).ToListAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    return devices;
    //    //}

    //    //public static async Task<List<Settings>> GetSettingsAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    List<Settings> settings = null;

    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            settings = await (from l in ctx.Settings
    //    //                              where l.Active
    //    //                              orderby l.SortBy
    //    //                             select new Settings
    //    //                             {
    //    //                                 Id = l.Id,
    //    //                                 DeviceId = l.DeviceId,
    //    //                                 SkuId = l.SkuId,
    //    //                                 Name = l.Name,
    //    //                                 Type = l.Type,
    //    //                                 Value = l.Value,
    //    //                                 Active = l.Active,
    //    //                                 SortBy = l.SortBy
    //    //                             }).ToListAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            settings = await (from l in ctx.Settings
    //    //                              where l.Active
    //    //                              orderby l.SortBy
    //    //                              select new Settings
    //    //                             {
    //    //                                 Id = l.Id,
    //    //                                 DeviceId = l.DeviceId,
    //    //                                 SkuId = l.SkuId,
    //    //                                 Name = l.Name,
    //    //                                 Type = l.Type,
    //    //                                 Value = l.Value,
    //    //                                 Active = l.Active,
    //    //                                 SortBy = l.SortBy
    //    //                             } ).ToListAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    return settings;
    //    //}


    //    //public static async Task<List<ValidationTriggerData>> GetValidationTriggerDataAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        List<ValidationTriggerData> validationData = await (from v in ctx.ValidationTriggerData.OrderByDescending(v => v.HostTime)
    //    //                                                            select new ValidationTriggerData
    //    //                                                            {
    //    //                                                                Id = v.Id,
    //    //                                                                HostTime = v.HostTime,
    //    //                                                                TID = v.TID,
    //    //                                                                EPC = v.EPC,
    //    //                                                                RawUser = v.RawUser,
    //    //                                                                DecodedGtin = v.DecodedGtin,
    //    //                                                                DecodedLot = v.DecodedLot,
    //    //                                                                DecodedExpiry = v.DecodedExpiry,
    //    //                                                                DecodedSerial = v.DecodedSerial,
    //    //                                                                EPCSerial = v.EPCSerial,
    //    //                                                                LockState = v.LockState,
    //    //                                                                ReadCount = v.ReadCount,
    //    //                                                                MaxRSSI = v.MaxRSSI,
    //    //                                                                ResultFlags = v.ResultFlags,
    //    //                                                            }).ToListAsync(ct).ConfigureAwait(false);



    //    //        return validationData;
    //    //    }
    //    //}

    //    //private static async Task<int> SyncFkDataToGlobalAsync(IList<FkData> fkData, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    if (fkData.Count > 0)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            ctx.FkData.AddRange(fkData);
    //    //            return await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    return 0;
    //    //}

    //    //private static async Task<int> SyncFkDataToGlobalWithCheckAsync(IList<FkData> fkData, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    if (fkData.Count > 0)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            var newFkData = from l in fkData
    //    //                            join c in ctx.FkData on l.Tid equals c.Tid
    //    //                            into matches
    //    //                            where !matches.Any()
    //    //                            select l;

    //    //            ctx.FkData.AddRange(newFkData);

    //    //            return await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    return 0;
    //    //}

    //    //private static async Task<int> SetSyncFkDataAsync(SyncStatus fromStatus, SyncStatus toStatus, Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        await ctx.FkData.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //        return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<int> SetSyncFkDataAsync(SyncStatus fromStatus, SyncStatus toStatus, int limit, Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        await ctx.FkData.Where(l => l.SyncStatus == (int)fromStatus).Take(limit).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //        return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<int> ClearSyncedFkDataAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        var del = await ctx.FkData.Where(d => d.SyncStatus == (int)SyncStatus.Synced).ToListAsync(ct).ConfigureAwait(false);
    //    //        ctx.FkData.RemoveRange(del);
    //    //        return await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //    }
    //    //}


    //    // Check for any records in fkData table that have been set to sync (SyncStatus = SyncStatus.Syncing) but still exist in table
    //    // If any records exists load into list and sync to global table if they don't exist already
    //    public bool CatchUnSyncedFkData()
    //    {
    //        bool unsyncedExists = false;

    //        try
    //        {
    //            using (var ctx = GetRLMLocalContext())
    //            {
    //                unsyncedExists = ctx.FkData.Where(f => f.SyncStatus == (int)SyncStatus.Syncing).Count() > 0;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            unsyncedExists = false;
    //            return unsyncedExists;
    //        }

    //        return unsyncedExists;
    //    }

    //    public int GetUnsyncedFkDataRecordCount()
    //    {
    //        int recCnt = 0;

    //        try
    //        {
    //            using (var ctx = GetRLMLocalContext())
    //            {
    //                recCnt = ctx.FkData.Where(f => f.SyncStatus == (int)SyncStatus.Unsynced).Count();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            recCnt = 0;
    //            return recCnt;
    //        }

    //        return recCnt;
    //    }

    //    public int GetLocalFkData()
    //    {
    //        int rowCount = 0;

    //        lstFkData.Clear();
    //        using (var ctx = GetRLMLocalContext())
    //        {
    //            try
    //            {
    //                lstFkData = (from s in ctx.FkData
    //                             where s.SyncStatus == (int)SyncStatus.Syncing
    //                             select new FkData
    //                             {
    //                                 Id = s.Id,
    //                                 Timestamp = s.Timestamp,
    //                                 LineName = s.LineName,
    //                                 WorkOrderName = s.WorkOrderName,
    //                                 ProductName = s.ProductName,
    //                                 Tid = s.Tid,
    //                                 RawEpc = s.RawEpc,
    //                                 DecodedGtin = s.DecodedGtin,
    //                                 DecodedSerial = s.DecodedSerial,
    //                                 RawUser = s.RawUser,
    //                                 DecodedLot = s.DecodedLot,
    //                                 DecodedExpiry = s.DecodedExpiry,
    //                                 SubmittedTime = s.SubmittedTime,
    //                                 TriggerTime = s.TriggerTime,
    //                                 SyncStatus = (int)SyncStatus.Synced
    //                             }).ToList();
    //            }
    //            catch (Exception ex)
    //            {
    //                rowCount = 0;
    //                return rowCount;
    //            }

    //            rowCount = lstFkData.Count();
    //            return rowCount;
    //        }
    //    }

    //    public bool SetSyncFkData(int fromStatus, int toStatus, bool limit)
    //    {
    //        bool statusUpdated = false;

    //        if (limit == true)
    //        {
    //            try
    //            {
    //                using (var ctx = GetRLMLocalContext())
    //                {

    //                    var update = ctx.FkData.Where(l => l.SyncStatus == fromStatus).Take(10000);

    //                    foreach (FkData l in update)
    //                    {
    //                        l.SyncStatus = toStatus;
    //                    }
    //                    ctx.SaveChanges();
    //                }

    //                statusUpdated = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                statusUpdated = false;
    //                return statusUpdated;
    //            }
    //        }
    //        else
    //            try
    //            {
    //                using (var ctx = GetRLMLocalContext())
    //                {
    //                    var update = ctx.FkData.Where(l => l.SyncStatus == fromStatus);

    //                    foreach (FkData l in update)
    //                    {
    //                        l.SyncStatus = toStatus;
    //                    }
    //                    ctx.SaveChanges();
    //                }

    //                statusUpdated = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                statusUpdated = false;
    //                return statusUpdated;
    //            }

    //        return statusUpdated;
    //    }

    //    public bool SyncFkDataToGlobal()
    //    {
    //        bool syncSuccess = false;

    //        if (lstFkData.Count > 0)
    //        {
    //            try
    //            {
    //                using (var ctx = GetRLMGlobalContext())
    //                {
    //                    foreach (var row in lstFkData)
    //                    {
    //                        ctx.FkData.Add(row);
    //                    }

    //                    ctx.SaveChanges();
    //                }

    //                syncSuccess = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                syncSuccess = false;
    //            }
    //        }

    //        return syncSuccess;
    //    }

    //    public bool SyncFkDataToGlobalCatch()
    //    {
    //        bool syncSuccess = false;

    //        if (lstFkData.Count > 0)
    //        {
    //            try
    //            {
    //                using (var ctx = GetRLMGlobalContext())
    //                {
    //                    var result = from l in lstFkData
    //                                 join c in ctx.FkData on l.Tid equals c.Tid
    //                                 into matches
    //                                 where !matches.Any()
    //                                 select l;

    //                    foreach (var row in result)
    //                    {
    //                        ctx.FkData.Add(row);
    //                    }

    //                    ctx.SaveChanges();
    //                }

    //                syncSuccess = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                syncSuccess = false;
    //            }
    //        }

    //        return syncSuccess;
    //    }

    //    public bool ClearSyncedFkData()
    //    {
    //        bool cleared = false;

    //        //DatabaseStructure dbs = new DatabaseStructure();
    //        using (var ctx = GetRLMLocalContext())
    //        {
    //            try
    //            {
    //                //ctx.Database.ExecuteSqlRaw("TRUNCATE TABLE " + tableName);
    //                var del = ctx.FkData.Where(d => d.SyncStatus == (int)SyncStatus.Synced).ToList();
    //                if (del.Count > 0)
    //                {
    //                    foreach (FkData l in del)
    //                        ctx.FkData.Remove(l);
    //                }

    //                ctx.SaveChanges();

    //                cleared = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                cleared = false;
    //            }
    //        }

    //        lstFkData.Clear();
    //        return cleared;
    //    }


    //    #endregion ***** FKDATA SYNC *****

    //    #region ***** LOGS SYNC *****

    //    private static SemaphoreSlim _syncLogLock {get; } = new SemaphoreSlim(1, 1);

    //    //public static async Task WriteLog(Logs log, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalLogContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);
    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            ctx.Logs.Add(log);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task SyncLogsAsync(int batchSize, Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalLogContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);
    //    //    try
    //    //    {
    //    //        if (!(await GetLocalLogsHasUnsyncedRecordsAsync(getRlmLocalContext, ct).ConfigureAwait(false)))
    //    //            return;

    //    //        // CATCH ANY RECORDS SET AS SYNC STATUS "1" AND SYNC IF THEY DON'T EXIST IN CACHE
    //    //        List<Logs> logs = await DataSync.GetLocalLogsToSyncAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    //        //List<TblLogs> tblLogs = await DataSync.ConvertLogsToTblLogsAsync(logs, getPlmLocalLogContext, ct).ConfigureAwait(false);
    //    //        if (logs.Count > 0)
    //    //        {
    //    //            await DataSync.SyncLogsToCacheWithCheckAsync(logs, getRlmGlobalLogContext, ct).ConfigureAwait(false);
    //    //            await DataSync.SetSyncLogsAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, ct).ConfigureAwait(false);
    //    //        }

    //    //        // SYNC LOG RECORDS FROM LOCAL TO CACHE
    //    //        int countToSync = await DataSync.SetSyncLogsAsync(SyncStatus.Unsynced, SyncStatus.Syncing, batchSize, getRlmLocalContext, ct).ConfigureAwait(false);
    //    //        while (countToSync > 0)
    //    //        {
    //    //            logs = await DataSync.GetLocalLogsToSyncAsync(getRlmLocalContext, ct).ConfigureAwait(false);
    //    //            //tblLogs = await DataSync.ConvertLogsToTblLogsAsync(logs, getPlmLocalLogContext, ct).ConfigureAwait(false);
    //    //            if (logs.Count > 0)
    //    //            {
    //    //                await DataSync.SyncLogsToCacheAsync(logs, getRlmGlobalLogContext, ct).ConfigureAwait(false);
    //    //                await DataSync.SetSyncLogsAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, ct).ConfigureAwait(false);
    //    //            }
    //    //            countToSync = await DataSync.SetSyncLogsAsync(SyncStatus.Unsynced, SyncStatus.Syncing, batchSize, getRlmLocalContext, ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}


    //    //public static async Task SyncAllSettingsToLocalAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, string RLMLineName, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);
    //    //    try
    //    //    {
    //    //        //SYNC ANY UNSYNCED LOCAL SETTINGS BEFORE PULLING DOWN GLOBAL SETTINGS
    //    //        List<Lines> lines = await GetLinesToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, RLMLineId, ct).ConfigureAwait(false);
    //    //        List<Skus> skus = await GetSkusToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        List<Devices> devices = await GetDevicesToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, RLMLineId, ct).ConfigureAwait(false);
    //    //        List<Settings> settings = await GetSettingsToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, RLMLineId, ct).ConfigureAwait(false);

    //    //        if (lines.Count > 0)
    //    //        {
    //    //            await SyncLinesAsync(lines, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global).ConfigureAwait(false);
    //    //            await SetSyncLinesAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //        if (skus.Count > 0)
    //    //        {
    //    //            await SyncSkusAsync(skus, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global).ConfigureAwait(false);
    //    //            await SetSyncSkusAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //        if (devices.Count > 0)
    //    //        {
    //    //            await SyncDevicesAsync(devices, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global).ConfigureAwait(false);
    //    //            await SetSyncDevicesAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //        if (settings.Count > 0)
    //    //        {
    //    //            await SyncSettingsAsync(settings, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global).ConfigureAwait(false);
    //    //            await SetSyncSettingsAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }

    //    //        //CLEAR OUT ANY EXISTING LOCAL SETTINGS
    //    //        //await ClearLocalSettingsAsync(getRlmLocalContext, ct).ConfigureAwait(false);

    //    //        //CLEAR LISTS
    //    //        lines.Clear();
    //    //        skus.Clear();
    //    //        devices.Clear();
    //    //        settings.Clear();

    //    //        //SET GLOBAL SETTINGS SYNC STATUS FLAG TO "SYNCING"
    //    //        //await SetSyncLinesAsync(SyncStatus.Unsynced, SyncStatus.Syncing, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Global, ct).ConfigureAwait(false);
    //    //        //await SetSyncSkusAsync(SyncStatus.Unsynced, SyncStatus.Syncing, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Global, ct).ConfigureAwait(false);
    //    //        //await SetSyncDevicesAsync(SyncStatus.Unsynced, SyncStatus.Syncing, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Global, ct).ConfigureAwait(false);
    //    //        //await SetSyncSettingsAsync(SyncStatus.Unsynced, SyncStatus.Syncing, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Global, ct).ConfigureAwait(false);

    //    //        RLMLineId = await GetRLMLineIdAsync(getRlmGlobalContext, RLMLineName, ct);

    //    //        //LOAD LISTS WITH GLOBAL SETTINGS
    //    //        lines = await GetLinesToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Global, RLMLineId, ct).ConfigureAwait(false);
    //    //        skus = await GetSkusToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Global, ct).ConfigureAwait(false);
    //    //        devices = await GetDevicesToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Global, RLMLineId, ct).ConfigureAwait(false);
    //    //        settings = await GetSettingsToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Global, RLMLineId, ct).ConfigureAwait(false);

    //    //        //SYNC GLOBAL SETTINGS TO LOCAL DATABASE AND SET SYNC STATUS TO "SYNCED"
    //    //        if (lines.Count > 0)
    //    //            await SyncLinesAsync(lines, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Local).ConfigureAwait(false);
    //    //        if (skus.Count > 0)
    //    //            await SyncSkusAsync(skus, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Local).ConfigureAwait(false);
    //    //        if (devices.Count > 0)
    //    //            await SyncDevicesAsync(devices, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Local).ConfigureAwait(false);
    //    //        if (settings.Count > 0)
    //    //            await SyncSettingsAsync(settings, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Local).ConfigureAwait(false);
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //public static async Task SyncAllSettingsToGlobalAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);

    //    //    try
    //    //    {
    //    //        //SYNC ANY UNSYNCED LOCAL SETTINGS TO GLOBAL
    //    //        List<Lines> lines = await GetLinesToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, RLMLineId, ct).ConfigureAwait(false);
    //    //        List<Skus> skus = await GetSkusToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        List<Devices> devices = await GetDevicesToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, RLMLineId, ct).ConfigureAwait(false);
    //    //        List<Settings> settings = await GetSettingsToSyncAsync(getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, RLMLineId, ct).ConfigureAwait(false);

    //    //        if (lines.Count > 0)
    //    //        {
    //    //            await SyncLinesAsync(lines, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global).ConfigureAwait(false);
    //    //            await SetSyncLinesAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //        if (skus.Count > 0)
    //    //        {
    //    //            await SyncSkusAsync(skus, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global).ConfigureAwait(false);
    //    //            await SetSyncSkusAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //        if (devices.Count > 0)
    //    //        {
    //    //            await SyncDevicesAsync(devices, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global).ConfigureAwait(false);
    //    //            await SetSyncDevicesAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }
    //    //        if (settings.Count > 0)
    //    //        {
    //    //            await SyncSettingsAsync(settings, getRlmGlobalContext, getRlmLocalContext, ct, SyncLocation.Global).ConfigureAwait(false);
    //    //            await SetSyncSettingsAsync(SyncStatus.Syncing, SyncStatus.Synced, getRlmLocalContext, getRlmGlobalContext, SyncLocation.Local, ct).ConfigureAwait(false);
    //    //        }

    //    //        //CLEAR LISTS
    //    //        lines.Clear();
    //    //        skus.Clear();
    //    //        devices.Clear();
    //    //        settings.Clear();

    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //private static async Task<Guid> GetRLMLineIdAsync(Func<RLMGlobalContext> getRlmGlobalContext, string RLMLineName, CancellationToken ct)
    //    //{
    //    //    Guid RLMLineId;

    //    //    using (var ctx = getRlmGlobalContext())
    //    //    {
    //    //        RLMLineId = await ctx.Lines.Where(l => l.Name == RLMLineName).Select(l => l.Id).FirstOrDefaultAsync();
    //    //    }

    //    //    return RLMLineId;
    //    //}

    //    //private static async Task<List<Lines>> GetLinesToSyncAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, SyncLocation loc, Guid RLMLineId, CancellationToken ct)
    //    //{
    //    //    List<Lines> lines = null;

    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            lines = await (from l in ctx.Lines
    //    //                           where l.SyncStatus == (int)SyncStatus.Syncing || l.SyncStatus == (int)SyncStatus.Unsynced
    //    //                           select new Lines
    //    //                           {
    //    //                               Id = l.Id,
    //    //                               Name = l.Name,
    //    //                               Active = l.Active,
    //    //                               SyncStatus = l.SyncStatus
    //    //                           }).ToListAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            lines = await (from l in ctx.Lines
    //    //                           where l.Id == RLMLineId
    //    //                           select new Lines
    //    //                           {
    //    //                               Id = l.Id,
    //    //                               Name = l.Name,
    //    //                               Active = l.Active,
    //    //                               SyncStatus = l.SyncStatus
    //    //                           }).ToListAsync().ConfigureAwait(false);
    //    //        }
    //    //    }

    //    //    return lines;
    //    //}

    //    //private static async Task<List<Skus>> GetSkusToSyncAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    List<Skus> skus = null;

    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            skus = await (from l in ctx.Skus
    //    //                          where l.SyncStatus == (int)SyncStatus.Syncing || l.SyncStatus == (int)SyncStatus.Unsynced
    //    //                          select new Skus
    //    //                          {
    //    //                              Id = l.Id,
    //    //                              Name = l.Name,
    //    //                              Active = l.Active,
    //    //                              SyncStatus = (int)SyncStatus.Unsynced
    //    //                          }).ToListAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            skus = await (from l in ctx.Skus
    //    //                          //where l.SyncStatus == (int)SyncStatus.Syncing
    //    //                          select new Skus
    //    //                          {
    //    //                              Id = l.Id,
    //    //                              Name = l.Name,
    //    //                              Active = l.Active,
    //    //                              SyncStatus = (int)SyncStatus.Unsynced
    //    //                          }).ToListAsync().ConfigureAwait(false);
    //    //        }
    //    //    }

    //    //    return skus;
    //    //}

    //    //private static async Task<List<Devices>> GetDevicesToSyncAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, SyncLocation loc, Guid RLMLineId, CancellationToken ct)
    //    //{
    //    //    List<Devices> devices = null;

    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            devices = await (from l in ctx.Devices
    //    //                             where l.SyncStatus == (int)SyncStatus.Syncing || l.SyncStatus == (int)SyncStatus.Unsynced
    //    //                             select new Devices
    //    //                             {
    //    //                                 Id = l.Id,
    //    //                                 LineId = l.LineId,
    //    //                                 Name = l.Name,
    //    //                                 Type = l.Type,
    //    //                                 Active = l.Active,
    //    //                                 SyncStatus = (int)SyncStatus.Unsynced,
    //    //                                 SortBy = l.SortBy
    //    //                             }).ToListAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            devices = await (from l in ctx.Devices
    //    //                             //join r in ctx.Lines on l.LineId equals r.Id
    //    //                             where l.LineId == RLMLineId
    //    //                             select new Devices
    //    //                             {
    //    //                                 Id = l.Id,
    //    //                                 LineId = l.LineId,
    //    //                                 Name = l.Name,
    //    //                                 Type = l.Type,
    //    //                                 Active = l.Active,
    //    //                                 SyncStatus = (int)SyncStatus.Unsynced,
    //    //                                 SortBy = l.SortBy
    //    //                             }).ToListAsync().ConfigureAwait(false);
    //    //        }
    //    //    }

    //    //        return devices;
    //    //}

    //    ////TODO: GET ONLY SETTINGS FOR THE DEVICES IN THE DEVICES LIST 
    //    //private static async Task<List<Settings>> GetSettingsToSyncAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, SyncLocation loc, Guid RLMLineId, CancellationToken ct)
    //    //{
    //    //    List<Settings> settings = null;

    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            settings = await (from l in ctx.Settings
    //    //                              where l.SyncStatus == (int)SyncStatus.Syncing || l.SyncStatus == (int)SyncStatus.Unsynced
    //    //                              select new Settings
    //    //                              {
    //    //                                  Id = l.Id,
    //    //                                  DeviceId = l.DeviceId,
    //    //                                  SkuId = l.SkuId,
    //    //                                  Name = l.Name,
    //    //                                  Type = l.Type,
    //    //                                  Value = l.Value,
    //    //                                  Active = l.Active,
    //    //                                  SyncStatus = (int)SyncStatus.Unsynced,
    //    //                                  SortBy = l.SortBy
    //    //                              }).ToListAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            try
    //    //            {
    //    //                settings = await (from l in ctx.Settings
    //    //                                  join d in ctx.Devices on l.DeviceId equals d.Id
    //    //                                  where d.LineId == RLMLineId
    //    //                                  select new Settings
    //    //                                  {
    //    //                                      Id = l.Id,
    //    //                                      DeviceId = l.DeviceId,
    //    //                                      SkuId = l.SkuId,
    //    //                                      Name = l.Name,
    //    //                                      Type = l.Type,
    //    //                                      Value = l.Value,
    //    //                                      Active = l.Active,
    //    //                                      SyncStatus = (int)SyncStatus.Unsynced,
    //    //                                      SortBy = l.SortBy
    //    //                                  }).ToListAsync().ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            { 

    //    //            }
    //    //        }
    //    //    }

    //    //        return settings;
    //    //}

    //    //private static async Task<bool> GetLocalLogsHasUnsyncedRecordsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        return await ctx.Logs.AnyAsync(p => p.SyncStatus == (int)SyncStatus.Unsynced || p.SyncStatus == (int)SyncStatus.Syncing).ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<List<Logs>> GetLocalLogsToSyncAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        List<Logs> logs = await (from l in ctx.Logs
    //    //                         where l.SyncStatus == (int)SyncStatus.Syncing
    //    //                         select new Logs
    //    //                         {
    //    //                             Id = l.Id,
    //    //                             LogTime = l.LogTime,
    //    //                             User = l.User,
    //    //                             Line = l.Line,
    //    //                             OrderName = l.OrderName,
    //    //                             Message = l.Message,
    //    //                             Code = l.Code,
    //    //                             LogLevel = l.LogLevel
    //    //                         }).ToListAsync().ConfigureAwait(false);
    //    //        return logs;
    //    //    }
    //    //}

    //    //public static async Task<List<Logs>> GetLogsAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, string RLMLineName, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    List<Logs> logs = null;
    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContext())
    //    //        {
    //    //            logs = await (from l in ctx.Logs
    //    //                          select new Logs
    //    //                          {
    //    //                              Id = l.Id,
    //    //                              LogTime = l.LogTime,
    //    //                              User = l.User,
    //    //                              Line = l.Line,
    //    //                              OrderName = l.OrderName,
    //    //                              Message = l.Message,
    //    //                              Code = l.Code,
    //    //                              LogLevel = l.LogLevel
    //    //                          }).ToListAsync().ConfigureAwait(false);

    //    //        } 
    //    //    }
    //    //    else
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            logs = await (from l in ctx.Logs
    //    //                          where l.Line == RLMLineName
    //    //                          select new Logs
    //    //                          {
    //    //                              Id = l.Id,
    //    //                              LogTime = l.LogTime,
    //    //                              User = l.User,
    //    //                              Line = l.Line,
    //    //                              OrderName = l.OrderName,
    //    //                              Message = l.Message,
    //    //                              Code = l.Code,
    //    //                              LogLevel = l.LogLevel
    //    //                          }).ToListAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    return logs;
    //    //}

    //    //private static async Task<int> SetSyncLogsAsync(SyncStatus fromStatus, SyncStatus toStatus, Func<RLMLocalContext> getRlmLocalContextFunc, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContextFunc())
    //    //    {
    //    //        await ctx.Logs.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //        return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<int> SetSyncLinesAsync(SyncStatus fromStatus, SyncStatus toStatus, Func<RLMLocalContext> getRlmLocalContextFunc, Func<RLMGlobalContext> getRlmGlobalContextFunc, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContextFunc())
    //    //        {
    //    //            await ctx.Lines.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //            return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContextFunc())
    //    //        {
    //    //            await ctx.Lines.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //            return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else return 0;
    //    //}

    //    //private static async Task<int> SetSyncSkusAsync(SyncStatus fromStatus, SyncStatus toStatus, Func<RLMLocalContext> getRlmLocalContextFunc, Func<RLMGlobalContext> getRlmGlobalContextFunc, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContextFunc())
    //    //        {
    //    //            await ctx.Skus.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //            return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContextFunc())
    //    //        {
    //    //            await ctx.Skus.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //            return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else return 0;
    //    //}

    //    //private static async Task<int> SetSyncDevicesAsync(SyncStatus fromStatus, SyncStatus toStatus, Func<RLMLocalContext> getRlmLocalContextFunc, Func<RLMGlobalContext> getRlmGlobalContextFunc, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContextFunc())
    //    //        {
    //    //            await ctx.Devices.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //            return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContextFunc())
    //    //        {
    //    //            await ctx.Devices.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //            return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else return 0;
    //    //}

    //    //private static async Task<int> SetSyncSettingsAsync(SyncStatus fromStatus, SyncStatus toStatus, Func<RLMLocalContext> getRlmLocalContextFunc, Func<RLMGlobalContext> getRlmGlobalContextFunc, SyncLocation loc, CancellationToken ct)
    //    //{
    //    //    if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var ctx = getRlmLocalContextFunc())
    //    //        {
    //    //            await ctx.Settings.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //            return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var ctx = getRlmGlobalContextFunc())
    //    //        {
    //    //            await ctx.Settings.Where(l => l.SyncStatus == (int)fromStatus).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //            return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    else return 0;
    //    //}


    //    //private static async Task<int> SetSyncLogsAsync(SyncStatus fromStatus, SyncStatus toStatus, int limit, Func<RLMLocalContext> getRlmLocalContextFunc, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContextFunc())
    //    //    {
    //    //        await ctx.Logs.Where(l => l.SyncStatus == (int)fromStatus).Take(limit).ForEachAsync(a => a.SyncStatus = (int)toStatus).ConfigureAwait(false);
    //    //        return await ctx.SaveChangesAsync().ConfigureAwait(false);
    //    //    }
    //    //}

    //    //private static async Task<int> SyncLinesAsync(IList<Lines> lines, Func<RLMGlobalContext> getRlmGlobalContext, Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct, SyncLocation loc)
    //    //{
    //    //    if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var gctx = getRlmGlobalContext())
    //    //        {
    //    //            // UPDATE EXISTING RECORDS

    //    //            var update =
    //    //                from l in lines
    //    //                join c in gctx.Lines on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = (int)SyncStatus.Synced;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from l in lines
    //    //                          join c in gctx.Lines on l.Id equals c.Id
    //    //                          into matches
    //    //                          where !matches.Any()
    //    //                          select l;

    //    //            gctx.Lines.AddRange(insert);

    //    //            try
    //    //            {
    //    //                await gctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            {

    //    //            }
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var lctx = getRlmLocalContext())
    //    //        {
    //    //            // UPDATE EXISTING RECORDS

    //    //            var update =
    //    //                from l in lines
    //    //                join c in lctx.Lines on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = u.l.SyncStatus;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from l in lines
    //    //                         join c in lctx.Lines on l.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select l;

    //    //            lctx.Lines.AddRange(insert);
    //    //            try
    //    //            {
    //    //                return await lctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            {

    //    //            }
    //    //        }
    //    //    }

    //    //    return 0;
    //    //}

    //    //private static async Task<int> SyncSkusAsync(IList<Skus> skus, Func<RLMGlobalContext> getRlmGlobalContext, Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct, SyncLocation loc)
    //    //{
    //    //    if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var gctx = getRlmGlobalContext())
    //    //        {

    //    //            // UPDATE EXISTING RECORDS

    //    //            var update =
    //    //                from l in skus
    //    //                join c in gctx.Skus on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = (int)SyncStatus.Synced;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from s in skus
    //    //                         join c in gctx.Skus on s.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select s;

    //    //            gctx.Skus.AddRange(insert);

    //    //            try
    //    //            {
    //    //                await gctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            {

    //    //            }
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var lctx = getRlmLocalContext())
    //    //        {
    //    //            // UPDATE EXISTING RECORDS

    //    //            var update =
    //    //                from l in skus
    //    //                join c in lctx.Skus on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = u.l.SyncStatus;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from s in skus
    //    //                         join c in lctx.Skus on s.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select s;

    //    //            lctx.Skus.AddRange(insert);

    //    //            try
    //    //            {
    //    //                return await lctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            {

    //    //            }
    //    //        }
    //    //    }
    //    //    return 0;
    //    //}

    //    //private static async Task<int> SyncDevicesAsync(IList<Devices> devices, Func<RLMGlobalContext> getRlmGlobalContext, Func<MQAKioskContext> getRlmLocalContext, CancellationToken ct, SyncLocation loc)
    //    //{
    //    //    if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var gctx = getRlmGlobalContext())
    //    //        {

    //    //            // UPDATE EXISTING RECORDS

    //    //            var update =
    //    //                from l in devices
    //    //                join c in gctx.Devices on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.LineId = u.l.LineId;
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Type = u.l.Type;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = (int)SyncStatus.Synced;
    //    //                u.c.SortBy = u.l.SortBy;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from d in devices
    //    //                         join c in gctx.Devices on d.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select d;

    //    //            gctx.Devices.AddRange(insert);

    //    //            try
    //    //            {
    //    //                await gctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            {

    //    //            }
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var lctx = getRlmLocalContext())
    //    //        {
    //    //            // UPDATE EXISTING RECORDS

    //    //            var update =
    //    //                from l in devices
    //    //                join c in lctx.Devices on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.LineId = u.l.LineId;
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Type = u.l.Type;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = u.l.SyncStatus;
    //    //                u.c.SortBy = u.l.SortBy;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from d in devices
    //    //                         join c in lctx.Devices on d.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select d;

    //    //            lctx.Devices.AddRange(insert);
    //    //            try
    //    //            {
    //    //                return await lctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            {

    //    //            }
    //    //        }
    //    //    }
    //    //    return 0;
    //    //}

    //    //private static async Task<int> SyncSettingsAsync(IList<Settings> settings, Func<RLMGlobalContext> getRlmGlobalContext, Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct, SyncLocation loc)
    //    //{
    //    //    if (loc == SyncLocation.Global)
    //    //    {
    //    //        using (var gctx = getRlmGlobalContext())
    //    //        {

    //    //            // UPDATE EXISTING RECORDS

    //    //            var update =
    //    //                from l in settings
    //    //                join c in gctx.Settings on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.DeviceId = u.l.DeviceId;
    //    //                u.c.SkuId = u.l.SkuId;
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Type = u.l.Type;
    //    //                u.c.Value = u.l.Value;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = (int)SyncStatus.Synced;
    //    //                u.c.SortBy = u.l.SortBy;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from s in settings
    //    //                         join c in gctx.Settings on s.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select s;

    //    //            gctx.Settings.AddRange(insert);

    //    //            try
    //    //            {
    //    //                await gctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            {

    //    //            }
    //    //        }
    //    //    }
    //    //    else if (loc == SyncLocation.Local)
    //    //    {
    //    //        using (var lctx = getRlmLocalContext())
    //    //        {
    //    //            // UPDATE EXISTING RECORDS

    //    //            var update =
    //    //                from l in settings
    //    //                join c in lctx.Settings on l.Id equals c.Id
    //    //                select new
    //    //                {
    //    //                    l,
    //    //                    c
    //    //                };

    //    //            foreach (var u in update)
    //    //            {
    //    //                u.c.DeviceId = u.l.DeviceId;
    //    //                u.c.SkuId = u.l.SkuId;
    //    //                u.c.Name = u.l.Name;
    //    //                u.c.Type = u.l.Type;
    //    //                u.c.Value = u.l.Value;
    //    //                u.c.Active = u.l.Active;
    //    //                u.c.SyncStatus = (int)SyncStatus.Synced;
    //    //                u.c.SortBy = u.l.SortBy;
    //    //            }

    //    //            // INSERT NEW RECORDS

    //    //            var insert = from s in settings
    //    //                         join c in lctx.Settings on s.Id equals c.Id
    //    //                         into matches
    //    //                         where !matches.Any()
    //    //                         select s;

    //    //            lctx.Settings.AddRange(insert);
    //    //            try
    //    //            {
    //    //                return await lctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }
    //    //            catch (Exception ex)
    //    //            {

    //    //            }
    //    //        }
    //    //    }
    //    //    return 0;
    //    //}

    //    //private static async Task<int> SyncLogsToCacheAsync(IList<Logs> logs, Func<RLMGlobalContext> getRlmGlobalLogContext, CancellationToken ct)
    //    //{
    //    //    if (logs.Count > 0)
    //    //    {
    //    //        using (var ctx = getRlmGlobalLogContext())
    //    //        {
    //    //            ctx.Logs.AddRange(logs);
    //    //            return await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    return 0;
    //    //}

    //    //private static async Task<int> SyncLogsToCacheWithCheckAsync(IList<Logs> logs, Func<RLMGlobalContext> getRlmGlobalLogContext, CancellationToken ct)
    //    //{
    //    //    if (logs.Count > 0)
    //    //    {
    //    //        using (var ctx = getRlmGlobalLogContext())
    //    //        {
    //    //            var newLogs = from l in logs
    //    //                            join c in ctx.Logs on l.Id equals  c.Id
    //    //                            into matches
    //    //                            where !matches.Any()
    //    //                            select l;

    //    //            ctx.Logs.AddRange(newLogs);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    return 0;
    //    //}




    //    public bool CatchUnSyncedLogs()
    //    {
    //        bool unsyncedExists = false;

    //        try
    //        {
    //            using (var ctx = GetRLMLocalContext())
    //            {
    //                unsyncedExists = ctx.Logs.Where(f => f.SyncStatus == (int)SyncStatus.Syncing).Count() > 0;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            unsyncedExists = false;
    //            return unsyncedExists;
    //        }

    //        return unsyncedExists;
    //    }

    //    public int GetUnsyncedLogRecordCount()
    //    {
    //        int recCnt = 0;

    //        try
    //        {
    //            using (var ctx = GetRLMLocalContext())
    //            {
    //                recCnt = ctx.Logs.Where(f => f.SyncStatus == (int)SyncStatus.Unsynced).Count();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            recCnt = 0;
    //            return recCnt;
    //        }

    //        return recCnt;
    //    }

    //    public bool SetSynLogs(bool limit)
    //    {
    //        bool statusUpdated = false;

    //        if (limit == true)
    //        {
    //            try
    //            {
    //                using (var ctx = GetRLMLocalContext())
    //                {
    //                    var update = ctx.Logs.Where(l => l.SyncStatus == (int)SyncStatus.Unsynced).Take(10000);

    //                    foreach (Logs l in update)
    //                    {
    //                        l.SyncStatus = (int)SyncStatus.Syncing;
    //                    }
    //                    ctx.SaveChanges();
    //                }

    //                statusUpdated = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                statusUpdated = false;
    //                return statusUpdated;
    //            }
    //        }
    //        else
    //            try
    //            {
    //                using (var ctx = GetRLMLocalContext())
    //                {
    //                    var update = ctx.Logs.Where(l => l.SyncStatus == (int)SyncStatus.Syncing);

    //                    foreach (Logs l in update)
    //                    {
    //                        l.SyncStatus = (int)SyncStatus.Synced;
    //                    }
    //                    ctx.SaveChanges();
    //                }

    //                statusUpdated = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                statusUpdated = false;
    //                return statusUpdated;
    //            }

    //        return statusUpdated;
    //    }

    //    public int GetLocalLogs()
    //    {
    //        int rowCount = 0;

    //        lstLogs.Clear();
    //        using (var ctx = GetRLMLocalContext())
    //        {
    //            try
    //            {
    //                lstLogs = (from l in ctx.Logs
    //                           where l.SyncStatus == (int)SyncStatus.Syncing
    //                           select new Logs
    //                           {
    //                               Id = l.Id,
    //                               LogTime = l.LogTime,
    //                               User = l.User,
    //                               Line = l.Line,
    //                               OrderName = l.OrderName,
    //                               Message = l.Message,
    //                               Code = l.Code,
    //                               LogLevel = l.LogLevel
    //                           }).ToList();
    //            }
    //            catch (Exception ex)
    //            {
    //                rowCount = 0;
    //                return rowCount;
    //            }

    //            rowCount = lstLogs.Count();
    //            return rowCount;
    //        }
    //    }

    //    public bool SetSyncLogs(int fromStatus, int toStatus, bool limit)
    //    {
    //        bool statusUpdated = false;

    //        if (limit == true)
    //        {
    //            try
    //            {
    //                using (var ctx = GetRLMLocalContext())
    //                {
    //                    var update = ctx.Logs.Where(l => l.SyncStatus == fromStatus).Take(10000);

    //                    foreach (Logs l in update)
    //                    {
    //                        l.SyncStatus = toStatus;
    //                    }
    //                    ctx.SaveChanges();
    //                }

    //                statusUpdated = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                statusUpdated = false;
    //                return statusUpdated;
    //            }
    //        }
    //        else
    //            try
    //            {
    //                using (var ctx = GetRLMLocalContext())
    //                {
    //                    var update = ctx.Logs.Where(l => l.SyncStatus == fromStatus);

    //                    foreach (Logs l in update)
    //                    {
    //                        l.SyncStatus = toStatus;
    //                    }
    //                    ctx.SaveChanges();
    //                }

    //                statusUpdated = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                statusUpdated = false;
    //                return statusUpdated;
    //            }

    //        return statusUpdated;
    //    }

    //    public bool SyncLogsCatch()
    //    {
    //        bool syncSuccess = false;

    //        //if (lstLogs.Count > 0)
    //        //{
    //        //    try
    //        //    {
    //        //        lstTblLogs.Clear();
    //        //        lstTblLogs = (from l in lstLogs
    //        //                      select new TblLogs
    //        //                      {
    //        //                          DteTime = l.DteTime,
    //        //                          IdUser = l.IdUser,
    //        //                          IdLine = l.IdLine,
    //        //                          StrMessage = l.StrMessage,
    //        //                          IntD1 = l.IntD1,
    //        //                          IntD2 = l.IntD2,
    //        //                          I64D1 = l.I64D1,
    //        //                          I64D2 = l.I64D2
    //        //                      }).ToList();

    //        //        using (var ctx = GetPLMLogContext())
    //        //        {
    //        //            var result = from l in lstTblLogs
    //        //                         join c in ctx.TblLogs on new { l.IntD1, l.IntD2, l.I64D1, l.I64D2, l.DteTime } equals new { c.IntD1, c.IntD2, c.I64D1, c.I64D2, c.DteTime }
    //        //                         into matches
    //        //                         where !matches.Any()
    //        //                         select l;

    //        //            foreach (var row in result)
    //        //            {
    //        //                ctx.TblLogs.Add(row);
    //        //            }

    //        //            ctx.SaveChanges();
    //        //        }

    //        //        syncSuccess = true;
    //        //    }
    //        //    catch (Exception ex)
    //        //    {
    //        //        syncSuccess = false;
    //        //    }
    //        //}

    //        return syncSuccess;
    //    }

    //    public bool SyncLogs()
    //    {
    //        bool syncSuccess = false;

    //        //// sync logs from local RLM to PLM cache
    //        //if (lstLogs.Count > 0)
    //        //{
    //        //    try
    //        //    {
    //        //        lstTblLogs = (from l in lstLogs
    //        //                      select new TblLogs
    //        //                      {
    //        //                          DteTime = l.DteTime,
    //        //                          IdUser = l.IdUser,
    //        //                          IdLine = l.IdLine,
    //        //                          StrMessage = l.StrMessage,
    //        //                          IntD1 = l.IntD1,
    //        //                          IntD2 = l.IntD2,
    //        //                          I64D1 = l.I64D1,
    //        //                          I64D2 = l.I64D2
    //        //                      }).ToList();

    //        //        using (var ctx = GetPLMLogContext())
    //        //        {
    //        //            foreach (var row in lstTblLogs)
    //        //            {
    //        //                ctx.TblLogs.Add(row);
    //        //            }
    //        //            ctx.SaveChanges();
    //        //        }

    //        //        syncSuccess = true;
    //        //    }
    //        //    catch (Exception ex)
    //        //    {
    //        //        syncSuccess = false;
    //        //    }
    //        //}

    //        return syncSuccess;
    //    }

    //    //update synced log records with synced flag
    //    //public void FlagSyncedLogs()
    //    //{
    //    //    bool flagged = false;

    //    //    try
    //    //    {
    //    //        using (var ctx = GetRLMLocalContext())
    //    //        {
    //    //            var update = ctx.Logs.Where(l => lstLogs.Contains(l)).ToList();
    //    //            foreach (Logs l in update)
    //    //            {
    //    //                l.SyncStatus = (int)SyncStatus.Syncing;
    //    //            }
    //    //            ctx.SaveChanges();
    //    //        }

    //    //        flagged = true;
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        flagged = false;
    //    //    }
    //    //}

    //    //Delete any log records flagged as synced
    //    //public void DeleteFlaggedLogs()
    //    //{
    //    //    try
    //    //    {
    //    //        using (var ctx = GetRLMLocalContext())
    //    //        {
    //    //            var del = ctx.Logs.Where(d => d.SyncStatus == (int)SyncStatus.Synced).ToList();
    //    //            if (del.Count > 0)
    //    //            {
    //    //                foreach (Logs l in del)
    //    //                {
    //    //                    ctx.Logs.Remove(l);
    //    //                }

    //    //                ctx.SaveChanges();
    //    //            }
    //    //        }
    //    //    }
    //    //    catch (Exception ex)
    //    //    {

    //    //    }
    //    //}

    //    public bool ClearSyncedLogs()
    //    {
    //        bool cleared = false;

    //        //DatabaseStructure dbs = new DatabaseStructure();
    //        using (var ctx = GetRLMLocalContext())
    //        {
    //            try
    //            {
    //                var del = ctx.Logs.Where(d => d.SyncStatus == (int)SyncStatus.Synced).ToList();
    //                if (del.Count > 0)
    //                {
    //                    foreach (Logs l in del)
    //                        ctx.Logs.Remove(l);
    //                }

    //                ctx.SaveChanges();

    //                cleared = true;
    //            }
    //            catch (Exception ex)
    //            {
    //                cleared = false;
    //            }
    //        }

    //        lstLogs.Clear();
    //        //lstTblLogs.Clear();

    //        return cleared;
    //    }

    //    #endregion ***** LOGS SYNC *****

    //    #region ***** DEVICES SYNC *****

    //    public bool CatchUnSyncedDevices()
    //    {
    //        bool unsyncedExists = false;

    //        try
    //        {
    //            using (var ctx = GetRLMLocalContext())
    //            {
    //                unsyncedExists = ctx.Devices.Where(f => f.SyncStatus == (int)SyncStatus.Syncing).Count() > 0;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            unsyncedExists = false;
    //            return unsyncedExists;
    //        }

    //        return unsyncedExists;
    //    }

    //    public int GetUnsyncedDevicesRecordCount()
    //    {
    //        int recCnt = 0;

    //        try
    //        {
    //            using (var ctx = GetMQAKioskContext())
    //            {
    //                recCnt = ctx.Devices.Where(f => f.SyncStatus == (int)SyncStatus.Unsynced).Count();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            recCnt = 0;
    //            return recCnt;
    //        }

    //        return recCnt;
    //    }

    //    public int GetLocalDevices()
    //    {
    //        //int rowCount = 0;

    //        //lstDevices.Clear();
    //        //using (var ctx = GetRLMLocalContext())
    //        //{
    //        //    try
    //        //    {
    //        //        lstDevices = (from s in ctx.Devices
    //        //                        where s.SyncStatus == (int)SyncStatus.Syncing
    //        //                        select new Devices
    //        //                        {
    //        //                            Id = s.Id,
    //        //                            LineId = s.LineId,
    //        //                            Name = s.Name,
    //        //                            Type = s.Type,
    //        //                            Active = s.Active,
    //        //                            SyncStatus = (int)SyncStatus.Synced,
    //        //                            SortBy = s.SortBy
    //        //                        } ).ToList();
    //        //    }
    //        //    catch (Exception ex)
    //        //    {
    //        //        rowCount = 0;
    //        //        return rowCount;
    //        //    }

    //        //    rowCount = lstDevices.Count();
    //        //    return rowCount;
    //        //}
    //    }

    //    public bool SetSyncDevices(int fromStatus, int toStatus, bool limit)
    //    {
    //        //bool statusUpdated = false;

    //        //if (limit == true)
    //        //{
    //        //    try
    //        //    {
    //        //        using (var ctx = GetRLMLocalContext())
    //        //        {

    //        //            var update = ctx.Devices.Where(l => l.SyncStatus == fromStatus).Take(10000);

    //        //            foreach (Devices l in update)
    //        //            {
    //        //                l.SyncStatus = toStatus;
    //        //            }
    //        //            ctx.SaveChanges();
    //        //        }

    //        //        statusUpdated = true;
    //        //    }
    //        //    catch (Exception ex)
    //        //    {
    //        //        statusUpdated = false;
    //        //        return statusUpdated;
    //        //    }
    //        //}
    //        //else
    //        //    try
    //        //    {
    //        //        using (var ctx = GetRLMLocalContext())
    //        //        {
    //        //            var update = ctx.Devices.Where(l => l.SyncStatus == fromStatus);

    //        //            foreach (Devices l in update)
    //        //            {
    //        //                l.SyncStatus = toStatus;
    //        //            }
    //        //            ctx.SaveChanges();
    //        //        }

    //        //        statusUpdated = true;
    //        //    }
    //        //    catch (Exception ex)
    //        //    {
    //        //        statusUpdated = false;
    //        //        return statusUpdated;
    //        //    }

    //        //return statusUpdated;
    //        return true;
    //    }

    //    #endregion ***** DEVICES SYNC *****

    //    //public static async Task SyncBatchOrderReportAsync(Func<RLMLocalContext> getRlmLocalContext, Func<RLMGlobalContext> getRlmGlobalContext, string orderName, string gtin, string expiry, string productName, string lot, string lineName, string kitcheckHeader, UserLogin user, CancellationToken ct)
    //    //{
    //    //    await _syncLogLock.WaitAsync(ct).ConfigureAwait(false);

    //    //    try
    //    //    {
    //    //        BatchOrderReport report = await DataSync.GetBatchOrderReportDataAsync(getRlmLocalContext, orderName, gtin,expiry,productName, lot, lineName, kitcheckHeader, user, CancellationToken.None).ConfigureAwait(false);
    //    //        await InsertBatchOrderReportDataAsync(getRlmGlobalContext, report, CancellationToken.None).ConfigureAwait(false);
    //    //    }
    //    //    finally
    //    //    {
    //    //        _syncLogLock.Release();
    //    //    }
    //    //}

    //    //private static async Task<BatchOrderReport> GetBatchOrderReportDataAsync(Func<RLMLocalContext> getRlmLocalContext, string orderName, string gtin, string expiry, string productName, string lot, string lineName, string kitcheckHeader, UserLogin user, CancellationToken ct)
    //    //{
    //    //    BatchOrderReport report = null;

    //    //    //UserLogin user = new UserLogin();

    //    //    //user.CopyTo(user);

    //    //    int 
    //    //        indexerTotCnt = 0,
    //    //        indexerSucCnt = 0,
    //    //        indexerFailCnt = 0,
    //    //        writerTotCnt = 0,
    //    //        writerSucCnt = 0,
    //    //        writerFailCnt = 0,
    //    //        validationTotCnt = 0,
    //    //        validationSucCnt = 0,
    //    //        validationFailCnt = 0;

    //    //    DateTime 
    //    //        prodStartTime = Convert.ToDateTime("1901-01-01"), 
    //    //        prodEndtime = Convert.ToDateTime("1901-01-01");
                        

    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        try
    //    //        {
    //    //            //INDEXER DATA
    //    //            writerTotCnt = await ctx.WriteTagData.CountAsync().ConfigureAwait(false);
    //    //            indexerSucCnt = indexerTotCnt;
    //    //            indexerFailCnt = 0;

    //    //            //WRITER DATA
    //    //            writerTotCnt = await ctx.WriteTagData.CountAsync().ConfigureAwait(false);
    //    //            writerSucCnt = await ctx.WriteTagData.CountAsync(i => i.WriteResult == 1 && i.LockResult == 1).ConfigureAwait(false);
    //    //            writerFailCnt = await ctx.WriteTagData.CountAsync(i => i.WriteResult != 1 || i.LockResult != 1).ConfigureAwait(false);

    //    //            //VALIDATION DATA
    //    //            validationTotCnt = await ctx.ValidationTriggerData.Select(p => p.HostTime).Distinct().CountAsync().ConfigureAwait(false);
    //    //            validationSucCnt = await ctx.ValidationTriggerData.Where(v => v.ResultFlags == (int)ValidationFlags.NoError).Select(p=>p.HostTime).Distinct().CountAsync().ConfigureAwait(false);
    //    //            validationFailCnt = await ctx.ValidationTriggerData.Where(v => v.ResultFlags != (int)ValidationFlags.NoError).Select(p => p.HostTime).Distinct().CountAsync().ConfigureAwait(false);

    //    //            //PRODUCTION TIMES
    //    //            DateTime minWrite = await ctx.WriteTagData.MinAsync(i => i.Indexed).ConfigureAwait(false);
    //    //            DateTime minValidation = await ctx.ValidationTriggerData.MinAsync(i => i.HostTime).ConfigureAwait(false);
    //    //            prodStartTime = minWrite < minValidation ? minWrite : minValidation;
    //    //            DateTime maxWrite = await ctx.WriteTagData.MaxAsync(i => i.Indexed).ConfigureAwait(false);
    //    //            DateTime maxValidation = await ctx.ValidationTriggerData.MaxAsync(i => i.HostTime).ConfigureAwait(false);
    //    //            prodEndtime = maxWrite < maxValidation ? maxWrite : maxValidation;

    //    //        }
    //    //        catch (Exception ex)
    //    //        {

    //    //        }

    //    //        report = new BatchOrderReport
    //    //        {
    //    //            LotName = lot,
    //    //            LineName = lineName,
    //    //            OrderName = orderName,
    //    //            UserName = user?.Username,
    //    //            Gtin = gtin,
    //    //            KitcheckHeader = kitcheckHeader,
    //    //            Sku = productName,
    //    //            Expiry = expiry,
    //    //            StartTimestamp = prodStartTime,
    //    //            EndTimestamp = prodEndtime,
    //    //            IndexerSuccessCount = indexerSucCnt,
    //    //            IndexerFailedCount = indexerFailCnt,
    //    //            IndexerTotalCount = indexerTotCnt,
    //    //            WriterSuccessCount = writerSucCnt,
    //    //            WriterFailedCount = writerFailCnt,
    //    //            WriterTotalCount = writerTotCnt,
    //    //            ValidatorSuccessCount = validationSucCnt,
    //    //            ValidatorFailedCount = validationFailCnt,
    //    //            ValidatorTotalCount = validationTotCnt

    //    //        };
    //    //    }

    //    //    return report;
    //    //}

    //    //private static async Task InsertBatchOrderReportDataAsync(Func<RLMGlobalContext> getRlmGlobalContext, BatchOrderReport report, CancellationToken ct)
    //    //{
    //    //    try
    //    //    {
    //    //        using (var ctx = getRlmGlobalContext())
    //    //        {
    //    //            ctx.BatchOrderReport.Add(report);
    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //    }
    //    //    catch (Exception ex)
    //    //    {

    //    //    }
    //    //}


    //    //public static async Task ClearLocalSettingsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    //bool cleared = false;

    //    //    //DatabaseStructure dbs = new DatabaseStructure();
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        try
    //    //        {
    //    //            //LINES
    //    //            var delLines = ctx.Lines.ToList();
    //    //            if (delLines.Count > 0)
    //    //            {
    //    //                foreach (Lines l in delLines)
    //    //                    ctx.Lines.Remove(l);

    //    //                await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }

    //    //            //SKUS
    //    //            var delSkus = ctx.Skus.ToList();
    //    //            if (delSkus.Count > 0)
    //    //            {
    //    //                foreach (Skus s in delSkus)
    //    //                    ctx.Skus.Remove(s);

    //    //                await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }

    //    //            //DEVICES
    //    //            var delDevices = ctx.Devices.ToList();
    //    //            if (delDevices.Count > 0)
    //    //            {
    //    //                foreach (Devices l in delDevices)
    //    //                    ctx.Devices.Remove(l);

    //    //                await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }

    //    //            //SETTINGS
    //    //            var delSettings = ctx.Settings.ToList();
    //    //            if (delSettings.Count > 0)
    //    //            {
    //    //                foreach (Settings s in delSettings)
    //    //                    ctx.Settings.Remove(s);

    //    //                await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }

    //    //            //cleared = true;
    //    //        }
    //    //        catch (Exception ex)
    //    //        {
    //    //            //cleared = false;
    //    //        }
    //    //    }

    //    //    //return cleared;
    //    //}

    //    //public static async Task ClearStatisticsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    //bool cleared = false;

    //    //    //DatabaseStructure dbs = new DatabaseStructure();
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        try
    //    //        {
    //    //            //Write Data
    //    //            var delWriteTags = ctx.WriteTagData.ToList();
    //    //            if (delWriteTags.Count > 0)
    //    //            {
    //    //                foreach (WriteTagData w in delWriteTags)
    //    //                    ctx.WriteTagData.Remove(w);

    //    //                await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }

    //    //            //Validation Tag Data
    //    //            var delValidTags = ctx.ValidationTagData.ToList();
    //    //            if (delValidTags.Count > 0)
    //    //            {
    //    //                foreach (ValidationTagData v in delValidTags)
    //    //                    ctx.ValidationTagData.Remove(v);

    //    //                await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }

    //    //            //Validation Trigger data
    //    //            var delValidTriggers = ctx.ValidationTriggerData.ToList();
    //    //            if (delValidTriggers.Count > 0)
    //    //            {
    //    //                foreach (ValidationTriggerData d in delValidTriggers)
    //    //                    ctx.ValidationTriggerData.Remove(d);

    //    //                await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //            }

    //    //            //cleared = true;
    //    //        }
    //    //        catch (Exception ex)
    //    //        {
    //    //            //cleared = false;
    //    //        }
    //    //    }

    //    //    //return cleared;
    //    //}

    //    //public static async Task ClearSyncedLogsAsync(Func<RLMLocalContext> getRlmLocalContext, CancellationToken ct)
    //    //{
    //    //    using (var ctx = getRlmLocalContext())
    //    //    {
    //    //        try
    //    //        {
    //    //            var del = ctx.Logs.Where(d => d.SyncStatus == (int)SyncStatus.Synced).ToList();
    //    //            if (del.Count > 0)
    //    //            {
    //    //                foreach (Logs l in del)
    //    //                    ctx.Logs.Remove(l);
    //    //            }

    //    //            await ctx.SaveChangesAsync(ct).ConfigureAwait(false);
    //    //        }
    //    //        catch (Exception ex)
    //    //        {

    //    //        }
    //    //    }

    //    //    lstLogs.Clear();
    //    //}
   // }
}
