﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EA.Common.Helpers.Models
{
    public class ProductionTagItem
    {
        public string Item { get; set; }
        public string MetaSKU { get; set; }
        public string ThingID { get; set; }
        public string EPC { get; set; }
        public string TID { get; set; }
    }
}
