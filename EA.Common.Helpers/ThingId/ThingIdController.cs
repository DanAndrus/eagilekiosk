﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Linq;
using EA.Common.Helpers.Database;
using System.Data;
using static EA.Common.Helpers.Database.DBSqlConnection;
using static Amazon.Kinesis.ClientLibrary.SampleProducer.BucketProducer;
using MySql.Data.MySqlClient;
using System.IO;
using Microsoft.Data.SqlClient;
using Amazon.Kinesis.ClientLibrary.SampleProducer;
using Microsoft.VisualBasic;
using EA.Common.Helpers.Logging;
using static EA.Common.Helpers.Logging.DailyLogger;

namespace EA.Common.Helpers.ThingId
{
    public class ThingIdController
    {

        //FROM BEN'S THING GENERATOR
        // https://www.wolframalpha.com/input/?i=Previous+Prime+24986644000165537791
        // https://preshing.com/20121224/how-to-generate-a-sequence-of-unique-random-integers/
        // https://gist.github.com/CodesInChaos/3175971

        // prime must be less than max value
        // values greater than prime will not be generated
        // (prime + 1) must be divisible by 4
        // wolfram alpha: (NextPrime[4882812500000000000, -1] + 1) / 4
        // change the -1 to -2, then -3 until the result is an integer
        // then NextPrime[4882812500000000000, -1] to get the value, this max value use NextPrime[4882812500000000000, -2]

        //private readonly BigInteger PRIME = BigInteger.Parse("24986644000165537759"); // full base58
        private readonly BigInteger PRIME = BigInteger.Parse("4882812499999999987"); // no vowels (base50)   50^11 = 4,882,812,500,000,000,000
        private readonly BigInteger SEED = BigInteger.Parse("875368751");
        private readonly BigInteger SEED_OFFSET = BigInteger.Parse("2216574413");
        private readonly BigInteger SALT = BigInteger.Parse("865435736");
        private readonly BigInteger SALT_OFFSET = BigInteger.Parse("12132124684654");

        public const string BASE_58_CHARS_NO_VOWELS = "123456789BCDFGHJKLMNPQRSTVWXYZbcdfghjkmnpqrstvwxyz";

        internal int GeneratorQty = 0;
        internal BigInteger FirstSerial = 0;
        internal BigInteger LastSerial = 0;
        internal BigInteger MaxSerial = 0;
        internal List<string> ThingIDs = null;

        public DBSqlConnection DB_Conn { get; set; }
        
        public ThingIdController(DBType DBconnect)
        {
            DB_Conn =  new DBSqlConnection(DBconnect);
        }

        public static string Encode(BigInteger intData)
        {
            // Encode BigInteger to Base58 string
            string result = "";
            while (intData > 0)
            {
                int remainder = (int)(intData % 50);
                intData /= 50;
                //result = BASE_58_CHARS[remainder] + result;
                result = BASE_58_CHARS_NO_VOWELS[remainder] + result;
            }

            return result;
        }

        public List<string> GenerateNewThingID(int safteyStockBaseLine, DailyLogger D_Logger)
        {
            Create.intJobSize = 0;

           // int tagCount = CheckCount(safteyStockBaseLine);

            if (safteyStockBaseLine > 0)
            {
                if (FindNextAndMax())
                {
                    if (BuildTagList(safteyStockBaseLine))
                    {
                        return SaveValuesStagingSQL(D_Logger);
 
                    }
                }
            }
            return new List<string>();
        }
        public void AddThingIdToStagingDatabase(List<AwsBucketModel> BucketList, DailyLogger D_Logger,string failedpath)
        {
            try
            {

                using (var c = DB_Conn.DB_Connection)
                {
                    StringBuilder sCommand = new StringBuilder("INSERT INTO `AmwayThingIdStaging` (`SerialId`, `ThingId`) VALUES ");
                    c.Open();
                    using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), c))
                    {
                        myCmd.CommandText = $"DELETE FROM AmwayThingIdStaging;";
                        int numRowsDeleted = myCmd.ExecuteNonQuery();

                        foreach (var additem in BucketList)
                        {
                            try
                            {
                                var testsql = $"INSERT INTO AmwayThingIdStaging (landing_url, short_url,thingId) VALUES ('{additem.scanBuyLandingUrl}','{additem.scanBuyShortUrl}','{additem.thingId}');";
                                myCmd.CommandText = testsql;
                                int numRowsUpdated = myCmd.ExecuteNonQuery();
                            }
                            catch(Exception ex)
                            {

                                StreamWriter sw = new StreamWriter($@"{failedpath}\\{additem.thingId}.txt");
                                var responseBody = $"ThingId: {additem.thingId}, LandingURL:{additem.scanBuyLandingUrl},ShortUrl:{ additem.scanBuyShortUrl}";
                                sw.WriteLine(responseBody);
                                sw.Close();
                                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Adding item to Staging.", Timestamp = DateTime.Now });
                                continue;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Adding to staging connection.", Timestamp = DateTime.Now });
            }
        }
        public bool AddThingIdToDatabase(List<AwsBucketModel> BucketList, DailyLogger D_Logger)
        {
            try
            {
                using (var c = DB_Conn.DB_Connection)
                {
                    StringBuilder sCommand = new StringBuilder("INSERT INTO `AmwayThingId` (`SerialId`, `ThingId`) VALUES ");
                    c.Open();
                    using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), c))
                    {
                        var HasFailed = false;
                        var timestamp = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now); ;
                        
                        //'2020/6/30 7:47:11'
                        foreach (var additem in BucketList)
                        {
                            try
                            {
                       //         var timestamp = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now); ;


                                myCmd.CommandText = $"UPDATE AmwayThingId SET LandingURL = '{additem.scanBuyLandingUrl}' , ShortURL = '{additem.scanBuyShortUrl}',UpdatedDate = '{timestamp.ToString()}'  WHERE ThingId = '{additem.thingId}' AND ShortURL IS NULL";
                                int numRowsUpdated = myCmd.ExecuteNonQuery();
                                if(numRowsUpdated == 0)
                                {
                                    StreamWriter sw = new StreamWriter($@"C:\\Dan\\testAWS\\Failed\\{additem.thingId}.txt");
                                    var responseBody = $"ThingId: {additem.thingId}, LandingURL:{additem.scanBuyLandingUrl},ShortUrl:{ additem.scanBuyShortUrl}";
                                    sw.WriteLine(responseBody);
                                    sw.Close();
                                    HasFailed = true;
                                    continue;
                                }
                                else
                                {
                                    HasFailed = false;
                                }
                            }
                            catch(Exception ex)
                            {
                                HasFailed = true;
                                StreamWriter sw = new StreamWriter($@"C:\\Dan\\testAWS\\Failed\\{additem.thingId}.txt");
                                var responseBody = $"ThingId: {additem.thingId}, LandingURL:{additem.scanBuyLandingUrl},ShortUrl:{ additem.scanBuyShortUrl}";
                                sw.WriteLine(responseBody);
                                sw.Close();
                                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Update item to ThingId table.", Timestamp = DateTime.Now });
                                continue;
                            }
                        }
                        if (HasFailed !=true )
                        {
                            myCmd.CommandText = $"DELETE FROM AmwayThingIdStaging;";
                            int numRowsDeleted = myCmd.ExecuteNonQuery();
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Connection ThingId table.", Timestamp = DateTime.Now });
            }
            return false;
        }
        public bool SaveValuesSQL(DailyLogger D_Logger)
        {
         
            var returnList = new List<string>();
            try
            {
                using (var c = DB_Conn.DB_Connection)
                {
                    c.Open();
                    StringBuilder sCommand = new StringBuilder("INSERT INTO `AmwayThingId` (`SerialId`, `ThingId`,`CreatedDate`) VALUES ");
                    List<string> Rows = new List<string>();

                    int procRowCount = 0;
                    int thingsRowCount = Create.objTagsList.Count();
                    int maxRowsInBatch = 20000;
                    var timestamp = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now); 
                    foreach (var thing in Create.objTagsList)
                    {
                        returnList.Add(thing.EPC);
                        Rows.Add(string.Format("('{0}','{1}','{2}')", MySqlHelper.EscapeString(thing.Serial), MySqlHelper.EscapeString(thing.EPC), timestamp));
                        procRowCount++;

                        if (procRowCount == maxRowsInBatch || procRowCount == thingsRowCount)
                        {
                            sCommand.Append(string.Join(",", Rows));
                            sCommand.Append(";");

                            using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), c))
                            {
                                myCmd.CommandType = CommandType.Text;
                                myCmd.ExecuteNonQuery();

                            }
                            Rows.Clear();
                            sCommand.Clear();
                            sCommand = new StringBuilder("INSERT INTO `AmwayThingId` (`SerialId`, `ThingId`,`CreatedDate`) VALUES ");
                            thingsRowCount = thingsRowCount - procRowCount;
                            procRowCount = 0;
                        }
                    }

                    Create.ResetCreate();
                }
                return true;
            }
            catch (MySqlException ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "MySQL Error.", Timestamp = DateTime.Now });
                return false;
            }
            catch (Exception ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error connecting to ThingId table.", Timestamp = DateTime.Now });
                return false;
            }
        }
        public int  CheckCount(int safteyStockBaseLine, DailyLogger D_Logger)
        {
            var currentcount = 0;

            try
            {
                using (var conn = DB_Conn.DB_Connection)
                {
                    conn.Open();
                    string sql = string.Empty;
                    if (DB_Conn.CurrentConnectionType == DBType.Production)
                    {
                        sql = "SELECT Count(*) FROM econtrol.AmwayThingId AS eREF WHERE eREF.ItemID IS NULL;";
                    }
                    else
                    {
                        sql = "SELECT Count(*) FROM econtrol_test.AmwayThingId AS eREF WHERE eREF.ItemID IS NULL;";
                    }
                    
                    
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        currentcount = Convert.ToInt32(rdr.GetString(0));

                        if (currentcount >= safteyStockBaseLine)
                        {
                            rdr.Close();
                            return 0;
                        }
                        else
                        {
                            var tmpcount = safteyStockBaseLine- currentcount ;
                            currentcount = tmpcount;
                        }

                    }
                    rdr.Close();
                }
            }
            catch (Exception ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Check count error.", Timestamp = DateTime.Now });
                return 0;
            }
            return currentcount;
        }
        public bool UpdateProductionFile(string updateitem, DailyLogger D_Logger)
        {

            try
            {
                SqlConnection con = new SqlConnection("Data Source=192.168.23.25;Initial Catalog=qc_log;Persist Security Info=True;Integrated Security=true;");
                con.Open();
                var sql = $"UPDATE [qc_log].[dbo].[thingID_QC] SET sync = '1' WHERE ITEM = '{updateitem}'";
                SqlCommand cmd = new SqlCommand(sql, con);

                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error updating Production QC table Sync method.", Timestamp = DateTime.Now });
                return false;
            }
            return true;
        }
        public List<ProductionTagItem> CreateProductionFile(DailyLogger D_Logger)
        {
            List<ProductionTagItem> BucketList = new List<ProductionTagItem>();
            try
            {
                SqlConnection con = new SqlConnection("Data Source=192.168.23.25;Initial Catalog=qc_log;Persist Security Info=True;Integrated Security=true;");
                con.Open();

                var sql = "SELECT item,epc,thingid,tid FROM[qc_log].[dbo].[thingID_QC] where sync = 0 and passed = 1";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    var additem = new ProductionTagItem() { Item = rdr.GetString(0).Trim(), MetaSKU = rdr.GetString(0).Trim(), EPC = rdr.GetString(1), ThingID = rdr.GetString(2), TID = rdr.GetString(3) };

                    BucketList.Add(additem);
                }
                rdr.Close();
            }
            catch (Exception ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error getting QC table.", Timestamp = DateTime.Now });
            }

           var testing = BucketList.Select(d => d.MetaSKU).Distinct();
            StringBuilder sb = new StringBuilder();
            var last = testing.Last();
            foreach (var tt in testing)
            {
                if (tt.Equals(last))
                {
                    // do something different with the last item
                    sb.Append($"'{tt}'");
                }
                else
                {
                    // do something different with every item but the last
                    sb.Append($"'{tt}',");
                }
            }

            string connStr = "server=192.168.23.100;user=dandrus;database=econtrol;port=3306;password=pfmq@eA1";

            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                string sql = $"SELECT id, part FROM econtrol.items  WHERE id in({sb});";
                
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                var newdict = new Dictionary<string, string>();
                while (rdr.Read())
                {
                    newdict.Add(rdr.GetString(0), rdr.GetString(1));
                }
                rdr.Close();

                foreach (var newitem in BucketList)
                {
                    string myvalue;
                    if (newdict.TryGetValue(newitem.MetaSKU.Trim(), out myvalue))
                    {
                        var splitdata = myvalue.Split("_");
                        newitem.MetaSKU = splitdata[1];
                    }
                }
            }
            catch (Exception ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error using Item table.", Timestamp = DateTime.Now });
            }
            return BucketList;
        }

        private List<string> SaveValuesStagingSQL(DailyLogger D_Logger)
        {
            var returnList = new List<string>();
            try
            {
                using (var c = DB_Conn.DB_Connection)
                {
                    c.Open();

                    //StringBuilder sCommand = new StringBuilder("INSERT INTO `AmwayThingIdStaging` (`SerialId`, `ThingId`) VALUES ");
                    StringBuilder sCommand = new StringBuilder("INSERT INTO `AmwayThingIdStaging` (`ThingId`) VALUES ");
                    List<string> Rows = new List<string>();
                    List<string> DeleteRows = new List<string>();

                    int procRowCount = 0;
                    int thingsRowCount = Create.objTagsList.Count();
                    int maxRowsInBatch = 20000;


                    foreach (var thing in Create.objTagsList)
                    {
                        returnList.Add(thing.EPC);
                        Rows.Add(string.Format("('{0}')", MySqlHelper.EscapeString(thing.EPC)));
                        DeleteRows.Add(string.Format("'{0}'", MySqlHelper.EscapeString(thing.EPC)));

                        procRowCount++;

                        if (procRowCount == maxRowsInBatch || procRowCount == thingsRowCount)
                        {
                            sCommand.Append(string.Join(",", Rows));
                            sCommand.Append(";");

                            using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), c))
                            {
                                myCmd.CommandType = CommandType.Text;
                                myCmd.ExecuteNonQuery();

                            }

                            //DELETE FROM  `AmwayThingIdStaging` WHERE ThingId IN('1136zz1KDmN', '113771mqMZk')
                            StringBuilder sCommand2 = new StringBuilder("DELETE FROM `AmwayThingIdStaging` WHERE ThingId IN (" );
                            sCommand2.Append(string.Join(",", DeleteRows));
                            sCommand2.Append(");");
                            using (MySqlCommand myCmd = new MySqlCommand(sCommand2.ToString(), c))
                            {
                                myCmd.CommandType = CommandType.Text;
                                myCmd.ExecuteNonQuery();
                            }
                            Rows.Clear();
                            sCommand.Clear();
                            sCommand2.Clear();

                            sCommand = new StringBuilder("INSERT INTO `AmwayThingIdStaging` (`ThingId`) VALUES ");
                            thingsRowCount = thingsRowCount - procRowCount;
                            procRowCount = 0;
                        }
                    }
                }
                return returnList;
            }
            catch (MySqlException ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error updating Production QC table(msql).", Timestamp = DateTime.Now });
                return returnList;
            }
            catch (Exception ex)
            {
                D_Logger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Gneral Error updating Production QC table.", Timestamp = DateTime.Now });
                return returnList;
            }

        }
        private bool FindNextAndMax()
        {
            try
            {
              //  var dbconn = new DBSqlConnection();
                using (var conn = DB_Conn.DB_Connection)
                {
                    conn.Open();
                    string sql = string.Empty;
                    if (DB_Conn.CurrentConnectionType == DBType.Production)
                    {
                        //sql = "SELECT eREF.next_serial, eREF.max_serial FROM econtrol.`references` AS eREF  WHERE id = 'AmwayThingID';";
                        sql = "Select max(AmwayThingId.SerialId),test.max_serial from AmwayThingId JOIN `references` as test WHERE id = 'AmwayThingId'";
                    }
                    else
                    {
                        //sql = "SELECT eREF.next_serial, eREF.max_serial FROM econtrol_test.`references` AS eREF  WHERE id = 'AmwayThingID';";
                        //sql = "select max(AmwayThingId.SerialId),test.max_serial from AmwayThingId  JOIN `references` as test WHERE id = 'AmwayThingId'";

                    }
                    
                    //string sql = "SELECT eREF.next_serial, eREF.max_serial FROM econtrol.`references` AS eREF  WHERE id = 'AmwayThingID';";
                    MySqlCommand cmd = new MySqlCommand(sql, conn);

                    MySqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        var tempnum = Convert.ToInt32(rdr.GetString(0)) + 1;
                        Create.strNextSerial = tempnum.ToString();
                        Create.strMaxSerial = rdr.GetString(1);
                    }
                    rdr.Close();
                }
            }
            catch (MySqlException ex)
            {
                return false;
            }

            return true;
        }
        private bool BuildTagList(int ThingIdQty)
        {
            try
            {
                Create.strCreateStage = "SQL_THINGID";
                Create.intJobSize = ThingIdQty;
                GeneratorQty = Create.intJobSize;


                if (!BigInteger.TryParse(Create.strNextSerial, out FirstSerial))
                {
                    return false;
                    //throw new ArgumentException("Start Index is invalid");
                }
                    

                if (!BigInteger.TryParse(Create.strMaxSerial, out MaxSerial))
                {
                    return false;
                    //throw new ArgumentException("Max Index in database is invalid");
                }
                    

                LastSerial = FirstSerial + (GeneratorQty - 1);

                if (LastSerial > MaxSerial)
                {
                    return false;
                    //throw new ArgumentException("Last Index is invalid");
                }
                    

                ThingIDGenerator generator = new ThingIDGenerator(PRIME, SEED, SEED_OFFSET, SALT, SALT_OFFSET);

                for (BigInteger i = FirstSerial; i <= LastSerial; i++)
                {
                    BigInteger nextVal = generator.GetIndexValue(i);
                    string thingId = Encode(nextVal).PadLeft(11, '1');

                    Create.objTags tag = new Create.objTags()
                    {
                        EPC = thingId,
                        Serial = i.ToString()
                    };

                    Create.objTagsList.Add(tag);
                }

                Create.strNextSerial = (LastSerial + 1).ToString();
            }
            catch 
            {
                return false;
            }

            return true;
        }


    }
}
