﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace EA.Common.Helpers.ThingId
{
    internal static class Create
    {
        private const string CLASS_NAME = "Create";

        public static string strNextSerial, strMaxSerial, strPrevMla, strCreateStage, strCreatePart, strListStyle, strDatabaseLoc, strDatabaseLoc2, strDatabaseLoc3, strInspecLoc;
        public static string strJobName, strReference, strItemLabel, strMasterItem, strOriginalMaster, strCustId, strMachine, strReqNumber, strNewJobName, strFile;
        public static string strStartSerial, strEndSerial, strPreviousItem, strProcess, strParentPart, strCustomerPart;
        public static int intNewJob, intJobSize, intItemSize, intItems, intSalesOrder, intLabelCount, intDbSize, intDb2Size, intDb3Size, intTagRepeatQty;
        public static double dblJobMultiplier = 1.00;
        public static bool blnMasterItem, blnExistingJob, blnVariableItemQty, blnShareMO;
        public static string strRefEncoding;
        public static string strItemReference;
        public static string strTestEnv;
        //intItems = JobSize / intItemsSize
        //intItemsSize = Loaded item size
        public static List<string> EPCList = new List<string>();
        public static List<string> SerialList = new List<string>();
        public static List<string> Param1List = new List<string>();
        public static List<string> Param2List = new List<string>();
        public static List<string> Param3List = new List<string>();
        public static List<string> Param4List = new List<string>();
        public static List<string> Param5List = new List<string>();
        public static List<string> Param6List = new List<string>();
        public static List<string> Param7List = new List<string>();
        public static List<string> DPSLTO = new List<string>();
        public static List<string> DPSNLTO = new List<string>();
        public static List<string> DPSMAINFRAME = new List<string>();
        public static List<string> DPSCASE = new List<string>();
        public static List<string[]> RollList = new List<string[]>();
        public static List<string> RemainderSerialList = new List<string>();
        public static List<int> RollCounts = new List<int>();

        public static List<objItem> objItemList = new List<objItem>();
        public static List<objItem> objItemExstJobList = new List<objItem>();
        public static List<objPart> objPartList = new List<objPart>();
        public static List<objJob> objExstJobList = new List<objJob>();
        public static List<objTags> objTagsList = new List<objTags>();
        public static List<objJob> objJobList = new List<objJob>();


        public static void ResetCreate()
        {
            strNextSerial = "";
            strCreateStage = "";
            strCreatePart = "";
            strListStyle = "";
            strDatabaseLoc = "";
            strDatabaseLoc2 = "";
            strDatabaseLoc3 = "";
            strInspecLoc = "";
            strJobName = "";
            strReference = "";
            strItemLabel = "";
            strStartSerial = "";
            strEndSerial = "";
            strMasterItem = "";
            strOriginalMaster = "";
            strRefEncoding = "";
            strNewJobName = "";
            strPreviousItem = "";
            strParentPart = "";
            strCustomerPart = "";
            //strProcess = "";
            blnMasterItem = false;
            blnVariableItemQty = false;
            blnShareMO = false;
            intNewJob = 0;
            intJobSize = 0;
            intItemSize = 0;
            intItems = 0;
            intSalesOrder = 0;
            intLabelCount = 0;
            intDbSize = 0;
            intDb2Size = 0;
            intDb3Size = 0;
            intTagRepeatQty = 0;
            EPCList.Clear();
            SerialList.Clear();
            Param1List.Clear();
            Param2List.Clear();
            Param3List.Clear();
            Param4List.Clear();
            Param5List.Clear();
            Param6List.Clear();
            Param7List.Clear();
            DPSLTO.Clear();
            DPSNLTO.Clear();
            DPSMAINFRAME.Clear();
            DPSCASE.Clear();
            RollList.Clear();
            RemainderSerialList.Clear();
            RollCounts.Clear();
            objItemList.Clear();
            objItemExstJobList.Clear();
            objExstJobList.Clear();
            objTagsList.Clear();
            objPartList.Clear();
            objJobList.Clear();
            dblJobMultiplier = 1.00;
        }

        public class objItem
        {
            public string itemID { get; set; }
            public string itemPart { get; set; }
            public string itemJob { get; set; }
            public string itemSerialStart { get; set; }
            public string itemSerialEnd { get; set; }
            public int itemQty { get; set; }
            public int itemCreateUser { get; set; }
            public DateTime itemCreatedDate { get; set; }
        }

        public class objPart
        {
            public string partID { get; set; }
            public string partLocked { get; set; }
            public string partClass { get; set; }
            public int partLastJob { get; set; }
            public int partNewJob { get; set; }
            public int partItemSize { get; set; }
            public string partNextSerial { get; set; }
            public string partReference { get; set; }
            public string partCustomerPart { get; set; }
            public bool partMasterItem { get; set; }
            public string partDb1 { get; set; }
            public int partDb1Size { get; set; }
            public string partDb2 { get; set; }
            public int partDb2Size { get; set; }
            public string partDb3 { get; set; }
            public int partDb3Size { get; set; }
            public string partItemLabel { get; set; }
            public int partLabelCount { get; set; }
            public string partTrackingColumn { get; set; }
            public int partTagRepeatQty { get; set; }

        }

        public class objJob
        {
            public string jobID { get; set; }
            public string jobPart { get; set; }
            public string jobSerialStart { get; set; }
            public string jobSerialEnd { get; set; }
            public int jobQuantity { get; set; }
            public int jobCreateUser { get; set; }
            public DateTime jobCreated { get; set; }
            public int jobQCStatus { get; set; }
        }

        public class objTags
        {
            public string EPC { get; set; }
            public string Serial { get; set; }
            public string LongSerial { get; set; }
            public string NonTrimedSerial { get; set; }
            public string Meta1 { get; set; }
            public string Meta2 { get; set; }
            public string Meta3 { get; set; }
            public string Meta4 { get; set; }
            public string Meta5 { get; set; }
        }
    }
}
