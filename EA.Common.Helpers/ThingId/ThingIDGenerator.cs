﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace EA.Common.Helpers.ThingId
{
    class ThingIDGenerator
    {
        // prime must be less than max value
        // values greater than prime will not be generated
        // (prime + 1) must be divisible by 4
        // wolfram alpha: (NextPrime[24986644000165537791, -1] + 1) / 4
        // change the -1 to -2, then -3 until the result is an integer
        // then NextPrime[24986644000165537791, -1] to get the value, this this max value use NextPrime[24986644000165537791, -2]

        public BigInteger PRIME { get; }
        public BigInteger HALFPRIME { get; }
        public BigInteger SEED { get; }
        public BigInteger SEED_OFFSET { get; }
        public BigInteger SALT { get; }
        public BigInteger SALT_OFFSET { get; }

        private BigInteger _indexOffset = 0;
        private BigInteger _intermediateOffset = 0;

        public ThingIDGenerator(BigInteger prime, BigInteger seed, BigInteger seedOffset, BigInteger salt, BigInteger saltOffset)
        {
            PRIME = prime;
            HALFPRIME = prime / 2;
            SEED = seed;
            SEED_OFFSET = seedOffset;
            SALT = salt;
            SALT_OFFSET = saltOffset;

            _indexOffset = PermuteQPR(PermuteQPR(SEED) + SEED_OFFSET) % (PRIME + 1);
            _intermediateOffset = PermuteQPR(PermuteQPR(SALT) + SALT_OFFSET) % (PRIME + 1);
        }


        public BigInteger GetIndexValue(BigInteger index)
        {
            BigInteger offset = _indexOffset + index;
            offset = PermuteQPR(offset);
            offset = offset + _intermediateOffset;
            return PermuteQPR(offset);
        }

        private BigInteger PermuteQPR(BigInteger index)
        {
            index = index % (PRIME + 1);

            BigInteger residue = (index * index) % PRIME;
            if (index <= HALFPRIME)
            {
                return residue;
            }
            else
            {
                return PRIME - residue;
            }
        }
    }
}
