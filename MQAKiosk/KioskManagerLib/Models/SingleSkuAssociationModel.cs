﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Models
{
    public class SingleSkuAssociationModel
    {
        public string ThingID { get; set; }
        public string EPC { get; set; }
        public string TID { get; set; }
        
    }
}
