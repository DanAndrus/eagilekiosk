﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Configuration
{
    public class AWSConnectionModel
    {
        public string NewConnection { get; set; }
        public string ConnectionAmway { get; set; }
        public string UserAmway { get; set; }
        public string connectionEA { get; set; }
        public string UserEA { get; set; }
        
        public string WriteStreamName { get; set; }
        public string WriteAccessKey { get; set; }
        public string WriteSecretKey { get; set; }
        public string WriteRegion { get; set; }
        public string WriteFormat { get; set; }
        public string WriteRoleARN { get; set; }

        public string SupportStreamName { get; set; }
        public string SupportAccessKey { get; set; }
        public string SupportSecretKey { get; set; }
        public string SupportRegion { get; set; }
        public string SupportFormat { get; set; }
        public string SupportRoleARN { get; set; }



        public string ReadStreamName { get; set; }
        public string ReadAccessKey { get; set; }
        public string ReadSecretKey { get; set; }
        public string ReadRegion { get; set; }
        public string ReadBucketName { get; set; }
        

    }
}
