﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Interfaces
{
    interface IKioskLogging
    {
        void CreateErrorLog();
        void WriteToErrorLog();
        void WriteTotalSkuAssociationLog();
        void WriteSingleSkuAssociationLog();

    }
}

