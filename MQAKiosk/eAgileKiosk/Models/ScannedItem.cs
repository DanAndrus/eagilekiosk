﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eAgileKiosk.Models
{
    public class KioskScannedItem
    {
        public string TID { get; set; }
        public string GTIN { get; set; }
        public string SerialNumber { get; set; }
        public string BatchLot { get; set; }
        public string Expry { get; set; }
        public string LockState { get; set; }

    }

}
