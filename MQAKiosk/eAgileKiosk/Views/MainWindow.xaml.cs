﻿using eAgileKiosk.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Documents.Spreadsheet.Model;

namespace eAgileKiosk.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window,IClosable
    {
        private ObservableCollection<RadPane> panes;

        public MainWindow()
        {
            StyleManager.ApplicationTheme = new Office2016Theme();
            InitializeComponent();
            radDocking.ActivePaneChanged += RadDocking_ActivePaneChanged;
            radDocking.Loaded += RadDocking_Loaded;

        }

        private void RadDocking_Loaded(object sender, RoutedEventArgs e)
        {
            object aa = e.Source;
        }

        private void RadDocking_ActivePaneChanged(object sender, Telerik.Windows.Controls.Docking.ActivePangeChangedEventArgs e)
        {
            RadPane olpane = e.OldPane;
            
            if (olpane != null)
            {

              //  var activepane = panes.Select(p => p.Name == 'RadPaneHome');
                //activepane
            }
            
        }
    }
}
