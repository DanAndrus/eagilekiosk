﻿using eAgileKiosk.Messages;
using eAgileKiosk.Models;
using eAgileKiosk.Views.BaseModels;
using eAgileKiosk.WPF;
using GalaSoft.MvvmLight.Messaging;
using KioskManagerLib;
using KioskManagerLib.Models;
using RFIDReader;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
//using Atom.SDK.Net;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls.Docking;
//using eAgileKiosk.Views.Vpn;

namespace eAgileKiosk.Views
{
    public class MainWindowViewModel : ViewModel
    {

        public string ClockTime => _clockDateTime.ToString("t", Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        public string ClockDate => _clockDateTime.ToString("d MMM yyyy", Thread.CurrentThread.CurrentCulture.DateTimeFormat);

        private DateTime _clockDateTime = DateTime.Now;


        private readonly Task _clockTask;
        private readonly CancellationTokenSource _clockCts;

        private string _messageBarMessage = $"*** Last update time : {DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")} ***";
        public string MessageBarMessage { get => _messageBarMessage; set => SetProperty(ref _messageBarMessage, value); }
        private string _kioskId = $"KioskId:";
        public string KioskId { get => _kioskId; set => SetProperty(ref _kioskId, value); }

        public bool _isActivePane = false;
        public bool IsActivePane { get => _isActivePane; set => SetProperty(ref _isActivePane, value); }
        

        public KioskController Controller { get; }

        public RfIDController ReaderRFID = new RfIDController(RfIDController.Mode.INVENTORY);

    //    private List<NavAction> NavActions { get; }
        
        //public NavAction LogAction { get; }
        //public NavAction ConfirmAction { get; }
        //public NavAction SettingsAction { get; }
        //public NavAction LoginAction { get; }
        //public NavAction HomeAction { get; }
        //public NavAction VPNAction { get; }

        //public HomeViewModel HomeView { get; }
        //public SettingsViewModel SettingsView { get; }
        //public LoginViewModel LoginView { get; }
        //public LoggingViewModel LogView { get; }
        //public VpnSettingsViewModel VPNView { get; }
        //public NavAction CurrentContent
        //{
        //    get => _currentContent;
        //    set
        //    {
        //        SetProperty(ref _currentContent, value);
        //        foreach (NavAction action in NavActions)
        //        {
        //            action.IsChecked = (_currentContent == action);
        //        }
        //    }
        //}
        //private NavAction _currentContent;

      //  AtomManager atomManagerInstance;

        public RelayCommand<IClosable>CommandExit { get; private set; }


        public bool CanAssociate { get => _canAssociate; private set => SetProperty(ref _canAssociate, value); }
        private bool _canAssociate =false;

        public MainWindowViewModel()
        {

            _clockCts = new CancellationTokenSource();
            _clockTask = ClockUpdater(TimeSpan.FromSeconds(1), _clockCts.Token);

            Controller = new KioskController();

            KioskId = $"KioskId: {Controller.SQLController.ActiveLine.Name}";
            //Create Views that will be used in the system

            //HomeView = new HomeViewModel();
            //SettingsView = new SettingsViewModel();
            //LoginView = new LoginViewModel(Controller);
            //LogView = new LoggingViewModel(Controller);
          //  VPNView = new VpnSettingsViewModel();


            //NavActions = new List<NavAction>()
            //{
            //    (HomeAction = new NavAction(HomeView) { GetLabel = ()=>Properties.Strings.Main_Home }),
            //    (LoginAction = new NavAction(LoginView) { GetLabel = ()=>Properties.Strings.Main_Login }),
            //    (SettingsAction = new NavAction(SettingsView) { GetLabel = ()=>Properties.Strings.Main_System_Settings }),
            //    (LogAction = new NavAction(LogView){ GetLabel = ()=>Properties.Strings.Main_Logging }),
            //    (VPNAction = new NavAction(VPNView) { GetLabel = ()=>Properties.Strings.Main_VPN }),

           // };

            //foreach (NavAction action in NavActions)
            //{
            //    action.IsVisible = true;
            //    action.IsEnabled = true;
            //    action.Clicked += (sender, args) => Navigate(sender as NavAction);
            //}

            CommandExit = new RelayCommand<IClosable>(ExecuteCommandExit);


          //  CurrentContent = LoginAction;
            Messenger.Default.Send(new LoginGotFocusMessage() { Loginhasfocus = true });
            RegisterMessages();
            
            ReaderRFID.RFIDEvent += ReaderRFID_RFIDEvent;



        }

        private void Controller_KioskEvent(object sender, string e)
        {

            MessageBarMessage = $"*** Last update time : {e} ***";

        }

        private async Task ClockUpdater(TimeSpan interval, CancellationToken ct)
        {
            try
            {
                while (!ct.IsCancellationRequested)
                {
                    _clockDateTime = DateTime.Now;
                    OnPropertyChanged(nameof(ClockTime), nameof(ClockDate));
                    await Task.Delay(interval, ct).ConfigureAwait(false);
                }
            }
            catch { }
        }

        private void ReaderRFID_RFIDEvent(object sender, List<EPCReport> e_report)
        {
            if (e_report.Count > 0)
            {
                var newthing = e_report[0];
                var founditem = Controller.CurrentAssociatedSkus.ScannedItems.Find(f => f.ThingID == newthing.EPC );

                if (founditem ==null)
                {
                    if (e_report.Count > 1)
                    {
                        Messenger.Default.Send(new ThingIdMessage() { NewRFIDInfo = new EPCReport("",DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture), "","") , Error = ThingError.Multiple});
                    }
                    else
                    {
                        Controller.WriteTotalSkuAssociationLog(newthing.EPC);
                        Controller.WriteSingleSkuAssociationLog(new SingleSkuAssociationModel()
                        {
                        EPC = newthing.RawEPC,
                        ThingID = newthing.EPC, // needs to change
                        TID= newthing.TID 
                        });
                        Messenger.Default.Send(new ThingIdMessage() { NewRFIDInfo = e_report[0], Error = ThingError.None });
                        Messenger.Default.Send(new LogThingIDMessage() { NewRFIDLogInfo = e_report[0] });
                    }
                }
                else
                {
                    Messenger.Default.Send(new ThingIdMessage() { NewRFIDInfo = new EPCReport(founditem.ThingID , DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture), "", ""), Error = ThingError.PreviousAssociated });
                }
            }
        }

        private void ExecuteCommandExit(IClosable window)
        {
            try
            {
                UnRegisterMessages();
                
                Application.Current.MainWindow.Close();
            }
            finally
            {

            }
        }

        //public void Navigate(NavAction navAction)
        //{
        //    if (navAction.ViewModel != null)
        //    {
        //            //CurrentContent = navAction;
        //            navAction?.ViewModel?.RefreshProperties();
        //    }
        //    if (navAction == ConfirmAction)
        //    {
        //        Confirm();
        //    }
        //}
        //public void Confirm()
        //{
        
        //}

        public override void RegisterMessages()
        {
            //Messages are used for passing information between viewmodels. This is a wrapper for events  
            Messenger.Default.Register<AssociationSkuMessage>(this, ExecuteCanAssociate);
            Messenger.Default.Register<LoginMessage>(this, ExecuteLogin);

        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Unregister<AssociationSkuMessage>(this, ExecuteCanAssociate);
            Messenger.Default.Unregister<LoginMessage>(this, ExecuteLogin);
        }



        private void ExecuteLogin(LoginMessage msg)
        {
            //var founduser = Controller.SQLController.TblUsers.FirstOrDefault(u => u.StrName.ToUpper() == msg.Login.ToUpper());
            
            ////TODO:Write to error log/ system log

            //if (founduser != null)
            //{
                CanAssociate = true;
           //var aa = RadDocking.ActivePaneChangedEvent
                IsActivePane = true;
            
            //    //CurrentContent = HomeAction;
            //    MessageBarMessage = $"*** Logged in successfully to system at time : {DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")} ***";
            //}
            //else
            //{
            //    CanAssociate = false;
            //    MessageBarMessage = $"*** Error Logging onto system at time : {DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")} ***";
            //    //CurrentContent = LoginAction;
            //}
        }

        private void ExecuteCanAssociate(AssociationSkuMessage msg)
        {
            
            if (!string.IsNullOrWhiteSpace(msg.SKUNumber))
            {
                CanAssociate = true;
            }
            else
            {
                CanAssociate = false;
            }

        }

    }


    public class NavAction : ObservableObject
    {
        public ViewModel ViewModel { get; }

        public ICommand Command => _command ?? (_command = new RelayCommand(OnClicked, () => IsEnabled));
        private ICommand _command;

        public Func<string> GetLabel = () => string.Empty;

        public string TitleLabel => GetLabel().ToUpper(Thread.CurrentThread.CurrentUICulture);
        public string ButtonLabel => GetLabel();
        public bool IsChecked { get => _isChecked; set => SetProperty(ref _isChecked, value); }
        private bool _isChecked;

        public bool IsVisible { get => _isVisible; set => SetProperty(ref _isVisible, value); }
        private bool _isVisible;

        public bool IsEnabled { get => _isEnabled; set => SetProperty(ref _isEnabled, value); }
        private bool _isEnabled;

        //public NavAction(ViewModel viewModel)
        //{
        //    ViewModel = viewModel;
            
        //}

        public event EventHandler Clicked;
        protected virtual void OnClicked()
        {
            try { Clicked?.Invoke(this, EventArgs.Empty); } catch { }
        }

    }

}
