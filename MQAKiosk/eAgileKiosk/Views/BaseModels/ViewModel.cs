﻿using eAgileKiosk.WPF;
using System;
using System.Collections.Generic;
using System.Text;

namespace eAgileKiosk.Views.BaseModels
{
    public abstract class ViewModel : ObservableObject
    {

        public abstract void RegisterMessages();
        public abstract void UnRegisterMessages();
    }
}
