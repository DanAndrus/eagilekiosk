﻿using eAgileKiosk.Messages;
using eAgileKiosk.Views.BaseModels;
using eAgileKiosk.WPF;
using GalaSoft.MvvmLight.Messaging;
using KioskManagerLib;
using Org.LLRP.LTK.LLRPV1.DataType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace eAgileKiosk.Views
{
    public class LoginViewModel : ViewModel
    {

        public RelayCommand LoginCommand { get; }

        private string _userPassword = string.Empty;
        public string UserPassword { get => _userPassword; set => SetProperty(ref _userPassword, value); }

        private string _userName = string.Empty;
        public string UserName { get => _userName; set => SetProperty(ref _userName, value); }
        private KioskController loginController { get; set; } 

        public bool _hasFocus = true;
        public bool HasFocus { get => _hasFocus; set => SetProperty(ref _hasFocus, value); }

        public LoginViewModel()
        {
            //KioskController Controller
            //loginController = Controller;
            LoginCommand = new RelayCommand(ExecuteLoginCommand, CanLogin);
            RegisterMessages();
        }

        private bool CanLogin()
        {
            return true;
        }
        private void ExecuteLoginCommand()
        {
            try
            {
                Messenger.Default.Send(new LoginMessage() { Login = UserName, Password = UserPassword });

                //var founduser = loginController.SQLController.TblUsers.FirstOrDefault(u => u.StrName.ToUpper() == UserName.ToUpper());
                //if (founduser == null)
                //{

                //    UserName = string.Empty;
                //    UserPassword = string.Empty;
                //    Messenger.Default.Send(new LoginMessage() { ValidLogin = false });

                //    return;
                //}
                //else
                //{


                //}
            }
            catch (Exception ex)
            {


            }

        }
        public override void RegisterMessages()
        {
            Messenger.Default.Register<LoginGotFocusMessage>(this, ExecuteLoginGotFocusMessage);
        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Register<LoginGotFocusMessage>(this, ExecuteLoginGotFocusMessage);
        }

        private void ExecuteLoginGotFocusMessage(LoginGotFocusMessage msg)
        {
            if (msg.Loginhasfocus)
            {

                HasFocus = true;
            }
            else
            {
                HasFocus = false;
            }

        }

    }
}
