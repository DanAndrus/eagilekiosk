﻿using Atom.SDK.Core;
using Atom.SDK.Net;
using Atom.SDK.Core.Enumerations;
using eAgileKiosk.Views.BaseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using eAgileKiosk.Views.Vpn.Helpers;
using Atom.Core.Models;
using eAgileKiosk.Views.Vpn.Interfaces;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;

namespace eAgileKiosk.Views.Vpn
{
    public class VpnSettingsViewModel : ViewModel,IConnection
    {
        public ICommand ActionCommand { get; private set; }

        #region Properties

        public List<Protocol> Protocols { get; set; }
        public List<Country> Countries { get; set; }

        private bool _UseOptimization;
        public bool UseOptimization
        {
            get { return _UseOptimization; }
            set
            {
                _UseOptimization = value;

                if (value)
                    UseSmartDialing = false;

                NotifyOfPropertyChange(() => UseOptimization);
            }
        }

        private bool _UseSplitTunneling;
        public bool UseSplitTunneling
        {
            get { return _UseSplitTunneling; }
            set
            {
                _UseSplitTunneling = value;
                NotifyOfPropertyChange(() => UseSplitTunneling);
            }
        }

        private bool _UseSmartDialing;
        public bool UseSmartDialing
        {
            get { return _UseSmartDialing; }
            set
            {
                _UseSmartDialing = value;

                if (!value)
                    SetCountries();
                else
                    SetCountries(isUseSmartConnect: true);

                if (value)
                    UseOptimization = false;

                NotifyOfPropertyChange(() => UseSmartDialing);
            }
        }

        private bool _UseCountryConnection;

        public bool UseCountryConnection
        {
            get => _UseCountryConnection;
            set
            {
                _UseCountryConnection = value;
                NotifyOfPropertyChange(() => UseCountryConnection);
            }
        }

        private bool _UseCityConnection;

        public bool UseCityConnection
        {
            get => _UseCityConnection;
            set
            {
                _UseCityConnection = value;
                NotifyOfPropertyChange(() => UseCityConnection);
            }
        }

        private bool _UseChannelConnection;

        public bool UseChannelConnection
        {
            get => _UseChannelConnection;
            set
            {
                _UseChannelConnection = value;
                NotifyOfPropertyChange(() => UseChannelConnection);
            }
        }


        private ObservableCollection<Country> _CountriesCollection;
        public ObservableCollection<Country> CountriesCollection
        {
            get { return _CountriesCollection; }
            set
            {
                _CountriesCollection = value;
                NotifyOfPropertyChange(() => CountriesCollection);
            }
        }

        private ObservableCollection<City> _CitiesCollection;
        public ObservableCollection<City> CitiesCollection
        {
            get { return _CitiesCollection; }
            set
            {
                _CitiesCollection = value;
                NotifyOfPropertyChange(() => CitiesCollection);
            }
        }

        private ObservableCollection<Channel> _ChannelsCollection;
        public ObservableCollection<Channel> ChannelsCollection
        {
            get { return _ChannelsCollection; }
            set
            {
                _ChannelsCollection = value;
                NotifyOfPropertyChange(() => ChannelsCollection);
            }
        }

        private Country selectedCountry;
        public Country SelectedCountry
        {
            get { return selectedCountry; }
            set
            {
                selectedCountry = value;
                NotifyOfPropertyChange(() => SelectedCountry);
            }
        }

        private City selectedCity;
        public City SelectedCity
        {
            get { return selectedCity; }
            set
            {
                selectedCity = value;
                NotifyOfPropertyChange(() => SelectedCity);
            }
        }

        private Channel selectedChannel;
        public Channel SelectedChannel
        {
            get { return selectedChannel; }
            set
            {
                selectedChannel = value;
                NotifyOfPropertyChange(() => SelectedChannel);
            }
        }

        private Protocol _PrimaryProtocol;
        public Protocol PrimaryProtocol
        {
            get { return _PrimaryProtocol; }
            set
            {
                _PrimaryProtocol = value;
                SetCountries();
                NotifyOfPropertyChange(() => PrimaryProtocol);
            }
        }

        private Protocol _SecondaryProtocol;
        public Protocol SecondaryProtocol
        {
            get { return _SecondaryProtocol; }
            set
            {
                _SecondaryProtocol = value;
                SetCountries();
                NotifyOfPropertyChange(() => SecondaryProtocol);
            }
        }

        private Protocol _TertiaryProtocol;
        public Protocol TertiaryProtocol
        {
            get { return _TertiaryProtocol; }
            set
            {
                _TertiaryProtocol = value;
                SetCountries();
                NotifyOfPropertyChange(() => TertiaryProtocol);
            }
        }

        private ObservableCollection<Protocol> _ProtocolsCollection;
        public ObservableCollection<Protocol> ProtocolsCollection
        {
            get { return _ProtocolsCollection; }
            set
            {
                _ProtocolsCollection = value;
                NotifyOfPropertyChange(() => ProtocolsCollection);
            }
        }

        private IConnection _ConnectionWithParams;
        public IConnection ConnectionWithParams
        {
            get { return _ConnectionWithParams = _ConnectionWithParams ?? new Views.UserControls.ConnectWithParams(); }
        }

        AtomManager atomManagerInstance;

        private bool _IsAutoCredMode;
        public bool IsAutoCredMode
        {
            get { return _IsAutoCredMode; }
            set
            {

                AtomHelper.IsAutoCredMode = value;
                SetProperty(ref _IsAutoCredMode, value);
            }
        }

        private string _UUID = string.Empty;

        public string UUID
        {
            get { return _UUID.Trim(); }
            set
            {

                AtomHelper.UUID = value;
                SetProperty(ref _UUID, value);
            }
        }

        private string _Username = string.Empty;

        public string Username
        {
            get { return _Username.Trim(); }
            set
            {

                AtomHelper.Username = value;
                SetProperty(ref _Username, value);
            }
        }

        private string _Password = string.Empty;

        public string Password
        {
            get { return _Password.Trim(); }
            set
            {

                AtomHelper.Password = value;
                SetProperty(ref _Password, value);
            }
        }

        private bool _IsConnDisconnAllowed;

        public bool IsConnDisconnAllowed
        {
            get { return _IsConnDisconnAllowed; }
            set
            {
                SetProperty(ref _IsConnDisconnAllowed, value);
            }
        }

        private bool _IsDisconnected = true;

        public bool IsDisconnected
        {
            get { return _IsDisconnected; }
            set
            {

                if (value)
                   // ActionButtonText = Messages.Connect;
                SetProperty(ref _IsDisconnected, value);
            }
        }

        private bool _IsConnected;

        public bool IsConnected
        {
            get { return _IsConnected; }
            set
            {

                if (value)
                 ///   ActionButtonText = Messages.Disconnect;
                SetProperty(ref _IsConnected, value);
            }
        }

        private bool _IsConnecting;

        public bool IsConnecting
        {
            get { return _IsConnecting; }
            set
            {

                if (value)
              //      ActionButtonText = Messages.Cancel;
                SetProperty(ref _IsConnecting, value);
            }
        }

        private string _ConnectionDialog;

        public string ConnectionDialog
        {
            get { return _ConnectionDialog; }
            set
            {

                SetProperty(ref _ConnectionDialog, value);
            }
        }

        private bool _ISSDKInitialized;

        public bool ISSDKInitialized
        {
            get { return _ISSDKInitialized; }
            set
            {

                SetProperty(ref _ISSDKInitialized, value);
            }
        }

        private bool _IsSDKInitializing;

        public bool IsSDKInitializing
        {
            get { return _IsSDKInitializing; }
            set
            {

                SetProperty(ref _IsSDKInitializing, value);
            }
        }

        private string _SecretKey = string.Empty;

        public string SecretKey
        {
            get { return _SecretKey.Trim(); }
            set
            {
                SetProperty(ref _SecretKey, value);
            }
        }

        private string _ActionButtonText = "Connect";

        public string ActionButtonText
        {
            get { return _ActionButtonText; }
            set
            {

                SetProperty(ref _ActionButtonText, value);
            }
        }
        public bool CanConnect => throw new NotImplementedException();

        #endregion

        public VpnSettingsViewModel()
        {

            ActionCommand = new RelayCommand(ExecuteActionCommand);
            IsSDKInitializing = true;

            var countries = new List<Country>();
            var protocols = new List<Protocol>();



            //await Task.Factory.StartNew(() =>
            //{
            //    //Can be initialized using this
            atomManagerInstance = AtomManager.Initialize("453a7c9175cbfbd5e6c8750de1feb0a1b673632e");
            //    //Or this
            //    //var atomConfig = new AtomConfiguration(SecretKey);
            //    //atomManagerInstance = AtomManager.Initialize(atomConfig);

            //    atomManagerInstance.Connected += AtomManagerInstance_Connected;
            //    atomManagerInstance.DialError += AtomManagerInstance_DialError;
            //    atomManagerInstance.Disconnected += AtomManagerInstance_Disconnected;
            //    atomManagerInstance.StateChanged += AtomManagerInstance_StateChanged;
            //    atomManagerInstance.Redialing += AtomManagerInstance_Redialing;
            //    atomManagerInstance.OnUnableToAccessInternet += AtomManagerInstance_OnUnableToAccessInternet;
            //    atomManagerInstance.SDKAlreadyInitialized += AtomManagerInstance_SDKAlreadyInitialized;
            //    atomManagerInstance.ConnectedLocation += AtomManagerInstance_ConnectedLocation;

            //    atomManagerInstance.AutoRedialOnConnectionDrop = true;

            //    //To get countries
            //    try
            //    {
            //        countries = atomManagerInstance.GetCountries();
            //    }
            //    catch { }

            //    //To get protocols
            //    try
            //    {
            //        protocols = atomManagerInstance.GetProtocols();
            //    }
            //    catch { }

            //    //AtomHelper lets you use the functionality of above created instance in all usercontrols and pages
            //    AtomHelper.SetAtomManagerInstance(atomManagerInstance);
            //});

            //ConnectionWithDedicatedIP.Initialize(protocols, countries);
            ConnectionWithParams.Initialize(protocols, countries);
            IsSDKInitializing = false;
            ISSDKInitialized = true;
            IsConnDisconnAllowed = true;














        }

        private void AtomManagerInstance_ConnectedLocation(object sender, ConnectedLocationEventArgs e)
        {
            try
            {
                var location = atomManagerInstance.GetConnectedLocation();

                if (location != null)
                {
                    ConnectionDialog += $"IP: {location.Ip} {Environment.NewLine}";

                    if (location.Country != null)
                        ConnectionDialog += $"Country: {location.Country.Name} {Environment.NewLine}";

                    if (location.City != null)
                        ConnectionDialog += $"City: {location.City.Name} {Environment.NewLine}";
                }
            }
            catch (Exception ex)
            {
                ConnectionDialog += ex.Message + Environment.NewLine;

                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message) && !string.IsNullOrWhiteSpace(ex.InnerException.Message))
                {
                    ConnectionDialog += ex.InnerException.Message + Environment.NewLine;

                    if (ex.InnerException.InnerException != null && string.IsNullOrWhiteSpace(ex.InnerException.InnerException.Message))
                        ConnectionDialog += ex.InnerException.InnerException.Message + Environment.NewLine;
                    else
                        ConnectionDialog += "No other inner exception message" + Environment.NewLine;
                }
            }
        }

        private void ExecuteActionCommand()
        {
            if (IsConnecting)
                Cancel();
            else if (IsConnected)
                Disconnect();
            else
                Connect();
        }


        // Indicates Connecting State
        public void ShowConnectingState(bool isClear = true)
        {
            if (isClear)
                ConnectionDialog = string.Empty;

            IsConnDisconnAllowed = false;
            IsDisconnected = false;
            IsConnecting = true;
        }

        public void ShowDisconnectedState()
        {
            IsDisconnected = true;
            IsConnecting = false;
            IsConnected = false;
            IsConnDisconnAllowed = true;
        }

        private void ActionButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsConnecting)
                Cancel();
            else if (IsConnected)
                Disconnect();
            else
                Connect();
        }

        // Here we call the connect method of the Selected Tab
        private void Connect()
        {
            var connection = ConnectionWithParams;
            connection.Connect();
        }

        private void Disconnect()
        {
            try
            {
                AtomHelper.Disconnect();
            }
            catch (Exception ex)
            {
                ConnectionDialog += $"{ex.Message}";
            }
        }

        private void Cancel()
        {
            try
            {
                AtomHelper.Cancel();
            }
            catch (Exception ex)
            {
                ConnectionDialog += $"{ex.Message}";
            }
        }

        private void ClosingApp(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsDisconnected)
            {
                //Messages.ShowMessage(Messages.DisconnectBeforeExit);
                e.Cancel = true;
            }
        }


        private void AtomManagerInstance_SDKAlreadyInitialized(object sender, ErrorEventArgs e)
        {
            //    Dispatcher.Invoke(() => MessageBox.Show(e.Message + Environment.NewLine + e.Exception?.Message));
        }

        private void AtomManagerInstance_OnUnableToAccessInternet(object sender, Atom.SDK.Core.CustomEventArgs.UnableToAccessInternetEventArgs e)
        {
            var a = e.ConnectionDetails;
            string message = "UTB Occured, Due want to Reconnect?";
            string caption = "Confirmation";
            MessageBoxButton buttons = MessageBoxButton.YesNo;
            MessageBoxImage icon = MessageBoxImage.Question;

            if (MessageBox.Show(message, caption, buttons, icon) == MessageBoxResult.Yes)
            {
                ConnectionDialog += "Reconnecting..." + Environment.NewLine;
                ShowConnectingState(false);
                atomManagerInstance.ReConnect();
            }
        }

        private void AtomManagerInstance_StateChanged(object sender, StateChangedEventArgs e)
        {
            if (e.State == VPNState.RECONNECTING)
            {
                ShowConnectingState(false);
            }

            ConnectionDialog += e.State.ToString() + Environment.NewLine;
        }

        private void AtomManagerInstance_Connected(object sender, EventArgs e)
        {
            ConnectionDialog += "CONNECTED" + Environment.NewLine;
            IsDisconnected = false;
            IsConnecting = false;
            IsConnected = true;
            IsConnDisconnAllowed = true;

            if (atomManagerInstance.VPNProperties.UseSplitTunneling)
            {
                atomManagerInstance.ApplySplitTunneling(new Atom.SDK.Core.Models.SplitApplication() { CompleteExePath = "Chrome.exe" });
            }
        }

        private void AtomManagerInstance_Disconnected(object sender, DisconnectedEventArgs e)
        {
            ConnectionDialog += e.Cancelled ? "CANCELLED" : "DISCONNECTED" + Environment.NewLine;
            IsDisconnected = true;
            IsConnecting = false;
            IsConnected = false;
            IsConnDisconnAllowed = true;
        }

        private void AtomManagerInstance_DialError(object sender, DialErrorEventArgs e)
        {
            ConnectionDialog += e.Message + Environment.NewLine;
            IsDisconnected = true;
            IsConnecting = false;
            IsConnected = false;
            IsConnDisconnAllowed = true;
        }

        private void AtomManagerInstance_Redialing(object sender, ErrorEventArgs e)
        {
            ConnectionDialog += e.Message + Environment.NewLine;
        }

        public override void RegisterMessages()
        {

        }

        public override void UnRegisterMessages()
        {

        }

        public void Initialize(List<Protocol> protocols = null, List<Country> countries = null)
        {
            throw new NotImplementedException();
        }

        void IConnection.Connect()
        {
            throw new NotImplementedException();
        }
    }
}
