﻿using eAgileKiosk.Models;
using eAgileKiosk.Views.BaseModels;
using eAgileKiosk.WPF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace eAgileKiosk.Views
{


    public class HomeViewModel : ViewModel
    {

        private ObservableCollection<KioskScannedItem> _scannedItems = new ObservableCollection<KioskScannedItem>();
        public ObservableCollection<KioskScannedItem> ScannedItems 
        {
            get { return _scannedItems; }
            set { SetProperty(ref _scannedItems, value); }

        }

        public RelayCommand ReadRFIDCommand { get; }
        public RelayCommand PrintCommand { get; }
        public HomeViewModel()
        {
            ReadRFIDCommand = new RelayCommand(ExecuteReadRFIDCommand, CanRead);
            PrintCommand = new RelayCommand(ExecutePrintCommand, CanPrint);

        }

        private bool CanRead()
        {
            return true;
        }
        private void ExecuteReadRFIDCommand()
        {

        }
        private bool CanPrint()
        {
            return true;
        }
        private void ExecutePrintCommand()
        {

        }

        public override void RegisterMessages()
        {
            
        }

        public override void UnRegisterMessages()
        {
            
        }
    }
}
