﻿
using RFIDReader;
using System;
using System.Collections.Generic;
using System.Text;

namespace eAgileKiosk.Messages
{
    public enum ThingError
    {
        None,
        PreviousAssociated,
        Multiple,
        InvalidHeader
    }

    public class ThingIdMessage
    {
        public EPCReport NewRFIDInfo;
        public ThingError Error;
    }
}
