﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace eAgileKiosk.WPF
{
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        #region SetProperty
        protected virtual bool SetProperty<T>(ref T backingField, T newValue, [CallerMemberName] string propertyName = null, params string[] dependentProperties)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, newValue))
            {
                return false;
            }

            backingField = newValue;

            OnPropertyChanged(propertyName, dependentProperties);

            return true;
        }
        #endregion SetProperty

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null, params string[] propertyNames)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            foreach (string p in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
            }
        }
        public virtual void RefreshProperties()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(string.Empty));
        }
        #endregion INotifyPropertyChanged
    }

    public abstract class ObservableEventArgs : EventArgs, INotifyPropertyChanged
    {
        #region SetProperty
        protected virtual bool SetProperty<T>(ref T backingField, T newValue, [CallerMemberName] string propertyName = null, params string[] dependentProperties)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, newValue))
            {
                return false;
            }

            backingField = newValue;

            OnPropertyChanged(propertyName, dependentProperties);

            return true;
        }
        #endregion SetProperty
        
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null, params string[] propertyNames)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            foreach (string p in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
            }
        }
        public virtual void RefreshProperties()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(string.Empty));
        }
        #endregion INotifyPropertyChanged
    }
}
