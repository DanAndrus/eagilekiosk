﻿using eAgileKiosk.Models;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace eAgileKiosk.WPF
{
    /// <summary>
    /// A command whose sole purpose is to relay its functionality to other
    /// objects by invoking delegates. The default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<Task> _executeAsync;
        private readonly Func<bool> _canExecute;
        private Task _task = Task.CompletedTask;

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action execute) : this(execute, null) { }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute;
            _executeAsync = null;
            _canExecute = canExecute;
        }

        /// <summary>
        /// Creates a new async command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Func<Task> execute) : this(execute, null) { }

        /// <summary>
        /// Creates a new async command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Func<Task> execute, Func<bool> canExecute = null)
        {
            _execute = null;
            _executeAsync = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return CanExecute();
        }

        private bool CanExecute()
        {
            return _task.IsCompleted && (_canExecute == null ? true : _canExecute());
        }

        public void Execute(object parameter)
        {
            if (CanExecute())
            {
                if (_execute != null)
                {
                    _execute();
                }
                else if (_executeAsync != null)
                {
                    _task = ExecuteAsync();
                }
            }
        }

        private async Task ExecuteAsync()
        {
            try { await _executeAsync(); } catch { }
            try { RaiseCanExecuteChanged(); } catch { }
        }

        public void RaiseCanExecuteChanged()
        {
            RefreshCanExecute();
        }

        /// <summary>
        /// Triggers CanExecute to be refreshed
        /// </summary>
        public static void RefreshCanExecute()
        {
            Action action = CommandManager.InvalidateRequerySuggested;
            Dispatcher dispatcher = Application.Current.Dispatcher;
            if (dispatcher == null || dispatcher.CheckAccess())
                action();
            else
                dispatcher.Invoke(action);
        }

        /// <summary>
        /// Tied to CommandManager.RequerySuggested 
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

       
    }

    /// <summary>
    /// A command whose sole purpose is to relay its functionality to other
    /// objects by invoking delegates. The default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, Task> _executeAsync;
        private readonly Func<T, bool> _canExecute;
        private Task _task = Task.CompletedTask;

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<T> execute, object canKey) : this(execute, null) { }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute;
            _executeAsync = null;
            _canExecute = canExecute;
        }

        /// <summary>
        /// Creates a new async command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Func<T, Task> execute) : this(execute, null) { }

        /// <summary>
        /// Creates a new async command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Func<T, Task> execute, Func<T, bool> canExecute = null)
        {
            _execute = null;
            _executeAsync = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }

        private bool CanExecute(T parameter)
        {
            return _task.IsCompleted && (_canExecute == null ? true : _canExecute(parameter));
        }

        public void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                if (_execute != null)
                {
                    _execute((T)parameter);
                }
                else if (_executeAsync != null)
                {
                    _task = ExecuteAsync((T)parameter);
                }
            }
        }

        private async Task ExecuteAsync(T parameter)
        {
            try { await _executeAsync(parameter); } catch { }
            try { RaiseCanExecuteChanged(); } catch { }
        }

        public void RaiseCanExecuteChanged()
        {
            RefreshCanExecute();
        }

        /// <summary>
        /// Triggers CanExecute to be refreshed
        /// </summary>
        public static void RefreshCanExecute()
        {
            Action action = CommandManager.InvalidateRequerySuggested;
            Dispatcher dispatcher = Application.Current.Dispatcher;
            if (dispatcher == null || dispatcher.CheckAccess())
                action();
            else
                dispatcher.Invoke(action);
        }

        /// <summary>
        /// Tied to CommandManager.RequerySuggested 
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
