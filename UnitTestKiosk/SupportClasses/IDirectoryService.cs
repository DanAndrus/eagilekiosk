﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestKiosk.SupportClasses
{
    public interface IDirectoryService
    {
        IEnumerable<string> EnumerateFiles(string path, string searchPattern);
    }
}