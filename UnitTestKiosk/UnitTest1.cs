using NUnit.Framework;
using Moq;
using UnitTestKiosk.SupportClasses;

namespace UnitTestKiosk
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void FolderCreation()
        {
            //Arrange
            var fakePath = @"C:\ProgramData\eAgile\AssociationKiosk\Settings";
            var fakeFiles = new[] {
            @"C:\\temp\\testfakefilename1.txt",
            @"C:\\temp\\testfakefilename2.txt",
            @"C:\\temp\\testfakefilename3.txt"

          //@"C:\ProgramData\eAgile\AssociationKiosk\Settings"
          //@"C:\ProgramData\eAgile\AssociationKiosk\TagLog"
          //@"C:\ProgramData\eAgile\AssociationKiosk\TagLog\Cache
          //@"C:\ProgramData\eAgile\AssociationKiosk\TagLog\Archive
          //@"C:\ProgramData\eAgile\AssociationKiosk\ErrorLog

        };
            var service = new FakeDIrectoryService(fakeFiles);
            var SystemUnderTest = new DemoCls(service);


            //Act
            SystemUnderTest.Execute(fakePath);

            //Assert

            Assert.Pass();
        }
    }
}