﻿using Amazon;
using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using eAgileAWSReader.Models;
using KioskManagerLib.Configuration;
using KioskManagerLib.Models;
using KioskManagerLib.Support_Classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace eAgileAWSReader
{



    public class AWSReader
    {

        private AmazonKinesisClient KinesisClient;

        private static IAmazonS3 Client;

        private const string ClientSharedSecret = "8bGToMI7xUt2H1d";

        private string keyName = string.Empty;
        
        //private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast2;

        private FilePathSettingModel NewfilePathSettings { get; set; }

        private AWSConnectionModel ConnectionSettings { get; set; }
        private List<AWSBucketModel> BucketList { get; set; } = new List<AWSBucketModel>();
        public AWSReader()
        {
            ReadConnectionSettings();

            if (string.IsNullOrWhiteSpace(ConnectionSettings.WriteStreamName))
            {
                KinesisClient = new AmazonKinesisClient(ConnectionSettings.ReadAccessKey, ConnectionSettings.ReadSecretKey, ConnectionSettings.ReadRegion);
                Client = new AmazonS3Client(ConnectionSettings.ReadAccessKey, ConnectionSettings.ReadSecretKey , ConnectionSettings.ReadRegion);
            }
            else
            {
                throw new ArgumentException("AWS profile not found");
            }
        }
        private void ReadConnectionSettings()
        {
            if (File.Exists(NewfilePathSettings.ConnectionPath))
            {
                var json = File.ReadAllText(NewfilePathSettings.ConnectionPath);
                ConnectionSettings = JsonConvert.DeserializeObject<AWSConnectionModel>(json);
                if (!string.IsNullOrEmpty(ConnectionSettings.ConnectionAmway))
                {
                    var UserAmway = Encryption.DecryptStringAES(ConnectionSettings.ConnectionAmway, ClientSharedSecret);
                    var connectionEA = Encryption.DecryptStringAES(ConnectionSettings.ConnectionAmway, ClientSharedSecret);
                    var UserEA = Encryption.DecryptStringAES(ConnectionSettings.ConnectionAmway, ClientSharedSecret);
                    var ConnectionAmway = Encryption.DecryptStringAES(ConnectionSettings.ConnectionAmway, ClientSharedSecret);

                }


                ConnectionSettings.WriteStreamName = Encryption.DecryptStringAES(ConnectionSettings.WriteStreamName, ClientSharedSecret);
                
                ConnectionSettings.WriteAccessKey = Encryption.DecryptStringAES(ConnectionSettings.WriteAccessKey, ClientSharedSecret);
                ConnectionSettings.WriteSecretKey = Encryption.DecryptStringAES(ConnectionSettings.WriteSecretKey, ClientSharedSecret);
                ConnectionSettings.WriteRegion = ConnectionSettings.WriteRegion;
                ConnectionSettings.WriteFormat = ConnectionSettings.WriteFormat;
                ConnectionSettings.WriteRoleARN = ConnectionSettings.WriteRoleARN;
                
                ConnectionSettings.ReadAccessKey = Encryption.DecryptStringAES(ConnectionSettings.ReadAccessKey, ClientSharedSecret);
                ConnectionSettings.ReadSecretKey = Encryption.DecryptStringAES(ConnectionSettings.ReadSecretKey, ClientSharedSecret);
                ConnectionSettings.ReadRegion = ConnectionSettings.ReadRegion;
                ConnectionSettings.ReadStreamName = Encryption.DecryptStringAES(ConnectionSettings.ReadStreamName, ClientSharedSecret);
                ConnectionSettings.ReadBucketName=ConnectionSettings.ReadBucketName;






            }
        }

        public async Task ReadObjectDataAsync()
        {

            ListObjectsRequest requestList = new ListObjectsRequest();
            requestList.BucketName = ConnectionSettings.ReadBucketName;
            ListObjectsResponse response2 = await Client.ListObjectsAsync(requestList);

            var currentday = $"{DateTime.Now.Year}-{DateTime.Now.ToString("MM")}-{DateTime.Now.Day}";

            foreach (S3Object o in response2.S3Objects)
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = ConnectionSettings.ReadBucketName,
                    Key = o.Key
                };

                if (o.Key.Contains(currentday))
                {
                    string responseBody = "";
                    using (GetObjectResponse response = await Client.GetObjectAsync(request))
                    using (Stream responseStream = response.ResponseStream)
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        string title = response.Metadata["x-amz-meta-title"]; // Assume you have "title" as medata added to the object.
                        string contentType = response.Headers["Content-Type"];

                        responseBody = reader.ReadToEnd(); // Now you process the response body.
                        if (!string.IsNullOrWhiteSpace(responseBody))
                        {
                            var splitresults = responseBody.Split("}");
                            for (int i = 0; i < splitresults.Length; i++)
                            {
                                if (!string.IsNullOrEmpty(splitresults[i]))
                                {
                                    try
                                    {
                                        var json = $"{splitresults[i]}{"}"}";
                                        AWSBucketModel newitem = JsonConvert.DeserializeObject<AWSBucketModel>(json);
                                        BucketList.Add(newitem);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public async Task ReadFromStream(CancellationToken ct = default)
        {
        
            DescribeStreamRequest describeRequest = new DescribeStreamRequest();
            describeRequest.StreamName = ConnectionSettings.WriteStreamName;

            DescribeStreamResponse describeResponse = await KinesisClient.DescribeStreamAsync(describeRequest);
            List<Shard> shards = describeResponse.StreamDescription.Shards;

            foreach (Shard shard in shards)
            {
                GetShardIteratorRequest iteratorRequest = new GetShardIteratorRequest();
                iteratorRequest.StreamName = ConnectionSettings.WriteStreamName;
                iteratorRequest.ShardId = shard.ShardId;
                iteratorRequest.ShardIteratorType = ShardIteratorType.TRIM_HORIZON;

                GetShardIteratorResponse iteratorResponse = await KinesisClient.GetShardIteratorAsync(iteratorRequest);
                string iteratorId = iteratorResponse.ShardIterator;

                while (!string.IsNullOrEmpty(iteratorId))
                {
                    GetRecordsRequest getRequest = new GetRecordsRequest();
                    getRequest.Limit = 1000;
                    getRequest.ShardIterator = iteratorId;

                    GetRecordsResponse getResponse = await KinesisClient.GetRecordsAsync(getRequest);
                    string nextIterator = getResponse.NextShardIterator;

                    List<Amazon.Kinesis.Model.Record> records = getResponse.Records;

                    if (records.Count > 0)
                    {
                        Console.WriteLine("Received {0} records. ", records.Count);
                        foreach (Record record in records)
                        {
                            try
                            {
                                string theMessage = Encoding.UTF8.GetString(record.Data.ToArray());
                                AWSBucketModel newitem = JsonConvert.DeserializeObject<AWSBucketModel>(theMessage);
                                BucketList.Add(newitem);
                            }
                            catch
                            {

                            }
                        }
                    }
                    iteratorId = nextIterator;
                    //per aws specs
                    await Task.Delay(TimeSpan.FromMilliseconds(200), ct).ConfigureAwait(false);
                }
            }
        }

    }




}
