﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using KioskManagerLib.Configuration;

namespace KioskManagerLib.Interfaces
{
    public interface IKioskSettings
    {
        public KioskSettingsModel Kiosksettings { get; set; }
        public AWSConnectionModel KioskConnectionSettings { get; set; }

        void ReadSettings();
        void ReadConnectionSettings();
        void CreateDirectories();


    }
}
