﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using KioskManagerLib.Logging;
using KioskManagerLib.Configuration;
using KioskManagerLib.Interfaces;
using KioskManagerLib.Models;
using static KioskManagerLib.Models.ErrorLogModel;
using BarCodeReader;
using System.ComponentModel;
using eAgileProducer;
using eAgileProducer.Models;
using KioskManagerLib.Support_Classes;
using static KioskManagerLib.Logging.EAgileLogger;
using System.Net;
using System.Runtime.Serialization.Json;
using EA.Common.Helpers.Generics;

namespace KioskManagerLib
{
    public class KioskController : IKioskSettings, IKioskLogging
    {
        private const string ClientSharedSecret = "8bGToMI7xUt2H1d";

        public string ActiveSKU { get; set; } = string.Empty;
        private string  CurrentThingID { get; set; } = string.Empty;
        public EAgileLogger KioskLogger { get; }
        
        public KioskSettingsModel Kiosksettings { get; set; } = new KioskSettingsModel();
        public AWSConnectionModel KioskConnectionSettings { get; set; }
        private NewErrorModel NewErrorItem { get; set; }
        private SingleSkuAssociationModel SingleSKUAdd { get; set; }
        public FilePathSettingModel NewfilePathSettings { get; set; }
        public SkusModel CurrentSkusList { get; set; } 
        public TotalSkuAssociationModel CurrentAssociatedSkus { get; set; } = new TotalSkuAssociationModel();
    
        private readonly System.Timers.Timer PayloadTimer;

        public KioskController()
        {
            NewfilePathSettings = new FilePathSettingModel();
            ReadFilePathSetting();
            ReadSettings();
            ReadConnectionSettings();
            CreateDirectories();
            CreateErrorLog();
            ReadSKUFile();
            ReadTotalSKUFile();
            ProcessPayload();
            if (Kiosksettings != null && !string.IsNullOrWhiteSpace(Kiosksettings.AWSSendInterval))
            {
                int Sendinterval = Convert.ToInt32(Kiosksettings.AWSSendInterval);
                PayloadTimer = new System.Timers.Timer(1000 * 60 * Sendinterval);
                //PayloadTimer = new System.Timers.Timer(1000 * 60 * 1);
                PayloadTimer.Elapsed += PayloadTimer_Elapsed;
                PayloadTimer.Start();
            }


            KioskLogger = new EAgileLogger(NewfilePathSettings.SystemErrorLogPath);
        }


        private void PayloadTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ProcessPayload();
        }
        private bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex,ErrorCode =0,Messagetext = "Error Connecting to internet", Timestamp= DateTime.Now });
                return false;
            }
        }
        public void  ProcessPayload()
        {

            try
            {
                if (CheckForInternetConnection())
                {
                    string[] filePaths = Directory.GetFiles(NewfilePathSettings.SingleAssociationPath);
                    if (filePaths.Length > 0)
                    {
                        ProductionModel NewPayLoad = new ProductionModel()
                        {

                            Location = Kiosksettings.KioskID,
                            Event = "Production",
                            TimeStamp = DateTime.Now.ToString("o"),

                        };

                        NewPayLoad.Items = new List<TagItem>();



                        foreach (string fileName in filePaths)
                        {
                            var currentfilename = fileName.Replace(NewfilePathSettings.SingleAssociationPath, "");
                            var json = File.ReadAllText(fileName);
                            SingleSkuAssociationModel SingleModel = JsonConvert.DeserializeObject<SingleSkuAssociationModel>(json);

                            TagItem additem = new TagItem()
                            {
                                EPC = SingleModel.EPC,
                                ThingID = SingleModel.ThingID,
                                TID = SingleModel.TID
                            };

                            NewPayLoad.Items.Add(additem);

                            var destinationpath = $"{NewfilePathSettings.TaglogArchivePath}{currentfilename}";
                            Directory.Move(fileName, destinationpath);
                        }

                        //var savesinglepath = $"{NewfilePathSettings.SingleAssociationPath}dan.json";
                        //var json2 = JsonConvert.SerializeObject(NewPayLoad);
                        //System.IO.File.WriteAllText(savesinglepath, json2);

                        //AWSProducer.SendPayLoad(EAgileRecordProducer.FileCreateType.Distribution);


                        //Kiosksettings.KioskID 

                        OnChangeKioskOccurred(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                    }
                }
            }
            catch(Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error processing Kinesis payload stream", Timestamp = DateTime.Now });
            }


        }
        public void CreateErrorLog()
        {
            try
            {


                var firstError = Kiosksettings.ErrorSettings.FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(firstError.PurgeDelay))
                {
                    var newdate = DateTime.Now.ToString("yyyyMMdd");

                    var newfiledateFile = $"{NewfilePathSettings.ErrorLogPath}AK_ERRORLOG_{newdate}.json";

                    if (!File.Exists(newfiledateFile))
                    {
                        var newErrorFile = new ErrorLogModel()
                        {
                            CreateDate = DateTime.Now.ToString("yyyy/MM/dd"),
                            Errors = new List<ErrorItems>()
                        };

                        var json = JsonConvert.SerializeObject(newErrorFile);
                        System.IO.File.WriteAllText(newfiledateFile, json);
                    }

                    foreach (var file in Directory.EnumerateFiles(NewfilePathSettings.ErrorLogPath, "*.json"))
                    {

                        var json1 = File.ReadAllText(file);
                        ErrorLogModel errorLogModel = JsonConvert.DeserializeObject<ErrorLogModel>(json1);

                        DateTime ogDate = Convert.ToDateTime(errorLogModel.CreateDate);

                        var Addeddays = ogDate.AddDays(Convert.ToDouble(firstError.PurgeDelay));

                        int result = DateTime.Compare(Addeddays, DateTime.Now);

                        if (result < 0)
                        {
                            File.Delete(file);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = "Error Creating ErrorLog", Timestamp = DateTime.Now });
            }


        }
        public void WriteToErrorLog(NewErrorModel NewErrorItem)
        {
            try
            {
                CreateErrorLog();

                if (NewErrorItem != null)
                {
                    var newdate = DateTime.Now.ToString("yyyyMMdd");

                    var newfiledateFile = $"{NewfilePathSettings.ErrorLogPath}AK_ERRORLOG_{newdate}.json";


                    var json = File.ReadAllText(newfiledateFile);
                    ErrorLogModel errorLogModel = JsonConvert.DeserializeObject<ErrorLogModel>(json);

                    errorLogModel.Errors.Add(new ErrorItems()
                    {

                        ErrorDateTime = NewErrorItem.ErrorDateTime.ToString("yyyy/MM/dd"),
                        Error = NewErrorItem.Error,
                        SKU = NewErrorItem.SKU,
                        RFID = NewErrorItem.RFID
                    }
                    );
                    // Add any Errors
                    // Update json data string
                    json = JsonConvert.SerializeObject(errorLogModel);
                    System.IO.File.WriteAllText(newfiledateFile, json);
                }
            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}"  , Timestamp = DateTime.Now });
            }


    //    WriteToErrorLog();
        }
        public void WriteToErrorLog()
        {
         
        }
        public void ReadTotalSKUFile()
        {
            try
            { 

                if (!string.IsNullOrWhiteSpace(ActiveSKU))
                {
                    var readpath = $"{NewfilePathSettings.TotalAssociationPath}{ActiveSKU}-{DateTime.Now.ToString("yyyyMMdd")}.json";

                    if (File.Exists(readpath))
                    {
                        var json = File.ReadAllText(readpath);
                        CurrentAssociatedSkus = JsonConvert.DeserializeObject<TotalSkuAssociationModel>(json);
                    }

                    else
                    {
                        CurrentAssociatedSkus = new TotalSkuAssociationModel()
                        {
                            SKU = ActiveSKU,
                            ScannedItems = new List<ScannedItem>()
                        {                     
                            }
                        };

                        var json = JsonConvert.SerializeObject(CurrentAssociatedSkus);
                        System.IO.File.WriteAllText(readpath, json);
                    }

                    CurrentThingID = string.Empty;

                }

            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
            }




        }
        public void WriteTotalSkuAssociationLog(string ThingID)
        {
            if (CurrentAssociatedSkus != null)
            {
                var foundThingID = CurrentAssociatedSkus.ScannedItems.FirstOrDefault(f => f.ThingID == ThingID);

                if(foundThingID ==null || foundThingID.ThingID == null)
                {
                    CurrentThingID = ThingID;
                    WriteTotalSkuAssociationLog();
                }
            }


        }
        public void WriteTotalSkuAssociationLog()
        {

            var savesinglepath = $"{NewfilePathSettings.TotalAssociationPath}{ActiveSKU}-{DateTime.Now.ToString("yyyyMMdd")}.json";

            if (File.Exists(savesinglepath))
            {
                var json = File.ReadAllText(savesinglepath);
                CurrentAssociatedSkus = JsonConvert.DeserializeObject<TotalSkuAssociationModel>(json);

                var newThingid = new ScannedItem()
                {
                    ThingID = CurrentThingID
                };

                CurrentAssociatedSkus.ScannedItems.Add(newThingid);

                json = JsonConvert.SerializeObject(CurrentAssociatedSkus);
                System.IO.File.WriteAllText(savesinglepath, json);


            }

            else
            {
                CurrentAssociatedSkus = new TotalSkuAssociationModel()
                {
                    SKU = ActiveSKU,
                    ScannedItems = new List<ScannedItem>()
                    { new ScannedItem()
                        {
                            ThingID = CurrentThingID
                        }
                    }
                };

                var json = JsonConvert.SerializeObject(CurrentAssociatedSkus);
                System.IO.File.WriteAllText(savesinglepath, json);
            }

            CurrentThingID = string.Empty;

        }
        public void WriteSingleSkuAssociationLog(SingleSkuAssociationModel SingleSku)
        {
           
            if (SingleSku != null)
            {
                var savesinglepath = $"{NewfilePathSettings.SingleAssociationPath}{DateTime.Now.ToString("yyyyMMddhhmmss")}.json";
                var json = JsonConvert.SerializeObject(SingleSku);
                System.IO.File.WriteAllText(savesinglepath, json);
            }
            SingleSKUAdd = null;

        }
        public void WriteSingleSkuAssociationLog()
        {
            
        }
        public void ReadSettings()
        {
            if (File.Exists(NewfilePathSettings.SettingsPath))
            {
                var json = File.ReadAllText(NewfilePathSettings.SettingsPath);
                                Kiosksettings = JsonConvert.DeserializeObject<KioskSettingsModel>(json);
            }
            else
            {
                Kiosksettings = new KioskSettingsModel()
                {
                    KioskID = "eAgile01",
                    AWSSendInterval= "30",
                    ReaderSettings = new List<ReaderSetting>()
                    { new ReaderSetting()
                        {
                            ReaderAddress = "COM3",
                            ReaderPower = "15"
                        }
                    },
                    BarCodeSettings = new List<BarCodeSetting>
                    {
                        new BarCodeSetting ()
                        {
                            BarcodeAddress = "COM4",
                            BarcodeLength= "11"
                        }
                    },

                    ErrorSettings = new List<ErrorSetting>
                    {
                        new ErrorSetting
                        {
                            Level="error",
                            PurgeDelay="90",
                            submit= "Daily"

                        }
                    },

                    LogSettings = new List<LogSetting>
                    {
                        new LogSetting()
                        {
                            Submit="daily",
                            PurgeDelay="90"
                        }
                    },
                    tagSettings = new List<TagSetting>()
                    {
                        new TagSetting()
                        {
                            TidHeader= "daily",
                            epcHeader="01"
                        }
                    }

                };

                var json = JsonConvert.SerializeObject(Kiosksettings);
                System.IO.File.WriteAllText(NewfilePathSettings.SettingsPath, json);
            }


        }
        public void ReadConnectionSettings()
        {
            try { 
                if (File.Exists(NewfilePathSettings.ConnectionPath))
                {

                    var firstjson = File.ReadAllText(NewfilePathSettings.ConnectionPath);
                    KioskConnectionSettings = JsonConvert.DeserializeObject<AWSConnectionModel>(firstjson);

                    if(KioskConnectionSettings.NewConnection.Contains("True"))
                    {
                        WriteConnectionSettings(KioskConnectionSettings);
                    }
                

                    var json = File.ReadAllText(NewfilePathSettings.ConnectionPath);
                    KioskConnectionSettings = JsonConvert.DeserializeObject<AWSConnectionModel>(json);


                    if (!string.IsNullOrEmpty(KioskConnectionSettings.ConnectionAmway))
                    {
                        KioskConnectionSettings.ConnectionAmway = Encryption.DecryptStringAES(KioskConnectionSettings.ConnectionAmway, ClientSharedSecret);
                        KioskConnectionSettings.connectionEA= Encryption.DecryptStringAES(KioskConnectionSettings.connectionEA, ClientSharedSecret);
                        KioskConnectionSettings.UserEA = Encryption.DecryptStringAES(KioskConnectionSettings.UserEA, ClientSharedSecret);
                        KioskConnectionSettings.UserAmway = Encryption.DecryptStringAES(KioskConnectionSettings.UserAmway, ClientSharedSecret);
                    }
                    KioskConnectionSettings.NewConnection = KioskConnectionSettings.NewConnection;
                    KioskConnectionSettings.WriteStreamName = Encryption.DecryptStringAES(KioskConnectionSettings.WriteStreamName, ClientSharedSecret);
                    KioskConnectionSettings.WriteAccessKey = Encryption.DecryptStringAES(KioskConnectionSettings.WriteAccessKey, ClientSharedSecret);
                    KioskConnectionSettings.WriteSecretKey = Encryption.DecryptStringAES(KioskConnectionSettings.WriteSecretKey, ClientSharedSecret);
                    KioskConnectionSettings.WriteRegion = KioskConnectionSettings.WriteRegion;
                    KioskConnectionSettings.WriteFormat = KioskConnectionSettings.WriteFormat;
                    KioskConnectionSettings.WriteRoleARN = KioskConnectionSettings.WriteRoleARN;
                    KioskConnectionSettings.SupportStreamName = Encryption.EncryptStringAES(KioskConnectionSettings.SupportStreamName, ClientSharedSecret);
                    KioskConnectionSettings.SupportAccessKey = Encryption.EncryptStringAES(KioskConnectionSettings.SupportAccessKey, ClientSharedSecret);
                    KioskConnectionSettings.SupportSecretKey = Encryption.EncryptStringAES(KioskConnectionSettings.SupportSecretKey, ClientSharedSecret);
                    KioskConnectionSettings.SupportRegion = KioskConnectionSettings.SupportRegion;
                    KioskConnectionSettings.SupportFormat = KioskConnectionSettings.SupportFormat;
                    KioskConnectionSettings.SupportRoleARN = KioskConnectionSettings.SupportRoleARN;


                }
                else
                {
                    AWSConnectionModel CreateNewConnection = new AWSConnectionModel()
                    {
                        NewConnection  ="True",
                        WriteStreamName = "thing-event-input-stream-test",
                        WriteAccessKey = "AKIAXQU5NULGPXCDLL6Z",
                        WriteSecretKey = "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",
                        WriteRegion = "us-east-1",
                        WriteFormat = "json",
                        WriteRoleARN = "arn:aws:iam::983054836469:role/thing-event-input-stream-role-test",

                        ConnectionAmway = "ConnectionAmway",
                        UserAmway = "UserAmway",
                        connectionEA = "connectionEA",
                        UserEA = "UserEA",
                        SupportStreamName = "eagile_test",
                        SupportAccessKey = "AKIAXQU5NULGDTB3UT6N",
                        SupportSecretKey = "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",
                        SupportRegion = "us-east-2",
                        SupportFormat = "json",
                        SupportRoleARN = "SupportRoleARN"
                    };

                    var json = JsonConvert.SerializeObject(CreateNewConnection);
                    System.IO.File.WriteAllText(NewfilePathSettings.ConnectionPath, json);

                    ReadConnectionSettings();
                }

            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
            }

        }
        public void WriteConnectionSettings(AWSConnectionModel kioskConnectionSettings)
        {
            try
            { 
                KioskConnectionSettings.NewConnection = "false";
                KioskConnectionSettings.ConnectionAmway = Encryption.EncryptStringAES(kioskConnectionSettings.ConnectionAmway, ClientSharedSecret);
                KioskConnectionSettings.UserAmway = Encryption.EncryptStringAES(kioskConnectionSettings.ConnectionAmway, ClientSharedSecret);
                KioskConnectionSettings.connectionEA = Encryption.EncryptStringAES(kioskConnectionSettings.connectionEA, ClientSharedSecret);
                KioskConnectionSettings.UserEA = Encryption.EncryptStringAES(kioskConnectionSettings.UserEA, ClientSharedSecret);
                KioskConnectionSettings.WriteStreamName = Encryption.EncryptStringAES(kioskConnectionSettings.WriteStreamName, ClientSharedSecret);
                KioskConnectionSettings.WriteAccessKey = Encryption.EncryptStringAES(kioskConnectionSettings.WriteAccessKey, ClientSharedSecret);
                KioskConnectionSettings.WriteSecretKey = Encryption.EncryptStringAES(kioskConnectionSettings.WriteSecretKey, ClientSharedSecret);
                KioskConnectionSettings.WriteRegion = kioskConnectionSettings.WriteRegion;
                KioskConnectionSettings.WriteFormat = kioskConnectionSettings.WriteFormat;
                KioskConnectionSettings.WriteRoleARN = kioskConnectionSettings.WriteRoleARN;
                KioskConnectionSettings.SupportStreamName = Encryption.EncryptStringAES(kioskConnectionSettings.SupportStreamName, ClientSharedSecret);
                KioskConnectionSettings.SupportAccessKey = Encryption.EncryptStringAES(kioskConnectionSettings.SupportAccessKey, ClientSharedSecret);
                KioskConnectionSettings.SupportSecretKey = Encryption.EncryptStringAES(kioskConnectionSettings.SupportSecretKey, ClientSharedSecret);
                KioskConnectionSettings.SupportRegion = kioskConnectionSettings.SupportRegion;
                KioskConnectionSettings.SupportFormat = kioskConnectionSettings.SupportFormat;
                KioskConnectionSettings.SupportRoleARN = kioskConnectionSettings.SupportRoleARN;

                var json = JsonConvert.SerializeObject(KioskConnectionSettings);
                System.IO.File.WriteAllText(NewfilePathSettings.ConnectionPath, json);

            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
            }


        }
        private void ReadFilePathSetting()
        {
            try 
            { 
                if (File.Exists(NewfilePathSettings.FilePathSettingPath))
                {
                
                    NewfilePathSettings = JsonGeneric.JsonDeserialize<FilePathSettingModel>(NewfilePathSettings.FilePathSettingPath);

                    //var json = File.ReadAllText(NewfilePathSettings.FilePathSettingPath);
                    //NewfilePathSettings = JsonConvert.DeserializeObject<FilePathSettingModel>(json);
                }
                else
                {
                    if (File.Exists(@"C:\ProgramData\eAgile\AssociationKiosk\Settings\FilePathSetting.json"))
                    {
                        NewfilePathSettings = JsonGeneric.JsonDeserialize<FilePathSettingModel>(@"C:\ProgramData\eAgile\AssociationKiosk\Settings\FilePathSetting.json");

                    }
                    else
                    {

                        NewfilePathSettings = new FilePathSettingModel()
                        {
                            FilePathSettingPath = @"C:\ProgramData\eAgile\AssociationKiosk\Settings\FilePathSetting.json",
                            SKUSettingsPath = @"C:\ProgramData\eAgile\AssociationKiosk\Settings\AK_SKU.json",
                            ErrorLogPath = @"C:\ProgramData\eAgile\AssociationKiosk\ErrorLog\",
                            ConnectionPath = @"C:\ProgramData\eAgile\AssociationKiosk\Settings\AK_Connection.json",
                            SettingsPath = @"C:\ProgramData\eAgile\AssociationKiosk\Settings\AK_Settings.json",
                            SingleAssociationPath = @"C:\ProgramData\eAgile\AssociationKiosk\TagLog\Cache\",
                            TotalAssociationPath = @"C:\ProgramData\eAgile\AssociationKiosk\TagLog\",
                            TaglogArchivePath = @"C:\ProgramData\eAgile\AssociationKiosk\TagLog\Archive\",
                            RFIDSettingsPath = @"C:\ProgramData\eAgile\AssociationKiosk\Settings\Settings.xml",
                            SystemErrorLogPath = @"C:\ProgramData\eAgile\AssociationKiosk\SystemLogs\SystemLogs.txt",
                        };

                        //var json = JsonConvert.SerializeObject(NewfilePathSettings);
                        //System.IO.File.WriteAllText(NewfilePathSettings.FilePathSettingPath, json);

                        JsonGeneric.JsonSerializer(NewfilePathSettings, NewfilePathSettings.FilePathSettingPath);
                    }

                }
            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
            }


        }
        public void WriteSKUFile(SkusItem newSku)
        {
            try
            { 
                var json = File.ReadAllText(NewfilePathSettings.SKUSettingsPath);
                SkusModel Skus = JsonConvert.DeserializeObject<SkusModel>(json);
                SkusItem foundsku  = Skus.SkusList.FirstOrDefault(s => s.Name == newSku.Name);
                if (foundsku == null)
                {
                
                    Skus.SkusList.Add(newSku);
                    json = JsonConvert.SerializeObject(Skus);
                    System.IO.File.WriteAllText(NewfilePathSettings.SKUSettingsPath, json);
                    ActiveSKU = newSku.Name;
                    ReadSKUFile();
                }
            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
            }

        }
        public void ReadSKUFile()
        {
            try 
            {
                CurrentSkusList = new SkusModel();
                CurrentSkusList.SkusList = new List<SkusItem>();
                
                if (File.Exists(NewfilePathSettings.SKUSettingsPath))
                {
                    var json = File.ReadAllText(NewfilePathSettings.SKUSettingsPath);
                    CurrentSkusList = JsonConvert.DeserializeObject<SkusModel>(json);
                }
                else
                {
                    var newSKU = new SkusModel()
                    {
                        SkusList = new List<SkusItem>()
                    };


                    var json = JsonConvert.SerializeObject(newSKU);
                    System.IO.File.WriteAllText(NewfilePathSettings.SKUSettingsPath, json);

                }
            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
            }

        }

        public event EventHandler<string> KioskEvent;
        protected virtual void OnChangeKioskOccurred(string newKioskMessage)
        {
            try { KioskEvent?.Invoke(this, newKioskMessage); } catch { }
        }

        public void CreateDirectories()
        {
            try 
            { 
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\Settings");
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\TagLog");
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\TagLog\Cache");
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\TagLog\Archive");
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\AssociationKiosk\ErrorLog");
            }
            catch (Exception ex)
            {
                KioskLogger.WriteToErrorLog(ErrorLevel.Info, new LogMessage() { Error = ex, ErrorCode = 0, Messagetext = $"Error in {ex.TargetSite}", Timestamp = DateTime.Now });
            }
        }


    }
}
