﻿using System;
using System.Collections.Generic;
using System.Text;
using NLog;
namespace KioskManagerLib.Logging
{
    public class EAgileLogger
    {

        public List<KeyValuePair<int, string>> RLMErrorCodes { get; set; } = new List<KeyValuePair<int, string>>();

        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public enum ErrorLevel
        {
            Error,
            Warn,
            Info,
            Debug,
        }

        public class LogMessage
        {
            public DateTime Timestamp { get; set; } = DateTime.UtcNow;

            public string Messagetext { get; set; }

            public int ErrorCode { get; set; }

            public Exception Error { get; set; }
            
            
        }

        public EAgileLogger(string Logfilelocation)
        {

            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = Logfilelocation };

            // Rules for mapping loggers to targets            
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logfile);
            //config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);
            //config.AddRule(LogLevel.Error, LogLevel.Fatal, logfile);
            // Apply config           
            NLog.LogManager.Configuration = config;

        }
        public void WriteToErrorLog(ErrorLevel errortype, LogMessage Message)
        {
            switch (errortype)
            {
                case ErrorLevel.Info:
                    Logger.Info($"{Message.Timestamp}:{Message.ErrorCode}:{Message.Messagetext}:{Message.Error.Message}");
                    break;
                case ErrorLevel.Warn:
                    break;
                case ErrorLevel.Error:
                    break;
            }
        }

        //public void RemoveFromErrorLog(LineManagerError currentErrorToRemove)
        //{
        //    ErrorLogs.Remove(ErrorLogs.FirstOrDefault(r => r.ErrorCode == currentErrorToRemove.ErrorCode && r.Timestamp == currentErrorToRemove.Timestamp));
        //}

        //public void WriteToErrorLog(LineManagerError currentError)
        //{
        //    //DateTime Timestamp ,string Message , int ErrorCode , ErrorLevel Level ,Exception Error , LogEventInfo LogInfo
        //    //Todo: add some validation

        //    var validError = new LineManagerError()
        //    {

        //        Timestamp = currentError.Timestamp,
        //        ErrorCode = currentError.ErrorCode,
        //        Message = RLMErrorCodes.FirstOrDefault(ec => ec.Key == currentError.ErrorCode).Value,
        //        Level = currentError.Level,
        //        Error = currentError.Error,
        //        LogInfo = currentError.LogInfo

        //    };

        //    ErrorLogs.Add(validError);
        //}

    }
}
