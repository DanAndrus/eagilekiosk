﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Models
{
    public class NewErrorModel
    {
        public string Error { get; set; }
        public string SKU { get; set; }
        public string RFID { get; set; }
        public DateTime ErrorDateTime { get; set; }
    }

}
