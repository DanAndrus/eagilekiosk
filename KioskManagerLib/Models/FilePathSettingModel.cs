﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Models
{
    public class FilePathSettingModel
    {
        public string FilePathSettingPath { get; set; }
        public string SKUSettingsPath { get; set; }
        public string ConnectionPath { get; set; }
        public string ErrorLogPath { get; set; }
        public string SettingsPath { get; set; }
        public string SingleAssociationPath { get; set; }
        public string TotalAssociationPath { get; set; }
        public string TaglogCachePath { get; set; }
        public string TaglogArchivePath { get; set; }
        public string RFIDSettingsPath { get; set; }
        public string SystemErrorLogPath { get; set; }

    }
}
