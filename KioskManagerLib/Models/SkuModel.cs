﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Models
{
    public class SkusModel
    {
        public List<SkusItem> SkusList { get; set; }

    }
    public class SkusItem
    {
        public string Name { get; set; }
    }
    

}
