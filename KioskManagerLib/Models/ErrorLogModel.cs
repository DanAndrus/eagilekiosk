﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Models
{
    public class ErrorLogModel
    {
        public string CreateDate { get; set; }
        public List<ErrorItems> Errors { get; set; }
    }
    public class ErrorItems
    {
        public string SKU { get; set; }
        public string RFID { get; set; }
        public string Error { get; set; }
        public string ErrorDateTime { get; set; }
    }
}
