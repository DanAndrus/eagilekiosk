﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KioskManagerLib.Configuration
{

    public class KioskSettingsModel
    {
        public string KioskID { get; set; }
        public string AWSSendInterval { get; set; }
        public List<ReaderSetting> ReaderSettings { get; set; }
        public List<BarCodeSetting> BarCodeSettings { get; set; }
        public List<ErrorSetting> ErrorSettings { get; set; }
        public List<LogSetting> LogSettings { get; set; }
        public List<TagSetting> tagSettings { get; set; }
    }
    public class ReaderSetting
    {
        public string ReaderAddress { get; set; }
        public string ReaderPower { get; set; }
    }

    public class BarCodeSetting
    {
        public string BarcodeAddress { get; set; }
        public string BarcodeLength { get; set; }
    }

    public class ErrorSetting
    {
        public string Level { get; set; }
        public string PurgeDelay { get; set; }
        public string submit { get; set; }
    }

    public class LogSetting
    {
        public string Submit { get; set; }
        public string PurgeDelay { get; set; }
    }

    public class TagSetting
    {
        public string TidHeader { get; set; }
        public string epcHeader { get; set; }
    }


}
