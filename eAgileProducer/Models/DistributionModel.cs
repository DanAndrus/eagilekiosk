﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eAgileProducer.Models
{
    public class DistributionModel
    {
        public string CartonID { get; set; }
        public string Location { get; set; }
        public string Event { get; set; }
        public string TimeStamp { get; set; }
        public List<Item> Items { get; set; }
        
    }
    public class Item
    {
        public string ThingID { get; set; }
        public string EPC { get; set; }
        public string TID { get; set; }
    }

}
