﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eAgileProducer.Models
{
    public class CreationModel
    {
        public string Location { get; set; }
        public string Event { get; set; }
        public DateTime TimeStamp { get; set; }
        public List<string> Items { get; set; }
    }

}
