﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eAgileProducer.Models
{
    public class ProductionModel
    {
        public string SKUID { get; set; }
        public string Location { get; set; }
        public string Event { get; set; }
        public string TimeStamp { get; set; }
        public List<TagItem> Items { get; set; }
        
    }
    public class TagItem
    {
        public string ThingID { get; set; }
        public string EPC { get; set; }
        public string TID { get; set; }
    }

}
