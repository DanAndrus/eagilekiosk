﻿/*
 * Copyright 2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;

/// <summary>
/// <para> Before running the code:
/// Fill in your AWS access credentials in the provided credentials file template,
/// and be sure to move the file to the default location under your home folder --
/// C:\users\username\.aws\credentials on Windows -- where the sample code will
/// load the credentials from.
/// https://console.aws.amazon.com/iam/home?#security_credential
/// </para>
/// <para>
/// WARNING:
/// To avoid accidental leakage of your credentials, DO NOT keep the credentials
/// file in your source directory.
/// </para>
/// </summary>

namespace eAgileProducer
{
    public class EAgileRecordProducer
    {
        public enum FileCreateType
        {
            Creation = 1,
            Production = 2,
            Distribution = 3
        }

        /// <summary>
        /// The AmazonKinesisClient instance used to establish a connection with AWS Kinesis,
        /// create a Kinesis stream, populate it with records, and (optionally) delete the stream.
        /// The SDK attempts to fetch credentials in the order described in:
        /// http://docs.aws.amazon.com/sdkfornet/latest/apidocs/items/MKinesis_KinesisClientctorNET4_5.html.
        /// You may also wish to change the RegionEndpoint.
        /// </summary>
        private static readonly AmazonKinesisClient kinesisClient = new AmazonKinesisClient(RegionEndpoint.USEast2);

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public class JsonHeaderInfo
        {
            public string SkuId { get; set; }
            public string Cartonid { get; set; }
            public string Location { get; set; }
            public string Event { get; set; }
            public string Timestamp { get; set; }
        }

        private const string bucketName = "amway-thingid-bucket";
        private const string keyName = "*** object key ***";
        private const string myStreamName = "eagile_test";
        private const int myStreamSize = 1;

        public PutRecordResponse SendPayLoad(FileCreateType filetype)
        {
            PutRecordResponse putResultResponse; 
            WaitForStreamToBecomeAvailable(myStreamName);
            StringBuilder buildstring = new StringBuilder();
            try
            {
                switch (filetype)
                {
                    case FileCreateType.Creation:
                        buildstring = WriteCreationFile(new JsonHeaderInfo());
                        break;
                    case FileCreateType.Distribution:
                        buildstring = WriteDistributionFile(new JsonHeaderInfo());
                        break;

                    case FileCreateType.Production:
                        buildstring = WriteProductionFile(new JsonHeaderInfo());
                        break;
                }
                var requestRecord = new PutRecordRequest
                {
                    StreamName = myStreamName,
                    PartitionKey = "partitionKey-1",
                    Data = new MemoryStream(Encoding.UTF8.GetBytes(buildstring.ToString()))

                };

                putResultResponse = kinesisClient.PutRecordAsync(requestRecord).Result;

                
                //todo: log errors
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return putResultResponse;
        }
        private static StringBuilder WriteProductionFile(JsonHeaderInfo headerInfo)
        {

            // TEMP BUILD 

            headerInfo.SkuId = RandomString(7);
            headerInfo.Cartonid = "";
            headerInfo.Location = "eAgile";
            headerInfo.Event = "Production";
            headerInfo.Timestamp = DateTime.Now.ToString("o");

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("SKUID");
                writer.WriteValue(headerInfo.SkuId);
                writer.WritePropertyName("Location");
                writer.WriteValue(headerInfo.Location);
                writer.WritePropertyName("Event");
                writer.WriteValue(headerInfo.Event);
                writer.WritePropertyName("TimeStamp");
                writer.WriteValue(headerInfo.Timestamp);
                writer.WritePropertyName("Items");
                writer.WriteStartArray();
                for (int j = 1; j <= 3; ++j)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName("ThingID");
                    writer.WriteValue(RandomString(10));
                    writer.WritePropertyName("EPC");
                    writer.WriteValue(RandomString(24));
                    writer.WritePropertyName("TID");
                    writer.WriteValue(RandomString(24));
                    writer.WriteEndObject();
                }
                writer.WriteEndArray();

            }

            Console.WriteLine(sb.ToString());

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\LogSource\Production1.log"))
            {
                file.WriteLine(sb.ToString()); // "sb" is the StringBuilder
            }

            return sb;

        }
        private static StringBuilder WriteDistributionFile(JsonHeaderInfo headerInfo)
        {

            // TEMP BUILD 

            headerInfo.Cartonid = RandomString(11);
            headerInfo.Location = "SantaFe Springs";
            headerInfo.Event  = "Distribution";
            headerInfo.Timestamp = DateTime.Now.ToString("o");
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("CartonID");
                writer.WriteValue(headerInfo.Cartonid);
                writer.WritePropertyName("Location");
                writer.WriteValue(headerInfo.Location );
                writer.WritePropertyName("Event");
                writer.WriteValue(headerInfo.Event );
                writer.WritePropertyName("TimeStamp");
                writer.WriteValue(headerInfo.Timestamp.ToString(CultureInfo.CurrentCulture));
                writer.WritePropertyName("Items");
                writer.WriteStartArray();
                
                for (int j = 1; j <= 3; ++j)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName("ThingID");
                    writer.WriteValue(RandomString(10));
                    writer.WritePropertyName("EPC");
                    writer.WriteValue(RandomString(24));
                    writer.WritePropertyName("TID");
                    writer.WriteValue(RandomString(24));
                    writer.WriteEndObject();
                }
                
                writer.WriteEndArray();

            }

            Console.WriteLine(sb.ToString());

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\LogSource\Distribution1.log"))
            {
                file.WriteLine(sb.ToString());
            }
            return sb;
        }
        private static StringBuilder WriteCreationFile(JsonHeaderInfo headerInfo)
        {
            // TEMP BUILD 
            headerInfo.Cartonid = "";
            headerInfo.Location = "eAgile";
            headerInfo.Event = "Creation";
            headerInfo.Timestamp = DateTime.Now.ToString("o");
            
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
           using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("Location");
                writer.WriteValue(headerInfo.Location);
                writer.WritePropertyName("Event");
                writer.WriteValue(headerInfo.Event);
                writer.WritePropertyName("TimeStamp");
                writer.WriteValue(headerInfo.Timestamp);
                writer.WritePropertyName("Items");
                writer.WriteStartArray();
                for (int j = 1; j <= 1000; ++j)
                {
                    var random = RandomString(10);
                    writer.WriteValue(random);
                }
                writer.WriteEndArray();
                
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\LogSource\Creation1.log"))
            {
                file.WriteLine(sb.ToString()); // "sb" is the StringBuilder
            }

            return sb;
        }

        public void GetPayLoadAsync()
        {
            AmazonS3Config terst = new AmazonS3Config();

            terst.RegionEndpoint = bucketRegion;
            

            client = new AmazonS3Client("AKIA3HE5JZ43JE53DD7G", "LiJ84LxUDlEBGlBRYTFuE11pcOlaYCzPsIbEZ71M", terst);
            ReadObjectDataAsync().Wait();
        }
        // Specify your bucket region (an example region is shown).
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast2;
        private static IAmazonS3 client;

        static async Task ReadObjectDataAsync()
        {
            var bucketName = string.Empty;
            ListBucketsResponse response = await client.ListBucketsAsync();
            //foreach (S3Bucket b in response.Buckets)
            //{
            //    bucketName = b.BucketName;
            //    Console.WriteLine("{0}\t{1}", b.BucketName, b.CreationDate);
            //}

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = bucketName;
            ListObjectsResponse response2 = await client.ListObjectsAsync(request);

            
            foreach (S3Object o in response2.S3Objects)
            {
                GetObjectRequest request3 = new GetObjectRequest();
                request3.BucketName = bucketName;
                request3.Key = o.Key;
                var aa = DateTime.Now.Ticks;
                if (o.Key.Contains("2020-30-09")  )
                { 
                    GetObjectResponse response3 = await  client.GetObjectAsync(request3);
                    try
                    {
                        await response3.WriteResponseStreamToFileAsync($"C:\\Dans\\{response3.Key}.txt", true, CancellationToken.None);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }



            }


            //string responseBody = "";
            //try
            //{
            //    GetObjectRequest request = new GetObjectRequest
            //    {
            //        BucketName = bucketName,
            //        Key = keyName
            //    };
            //    using (GetObjectResponse response = await client.GetObjectAsync(request))
            //    using (Stream responseStream = response.ResponseStream)
            //    using (StreamReader reader = new StreamReader(responseStream))
            //    {
            //        string title =
            //            response.Metadata["x-amz-meta-title"]; // Assume you have "title" as medata added to the object.
            //        string contentType = response.Headers["Content-Type"];
            //        Console.WriteLine("Object metadata, Title: {0}", title);
            //        Console.WriteLine("Content type: {0}", contentType);

            //        responseBody = reader.ReadToEnd(); // Now you process the response body.
            //    }
            //}
            //catch (AmazonS3Exception e)
            //{
            //    Console.WriteLine("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            //}
        }

       
        /// <summary>
        /// This method verifies your credentials, creates a Kinesis stream, waits for the stream
        /// to become active, then puts 10 records in it, and (optionally) deletes the stream.
        /// </summary>
        public static void Main(string[] args )
        {

           client = new AmazonS3Client(bucketRegion);
            ReadObjectDataAsync().Wait();


           // kinesisClient = new AmazonKinesisClient(RegionEndpoint.USEast2);

            const string myStreamName = "eagile_test";
            const int myStreamSize = 1;


            //WaitForStreamToBecomeAvailable(myStreamName);

            //Console.Error.WriteLine("Putting records in stream : " + myStreamName);


            //// Write 10 UTF-8 encoded records to the stream.
            //try
            //{
            //    var bb = WriteCreationFile(new JsonHeaderInfo());

            //    var requestRecord = new PutRecordRequest
            //    {
            //        StreamName = myStreamName,
            //        PartitionKey = "partitionKey-1",
            //        Data = new MemoryStream(Encoding.UTF8.GetBytes(bb.ToString()))

            //    };

            //    var putResultResponse = kinesisClient.PutRecordAsync(requestRecord).Result;

            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    throw;
            //}

            // WriteDistributionFile(new JsonHeaderInfo());
            // WriteCreationFile(new JsonHeaderInfo());
            // WriteProductionFile(new JsonHeaderInfo());

            // try
            // {
            //     var createStreamRequest = new CreateStreamRequest();
            //     createStreamRequest.StreamName = myStreamName;
            //     createStreamRequest.ShardCount = myStreamSize;
            //     var createStreamReq = createStreamRequest;
            //     var CreateStreamResponse = kinesisClient.CreateStreamAsync(createStreamReq).Result;
            //     Console.Error.WriteLine("Created Stream : " + myStreamName);
            // }
            // catch (ResourceInUseException)
            // {
            //     Console.Error.WriteLine("Producer is quitting without creating stream " + myStreamName +
            //         " to put records into as a stream of the same name already exists.");
            //     Environment.Exit(1);
            // }


            // var testiterator = new GetShardIteratorRequest();
            // testiterator.StreamName = myStreamName;
            // testiterator.ShardId = "shardId-000000000000";
            // testiterator.ShardIteratorType = ShardIteratorType.TRIM_HORIZON;
            // Task<GetShardIteratorResponse> shardX = kinesisClient.GetShardIteratorAsync(testiterator);

            // Task<GetRecordsResponse> aa = kinesisClient.GetRecordsAsync(new GetRecordsRequest() { Limit = 5000, ShardIterator = shardX.Result.ToString() });


            //// Uncomment the following if you wish to delete the stream here.
            // Console.Error.WriteLine("Deleting stream : " + myStreamName);
            // DeleteStreamRequest deleteStreamReq = new DeleteStreamRequest();
            // deleteStreamReq.StreamName = "test_eagile";
            // try
            // {
            //     var deleteStreamResponse = kinesisClient.DeleteStreamAsync(deleteStreamReq);


            //     //kinesisClient.DeleteStream(deleteStreamReq);
            //     Console.Error.WriteLine("Stream is now being deleted : " + myStreamName);
            // }
            // catch (ResourceNotFoundException ex)
            // {

            //     Console.Error.WriteLine("Stream could not be found; " + ex);
            // }
            // catch (AmazonClientException ex)
            // {
            //     Console.Error.WriteLine("Error deleting stream; " + ex);
            // }
        }

        /// <summary>
        /// This method waits a maximum of 10 minutes for the specified stream to become active.
        /// <param name="myStreamName">Name of the stream whose active status is waited upon.</param>
        /// </summary>
        private static void WaitForStreamToBecomeAvailable(string myStreamName)
        {
            var deadline = DateTime.UtcNow + TimeSpan.FromMinutes(10);
            while (DateTime.UtcNow < deadline)
            {
                DescribeStreamRequest describeStreamReq = new DescribeStreamRequest();
                describeStreamReq.StreamName = myStreamName;
                var describeResult = kinesisClient.DescribeStreamAsync(describeStreamReq).Result;
                string streamStatus = describeResult.StreamDescription.StreamStatus;
                Console.Error.WriteLine("  - current state: " + streamStatus);
                if (streamStatus == StreamStatus.ACTIVE)
                {
                    return;
                }
                Thread.Sleep(TimeSpan.FromSeconds(20));
            }

            throw new Exception("Stream " + myStreamName + " never went active.");
        }

    }
}