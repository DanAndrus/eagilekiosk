﻿using System;
using System.Collections;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Eagile.RFID.SettingsXml
{
    // Custom settings attributes

    /// <summary>
    /// The root xml node name of a class, required for the base ESettings class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class XmlRootAttribute : ESettingsAttribute
    {
        /// <summary>
        /// The name of the root element
        /// </summary>
        public string ElementName { get; set; }
        public XmlRootAttribute() : base()
        {
        }
    }

    /// <summary>
    /// Add the setting to the settings xml as an xml element
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class XmlElementAttribute : ESettingsAttribute
    {
        /// <summary>
        /// The name of the element
        /// </summary>
        public string ElementName { get; set; }
        public XmlElementAttribute() : base()
        {
        }
    }

    /// <summary>
    /// Create an element surrounding the list elements
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class XmlListAttribute : ESettingsAttribute
    {
        /// <summary>
        /// The name of the surrounding element
        /// </summary>
        public string ListName { get; set; }
        public XmlListAttribute() : base()
        {
        }
    }

    /// <summary>
    /// Add the setting and value to the settings xml as an xml attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class XmlAttributeAttribute : ESettingsAttribute
    {
        public string AttributeName { get; set; }
        public XmlAttributeAttribute() : base()
        {
        }
    }

    /// <summary>
    /// Adds a comment description to the settings xml file (Only applies to XML Element properties)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class XmlCommentAttribute : ESettingsAttribute
    {
        /// <summary>
        /// Inline will append the comment after the element on the same line (does not apply to Lists or ESettings)
        /// </summary>
        public bool Inline { get; set; }
        /// <summary>
        /// Comment text to be added to the xml settings file
        /// </summary>
        public string Comment { get; private set; }
        public XmlCommentAttribute(string Comment) : base()
        {
            this.Comment = Comment;
        }
    }

    /// <summary>
    /// Specify a custom data parser for the setting and an error message for if parsing failed
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ParseAttribute : ESettingsAttribute
    {
        public ESettingsParser Parser { get; private set; }
        public string ErrorMessage { get; set; }
        public ParseAttribute(Type parserType) : base()
        {
            ErrorMessage = "Setting value could not be parsed";
            if (parserType != null && typeof(ESettingsParser).IsAssignableFrom(parserType))
                Parser = Activator.CreateInstance(parserType) as ESettingsParser;
            else
                Parser = new DefaultParser();
        }
        public ParseAttribute() : base()
        {
            ErrorMessage = "Setting value could not be parsed";
            Parser = new DefaultParser();
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class DefaultValueAttribute : ESettingsAttribute
    {
        public object Value { get; private set; }
        public DefaultValueAttribute(object value) : base()
        {
            Value = value;
        }
        public DefaultValueAttribute(params object[] values) : base()
        {
            Value = values;
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredAttribute : ESettingsValidationAttribute
    {
        // [Required]
        public string ErrorMessage { get; set; }
        public RequiredAttribute() : base()
        {
            ErrorMessage = "Setting is required";
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            if (string.IsNullOrEmpty(text))
                throw new ESettingsValidationException(propertyInfo.GetXmlElementName(), ErrorMessage);
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo)
        {
            if (settings == null)
                throw new ESettingsValidationException(propertyInfo.GetXmlElementName(), ErrorMessage);
        }
        public override void Validate(IList list, PropertyInfo propertyInfo)
        {
            if (list == null || list.Count == 0)
                throw new ESettingsValidationException(propertyInfo.GetXmlElementName(), ErrorMessage);
        }
    }


    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ProtectedAttribute : ESettingsAttribute
    {
        public ESettingsProtector Protector { get; private set; }
        public string ErrorMessage { get; set; }
        public ProtectedAttribute(Type protectorType = null) : base()
        {
            ErrorMessage = "Setting value is not protected";
            if (protectorType != null && typeof(ESettingsProtector).IsAssignableFrom(protectorType))
                Protector = Activator.CreateInstance(protectorType) as ESettingsProtector;
            else
                Protector = new DefaultProtector();
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MinimumValueAttribute : ESettingsValidationAttribute
    {
        public object MinimumValue { get; private set; }
        public string ErrorMessage { get; set; }
        public MinimumValueAttribute(int MinimumValue) : base()
        {
            ErrorMessage = "Setting is smaller than the minimum allowed value";
            this.MinimumValue = MinimumValue;
        }
        public MinimumValueAttribute(double MinimumValue) : base()
        {
            ErrorMessage = "Setting is smaller than the minimum allowed value";
            this.MinimumValue = MinimumValue;
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsArray)
            {
                foreach (string s in text.Split(','))
                {
                    ValidateValue(s, propertyInfo.PropertyType.GetElementType(), propertyInfo.GetXmlElementName());
                }
            }
            else
            {
                ValidateValue(text, propertyInfo.PropertyType, propertyInfo.GetXmlElementName());
            }
        }
        private void ValidateValue(string text, Type type, string elementName)
        {
            try
            {
                if (type == typeof(int))
                {
                    int intValue = int.Parse(text);
                    if (intValue < Convert.ToInt32(MinimumValue))
                        throw new ESettingsValidationException(elementName, ErrorMessage);
                }
                else if (type == typeof(double))
                {
                    double doubleValue = double.Parse(text);
                    if (doubleValue < Convert.ToDouble(MinimumValue))
                        throw new ESettingsValidationException(elementName, ErrorMessage);
                }
            }
            catch (ESettingsException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ESettingsValidationException(elementName, ErrorMessage, ex);
            }
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo) { } // N/A
        public override void Validate(IList list, PropertyInfo propertyInfo) { } // N/A
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MaximumValueAttribute : ESettingsValidationAttribute
    {
        public object MaximumValue { get; private set; }
        public string ErrorMessage { get; set; }
        public MaximumValueAttribute(int MaximumValue) : base()
        {
            ErrorMessage = "Setting is larger than the maximum allowed value";
            this.MaximumValue = MaximumValue;
        }
        public MaximumValueAttribute(double MaximumValue) : base()
        {
            ErrorMessage = "Setting is larger than the maximum allowed value";
            this.MaximumValue = MaximumValue;
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsArray)
            {
                foreach (string s in text.Split(','))
                {
                    ValidateValue(s, propertyInfo.PropertyType.GetElementType(), propertyInfo.GetXmlElementName());
                }
            }
            else
            {
                ValidateValue(text, propertyInfo.PropertyType, propertyInfo.GetXmlElementName());
            }
        }
        private void ValidateValue(string text, Type type, string elementName)
        {
            try
            {
                if (type == typeof(int))
                {
                    int intValue = int.Parse(text);
                    if (intValue > Convert.ToInt32(MaximumValue))
                        throw new ESettingsValidationException(elementName, ErrorMessage);
                }
                else if (type == typeof(double))
                {
                    double doubleValue = double.Parse(text);
                    if (doubleValue > Convert.ToDouble(MaximumValue))
                        throw new ESettingsValidationException(elementName, ErrorMessage);
                }
            }
            catch (ESettingsException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ESettingsValidationException(elementName, ErrorMessage, ex);
            }
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo) { } // N/A
        public override void Validate(IList list, PropertyInfo propertyInfo) { } // N/A
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MinimumLengthAttribute : ESettingsValidationAttribute
    {
        public int MinimumLength { get; private set; }
        public string ErrorMessage { get; set; }
        public MinimumLengthAttribute(int MinimumLength) : base()
        {
            ErrorMessage = "Setting is shorted than the minimum allowed length";
            this.MinimumLength = MinimumLength;
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsArray)
            {
                foreach (string s in text.Split(','))
                {
                    ValidateValue(s, propertyInfo.PropertyType.GetElementType(), propertyInfo.GetXmlElementName());
                }
            }
            else
            {
                ValidateValue(text, propertyInfo.PropertyType, propertyInfo.GetXmlElementName());
            }
        }
        private void ValidateValue(string text, Type type, string elementName)
        {
            if (text.Length < MinimumLength)
                throw new ESettingsValidationException(elementName, ErrorMessage);
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo) { } // N/A
        public override void Validate(IList list, PropertyInfo propertyInfo) { } // N/A
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MaximumLengthAttribute : ESettingsValidationAttribute
    {
        public int MaximumLength { get; private set; }
        public string ErrorMessage { get; set; }
        public MaximumLengthAttribute(int MaximumLength) : base()
        {
            ErrorMessage = "Setting is longer than the maximum allowed length";
            this.MaximumLength = MaximumLength;
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsArray)
            {
                foreach (string s in text.Split(','))
                {
                    ValidateValue(s, propertyInfo.PropertyType.GetElementType(), propertyInfo.GetXmlElementName());
                }
            }
            else
            {
                ValidateValue(text, propertyInfo.PropertyType, propertyInfo.GetXmlElementName());
            }
        }
        private void ValidateValue(string text, Type type, string elementName)
        {
            if (text.Length > MaximumLength)
                throw new ESettingsValidationException(elementName, ErrorMessage);
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo) { } // N/A
        public override void Validate(IList list, PropertyInfo propertyInfo) { } // N/A
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MinimumCountAttribute : ESettingsValidationAttribute
    {
        public int MinimumCount { get; private set; }
        public string ErrorMessage { get; set; }
        public MinimumCountAttribute(int MinimumCount) : base()
        {
            ErrorMessage = "Setting contains fewer elements than the minimum allowed count";
            this.MinimumCount = MinimumCount;
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsArray)
            {
                string[] split;
                if (string.IsNullOrEmpty(text))
                    split = new string[0];
                else
                    split = text.Split(',');

                if (split.Length < MinimumCount)
                    throw new ESettingsValidationException(propertyInfo.GetXmlElementName(), ErrorMessage);
            }
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo) { } // N/A
        public override void Validate(IList list, PropertyInfo propertyInfo)
        {
            if (list == null || list.Count < MinimumCount)
                throw new ESettingsValidationException(propertyInfo.GetXmlElementName(), ErrorMessage);
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MaximumCountAttribute : ESettingsValidationAttribute
    {
        public int MaximumCount { get; private set; }
        public string ErrorMessage { get; set; }
        public MaximumCountAttribute(int MaximumCount) : base()
        {
            ErrorMessage = "Setting contains more elements than the maximum allowed count";
            this.MaximumCount = MaximumCount;
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsArray)
            {
                string[] split;
                if (string.IsNullOrEmpty(text))
                    split = new string[0];
                else
                    split = text.Split(',');

                if (split.Length > MaximumCount)
                    throw new ESettingsValidationException(propertyInfo.GetXmlElementName(), ErrorMessage);
            }
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo) { } // N/A
        public override void Validate(IList list, PropertyInfo propertyInfo)
        {
            if (list != null && list.Count > MaximumCount)
                throw new ESettingsValidationException(propertyInfo.GetXmlElementName(), ErrorMessage);
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ValueFilterAttribute : ESettingsValidationAttribute
    {
        public FilterType FilterType { get; private set; }
        public object[] Values { get; private set; }
        public bool IgnoreCase { get; set; }
        public string ErrorMessage { get; set; }
        public ValueFilterAttribute(FilterType FilterType, params object[] Values) : base()
        {
            ErrorMessage = "Setting is not an allowed value";
            this.FilterType = FilterType;
            this.Values = Values;
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            ESettingsParser parser = propertyInfo.GetParser();
            if (propertyInfo.PropertyType.IsArray)
            {
                foreach (string s in text.Split(','))
                {
                    ValidateValue(s, propertyInfo.PropertyType.GetElementType(), propertyInfo.GetXmlElementName(), parser);
                }
            }
            else
            {
                ValidateValue(text, propertyInfo.PropertyType, propertyInfo.GetXmlElementName(), parser);
            }
        }
        private void ValidateValue(string text, Type type, string elementName, ESettingsParser parser)
        {
            string valueString = parser.ParseUnparse(text, type);
            if (FilterType == FilterType.ALLOW)
            {
                bool whitelisted = false;
                for (int i = 0; i < Values.Length; i++)
                {
                    string wlStr = parser.Unparse(parser.UnparseParse(Values[i], type), type);
                    if (string.Equals(wlStr, valueString, IgnoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal))
                    {
                        whitelisted = true;
                        break;
                    }
                }
                if (!whitelisted)
                {
                    throw new ESettingsValidationException(elementName, ErrorMessage);
                }
            }
            else
            {
                for (int i = 0; i < Values.Length; i++)
                {
                    string wlStr = parser.Unparse(parser.UnparseParse(Values[i], type), type);
                    if (string.Equals(wlStr, valueString, IgnoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal))
                    {
                        throw new ESettingsValidationException(elementName, ErrorMessage);
                    }
                }
            }
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo) { } // N/A
        public override void Validate(IList list, PropertyInfo propertyInfo) { } // N/A
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PatternFilterAttribute : ESettingsValidationAttribute
    {
        public FilterType FilterType { get; private set; }
        public string[] Patterns { get; private set; }
        public string ErrorMessage { get; set; }
        public RegexOptions Options { get; set; }
        public PatternFilterAttribute(FilterType FilterType, params string[] Patterns)
        {
            ErrorMessage = "Setting is not an allowed value";
            this.FilterType = FilterType;
            this.Patterns = Patterns;
        }
        public override void Validate(string text, PropertyInfo propertyInfo)
        {
            ESettingsParser parser = propertyInfo.GetParser();
            if (propertyInfo.PropertyType.IsArray)
            {
                foreach (string s in text.Split(','))
                {
                    ValidateValue(s, propertyInfo.PropertyType.GetElementType(), propertyInfo.GetXmlElementName(), parser);
                }
            }
            else
            {
                ValidateValue(text, propertyInfo.PropertyType, propertyInfo.GetXmlElementName(), parser);
            }
        }
        private void ValidateValue(string text, Type type, string elementName, ESettingsParser parser)
        {
            string valueString = parser.ParseUnparse(text, type);
            if (FilterType == FilterType.ALLOW)
            {
                bool matched = false;
                foreach (string pattern in Patterns)
                {
                    if (Regex.IsMatch(valueString, pattern, Options))
                    {
                        matched = true;
                        break;
                    }
                }
                if (!matched)
                {
                    throw new ESettingsValidationException(elementName, ErrorMessage);
                }
            }
            else
            {
                foreach (string pattern in Patterns)
                {
                    if (Regex.IsMatch(valueString, pattern, Options))
                    {
                        throw new ESettingsValidationException(elementName, ErrorMessage);
                    }
                }
            }
        }
        public override void Validate(ESettings settings, PropertyInfo propertyInfo) { } // N/A
        public override void Validate(IList list, PropertyInfo propertyInfo) { } // N/A
    }

    public enum FilterType
    {
        ALLOW,
        BLOCK,
    }

}
