﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EStreamWrapper.RFID.Com
{
    public abstract class EStreamServer : IDisposable
    {
        public int ConnectedClientCount { get { return GetClients().Count(p => p.IsConnected); } }
        public virtual bool IsRunning { get { return !_serverCt.IsCancellationRequested; } }

        public int BufferSize { get; set; }
        public int MaxClients { get; set; }
        public bool AutoRestartOnError { get; set; }
        public TimeSpan AutoRestartInterval { get; set; }

        public EStreamHeartbeatSettings HeartbeatSettings { get; set; }
        public EStreamMessageFramingSettings MessageFramingSettings { get; set; }

        private volatile int _lastClientID = 0;
        protected int _nextClientID { get { return Interlocked.Increment(ref _lastClientID); } }
        
        private readonly List<EStream> _clients = new List<EStream>();
        
        private System.Timers.Timer _listenTimer;

        protected volatile bool _listening = false;

        protected readonly object _serverLock = new object();
        private readonly object _clientsLock = new object();

        private CancellationTokenSource _serverCts = new CancellationTokenSource();
        protected CancellationToken _serverCt { get { return _serverCts.Token; } }

        public EStreamServer()
        {
            HeartbeatSettings = new EStreamHeartbeatSettings();
            MessageFramingSettings = new EStreamMessageFramingSettings();

            BufferSize = EStream.DEFAULT_BUFFER_SIZE;
            MaxClients = -1;
            AutoRestartOnError = true;
            AutoRestartInterval = new TimeSpan(0, 0, 1);

            _listenTimer = new System.Timers.Timer();
            _listenTimer.AutoReset = false;
            _listenTimer.Elapsed += _listenTimer_Elapsed;

            _serverCts.Cancel();
        }

        #region StartStop

        public virtual void Start()
        {
            lock (_serverLock)
            {
                if (_serverCts.IsCancellationRequested)
                {
                    _serverCts = new CancellationTokenSource();
                    BeginAcceptClient(TimeSpan.FromMilliseconds(1));
                }
            }
        }

        public virtual void Stop()
        {
            lock (_serverLock)
            {
                _listenTimer.Stop();
                _serverCts.Cancel();
            }
        }

        #endregion StartStop

        #region Timers

        private void _listenTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _listenTimer.Stop();
            BeginAcceptClient(_serverCt);
        }

        #endregion Timers

        #region  Listen

        protected void BeginAcceptClient(TimeSpan delay)
        {
            _listenTimer.Interval = delay.TotalMilliseconds;
            _listenTimer.Start();
        }

        protected abstract void BeginAcceptClient(CancellationToken ct);
        

        #endregion Listen

        #region PushBytes

        public List<Task> SendBytes(byte[] bytes, CancellationToken ct)
        {
            List<Task> tasks = new List<Task>();
            foreach (EStream client in GetClients())
            {
                try
                {
                    tasks.Add(client.SendBytes(bytes, ct));
                }
                catch (Exception ex)
                {
                    OnClientError(new EStreamExceptionEventArgs(client, ex));
                }
            }
            return tasks;
        }

        public List<Task> SendBytes(byte[] bytes, int clientId, CancellationToken ct)
        {
            List<Task> tasks = new List<Task>();
            IEnumerable<EStream> clients = GetClients().Where(p => p.ID == clientId);
            if (!clients.Any())
                throw new ArgumentException("Client does not exist", "clientId");
            foreach (EStream client in clients)
            {
                try
                {
                    tasks.Add(client.SendBytes(bytes, ct));
                }
                catch (Exception ex)
                {
                    OnClientError(new EStreamExceptionEventArgs(client, ex));
                }
            }
            return tasks;
        }

        #endregion PushBytes

        #region ClientList

        public virtual void Close()
        {
            IEnumerable<EStream> clients = GetClients();
            foreach (EStream client in clients)
            {
                client.Close();
            }
        }

        public virtual void Close(int clientId)
        {
            IEnumerable<EStream> clients = GetClients().Where(p => p.ID == clientId);
            if (!clients.Any())
                throw new ArgumentException("Client does not exist", "clientId");
            foreach (EStream client in clients)
            {
                client.Close();
            }
        }

        public virtual void Close(EStream clientStream)
        {
            IEnumerable<EStream> clients = GetClients().Where(p => p == clientStream);
            if (!clients.Any())
                throw new ArgumentException("Client does not exist", "clientId");
            foreach (EStream client in clients)
            {
                client.Close();
            }
        }

        public List<EStream> GetClients()
        {
            lock (_clientsLock)
            {
                return _clients.ToList();
            }
        }

        protected int GetClientCount()
        {
            lock (_clientsLock)
            {
                return _clients.Count;
            }
        }

        protected void AddClient(EStream client)
        {
            lock (_clientsLock)
            {
                _clients.Add(client);
            }
            AttatchListeners(client);
            client.Open();
        }

        protected void RemoveClient(EStream client)
        {
            lock (_clientsLock)
            {
                while (_clients.Contains(client))
                    _clients.Remove(client);
            }
            DetachListeners(client);
            try { client.Close(); } catch { }
        }

        protected void DisposeClients()
        {
            List<EStream> clients = new List<EStream>();
            lock (_clientsLock)
            {
                clients = _clients.ToList();
                _clients.Clear();
            }
            foreach (EStream client in clients)
            {
                DetachListeners(client);
                try { client.Dispose(); } catch { }
            }
        }

        protected virtual void AttatchListeners(EStream eStream)
        {
            eStream.Connected += Client_Connected;
            eStream.Disconnected += Client_Disconnected;
            eStream.HeartbeatSent += Client_HeartbeatSent;
            eStream.HeartbeatReceived += Client_HeartbeatReceived;
            eStream.BytesReceived += Client_BytesReceived;
            eStream.Error += Client_Error;
        }

        protected virtual void DetachListeners(EStream eStream)
        {
            eStream.Connected -= Client_Connected;
            eStream.Disconnected -= Client_Disconnected;
            eStream.HeartbeatSent -= Client_HeartbeatSent;
            eStream.HeartbeatReceived -= Client_HeartbeatReceived;
            eStream.BytesReceived -= Client_BytesReceived;
            eStream.Error -= Client_Error;
        }

        #endregion ClientList

        #region Client
        
        protected virtual void Client_Connected(object sender, EStreamEventArgs e)
        {
            OnClientConnected(e);
        }

        protected virtual void Client_Disconnected(object sender, EStreamEventArgs e)
        {
            RemoveClient(e.EStream);
            OnClientDisconnected(e);
            e.EStream.Dispose();

            BeginAcceptClient(TimeSpan.FromMilliseconds(1));
        }

        protected virtual void Client_HeartbeatSent(object sender, EStreamEventArgs e)
        {
            OnClientHeartbeatSent(e);
        }

        protected virtual void Client_HeartbeatReceived(object sender, EStreamEventArgs e)
        {
            OnClientHeartbeatReceived(e);
        }

        protected virtual void Client_BytesReceived(object sender, EStreamBytesEventArgs e)
        {
            OnClientBytesReceived(e);
        }

        protected virtual void Client_Error(object sender, EStreamExceptionEventArgs e)
        {
            OnClientError(e);
        }

        #endregion Client

        #region Events

        public event EventHandler<EStreamEventArgs> ClientConnected;
        protected virtual void OnClientConnected(EStreamEventArgs e)
        {
            EventHandler<EStreamEventArgs> handler = ClientConnected;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamEventArgs> ClientDisconnected;
        protected virtual void OnClientDisconnected(EStreamEventArgs e)
        {
            EventHandler<EStreamEventArgs> handler = ClientDisconnected;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamEventArgs> ClientHeartbeatSent;
        protected virtual void OnClientHeartbeatSent(EStreamEventArgs e)
        {
            EventHandler<EStreamEventArgs> handler = ClientHeartbeatSent;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamEventArgs> ClientHeartbeatReceived;
        protected virtual void OnClientHeartbeatReceived(EStreamEventArgs e)
        {
            EventHandler<EStreamEventArgs> handler = ClientHeartbeatReceived;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamBytesEventArgs> ClientBytesReceived;
        protected virtual void OnClientBytesReceived(EStreamBytesEventArgs e)
        {
            EventHandler<EStreamBytesEventArgs> handler = ClientBytesReceived;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamExceptionEventArgs> ClientError;
        protected virtual void OnClientError(EStreamExceptionEventArgs e)
        {
            EventHandler<EStreamExceptionEventArgs> handler = ClientError;
            if (handler != null) handler(this, e);
        }

        public event EventHandler<EStreamExceptionEventArgs> Error;
        protected virtual void OnError(EStream connection, Exception exception, CancellationToken ct = default(CancellationToken))
        {
            if (ct.IsCancellationRequested)
                return;
            EventHandler<EStreamExceptionEventArgs> handler = Error;
            if (handler != null) handler(this, new EStreamExceptionEventArgs(connection, exception));
        }

        public event EventHandler ListenerStarted;
        protected virtual void OnListenerStarted()
        {
            EventHandler handler = ListenerStarted;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public event EventHandler ListenerStopped;
        protected virtual void OnListenerStopped()
        {
            EventHandler handler = ListenerStopped;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        #endregion Events


        #region Dispose

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                try { Stop(); } catch { }
                try { DisposeClients(); } catch { }
                if (_listenTimer != null)
                {
                    try { _listenTimer.Stop(); } catch { }
                    try { _listenTimer.Elapsed -= _listenTimer_Elapsed; } catch { }
                    try { _listenTimer.Dispose(); } catch { }
                }
            }

            // Free any unmanaged objects here
            disposed = true;
        }
        ~EStreamServer()
        {
            Dispose(false);
        }

        #endregion Dispose
    }

}
