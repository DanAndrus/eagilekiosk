﻿using System;
using System.IO.Ports;

namespace EStreamWrapper.RFID.Com
{
    public class EComClient : EStreamClient
    {
        public readonly EComPort EComPort;

        public string CurrentPortName { get; protected set; }
        public string PortName { get { return EComPort.PortName; } }
        public int BaudRate { get { return EComPort.BaudRate; } }
        public Parity Parity { get { return EComPort.Parity; } }
        public int DataBits { get { return EComPort.DataBits; } }
        public StopBits StopBits { get { return EComPort.StopBits; } }
        public bool IsAutoPort { get { return string.Equals(PortName, AUTO_PORT, StringComparison.OrdinalIgnoreCase); } }

        protected string AUTO_PORT = "AUTO";
        protected string DEFAULT_AUTO_PORT = "COM0";

        public EComClient(EComPort eComPort)
            : base()
        {
            EComPort = eComPort;
            CurrentPortName = IsAutoPort ? DEFAULT_AUTO_PORT : PortName;
        }

        public EComClient(string comString)
            : base()
        {
            EComPort = new EComPort(comString);
            CurrentPortName = IsAutoPort ? DEFAULT_AUTO_PORT : PortName;
        }

        public EComClient(string portName = EComPort.DEFAULT_PORT_NAME, int baudRate = EComPort.DEFAULT_BAUD_RATE,
            Parity parity = EComPort.DEFAULT_PARITY, int dataBits = EComPort.DEFAULT_DATA_BITS, StopBits stopBits = EComPort.DEFAULT_STOP_BITS)
            : base()
        {
            EComPort = new EComPort(portName, baudRate, parity, dataBits, stopBits);
            CurrentPortName = IsAutoPort ? DEFAULT_AUTO_PORT : PortName;
        }

        protected override EStream CreateStream()
        {
            if (IsAutoPort)
            {
                CurrentPortName = GetAutoPort(CurrentPortName);
            }
            else
            {
                CurrentPortName = PortName;
            }
            return new EComStream(CurrentPortName, BaudRate, Parity, DataBits, StopBits,
                MessageFramingSettings, HeartbeatSettings, BufferSize);
        }

        protected virtual string GetAutoPort(string lastAutoPort = null)
        {
            string[] portNames = SerialPort.GetPortNames();
            Array.Sort(portNames);
            for (int i = 0; i < portNames.Length; i++)
            {
                if (string.Equals(lastAutoPort, portNames[i], StringComparison.OrdinalIgnoreCase))
                    return portNames[i];
            }
            if (portNames.Length > 0)
                return portNames[0];
            else
                return DEFAULT_AUTO_PORT;
        }
    }

    public struct EComPort
    {
        public const string DEFAULT_PORT_NAME = "COM1";
        public const int DEFAULT_BAUD_RATE = 9600;
        public const Parity DEFAULT_PARITY = Parity.None;
        public const int DEFAULT_DATA_BITS = 8;
        public const StopBits DEFAULT_STOP_BITS = StopBits.One;

        public string PortName;
        public int BaudRate;
        public Parity Parity;
        public int DataBits;
        public StopBits StopBits;

        public EComPort(string comString)
        {
            if (string.IsNullOrEmpty(comString))
                throw new ArgumentNullException("comPort");

            // port parameters passed inline
            string[] split = comString.Split(',');
            PortName = split.Length >= 1 ? split[0].Trim() : DEFAULT_PORT_NAME;
            BaudRate = split.Length >= 2 ? int.Parse(split[1].Trim()) : DEFAULT_BAUD_RATE;
            Parity = split.Length >= 3 ? (Parity)Enum.Parse(typeof(Parity), split[2].Trim(), true) : DEFAULT_PARITY;
            DataBits = split.Length >= 4 ? int.Parse(split[3].Trim()) : DEFAULT_DATA_BITS;
            StopBits = split.Length >= 5 ? (StopBits)Enum.Parse(typeof(StopBits), split[4].Trim(), true) : DEFAULT_STOP_BITS;

            if (string.IsNullOrEmpty(PortName))
                throw new ArgumentNullException("portName");
        }

        public EComPort(string portName = DEFAULT_PORT_NAME, int baudRate = DEFAULT_BAUD_RATE,
            Parity parity = DEFAULT_PARITY, int dataBits = DEFAULT_DATA_BITS, StopBits stopBits = DEFAULT_STOP_BITS)
        {

            if (string.IsNullOrEmpty(portName))
                throw new ArgumentNullException("portName");

            PortName = portName;
            BaudRate = baudRate;
            Parity = parity;
            DataBits = dataBits;
            StopBits = stopBits;
        }

        public EComPort(SerialPort serialPort)
        {
            if (serialPort == null)
                throw new ArgumentNullException("serialPort");

            PortName = serialPort.PortName;
            BaudRate = serialPort.BaudRate;
            Parity = serialPort.Parity;
            DataBits = serialPort.DataBits;
            StopBits = serialPort.StopBits;

            if (string.IsNullOrEmpty(PortName))
                throw new ArgumentNullException("portName");
        }

        public static bool operator ==(EComPort left, EComPort right)
        {
            return Equals(left, right);
        }
        public static bool operator !=(EComPort left, EComPort right)
        {
            return !Equals(left, right);
        }

        public static bool Equals(EComPort left, EComPort right)
        {
            if (ReferenceEquals(null, left) && ReferenceEquals(null, right)) return true;
            if (ReferenceEquals(null, left) || ReferenceEquals(null, right)) return false;
            return left.BaudRate == right.BaudRate
                && left.Parity == right.Parity
                && left.DataBits == right.DataBits
                && left.StopBits == right.StopBits
                && string.Equals(left.PortName, right.PortName, StringComparison.OrdinalIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            return obj is EComPort && Equals(this, (EComPort)obj);
        }

        public bool Equals(string comString)
        {
            try
            {
                return Equals(this, new EComPort(comString));
            }
            catch
            {
                return false;
            }
        }

        public override string ToString()
        {
            return PortName.ToUpperInvariant()
                + "," + BaudRate.ToString()
                + "," + Parity.ToString()
                + "," + DataBits.ToString()
                + "," + StopBits.ToString();
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public SerialPort SerialPort()
        {
            return new SerialPort(PortName, BaudRate, Parity, DataBits, StopBits);
        }
    }
}
