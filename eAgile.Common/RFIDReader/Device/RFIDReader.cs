﻿using System;
using System.IO.Ports;
using NdefLibrary.Ndef;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ThingMagic;
using ThingMagic.URA2.BL;

namespace RFIDReader
{
    public class RFIDReader : IDisposable
    {
        //public static string GetFirstScannerPort(string lastUsedPortName = null)
        //{
        //    // Get list of serial ports sorted by name but with the last used port name first if it exists
        //    List<string> portNames = SerialPort.GetPortNames()
        //        .OrderBy(portName => !string.Equals(portName, lastUsedPortName, StringComparison.OrdinalIgnoreCase)) // false comes before true
        //        .ThenBy(portName => portName).ToList();

        //    byte[] REVINF = GetMenuCommand("REVINF");
        //    string response = "REVINF" + (char)0x06; // ACK
        //    string buffer = string.Empty;

        //    Hex hex = REVINF.ToHex();

        //    for (int i = 0; i < portNames.Count; i++)
        //    {
        //        buffer = string.Empty;
        //        try
        //        {
        //            using (SerialPort port = new SerialPort(portNames[i]))
        //            {
        //                try
        //                {
        //                    port.ReadTimeout = port.WriteTimeout = 100;
        //                    port.Open();
        //                    port.Write(REVINF, 0, REVINF.Length);

        //                    for (int j = 0; j < 10; j++)
        //                    {
        //                        Thread.Sleep(10);
        //                        buffer += port.ReadExisting();
        //                        if (buffer.IndexOf(response, 0) >= 0)
        //                        {
        //                            return portNames[i];
        //                        }
        //                    }
        //                }
        //                finally
        //                {
        //                    port.Close();
        //                }
        //            }
        //        }
        //        catch (Exception)// ex)
        //        {
        //            //string msg = ex.Message;
        //        }
        //    }
        //    return null;
        //}

        public static byte[] GetMenuCommand(string command)
        {
            byte[] bytes = new byte[command.Length + 4];
            bytes[0] = 0x16; // SYN
            bytes[1] = 0x4D; // M
            bytes[2] = 0x0D; // CR
            for (int i = 0; i < command.Length; i++)
            {
                bytes[i + 3] = (byte)command[i];
            }
            bytes[bytes.Length - 1] = 0x2E; // .
            return bytes;
        }

        public static string GetFirstReaderPort(string lastUsedPortName = null)
        {
            // Get list of serial ports sorted by name but with the last used port name first if it exists
            List<string> portNames = SerialPort.GetPortNames()
                .OrderBy(portName => !string.Equals(portName, lastUsedPortName, StringComparison.OrdinalIgnoreCase)) // false comes before true
                .ThenBy(portName => portName).ToList();
            for (int i = 0; i < portNames.Count; i++)
            {
                try
                {
                    using (Reader reader = Reader.Create("eapi:///" + portNames[i]))
                    {
                        try
                        {
                            reader.ParamSet("/reader/transportTimeout", 100);
                            reader.Connect();
                            return portNames[i];
                        }
                        finally
                        {
                            reader.Destroy();
                        }
                    }
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }
            }
            return null;
        }

        
        private Reader RfidReader;

        public bool IsConnected => IsReaderConnected();

        public double ReadPowerDbm { get; set; } = 15;
        public double WritePowerDbm { get; set; } = 15;
        public int ConnectTimeout { get; set; } = 500;
        public List<int> UseAntennas { get; set; } = new List<int>() { 1 };

        public int MinPower { get; private set; }
        public int MaxPower { get; private set; }

        public string ModelName { get; private set; }
        public int[] PortList { get; private set; } = new int[] { };

        public string PortName { get; private set; } = "COM1";

        private Dictionary<string, EPCReport> ReadAsyncList = new Dictionary<string, EPCReport>();

        private readonly SemaphoreSlim ReaderLock = new SemaphoreSlim(1, 1);
        private CancellationTokenSource Cts = new CancellationTokenSource();
        private bool disposed = false;
        

        public RFIDReader(string portName = "COM1")
        {
            PortName = portName;


        }

        ~RFIDReader()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                Cts.Cancel();
                Disconnect();
            }
            disposed = true;
        }

        public async Task ConnectAsync(string portName, CancellationToken ct)
        {
            await Task.Run(() => Connect(portName, ct), ct).ConfigureAwait(false);
        }

        public void Connect(string portName, CancellationToken ct)
        {
            PortName = portName;
            Connect(ct);
        }

        public async Task ConnectAsync(CancellationToken ct)
        {
            await Task.Run(() => Connect(ct), ct).ConfigureAwait(false);
        }

        public void Connect(CancellationToken ct)
        {
            ReaderLock.Wait(ct);
            try
            {
                
                Disconnect();
                RfidReader = Reader.Create("eapi:///" + PortName);

                RfidReader.ParamSet("/reader/transportTimeout", ConnectTimeout);
                RfidReader.Connect(); // Connect to reader

                ModelName = (string)RfidReader.ParamGet("/reader/version/model");
                PortList = (int[])RfidReader.ParamGet("/reader/antenna/portList");
                MinPower = (int)RfidReader.ParamGet("/reader/radio/powerMin") / 100;
                MaxPower = (int)RfidReader.ParamGet("/reader/radio/powerMax") / 100;

                RfidReader.ParamSet("/reader/tagReadData/uniqueByData", false); // Allow non-unique EPCs
                RfidReader.ParamSet("/reader/region/id", Reader.Region.NA); // Set reader region

                if (ReadPowerDbm > MaxPower) ReadPowerDbm = MaxPower;
                if (ReadPowerDbm < MinPower) ReadPowerDbm = MinPower;
                RfidReader.ParamSet("/reader/radio/readPower", (int)(ReadPowerDbm * 100)); // Set read power in centidBm

                if (WritePowerDbm > MaxPower) WritePowerDbm = MaxPower;
                if (WritePowerDbm < MinPower) WritePowerDbm = MinPower;
                RfidReader.ParamSet("/reader/radio/writePower", (int)(WritePowerDbm * 100)); // Set write power in centidBm
            }
            finally
            {
                ReaderLock.Release();
            }
        }

        public void Disconnect()
        {
            if (RfidReader != null)
            {
                try { RfidReader?.StopReading(); } catch { }
                try { RfidReader?.Destroy(); } catch { }
                RfidReader = null;
            }
        }


        


        public async Task<List<EPCReport>> ReadAsync(int timeout, CancellationToken ct)
        {
            List<EPCReport> reads = new List<EPCReport>();
            //await ReaderLock.WaitAsync(ct).ConfigureAwait(false);
            try
            {
                // Read EPCs
                SimpleReadPlan plan = new SimpleReadPlan(UseAntennas, TagProtocol.GEN2);
                RfidReader.ParamSet("/reader/read/plan", plan);
                ReadAsyncList.Clear();
                RfidReader.StartReading();
                RfidReader.TagRead += ReadAsyncListener;
                //await (Task.Delay(timeout, ct)).ConfigureAwait(false);
                //RfidReader.StopReading();
                reads = ReadAsyncList.Values.ToList();
                ReadAsyncList.Clear();
            }
            finally
            {
              // try { RfidReader.TagRead -= ReadAsyncListener; } catch { }
              //ReaderLock.Release();
            }
            return reads;
        }

        private void ReadAsyncListener(object sender, TagReadDataEventArgs e)
        {
            if (ReadAsyncList.ContainsKey(e.TagReadData.EpcString))
            {
                ReadAsyncList[e.TagReadData.EpcString].ReadCount++;

                ReadAsyncList[e.TagReadData.EpcString].TimeStamp = e.TagReadData.Time.ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                ReadAsyncList.Add(e.TagReadData.EpcString, new EPCReport(e.TagReadData.EpcString, e.TagReadData.Time.ToString(System.Globalization.CultureInfo.InvariantCulture), "",""));
            }
        }

        public List<EPCReport> Read(int timeout, List<string> currentlist)
        {
            ReaderLock.Wait();
            
            try
            {
                // Read EPCs
                SimpleReadPlan plan = new SimpleReadPlan(UseAntennas, TagProtocol.GEN2);
                RfidReader.ParamSet("/reader/read/plan", plan);
                RfidReader.ParamSet("/reader/tagop/antenna", 2);
                TagReadData[] tagreads = RfidReader.Read(timeout);
                List<EPCReport> reads = new List<EPCReport>();
                foreach (TagReadData trd in tagreads)
                {
                    if(trd.EpcString.StartsWith ("01") )
                    { 
                        bool Allitemsmetrule = currentlist.IndexOf(trd.EpcString) == -1;
                        if (Allitemsmetrule)
                        {
                            TagFilter searchSelect = null;
                            var model = "";
                            ReadTagMemory readTagMem = new ReadTagMemory(RfidReader, model);

                            ushort[] tidData = null;
                            searchSelect = new TagData(trd.Epc);
                            readTagMem.ReadTagMemoryData(Gen2.Bank.TID, searchSelect, ref tidData);
                            var newTID = ParseTIDMemData(tidData);
                            var newEPC = HexStringToAsciiString(trd.EpcString);
                            reads.Add(new EPCReport(newEPC, trd.Time.ToString(System.Globalization.CultureInfo.InvariantCulture), newTID, trd.EpcString));
                        }
                    }
                }

                return reads;
            }
            finally
            {
                ReaderLock.Release();
            }
        }
    
        public static string HexStringToAsciiString(string hexString)
        {
            string StrValue = "";
            
            if (hexString.Length > 0)
            {
                //hexString = hexString.Substring(2, hexString.Length);
                while (hexString.Length > 0)
                {

                    var aa = System.Convert.ToChar(System.Convert.ToUInt32(hexString.Substring(0, 2), 16)).ToString();
                    if(!aa.Contains("\u0001") )
                    {
                        StrValue += aa;
                    }
                    //    StrValue += System.Convert.ToChar(System.Convert.ToUInt32(hexString.Substring(0, 2), 16)).ToString();
                    hexString = hexString.Substring(2, hexString.Length - 2);
                }

                return StrValue;
            }
            else
            {
                return StrValue;
            }
        }

        private string ParseTIDMemData(ushort[] tidData)
        {
            string tidBankData = string.Empty;
            if (null != tidData)
                tidBankData = ByteFormat.ToHex(ByteConv.ConvertFromUshortArray(tidData), "", " ");

            if (tidBankData.Length > 0)
            {
                return tidBankData;
            }
            return string.Empty; 
        }


        public string ReadFirstUrl(int duration)
        {
            ReaderLock.Wait();
            try
            {
                SimpleReadPlan plan = new SimpleReadPlan(UseAntennas, TagProtocol.GEN2);
                RfidReader.ParamSet("/reader/read/plan", plan);
                TagReadData[] tagreads = RfidReader.Read(duration);
                foreach (string epc in tagreads.Select(p => p.EpcString).Distinct().OrderBy(p => p))
                {
                    try
                    {
                        //TagOp op = new Gen2.ReadData(Gen2.Bank.USER, 38, 2);
                        //TagFilter filter = new TagData(epc);
                        //SimpleReadPlan readData = new SimpleReadPlan(UseAntennas, TagProtocol.GEN2, filter, op, 1000);
                        //RfidReader.ParamSet("/reader/read/plan", readData);

                        // Get capability container
                        TagOp op = new Gen2.ReadData(Gen2.Bank.USER, 38, 2);
                        TagFilter filter = new TagData(epc);
                        RfidReader.ParamSet("/reader/tagop/antenna", UseAntennas.First());
                        ushort[] data = (ushort[])RfidReader.ExecuteTagOp(op, filter);
                        byte[] ccBytes = ByteConv.ConvertFromUshortArray(data);
                        string cchex = ByteFormat.ToHex(ccBytes, "", "");
                        if (ccBytes[0] != 0xE1)
                            throw new Exception("No NDEF Magic Number");

                        byte ndefMaxSize = (byte)(ccBytes[2] * 8);
                        op = new Gen2.ReadData(Gen2.Bank.USER, 40, (byte)(ndefMaxSize / 2));
                        data = (ushort[])RfidReader.ExecuteTagOp(op, filter);
                        byte[] ndefBytes = ByteConv.ConvertFromUshortArray(data);
                        string hex = ByteFormat.ToHex(ndefBytes, "", "");

                        Dictionary<byte, byte[]> tlvBlocks = new Dictionary<byte, byte[]>();
                        for (int i = 0; i < ndefBytes.Length; i++)
                        {
                            byte tag = ndefBytes[i];

                            if (tag == 0x00) continue; // NULL TLV
                            if (tag == 0xFE) break; // Terminator TLV

                            uint valueLength = ndefBytes[i + 1];
                            int valueStart = i + 2;
                            if (valueLength == 0xFF)
                            {
                                valueLength = ByteConv.ToU32(new byte[] { ndefBytes[i + 2], ndefBytes[i + 3] }, 0);
                                valueStart = i + 4;
                            }
                            byte[] valueBytes = new byte[valueLength];
                            Array.Copy(ndefBytes, valueStart, valueBytes, 0, valueLength);
                            tlvBlocks.Add(tag, valueBytes);
                            i = (int)(valueStart + valueLength - 1);
                        }

                        byte[] ndefMsgBytes = tlvBlocks.FirstOrDefault(tlv => tlv.Key == 0x03).Value; // T=0x03 means NDEF message

                        NdefMessage msg = NdefMessage.FromByteArray(ndefMsgBytes);
                        foreach (NdefRecord record in msg)
                        {
                            try
                            {
                                if (NdefUriRecord.IsRecordType(record))
                                {
                                    NdefUriRecord uriRecord = new NdefUriRecord(record);
                                    if (!String.IsNullOrEmpty(uriRecord.Uri))
                                        return uriRecord.Uri;
                                }
                                else if (NdefTextRecord.IsRecordType(record))
                                {
                                    NdefTextRecord textRecord = new NdefTextRecord(record);
                                    string s = textRecord.Text;

                                    NdefTextRecord newTextRecord = new NdefTextRecord();
                                    newTextRecord.Text = s;

                                    NdefMessage newMessage = new NdefMessage();
                                    newMessage.Add(newTextRecord);

                                    byte[] newNdefMsgBytes = newMessage.ToByteArray();


                                }
                            }
                            catch { }
                        }
                    }
                    catch (Exception ex)
                    {
                        string e = ex.Message;
                    }
                }
                return null;
            }
            finally
            {
                ReaderLock.Release();
            }
        }
        public struct TLV
        {
            public byte TagField;
            public byte[] Value;
        }

        private bool IsReaderConnected()
        {
            try
            {
                ReaderLock.Wait();
                try
                {
                    if (RfidReader != null)
                    {
                        RfidReader.GpiGet();
                        return true;
                    }
                    return false;
                }
                finally
                {
                    ReaderLock.Release();
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void RfidReader_Transport(object sender, TransportListenerEventArgs e)
        {
        }

        private void RfidReader_TagRead(object sender, TagReadDataEventArgs e)
        {
        }

        private void RfidReader_StatusListener(object sender, StatusReportEventArgs e)
        {
        }

        private void RfidReader_StatsListener(object sender, StatsReportEventArgs e)
        {
        }

        private void RfidReader_ReadException(object sender, ReaderExceptionEventArgs e)
        {
        }

        private void RfidReader_ReadAuthentication(object sender, ReadAuthenticationEventArgs e)
        {
        }

        private void RfidReader_Log(string message)
        {
        }

    }

    public delegate void EPCReportedHandler(object sender, EPCReport e);
    public class EPCReport : EventArgs
    {
        public string EPC { get; set; }
        public string RawEPC { get; set; }
        public string TimeStamp { get; set; }
        public int ReadCount { get; set; }
        public string TID { get; set; }
        

        public EPCReport(string epc, string timestamp, string tid ,string rawEPC , int readCount = 1)
        {
            EPC = epc;
            TimeStamp = timestamp;
            ReadCount = readCount;
            RawEPC = rawEPC;
            TID = tid;
        }
    }
}
