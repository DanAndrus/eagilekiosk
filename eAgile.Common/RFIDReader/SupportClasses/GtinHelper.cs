﻿using Eagile.RFID.Extentions.Byte;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RFIDReader.SupportClasses
{
    public enum FilterValues : byte
    {
        AllOthers = 0x00, // 000
        RetailConsumerTradeItem = 0x01, // 001
        StandardTradeItemGrouping = 0x02, // 010
        SingleShippingConsumerTradeItem = 0x03, // 011
        //Reserved = 0x04, // 100
        //Reserved = 0x05, // 101
        UnitLoad = 0x06, // 110
        //Reserved = 0x07, // 111
    }

    public static class GTINHelper
    {

    }

    public class SGTIN96
    {
        public const byte Header = 0x30; // 00110000

        public FilterValues FilterValue { get; set; }
        public string CompanyPrefix { get; set; }
        public string ItemReference { get; set; }
        public string SerialNumber { get; set; }
        public string GTIN14 { get; set; }

        public SGTIN96(Hex epc)
        {
            Bit[] epcBits = epc.ToBits();
            if (epcBits.Length != 96)
                throw new ArgumentException("Invalid Length");

            if (epcBits.ToByte(0, 8) != Header)
                throw new ArgumentException("Invalid Header");

            byte filterValueByte = epcBits.ToByte(8, 3);
            byte partitionValueByte = epcBits.ToByte(11, 3);

            if (Enum.IsDefined(typeof(FilterValues), filterValueByte))
                FilterValue = (FilterValues)filterValueByte;
            else
                throw new ArgumentException("Invalid Filter Value");

            PartitionValue pv = PartitionValue.FromByte(partitionValueByte);

            //long companyPrefix = epcBits.ToBytes(14, pv.CompanyBits).ToLong();
            long companyPrefix = epcBits.ToInt64(14, pv.CompanyBits);
            if (companyPrefix >= Math.Pow(10, pv.CompanyDigits))
                throw new ArgumentException("Invalid Company Prefix Length");
            CompanyPrefix = companyPrefix.ToString().PadLeft(pv.CompanyDigits, '0');

            //long itemReference = epcBits.ToBytes(14 + pv.CompanyBits, pv.ItemBits).ToLong();
            long itemReference = epcBits.ToInt64(14 + pv.CompanyBits, pv.ItemBits);
            if (itemReference >= Math.Pow(10, pv.ItemDigits))
                throw new ArgumentException("Invalid Item Reference Length");
            ItemReference = itemReference.ToString().PadLeft(pv.ItemDigits, '0');

            GTIN14 = ItemReference.Substring(0, 1) + CompanyPrefix + ItemReference.Substring(1);
            int[] d = new int[14]; // 1 based...
            for (int i = 0; i < GTIN14.Length; i++)
            {
                d[i + 1] = (int)char.GetNumericValue(GTIN14[i]);
            }
            int checkDigit = 10 - (3 * (d[1] + d[3] + d[5] + d[7] + d[9] + d[11] + d[13])
                + d[2] + d[4] + d[6] + d[8] + d[10] + d[12]) % 10;
            GTIN14 += checkDigit.ToString();
            // Verified with https://www.gs1us.org/tools/epc-encoder-decoder

            //long serialNumber = epcBits.ToBytes(58, 38).ToLong();
            long serialNumber = epcBits.ToInt64(58, 38);
            SerialNumber = serialNumber.ToString();
        }

        private struct PartitionValue
        {
            public readonly int CompanyBits;
            public readonly int CompanyDigits;
            public readonly int ItemBits;
            public readonly int ItemDigits;
            public PartitionValue(int companyPrefixBits, int companyPrefixDigits, int itemReferenceBits, int itemReferenceDigits)
            {
                CompanyBits = companyPrefixBits;
                CompanyDigits = companyPrefixDigits;
                ItemBits = itemReferenceBits;
                ItemDigits = itemReferenceDigits;
            }
            public static PartitionValue FromByte(byte partitionValueByte)
            {
                switch (partitionValueByte)
                {
                    case 0x00:
                        return new PartitionValue(40, 12, 4, 1);
                    case 0x01:
                        return new PartitionValue(37, 11, 7, 2);
                    case 0x02:
                        return new PartitionValue(34, 10, 10, 3);
                    case 0x03:
                        return new PartitionValue(30, 9, 14, 4);
                    case 0x04:
                        return new PartitionValue(27, 8, 17, 5);
                    case 0x05:
                        return new PartitionValue(24, 7, 20, 6);
                    case 0x06:
                        return new PartitionValue(20, 6, 24, 7);
                    default:
                        throw new ArgumentException("Invalid Partition Value");
                }
            }
        }
    }


}
