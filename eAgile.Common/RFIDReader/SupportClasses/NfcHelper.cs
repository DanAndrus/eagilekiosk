﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RFIDReader.SupportClasses
{
    public class NfcHelper
    {

    }

    public class NdefCapabilityContainer
    {
        public byte Byte0_MagicNumber { get; set; }
        public byte Byte1_Version { get; set; }
        public byte Byte2_MLEN { get; set; }
        public byte Byte3_Feature { get; set; }

        public int MajorVersion { get; set; }
        public int MinorVersion { get; set; }
    }
}
