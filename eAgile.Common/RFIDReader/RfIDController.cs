﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Threading.Tasks;
using DFD.RFID;
using Eagile.RFID.SettingsXml;
using System.IO.Ports;
using System.Linq;
using System.ComponentModel;

namespace RFIDReader
{
    public class RfIDController
    {

        private BackgroundWorker worker = new BackgroundWorker();
        
        public RFIDReader RfidReader = null;
        private readonly SemaphoreSlim RfidReaderLock = new SemaphoreSlim(1, 1);
        private CancellationTokenSource RfidReaderCts = new CancellationTokenSource();
        private DFDSettings Settings { get; set; }
        
        public static DirectoryInfo SettingsFolder = new DirectoryInfo(@"C:\ProgramData\eAgile\AssociationKiosk\Settings");
        public static FileInfo SettingsFile = new FileInfo(Path.Combine(SettingsFolder.FullName, "Settings.xml"));
        private string ReaderErrorMsg { get; set; }
        private string SettingsErrorMsg { get; set; }
        private string portName { get; set; }
        private Mode CurrentMode = Mode.OFF;
        public enum Mode
        {
            OFF, 
            NFC, // not used
            INVENTORY,
        }
        public RfIDController(Mode currentMode)
        {
            //RfidReader = new RFIDReader();
        
            CurrentMode = currentMode;

            Settings = new DFD.RFID.DFDSettings();
            Settings.SetCreateIfNotFound(true);
            Settings.SetProtectIfUnprotected(true);
            Settings.SettingsChanged += Settings_SettingsChanged;
            Settings.StartMonitoring(SettingsFile, TimeSpan.FromSeconds(5));
        }

        private void Settings_SettingsChanged(object sender, ESettingsChangedEventArgs e)
        {

            if (e.Error != null)
            {
                SettingsErrorMsg = "Application settings could not be loaded:";
                SettingsErrorMsg += Environment.NewLine;
                SettingsErrorMsg += e.Error.Message;
            }
            else if (e.ESettingsAggregateException != null)
            {
                SettingsErrorMsg = "Invalid Settings:";
                foreach (ESettingsException ese in e.ESettingsAggregateException.InnerExceptions)
                {
                    SettingsErrorMsg += Environment.NewLine;
                    SettingsErrorMsg += ese.SettingName + ": " + ese.Message;
                }
            }
            else
            {
                SettingsErrorMsg = String.Empty;
            }

            if (e.OldSettings != null && e.OldSettings is DFDSettings oldSettings)
            {


            }
        }

   

        public void CloseRFIDReader()
        {
            //worker.CancelAsync();
            try
            {
                if(RfidReader != null)
                { 
                    RfidReader.Disconnect();
                    RfidReader.Dispose();
                    RfidReader = null;
                }

            }
            catch
            {

            }
        }

        public async void OpenRFIDReader()
        {
            RfidReader = new RFIDReader();

            var ct = RfidReaderCts.Token;

            if (Settings?.RfidPort != null && Settings.RfidPort.StartsWith("AUTO", StringComparison.OrdinalIgnoreCase))
            {
                portName = RFIDReader.GetFirstReaderPort(Settings.AutoRfidPort);
            }
            else
            {
                portName = SerialPort.GetPortNames().FirstOrDefault(p => String.Equals(p, Settings.RfidPort, StringComparison.OrdinalIgnoreCase));
            }


            if (String.IsNullOrEmpty(portName))
            {
                // ReaderErrorMsg = "RFID Reader port is invalid";
                portName = "COM4";
                //return;

            }
            else
            {
                Settings.AutoRfidPort = portName;
                RfidReader = new RFIDReader(portName);
                RfidReader.UseAntennas = new List<int>() { Settings.Antenna };
                if (CurrentMode == Mode.NFC)
                {
                    RfidReader.ReadPowerDbm = Settings.HFDemo.Power;
                }
                else //if (CurrentMode == Mode.INVENTORY)
                {
                    RfidReader.ReadPowerDbm = Settings.UHFDemo.Power;
                }

                await RfidReader.ConnectAsync(RfidReaderCts.Token);
                //ReaderErrorMsg = String.Empty;
            }

            if (Settings.UHFDemo.Power != RfidReader.ReadPowerDbm)
            {
                RfidReader?.Dispose();
                RfidReader = null;
                throw new OperationCanceledException();
            }


            await Task.Delay(Settings.UHFDemo.Interval, ct);


            worker.WorkerSupportsCancellation = true;

            if (worker.IsBusy)
            {
              //  worker.CancelAsync();

                // wait for the worker to finish, so it can be reused.
               /// _resetEvent.WaitOne(); // will block until _resetEvent.Set() call made

                worker = new BackgroundWorker();
                worker.DoWork += Worker_DoWork;

            }
            else
            {
                worker.DoWork += Worker_DoWork;
                worker.RunWorkerAsync();

            }



        }

        private AutoResetEvent _resetEvent = new AutoResetEvent(false);

        private  void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var ct = RfidReaderCts.Token;
            var CurrentReadList = new List<string>();
            while (RfidReader != null)
            {
                try
                {
                    List<EPCReport> epcReports =  RfidReader.Read(1, CurrentReadList);

                    if (epcReports.Count > 0)
                    {
                        foreach (var found in epcReports)
                        {
                            bool Allitemsmetrule = CurrentReadList.IndexOf(found.EPC) == -1;
                            if (Allitemsmetrule)
                            {
                                CurrentReadList.Add(found.EPC);
                                OnChangeRFIDOccurred(epcReports);
                            }
                            else
                            {

                            }
                        }
                    }
                }
                catch (Exception)
                {

                }

            }
        }
        
        public event EventHandler<List<EPCReport>> RFIDEvent;
        protected virtual void OnChangeRFIDOccurred(List<EPCReport> newRFIDList)
        {
            try { RFIDEvent?.Invoke(this, newRFIDList); } catch { }
        }
               

    }
}
