﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAgile.Extentions.Byte
{
    public enum ByteOrder
    {
        BigEndian,
        LittleEndian,
    }

    public static class ByteExtensions
    {
        // all numeric conversions assumed to be Big Endian

        private const int BITS_PER_BYTE = 8;
        private const int BYTES_PER_INT16 = sizeof(Int16); // 2
        private const int BYTES_PER_INT32 = sizeof(Int32); // 4
        private const int BYTES_PER_INT64 = sizeof(Int64); // 8


        /// <summary>
        /// Treat string as Hex object, no conversion is applied to the string as it is assumed to already be hexidecimal characters
        /// </summary>
        /// <param name="hexString">String of hexidecimal characters</param>
        /// <returns>Hexidecimal value</returns>
        public static Hex AsHex(this string hexString)
        {
            if (hexString == null)
                return null;

            return new Hex(hexString);
        }

        /// <summary>
        /// Treat Hex object as a string, no conversion is applied to the object
        /// </summary>
        /// <param name="hex">Hexidecimal value</param>
        /// <returns>String of hexidecimal characters</returns>
        public static string AsString(this Hex hex)
        {
            if (hex == null)
                return null;

            return hex.Value;
        }

        /// <summary>
        /// Converts ASCII text to a hexidecimal value
        /// </summary>
        /// <param name="ascii">ASCII text</param>
        /// <returns>Hexidecimal value</returns>
        public static Hex ToHex(this string ascii)
        {
            if (ascii == null)
                throw new ArgumentNullException("ascii");

            return ascii.ToBytes().ToHex();
        }
        /// <summary>
        /// Converts ASCII text to an array of bytes
        /// </summary>
        /// <param name="ascii">ASCII text</param>
        /// <returns>Array of bytes</returns>
        public static byte[] ToBytes(this string ascii)
        {
            if (ascii == null)
                throw new ArgumentNullException("ascii");

            byte[] b = new byte[ascii.Length];
            for (int i = 0; i < ascii.Length; i++)
            {
                if (ascii[i] > byte.MaxValue)
                    throw new ArgumentOutOfRangeException("ascii");
                b[i] = (byte)ascii[i];
            }
            return b;
        }


        /// <summary>
        /// Converts hexidecimal value to ASCII text
        /// </summary>
        /// <param name="hex">Hexidecimal value</param>
        /// <returns>ASCII text</returns>
        public static string ToAscii(this Hex hex)
        {
            if (hex == null || hex.Value == null)
                throw new ArgumentNullException("hex");

            return hex.ToBytes().ToAscii();
        }
        /// <summary>
        /// Converts hexidecimal value to numeric value
        /// </summary>
        /// <param name="hex">Hexidecimal value</param>
        /// <returns>Numeric value</returns>
        public static Int64 ToInt64(this Hex hex)
        {
            if (hex == null || hex.Value == null)
                throw new ArgumentNullException("hex");

            return hex.ToBytes().ToInt64();
        }
        /// <summary>
        /// Converts hexidecimal value to an array of bytes
        /// </summary>
        /// <param name="hex">Hexidecimal value</param>
        /// <returns>Array of bytes</returns>
        public static byte[] ToBytes(this Hex hex)
        {
            if (hex == null || hex.Value == null)
                throw new ArgumentNullException("hex");

            if (hex.Length % 2 != 0)
                hex = new Hex("0" + hex);

            byte[] bytes = new byte[hex.Length / 2];
            byte h1, h2;
            for (int b = 0, h = 0; h < hex.Length; b++, h++)
            {
                h1 = (byte)hex[h];
                if (h1 >= 0x30 && h1 <= 0x39) h1 -= 0x30; // 0-9
                else if (h1 >= 0x41 && h1 <= 0x46) h1 -= 0x37; // A-F
                else if (h1 >= 0x61 && h1 <= 0x66) h1 -= 0x57; // a-f
                else throw new ArgumentException("Invalid character: " + hex[h]);
                h2 = (byte)hex[++h];
                if (h2 >= 0x30 && h2 <= 0x39) h2 -= 0x30; // 0-9
                else if (h2 >= 0x41 && h2 <= 0x46) h2 -= 0x37; // A-F
                else if (h2 >= 0x61 && h2 <= 0x66) h2 -= 0x57; // a-f
                else throw new ArgumentException("Invalid character: " + hex[h]);
                bytes[b] = (byte)((h1 << 4) + h2);
            }
            return bytes;
        }
        /// <summary>
        /// Converts hexidecimal value to an array of bits
        /// </summary>
        /// <param name="hex">Hexidecimal value</param>
        /// <returns>Array of bits</returns>
        public static Bit[] ToBits(this Hex hex)
        {
            if (hex == null || hex.Value == null)
                throw new ArgumentNullException("hex");

            return hex.ToBytes().ToBits();
        }


        /// <summary>
        /// Converts numeric value to a hexidecimal value
        /// </summary>
        /// <param name="num">Numeric value</param>
        /// <returns>Hexidecimal value</returns>
        public static Hex ToHex(this Int64 num, int length = BYTES_PER_INT64 * 2)
        {
            if (num < 0)
                throw new NotImplementedException();
            if (length < 0)
                throw new ArgumentOutOfRangeException("length");

            Hex hex = num.ToBytes().ToHex();
            if (length == hex.Length)
                return hex;
            else if (length > hex.Length)
                return hex.PadLeft(length, '0');
            else // if (length < hex.Length)
            {
                if (hex.AsString().Substring(0, hex.Length - length).Any(c => c != '0'))
                    throw new ArgumentOutOfRangeException("length", "Not enough characters specified");
                return hex.Substring(hex.Length - length);
            }
        }
        /// <summary>
        /// Converts numeric value to an array of bytes with the specified length
        /// </summary>
        /// <param name="num">Numeric value</param>
        /// <param name="length">The desired length of the returned array</param>
        /// <returns>Array of bytes</returns>
        public static byte[] ToBytes(this Int64 num, int length = BYTES_PER_INT64)
        {
            if (num < 0)
                throw new NotImplementedException();
            if (length < 0)
                throw new ArgumentOutOfRangeException("length");
            if (length < BYTES_PER_INT64 && ((1L << (length * BITS_PER_BYTE)) - 1) < num)
                throw new ArgumentOutOfRangeException("length", "Not enough bytes specified");

            byte[] bytes = new byte[length];
            for (int i = 0, b = (bytes.Length - 1); b >= 0; i++, b--)
            {
                bytes[b] = (byte)(num >> ((BITS_PER_BYTE * i) & 0xFF));
            }
            return bytes;
        }
        /// <summary>
        /// Converts numeric value to an array of bits with the specified length
        /// </summary>
        /// <param name="num">Numeric value</param>
        /// <param name="length">The desired length of the returned array</param>
        /// <returns>Array of bits</returns>
        public static Bit[] ToBits(this Int64 num, int length = BYTES_PER_INT64 * BITS_PER_BYTE)
        {
            if (num < 0)
                throw new NotImplementedException();
            if (length < 0)
                throw new ArgumentOutOfRangeException("length");

            return num.ToBytes().ToBits(length);
        }


        /// <summary>
        /// Converts byte value to an array of bits with the specified length
        /// </summary>
        /// <param name="num">Byte value</param>
        /// <param name="length">The desired length of the returned array</param>
        /// <returns>Array of bits</returns>
        public static Bit[] ToBits(this byte num, int length = BITS_PER_BYTE)
        {
            if (num < 0)
                throw new NotImplementedException();
            if (length < 0)
                throw new ArgumentOutOfRangeException("length");
            if (length < BITS_PER_BYTE && ((1L << length) - 1) < num)
                throw new ArgumentOutOfRangeException("length", "Not enough bits specified");

            Bit[] bits = new Bit[length];
            for (int i = 0, b = (bits.Length - 1); b >= 0; i++, b--)
            {
                if (((1L << i) & num) > 0)
                    bits[b] = true;
            }
            return bits;
        }


        /// <summary>
        /// Converts an array of bytes to ASCII text
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <returns>ASCII Text</returns>
        public static string ToAscii(this byte[] bytes)
        {
            if (bytes == null)
                throw new NullReferenceException("bytes");

            return bytes.ToAscii(0, bytes.Length);
        }
        /// <summary>
        /// Converts a specified number of bytes starting at a specified address to ASCII text
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <param name="index">The index of the first byte to convert.</param>
        /// <param name="count">The number of bytes to convert.</param>
        /// <returns></returns>
        public static string ToAscii(this byte[] bytes, int index, int count)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            if (index < 0 || index > bytes.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bytes.Length)
                throw new ArgumentOutOfRangeException("count");

            char[] c = new char[count];
            for (int i = 0, b = index; i < count; i++, b++)
            {
                c[i] = (char)bytes[b];
            }
            return new string(c);
        }

        /// <summary>
        /// Converts an array of bytes to a hexidecimal value
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <returns></returns>
        public static Hex ToHex(this byte[] bytes)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");

            return bytes.ToHex(0, bytes.Length);
        }
        /// <summary>
        /// Converts a specified number of bytes starting at a specified address to a hexidecimal value
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <param name="index">The index of the first byte to convert.</param>
        /// <param name="count">The number of bytes to convert.</param>
        /// <returns></returns>
        public static Hex ToHex(this byte[] bytes, int index, int count)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            if (index < 0 || index > bytes.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bytes.Length)
                throw new ArgumentOutOfRangeException("count");

            char[] lookup = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
            char[] c = new char[count * 2];
            for (int bx = index, cx = 0; bx < (index + count); ++bx, ++cx)
            {
                c[cx] = lookup[(bytes[bx] >> 4)];
                c[++cx] = lookup[(bytes[bx] & 0x0F)];
            }
            return new Hex(c);
        }

        /// <summary>
        /// Converts an array of bytes to a numeric value
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <returns></returns>
        public static Int64 ToInt64(this byte[] bytes)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");

            return bytes.ToInt64(0, bytes.Length);
        }
        /// <summary>
        /// Converts a specified number of bytes starting at a specified address to a numeric value
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <param name="index">The index of the first byte to convert.</param>
        /// <param name="count">The number of bytes to convert.</param>
        /// <returns></returns>
        public static Int64 ToInt64(this byte[] bytes, int index, int count)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            if (index > bytes.Length)
                throw new ArgumentOutOfRangeException("index");
            if ((index + count) > bytes.Length)
                throw new ArgumentOutOfRangeException("count");

            Int64 num = 0;
            for (int i = 0, b = (index + count - 1); b >= index; i++, b--)
            {
                if (bytes[b] > 0)
                {
                    if (i < BYTES_PER_INT64)
                        num += ((Int64)bytes[b] & 0xFF) << (BITS_PER_BYTE * i);
                    else
                        throw new ArgumentOutOfRangeException("bytes", "Maximum of " + BYTES_PER_INT64 + " bytes");
                }
            }
            return num;
        }

        /// <summary>
        /// Converts an array of bytes to an array of bits
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <returns></returns>
        public static Bit[] ToBits(this byte[] bytes)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");

            return bytes.ToBits(0, bytes.Length, bytes.Length * BITS_PER_BYTE);
        }
        /// <summary>
        /// Converts an array of bytes to an array of bits with the specified length
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static Bit[] ToBits(this byte[] bytes, int length)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            if (length < 0)
                throw new ArgumentOutOfRangeException("length");

            return bytes.ToBits(0, bytes.Length, length);
        }
        /// <summary>
        /// Converts a specified number of bytes starting at a specified address to an array of bits
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <param name="index">The index of the first byte to convert.</param>
        /// <param name="count">The number of bytes to convert.</param>
        /// <returns></returns>
        public static Bit[] ToBits(this byte[] bytes, int index, int count)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            if (index < 0 || index > bytes.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bytes.Length)
                throw new ArgumentOutOfRangeException("count");

            return bytes.ToBits(index, count, count * BITS_PER_BYTE);
        }
        /// <summary>
        /// Converts a specified number of bytes starting at a specified address to an array of bits with the specified length
        /// </summary>
        /// <param name="bytes">Array of bytes</param>
        /// <param name="index">The index of the first byte to convert.</param>
        /// <param name="count">The number of bytes to convert.</param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static Bit[] ToBits(this byte[] bytes, int index, int count, int length)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            if (index < 0 || index > bytes.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bytes.Length)
                throw new ArgumentOutOfRangeException("count");
            if (length < 0)
                throw new ArgumentOutOfRangeException("length");

            Bit[] bits = new Bit[count * BITS_PER_BYTE];
            for (int i = 0, b = index; i < count; i++, b++)
            {
                Bit[] byteBits = bytes[b].ToBits(BITS_PER_BYTE);
                Array.Copy(byteBits, 0, bits, i * byteBits.Length, byteBits.Length);
            }
            if (length < bits.Length)
            {
                Bit[] targetBits = new Bit[length];
                for (int i = 0; i < (bits.Length - targetBits.Length); i++)
                {
                    if (bits[i])
                        throw new ArgumentOutOfRangeException("length");
                }
                Array.Copy(bits, bits.Length - targetBits.Length, targetBits, 0, targetBits.Length);
                bits = targetBits;
            }
            else if (length > bits.Length)
            {
                Bit[] targetBits = new Bit[length];
                Array.Copy(bits, 0, targetBits, targetBits.Length - bits.Length, bits.Length);
                bits = targetBits;
            }
            return bits;
        }



        public static byte ToByte(this Bit[] bits)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");

            return bits.ToByte(0, bits.Length);
        }
        public static byte ToByte(this Bit[] bits, int index, int count)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");
            if (index < 0 || index > bits.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bits.Length)
                throw new ArgumentOutOfRangeException("count");

            byte num = 0;
            for (int i = 0, b = (index + count - 1); b >= index; i++, b--)
            {
                if (bits[b])
                {
                    if (i < BITS_PER_BYTE)
                        num += (byte)(1 << i);
                    else
                        throw new ArgumentOutOfRangeException("bits", "Maximum of " + BITS_PER_BYTE + " bits");
                }
            }
            return num;
        }

        public static byte[] ToBytes(this Bit[] bits)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");

            return bits.ToBytes(0, bits.Length);
        }
        public static byte[] ToBytes(this Bit[] bits, int index, int count)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");
            if (index < 0 || index > bits.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bits.Length)
                throw new ArgumentOutOfRangeException("count");

            int mod = count % BITS_PER_BYTE;
            int missing = (mod > 0) ? (BITS_PER_BYTE - mod) : 0;

            byte[] bytes = new byte[(count + missing) / BITS_PER_BYTE];
            for (int i = 0; i < bytes.Length; i++)
            {
                if (mod > 0)
                {
                    if (i == 0)
                        bytes[i] = bits.ToByte(index, mod);
                    else
                        bytes[i] = bits.ToByte(index + mod + (i - 1) * BITS_PER_BYTE, BITS_PER_BYTE);
                }
                else
                {
                    bytes[i] = bits.ToByte(index + i * BITS_PER_BYTE, BITS_PER_BYTE);
                }
            }
            return bytes;
        }

        public static Int64 ToInt64(this Bit[] bits)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");

            return bits.ToInt64(0, bits.Length);
        }
        public static Int64 ToInt64(this Bit[] bits, int index, int count)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");
            if (index < 0 || index > bits.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bits.Length)
                throw new ArgumentOutOfRangeException("count");

            return bits.ToBytes(index, count).ToInt64();
        }


        public static Hex ToHex(this Bit[] bits)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");

            return bits.ToBytes(0, bits.Length).ToHex();
        }
        public static Hex ToHex(this Bit[] bits, int index, int count)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");
            if (index < 0 || index > bits.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bits.Length)
                throw new ArgumentOutOfRangeException("count");

            return bits.ToBytes(index, count).ToHex();
        }

        public static string ToBinaryString(this Bit[] bits)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");

            return bits.ToBinaryString(0, bits.Length);
        }
        public static string ToBinaryString(this Bit[] bits, int index, int count)
        {
            if (bits == null)
                throw new ArgumentNullException("bits");
            if (index < 0 || index > bits.Length)
                throw new ArgumentOutOfRangeException("index");
            if (count < 0 || (index + count) > bits.Length)
                throw new ArgumentOutOfRangeException("count");

            StringBuilder sb = new StringBuilder();
            for (int i = index; i < (index + count); i++)
            {
                sb.Append(bits[i] ? '1' : '0');
            }
            return sb.ToString();
        }

        public static bool StartsWith(this Bit[] bits1, Bit[] bits2)
        {
            if (bits1 == null)
                throw new ArgumentNullException("bits1");
            if (bits2 == null)
                throw new ArgumentNullException("bits2");

            if (bits2.Length > bits1.Length)
                return false;
            for (int i = 0; i < bits2.Length; i++)
            {
                if (bits2[i] != bits1[i])
                    return false;
            }
            return true;
        }
        public static bool EndsWith(this Bit[] bits1, Bit[] bits2)
        {
            if (bits1 == null)
                throw new ArgumentNullException("bits1");
            if (bits2 == null)
                throw new ArgumentNullException("bits2");

            if (bits2.Length > bits1.Length)
                return false;
            for (int i = 0, b = (bits1.Length - bits2.Length); i < bits2.Length; i++, b++)
            {
                if (bits2[i] != bits1[b])
                    return false;
            }
            return true;
        }
        public static bool StartsWith(this byte[] bytes1, byte[] bytes2)
        {
            if (bytes1 == null)
                throw new ArgumentNullException("bytes1");
            if (bytes2 == null)
                throw new ArgumentNullException("bytes2");

            if (bytes2.Length > bytes1.Length)
                return false;
            for (int i = 0; i < bytes2.Length; i++)
            {
                if (bytes2[i] != bytes1[i])
                    return false;
            }
            return true;
        }
        public static bool EndsWith(this byte[] bytes1, byte[] bytes2)
        {
            if (bytes1 == null)
                throw new ArgumentNullException("bytes1");
            if (bytes2 == null)
                throw new ArgumentNullException("bytes2");

            if (bytes2.Length > bytes1.Length)
                return false;
            for (int i = 0, b = (bytes1.Length - bytes2.Length); i < bytes2.Length; i++, b++)
            {
                if (bytes2[i] != bytes1[b])
                    return false;
            }
            return true;
        }
               
        public static bool EqualsBits(this Bit[] bits1, Bit[] bits2)
        {
            if (bits1 == null)
                throw new ArgumentNullException("bits1");
            if (bits2 == null)
                throw new ArgumentNullException("bits2");

            if (bits1.Length != bits2.Length)
                return false;
            return bits1.EqualsBits(bits2, 0, bits1.Length);
        }
        public static bool EqualsBits(this Bit[] bits1, Bit[] bits2, int index, int count)
        {
            if (bits1 == null)
                throw new ArgumentNullException("bits1");
            if (bits2 == null)
                throw new ArgumentNullException("bits2");

            if (bits1.Length < index + count || bits2.Length < index + count)
                return false;
            for (int i = index; i < count; i++)
            {
                if (bits1[i] != bits2[i])
                    return false;
            }
            return true;
        }
        public static bool EqualsBytes(this byte[] bytes1, byte[] bytes2)
        {
            if (bytes1 == null)
                throw new ArgumentNullException("bytes1");
            if (bytes2 == null)
                throw new ArgumentNullException("bytes2");

            if (bytes1.Length != bytes2.Length)
                return false;
            return bytes1.EqualsBytes(bytes2, 0, bytes1.Length);
        }
        public static bool EqualsBytes(this byte[] bytes1, byte[] bytes2, int index, int count)
        {
            if (bytes1 == null)
                throw new ArgumentNullException("bytes1");
            if (bytes2 == null)
                throw new ArgumentNullException("bytes2");

            if (bytes1.Length < index + count || bytes2.Length < index + count)
                return false;
            for (int i = 0; i < bytes1.Length; i++)
            {
                if (bytes1[i] != bytes2[i])
                    return false;
            }
            return true;
        }

    }

    public class Hex
    {
        public string Value { get; set; }

        public int Length {get{ return Value.Length;}}
        public char this[int index] {get{ return Value[index];}}

        public Hex(char[] value) : this(new String(value)) { }
        public Hex(string value)
        {
            Value = value.ToUpperInvariant();
            if (Value.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
                Value = Value.Substring(2);
            foreach (char c in Value)
            {
                if (!(c >= '0' && c <= '9') && !(c >= 'A' && c <= 'F'))
                    throw new ArgumentException();
            }
        }
        //public static implicit operator string(Hex h)
        //{
        //    return h.Value;
        //}
        //public static implicit operator Hex(string s)
        //{
        //    return new Hex(s);
        //}

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Hex && string.Equals(Value, ((Hex)obj).Value, StringComparison.Ordinal);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString() {return Value;}
        public int CompareTo(Hex other) { return Equals(this, other) ? 0 : Value.CompareTo(other.Value);}

        public static Hex operator +(Hex left, Hex right)
        {
            return new Hex((left != null ? left.Value : null) + (right != null ? right.Value : null));
        }
        public static bool operator ==(Hex left, Hex right)
        {
            if (ReferenceEquals(null, left) && ReferenceEquals(null, right)) return true;
            if (ReferenceEquals(null, left) || ReferenceEquals(null, right)) return false;
            return left.Value == right.Value;
        }
        public static bool operator !=(Hex left, Hex right)
        {
            if (ReferenceEquals(null, left) && ReferenceEquals(null, right)) return false;
            if (ReferenceEquals(null, left) || ReferenceEquals(null, right)) return true;
            return left.Value != right.Value;
        }

        public static bool Equals(Hex a, Hex b)
        {
            if (ReferenceEquals(null, a) && ReferenceEquals(null, b)) return true;
            if (ReferenceEquals(null, a) || ReferenceEquals(null, b)) return false;
            return string.Equals(a.Value, b.Value, StringComparison.Ordinal);
        }

        public Hex Substring(int startIndex)
        {
            return new Hex(Value.Substring(startIndex));
        }
        public Hex Substring(int startIndex, int length)
        {
            return new Hex(Value.Substring(startIndex, length));
        }

        public Hex PadLeft(int totalWidth)
        {
            return new Hex(Value.PadLeft(totalWidth, '0'));
        }
        public Hex PadLeft(int totalWidth, char paddingChar)
        {
            return new Hex(Value.PadLeft(totalWidth, paddingChar));
        }
        public Hex PadRight(int totalWidth)
        {
            return new Hex(Value.PadRight(totalWidth, '0'));
        }
        public Hex PadRight(int totalWidth, char paddingChar)
        {
            return new Hex(Value.PadRight(totalWidth, paddingChar));
        }
    }

    public struct Bit
    {
        public bool BoolValue { get; private set; }
        public int IntValue { get { return BoolValue ? 1 : 0; } }

        public Bit(int value) : this()
        {
            if (value == 1)
                BoolValue = true;
            else if (value == 0)
                BoolValue = false;
            else
                throw new ArgumentOutOfRangeException("value", value, "value must be 0 or 1");
        }

        public Bit(bool value) : this()
        {
            BoolValue = value;
        }

        public static implicit operator int(Bit value)
        {
            return value.BoolValue ? 1 : 0;
        }

        public static implicit operator Bit(int value)
        {
            return new Bit(value);
        }

        public static implicit operator bool(Bit value)
        {
            return value.BoolValue;
        }

        public static implicit operator Bit(bool value)
        {
            return new Bit(value);
        }

        public static Bit operator |(Bit value1, Bit value2)
        {
            return value1.IntValue | value2.IntValue;
        }

        public static Bit operator &(Bit value1, Bit value2)
        {
            return value1.IntValue & value2.IntValue;
        }

        public static Bit operator ^(Bit value1, Bit value2)
        {
            return value1.IntValue ^ value2.IntValue;
        }

        public static Bit operator ~(Bit value)
        {
            return new Bit(!value.BoolValue);
        }

        public static Bit operator !(Bit value)
        {
            return new Bit(!value.BoolValue);
        }

        public static bool operator true(Bit value)
        {
            return value.BoolValue == true;
        }

        public static bool operator false(Bit value)
        {
            return value.BoolValue == false;
        }

        public static bool operator ==(Bit bitValue, int intValue)
        {
            return bitValue.IntValue == intValue;
        }

        public static bool operator !=(Bit bitValue, int intValue)
        {
            return bitValue.IntValue != intValue;
        }

        public override bool Equals(object obj)
        {
            if (obj is Bit)
                return BoolValue == ((Bit)obj).BoolValue;
            else if (obj is bool)
                return BoolValue == (bool)obj;
            else if (obj is int)
                return IntValue == (int)obj;
            else
                return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return BoolValue.GetHashCode();
        }

        public override string ToString()
        {
            return BoolValue ? "1" : "0";
        }

        public static Bit[] FromBinaryString(string binaryString)
        {
            if (binaryString == null)
                throw new ArgumentNullException("binaryString");

            Bit[] bits = new Bit[binaryString.Length];
            for (int i = 0; i < binaryString.Length; i++)
            {
                switch (binaryString[i])
                {
                    case '0':
                        break;
                    case '1':
                        bits[i] = true;
                        break;
                    default:
                        throw new ArgumentException("binaryString must contain only 0 or 1", "binaryString");
                }
            }
            return bits;
        }

        public static List<Bit[]> FromBinaryStrings(params string[] binaryStrings)
        {
            if (binaryStrings == null)
                throw new ArgumentNullException("binaryStrings");

            return binaryStrings.Select(p => Bit.FromBinaryString(p)).ToList();
        }
    }

}
