﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EStreamWrapper
{
    //public abstract class EStream<T> : EStream<T, T>
    //    where T : class
    //{
    //    public EStream(EStreamMessageFramingSettings framingSettings, IEStreamSerializer<T> serializer, int bufferSize = DEFAULT_BUFFER_SIZE)
    //        : this(framingSettings, null, serializer, bufferSize) { }
    //    public EStream(EStreamMessageFramingSettings framingSettings, EStreamHeartbeatSettings heartbeatSettings, IEStreamSerializer<T> serializer, int bufferSize = DEFAULT_BUFFER_SIZE)
    //        : this(framingSettings, framingSettings, heartbeatSettings, serializer, bufferSize) { }
    //    public EStream(EStreamMessageFramingSettings sendFramingSettings, EStreamMessageFramingSettings receiveFramingSettings, EStreamHeartbeatSettings heartbeatSettings, IEStreamSerializer<T> serializer, int bufferSize = DEFAULT_BUFFER_SIZE)
    //        : base(sendFramingSettings, receiveFramingSettings, heartbeatSettings, serializer, serializer, bufferSize) { }
    //}

    //public abstract class EStream<TSend, TReceive> : EStream
    //    where TSend : class
    //    where TReceive : class
    //{
    //    public readonly IEStreamSerializer<TSend> SendSerializer;
    //    public readonly IEStreamSerializer<TReceive> ReceiveSerializer;

    //    public EStream(EStreamMessageFramingSettings framingSettings, IEStreamSerializer<TSend> sendSerializer, IEStreamSerializer<TReceive> receiveSerializer, int bufferSize = DEFAULT_BUFFER_SIZE)
    //        : this(framingSettings, null, sendSerializer, receiveSerializer, bufferSize) { }
    //    public EStream(EStreamMessageFramingSettings framingSettings, EStreamHeartbeatSettings heartbeatSettings, IEStreamSerializer<TSend> sendSerializer, IEStreamSerializer<TReceive> receiveSerializer, int bufferSize = DEFAULT_BUFFER_SIZE)
    //        : this(framingSettings, framingSettings, heartbeatSettings, sendSerializer, receiveSerializer, bufferSize) { }
    //    public EStream(EStreamMessageFramingSettings sendFramingSettings, EStreamMessageFramingSettings receiveFramingSettings, EStreamHeartbeatSettings heartbeatSettings, IEStreamSerializer<TSend> sendSerializer, IEStreamSerializer<TReceive> receiveSerializer, int bufferSize = DEFAULT_BUFFER_SIZE)
    //        : base(sendFramingSettings, receiveFramingSettings, heartbeatSettings, bufferSize)
    //    {
    //        SendSerializer = sendSerializer;
    //        ReceiveSerializer = receiveSerializer;
    //    }

    //    public Task SendItem(TSend item, CancellationToken ct)
    //    {
    //        byte[] data;
    //        try
    //        {
    //            data = SendSerializer.Serialize(item);
    //            return base.SendBytes(data, ct);
    //        }
    //        catch (Exception ex)
    //        {
    //            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
    //            tcs.TrySetException(ex);
    //            return tcs.Task;
    //        }
    //    }

    //    public event EventHandler<EStreamItemReceivedEventArgs<TReceive>> ItemReceived;
    //    protected override void OnBytesReceived(byte[] bytes)
    //    {
    //        base.OnBytesReceived(bytes);

    //        TReceive item = null;
    //        Exception serializationException = null;
    //        if (ReceiveSerializer != null)
    //        {
    //            try
    //            {
    //                item = ReceiveSerializer.Deserialize(bytes);
    //            }
    //            catch (Exception ex)
    //            {
    //                serializationException = ex;
    //            }
    //        }
    //        EventHandler<EStreamItemReceivedEventArgs<TReceive>> handler = ItemReceived;
    //        if (handler != null) handler(this, new EStreamItemReceivedEventArgs<TReceive>(this, item, bytes, serializationException));
    //    }
    //}

    public interface IEStreamSerializer<T>
        where T : class
    {
        byte[] Serialize(T item);
        T Deserialize(byte[] bytes);
    }

    #region String Serializer

    /// <summary>
    /// Uses Encoder to convert between strings and bytes
    /// Deserialize returns null if T is not String
    /// </summary>
    /// <typeparam name="T">The type of item</typeparam>
    public class EStreamStringSerializer<T> : IEStreamSerializer<T>
        where T : class
    {
        public Encoding Encoding { get; set; }

        public EStreamStringSerializer()
        {
            Encoding = Encoding.UTF8;
        }

        public byte[] Serialize(T item)
        {
            return Encoding.GetBytes(item.ToString());
        }

        public T Deserialize(byte[] bytes)
        {
            object o = null;
            if (typeof(T) == typeof(string))
                o = Encoding.GetString(bytes);
            return (T)o;
        }
    }

    #endregion String Serializer

    #region Binary Serializer

    ///// <summary>
    ///// Use BinaryFormatter.
    ///// Requires T class definition to be in in a common assembly.
    ///// Requires [Serializable] attribute on T class.
    ///// </summary>
    ///// <typeparam name="T">The type of item</typeparam>
    //public class EStreamBinarySerializer<T> : IEStreamSerializer<T>
    //    where T : class
    //{
    //    private static BinaryFormatter serializer = new BinaryFormatter();

    //    public byte[] Serialize(T item)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        {
    //            serializer.Serialize(memoryStream, item);
    //            return memoryStream.ToArray();
    //        }
    //    }

    //    public T Deserialize(byte[] bytes)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream(bytes))
    //        {
    //            return (T)serializer.Deserialize(memoryStream);
    //        }
    //    }
    //}

    #endregion Binary Serializer

    #region XML Serializer

    ///// <summary>
    ///// Use XmlSerializer.
    ///// </summary>
    ///// <typeparam name="T">The type of item</typeparam>
    //public class EStreamXmlSerializer<T> : IEStreamSerializer<T>
    //    where T : class
    //{
    //    private static XmlSerializerNamespaces xmlEmptyNamespace = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
    //    private static XmlWriterSettings xmlWriterSettings = new XmlWriterSettings { OmitXmlDeclaration = true };
    //    private static XmlSerializer serializer = new XmlSerializer(typeof(T));

    //    public bool OmitXmlDeclaration { get; set; }
    //    public bool OmitNamespace { get; set; }
    //    public Encoding Encoding { get; set; }

    //    public EStreamXmlSerializer()
    //    {
    //        Encoding = Encoding.UTF8;
    //    }

    //    public byte[] Serialize(T item)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        using (StreamWriter streamWriter = new StreamWriter(memoryStream, Encoding))
    //        {
    //            if (OmitXmlDeclaration)
    //            {
    //                using (XmlWriter xmlWriter = XmlWriter.Create(streamWriter, xmlWriterSettings))
    //                {
    //                    if (OmitNamespace)
    //                        serializer.Serialize(xmlWriter, item, xmlEmptyNamespace);
    //                    else
    //                        serializer.Serialize(xmlWriter, item);
    //                }
    //            }
    //            else
    //            {
    //                if (OmitNamespace)
    //                    serializer.Serialize(streamWriter, item, xmlEmptyNamespace);
    //                else
    //                    serializer.Serialize(streamWriter, item);
    //            }
    //            streamWriter.Flush();
    //            return memoryStream.ToArray();
    //        }
    //    }

    //    public T Deserialize(byte[] bytes)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream(bytes))
    //        using (StreamReader streamReader = new StreamReader(memoryStream, Encoding))
    //        {
    //            return (T)serializer.Deserialize(streamReader);
    //        }
    //    }
    //}

    #endregion XML Serializer

    #region DataContract JSON Serializer

    ///// <summary>
    ///// Use DataContractJsonSerializer.
    ///// Requires reference to System.Runtime.Serialization.
    ///// Add [DataContract] attribute to T class and [DataMember] attribute to T properties.
    ///// </summary>
    ///// <typeparam name="T">The type of item</typeparam>
    //public class EStreamJsonSerializer<T> : IEStreamSerializer<T>
    //    where T : class
    //{
    //    private static System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));

    //    public byte[] Serialize(T item)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        {
    //            serializer.WriteObject(memoryStream, item);
    //            return memoryStream.ToArray();
    //        }
    //    }

    //    public T Deserialize(byte[] bytes)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream(bytes))
    //        {
    //            return (T)serializer.ReadObject(memoryStream);
    //        }
    //    }
    //}

    #endregion DataContract JSON Serializer

    #region DataContract XML Serializer

    ///// <summary>
    ///// Use DataContractSerializer
    ///// Requires reference to System.Runtime.Serialization.
    ///// Add [DataContract] attribute to T class and [DataMember] attribute to T properties.
    ///// Use [DataContract(Namespace = "")] atrribute on T class to omit namespace
    ///// </summary>
    ///// <typeparam name="T">The type of item</typeparam>
    //public class EStreamDataContractSerializer<T> : IEStreamSerializer<T>
    //    where T : class
    //{
    //    private static System.Runtime.Serialization.DataContractSerializer serializer = new System.Runtime.Serialization.DataContractSerializer(typeof(T));

    //    public byte[] Serialize(T item)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        {
    //            serializer.WriteObject(memoryStream, item);
    //            return memoryStream.ToArray();
    //        }
    //    }

    //    public T Deserialize(byte[] bytes)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream(bytes))
    //        {
    //            return (T)serializer.ReadObject(memoryStream);
    //        }
    //    }
    //}

    #endregion DataContract XML Serializer

    #region NetDataContract XML Serializer

    ///// <summary>
    ///// Use NetDataContractSerializer.
    ///// Requires T class definition to be in in a common assembly.
    ///// Requires reference to System.Runtime.Serialization.
    ///// Use [DataContract(Namespace = "")] atrribute on T class to omit namespace.
    ///// </summary>
    ///// <typeparam name="T">The type of item</typeparam>
    //public class EStreamNetDataContractSerializer<T> : IEStreamSerializer<T>
    //    where T : class
    //{
    //    private static System.Runtime.Serialization.NetDataContractSerializer serializer = new System.Runtime.Serialization.NetDataContractSerializer();

    //    public byte[] Serialize(T item)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        {
    //            serializer.WriteObject(memoryStream, item);
    //            return memoryStream.ToArray();
    //        }
    //    }

    //    public T Deserialize(byte[] bytes)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream(bytes))
    //        {
    //            return (T)serializer.ReadObject(memoryStream);
    //        }
    //    }
    //}

    #endregion NetDataContract XML Serializer

    #region Soap XML Serializer

    ///// <summary>
    ///// Use SoapFormatter.
    ///// Requires T class definition to be in in a common assembly.
    ///// Requires reference to System.Runtime.Serialization.Formatters.Soap.
    ///// Add [Serializable] attribute to T class.
    ///// </summary>
    ///// <typeparam name="T">The type of item</typeparam>
    //public class EStreamSoapSerializer<T> : IEStreamSerializer<T>
    //    where T : class
    //{
    //    private static System.Runtime.Serialization.Formatters.Soap.SoapFormatter serializer = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();

    //    public byte[] Serialize(T item)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        {
    //            serializer.Serialize(memoryStream, item);
    //            return memoryStream.ToArray();
    //        }
    //    }

    //    public T Deserialize(byte[] bytes)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream(bytes))
    //        {
    //            return (T)serializer.Deserialize(memoryStream);
    //        }
    //    }
    //}

    #endregion Soap XML Serializer

    #region JavaScript JSON Serializer

    ///// <summary>
    ///// Use JavaScriptSerializer.
    ///// Requires reference to System.Web.Extensions (Not included in client profile)
    ///// </summary>
    ///// <typeparam name="T">The type of item</typeparam>
    //public class EStreamJavaScriptSerializer<T> : IEStreamSerializer<T>
    //    where T : class
    //{
    //    private static System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

    //    public Encoding Encoding { get; set; }

    //    public EStreamJavaScriptSerializer()
    //    {
    //        Encoding = Encoding.UTF8;
    //    }

    //    public byte[] Serialize(T item)
    //    {
    //        StringBuilder sb = new StringBuilder();
    //        serializer.Serialize(item, sb);
    //        return Encoding.GetBytes(sb.ToString());
    //    }

    //    public T Deserialize(byte[] bytes)
    //    {
    //        return (T)serializer.Deserialize(Encoding.GetString(bytes), typeof(T));
    //    }
    //}

    #endregion JavaScript JSON Serializer

    #region Newtonsoft Json.NET JSON Serializer

    ///// <summary>
    ///// Use Newtonsoft Json.NET
    ///// Requires reference to Newtonsoft.Json
    ///// </summary>
    ///// <typeparam name="T">The type of item</typeparam>
    //public class EStreamNewtonsoftJsonSerializer<T> : IEStreamSerializer<T>
    //    where T : class
    //{
    //    private static Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();

    //    public Encoding Encoding { get; set; }

    //    public EStreamNewtonsoftJsonSerializer()
    //    {
    //        Encoding = Encoding.UTF8;
    //    }

    //    public byte[] Serialize(T item)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        using (StreamWriter streamWriter = new StreamWriter(memoryStream, Encoding))
    //        {
    //            serializer.Serialize(streamWriter, item);
    //            streamWriter.Flush();
    //            return memoryStream.ToArray();
    //        }
    //    }

    //    public T Deserialize(byte[] bytes)
    //    {
    //        using (MemoryStream memoryStream = new MemoryStream(bytes))
    //        using (StreamReader streamReader = new StreamReader(memoryStream, Encoding))
    //        {
    //            return (T)serializer.Deserialize(streamReader, typeof(T));
    //        }
    //    }
    //}

    #endregion Newtonsoft Json.NET JSON Serializer

}
