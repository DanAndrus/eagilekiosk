﻿using EStreamWrapper;
using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;

namespace EStreamWrapper.Com
{
    public class EComClient<TSend, TReceive> : EComClient
        where TSend : class
        where TReceive : class
    {
        public IEStreamSerializer<TSend> SendSerializer { get; set; }
        public IEStreamSerializer<TReceive> ReceiveSerializer { get; set; }

        public EComClient(EComPort eComPort) : base(eComPort) { }
        public EComClient(string comString) : base(comString) { }
        public EComClient(string portName = EComPort.DEFAULT_PORT_NAME, int baudRate = EComPort.DEFAULT_BAUD_RATE,
            Parity parity = EComPort.DEFAULT_PARITY, int dataBits = EComPort.DEFAULT_DATA_BITS, StopBits stopBits = EComPort.DEFAULT_STOP_BITS)
            : base(portName, baudRate, parity, dataBits, stopBits) { }

        public Task SendItem(TSend item, CancellationToken ct)
        {
            byte[] data = SendSerializer.Serialize(item);
            return SendBytes(data, ct);
        }

        public event EventHandler<EStreamItemReceivedEventArgs<TReceive>> ClientItemReceived;
        protected override void OnClientBytesReceived(EStreamBytesEventArgs e)
        {
            base.OnClientBytesReceived(e);

            TReceive item = null;
            Exception serializationException = null;
            if (ReceiveSerializer != null)
            {
                try
                {
                    item = ReceiveSerializer.Deserialize(e.Bytes);
                }
                catch (Exception ex)
                {
                    serializationException = ex;
                }
            }
            EventHandler<EStreamItemReceivedEventArgs<TReceive>> handler = ClientItemReceived;
            if (handler != null) handler(this, new EStreamItemReceivedEventArgs<TReceive>(e.EStream, item, e.Bytes, serializationException));
        }
    }
}
