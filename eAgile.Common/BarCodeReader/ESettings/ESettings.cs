﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace EAgile.SettingsXml
{
    /// <summary>
    /// Group of settings
    /// </summary>
    public abstract class ESettings : IDisposable
    {
        public bool GetMonitoringEnabled()
        {
            return _file != null;
        }
        public FileInfo GetMonitoringFile()
        {
            if (_file != null)
            {
                _file.Refresh();
                return _file;
            }
            return null;
        }
        public TimeSpan GetMonitoringInterval()
        {
            return _fileTimer != null ? _fileTimerInterval : TimeSpan.Zero;
        }

        public void SetCreateIfNotFound(bool createIfNotFound)
        {
            _createIfNotFound = createIfNotFound;
        }
        public bool GetProtectIfUnprotected()
        {
            return _createIfNotFound;
        }

        public void SetProtectIfUnprotected(bool protectIfUnprotected)
        {
            _protectIfUnprotected = protectIfUnprotected;
        }
        public bool GetSettingsFileCreation()
        {
            return _protectIfUnprotected;
        }

        public Task GetSettingsMonitorLoadedTask()
        {
            return _settingsMonitorLoadedTcs.Task;
        }
        private TaskCompletionSource<bool> _settingsMonitorLoadedTcs = new TaskCompletionSource<bool>();

        private bool _createIfNotFound = true;
        private bool _protectIfUnprotected = true;
        private readonly object _fileLock = new object();
        private System.Timers.Timer _fileTimer = null;
        private TimeSpan _fileTimerInterval = TimeSpan.FromMinutes(1);
        private FileInfo _file = null;
        private DateTime _lastFileWriteTimeUtc;
        private long _lastFileLength;

        public ESettings()
        {
            InitializeSettings();
        }

        public void InitializeSettings()
        {
            // Create empty root element to load all defaults into
            XElement rootElement = new XElement("root");

            // Load empty xml ignoring any validation errors
            try
            {
                LoadXml(rootElement);
            }
            catch (ESettingsAggregateException) { }

            if (_settingsMonitorLoadedTcs.Task.IsCompleted)
                _settingsMonitorLoadedTcs = new TaskCompletionSource<bool>();
        }




        public void Load(string settingsXmlFilePath)
        {
            if (settingsXmlFilePath == null)
                throw new ArgumentNullException("settingsXmlFilePath");

            FileInfo settingsXmlFile = new FileInfo(settingsXmlFilePath);
            Load(settingsXmlFile);
        }
        public void Load(FileInfo settingsXmlFile)
        {
            try
            {
                LoadFile(settingsXmlFile);
            }
            catch (ESettingsAggregateException esae)
            {
                if (_protectIfUnprotected && esae.InnerExceptions.Any(p => p is ESettingsProtectionException))
                {
                    // Save unprotected values and reload
                    Save(settingsXmlFile);
                    LoadFile(settingsXmlFile);
                }
                else
                {
                    throw;
                }
            }
        }
        public void Load(System.Xml.XmlDocument settingsXmlDoc)
        {
            if (settingsXmlDoc == null)
                throw new ArgumentNullException("settingsXmlDoc");

            XDocument doc;
            using (System.Xml.XmlNodeReader nodeReader = new System.Xml.XmlNodeReader(settingsXmlDoc))
            {
                doc = XDocument.Load(nodeReader);
            }
            Load(doc);
        }
        public void Load(XDocument settingsXmlDoc)
        {
            if (settingsXmlDoc == null)
                throw new ArgumentNullException("settingsXmlDoc");

            string xmlRootName = GetType().GetXmlRootName();
            if (string.IsNullOrEmpty(xmlRootName))
                throw new InvalidOperationException("Settings root element not specified");

            XDocument doc = settingsXmlDoc;
            XElement rootElement = doc.Element(xmlRootName);

            if (rootElement == null)
                throw new InvalidOperationException("Settings root element not found.");

            LoadXml(rootElement);
        }
        private void LoadXml(XElement rootElement)
        {
            PopulateDefaultsToXml(rootElement, GetType());
            DeserializeXmlToSettings(rootElement, this, null);
        }

        private void LoadFile(FileInfo settingsXmlFile)
        {
            if (settingsXmlFile == null)
                throw new ArgumentNullException("settingsXmlFile");

            XDocument doc = null;
            try
            {
                lock (_fileLock)
                {
                    settingsXmlFile.Refresh();
                    if (!settingsXmlFile.Exists)
                    {
                        if (_createIfNotFound)
                        {
                            Save(settingsXmlFile);
                            settingsXmlFile.Refresh();
                        }
                        else
                        {
                            throw new FileNotFoundException("Settings file does not exist. Settings file location: " + settingsXmlFile.FullName);
                        }
                    }

                    doc = XDocument.Load(settingsXmlFile.FullName, LoadOptions.PreserveWhitespace);
                    _lastFileWriteTimeUtc = settingsXmlFile.LastWriteTimeUtc;
                    _lastFileLength = settingsXmlFile.Length;
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Settings file is invalid or cannot be read. Settings file location: " + settingsXmlFile.FullName, ex);
            }
            Load(doc);
        }

        public void Save(string settingsXmlFilePath)
        {
            if (settingsXmlFilePath == null)
                throw new ArgumentNullException("settingsXmlFilePath");

            FileInfo settingsXmlFile = new FileInfo(settingsXmlFilePath);
            Save(settingsXmlFile);
        }
        public void Save(FileInfo settingsXmlFile)
        {
            if (settingsXmlFile == null)
                throw new ArgumentNullException("settingsXmlFile");

            try
            {
                // Create xml by serializing the settings object
                XDocument doc = ToXDocument();

                lock (_fileLock)
                {
                    settingsXmlFile.Refresh();
                    if (!settingsXmlFile.Directory.Exists)
                    {
                        settingsXmlFile.Directory.Create();
                        settingsXmlFile.Refresh();
                    }
                    doc.Save(settingsXmlFile.FullName, SaveOptions.DisableFormatting);
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Settings file could not be saved. Settings file path: " + settingsXmlFile.FullName, ex);
            }
        }
        public XDocument ToXDocument()
        {
            string xmlRootName = GetType().GetXmlRootName();
            if (string.IsNullOrEmpty(xmlRootName))
                throw new InvalidOperationException("Settings root element not specified");

            try
            {
                // Create xml by serializing the settings object
                XElement rootElement = new XElement(xmlRootName);
                XDocument doc = new XDocument(rootElement);
                SerializeSettingsToXml(rootElement, this);
                PrependCommentsToXml(rootElement, GetType());

                // Save and load the xml to get it formatted
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(memoryStream,
                        new System.Xml.XmlWriterSettings
                        {
                            Indent = true,
                            IndentChars = "    ",
                        }))
                    {
                        doc.Save(xmlWriter);
                    }
                    memoryStream.Position = 0;
                    using (StreamReader streamReader = new StreamReader(memoryStream))
                    {
                        doc = XDocument.Load(streamReader, LoadOptions.PreserveWhitespace);
                    }
                }

                // Insert inline comments after the xml is formatted
                rootElement = doc.Element(xmlRootName);
                AppendCommentsToXml(rootElement, GetType());
                return doc;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Settings could not be serialized.", ex);
            }
        }
        private XDocument ToComparableXDocument()
        {
            string xmlRootName = GetType().GetXmlRootName();
            if (string.IsNullOrEmpty(xmlRootName))
                throw new InvalidOperationException("Settings root element not specified");

            try
            {
                // Create xml by serializing the settings object
                XElement rootElement = new XElement(xmlRootName);
                XDocument doc = new XDocument(rootElement);
                SerializeSettingsToXml(rootElement, this, true);
                return doc;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Settings could not be serialized.", ex);
            }
        }
        public System.Xml.XmlDocument ToXmlDocument()
        {
            try
            {
                // Create xml by serializing the settings object
                XDocument doc = ToXDocument();
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                using (System.Xml.XmlReader xmlReader = doc.CreateReader())
                {
                    xmlDoc.Load(xmlReader);
                }
                return xmlDoc;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Settings could not be serialized.", ex);
            }
        }
        public override string ToString()
        {
            return ToXDocument().ToString();
        }



        private static void PopulateDefaultsToXml(XElement xmlElement, Type eSettingsType)
        {
            if (xmlElement == null)
                throw new ArgumentNullException("xmlElement");
            if (eSettingsType == null)
                throw new ArgumentNullException("eSettingsType");
            if (!eSettingsType.IsESettings())
                throw new ArgumentException(eSettingsType + " does not inherit ESettings");

            foreach (PropertyInfo pi in eSettingsType.GetProperties())
            {
                Type valueType = pi.IsList() ? pi.PropertyType.GetGenericArguments().Single() : pi.PropertyType;

                ESettingsParser parser = pi.GetParser();
                ESettingsProtector protector = pi.GetProtector();

                string elementName = pi.GetXmlElementName();
                if (!string.IsNullOrEmpty(elementName))
                {
                    if (pi.IsESettings())
                    {
                        XElement childElement = xmlElement.Element(elementName);
                        if (childElement == null)
                        {
                            ESettings subSettings = (ESettings)pi.GetDefaultValue();
                            if (subSettings != null)
                            {
                                childElement = new XElement(elementName);
                                SerializeSettingsToXml(childElement, subSettings);
                                xmlElement.Add(childElement);
                            }
                        }
                        if (childElement != null)
                        {
                            PopulateDefaultsToXml(childElement, valueType);
                        }
                    }
                    else if (pi.IsList())
                    {
                        XElement listParent = null;
                        string listElementName = pi.GetXmlListElementName();
                        if (listElementName != null)
                        {
                            listParent = xmlElement.Element(listElementName);
                            if (listParent == null)
                            {
                                // List has parent element does not exist, use defaults if set
                                object defaultValue = pi.GetDefaultValue();
                                if (defaultValue != null)
                                {
                                    listParent = new XElement(listElementName);
                                    xmlElement.Add(listParent);
                                    foreach (object listObject in (IList)defaultValue)
                                    {
                                        if (listObject != null)
                                        {
                                            if (valueType.IsESettings())
                                            {
                                                XElement childElement = new XElement(elementName);
                                                SerializeSettingsToXml(childElement, (ESettings)listObject);
                                                listParent.Add(childElement);
                                            }
                                            else if (valueType.IsList())
                                            {
                                                // List of Lists not supported
                                                throw new NotImplementedException();
                                            }
                                            else
                                            {
                                                string xmlTextValue = parser.Unparse(listObject, valueType);
                                                if (protector != null && !string.IsNullOrEmpty(xmlTextValue))
                                                {
                                                    xmlTextValue = protector.Protect(xmlTextValue);
                                                }
                                                if (xmlTextValue != null)
                                                {
                                                    XElement childElement = new XElement(elementName);
                                                    childElement.Add(new XText(xmlTextValue ?? string.Empty));
                                                    listParent.Add(childElement);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            // No list parent element, do not use defaults since we don't know if the list is just empty
                            listParent = xmlElement;
                        }
                        if (valueType.IsESettings())
                        {
                            // Propegate defaults to sub settings
                            foreach (XElement subSettingsElement in listParent.Elements(elementName))
                            {
                                PopulateDefaultsToXml(subSettingsElement, valueType);
                            }
                        }
                    }
                    else
                    {
                        XElement childElement = xmlElement.Element(elementName);
                        if (childElement == null)
                        {
                            object defaultValue = pi.GetDefaultValue();
                            if (defaultValue != null)
                            {
                                string xmlTextValue = parser.Unparse(defaultValue, valueType);
                                if (protector != null && !string.IsNullOrEmpty(xmlTextValue))
                                {
                                    xmlTextValue = protector.Protect(xmlTextValue);
                                }
                                if (xmlTextValue != null)
                                {
                                    childElement = new XElement(elementName);
                                    childElement.Add(new XText(xmlTextValue ?? string.Empty));
                                    xmlElement.Add(childElement);
                                }
                            }
                            if (childElement == null && pi.IsRequired())
                            {
                                // Add blank entry for required settings
                                childElement = new XElement(elementName);
                                childElement.Add(new XText(string.Empty));
                                xmlElement.Add(childElement);
                            }
                        }
                    }
                }

                string attributeName = pi.GetXmlAttributeName();
                if (!string.IsNullOrEmpty(attributeName) && !pi.IsESettings() && !pi.IsList())
                {
                    XAttribute xAttribute = xmlElement.Attribute(attributeName);
                    if (xAttribute == null)
                    {
                        object defaultValue = pi.GetDefaultValue();
                        if (defaultValue != null)
                        {
                            string xmlTextValue = parser.Unparse(defaultValue, valueType);
                            if (protector != null && !string.IsNullOrEmpty(xmlTextValue))
                            {
                                xmlTextValue = protector.Protect(xmlTextValue);
                            }
                            if (xmlTextValue != null)
                            {
                                xmlElement.SetAttributeValue(attributeName, xmlTextValue);
                            }
                        }
                    }
                }
            }
        }

        private static void SerializeSettingsToXml(XElement xElement, ESettings eSettings, bool unprotected = false)
        {
            if (xElement == null)
                throw new ArgumentNullException("xmlElement");
            if (eSettings == null)
                throw new ArgumentNullException("eSettings");

            foreach (PropertyInfo pi in eSettings.GetType().GetProperties())
            {
                ESettingsParser parser = pi.GetParser();
                ESettingsProtector protector = pi.GetProtector();

                string elementName = pi.GetXmlElementName();
                if (!string.IsNullOrEmpty(elementName) && xElement != null)
                {
                    object value = pi.GetValue(eSettings, null);
                    IEnumerable values;
                    Type elementType;
                    XElement element;
                    if (pi.IsList())
                    {
                        elementType = pi.PropertyType.GetGenericArguments().Single(); // List element type
                        values = (IList)value; // List of values
                        string listElementName = pi.GetXmlListElementName();
                        if (listElementName != null && values != null)
                        {
                            XElement listElement = new XElement(listElementName);
                            xElement.Add(listElement);
                            element = listElement;
                        }
                        else
                        {
                            element = xElement;
                        }
                    }
                    else // Single value
                    {
                        elementType = pi.PropertyType;
                        values = new[] { value };
                        element = xElement;
                    }

                    if (values != null && !elementType.IsList()) // List of Lists not supported
                    {
                        foreach (object listValue in values)
                        {
                            if (listValue != null) // Don't write null values
                            {
                                if (elementType.IsESettings())
                                {
                                    XElement childElement = new XElement(elementName);
                                    ESettings subsetting = (ESettings)listValue;
                                    SerializeSettingsToXml(childElement, subsetting);
                                    element.Add(childElement);
                                }
                                else
                                {
                                    string xmlTextValue = parser.Unparse(listValue, elementType);
                                    if (!unprotected && protector != null && !string.IsNullOrEmpty(xmlTextValue))
                                    {
                                        xmlTextValue = protector.Protect(xmlTextValue);
                                    }
                                    if (xmlTextValue != null)
                                    {
                                        XElement childElement = new XElement(elementName);
                                        childElement.Add(new XText(xmlTextValue));
                                        element.Add(childElement);
                                    }
                                }
                            }
                        }
                    }
                }

                string attributeName = pi.GetXmlAttributeName();
                if (!string.IsNullOrEmpty(attributeName) && !pi.IsESettings() && !pi.IsList())
                {
                    Type attributeType = pi.PropertyType;
                    object value = pi.GetValue(eSettings, null);
                    if (value != null)
                    {
                        string xmlTextValue = parser.Unparse(value, attributeType);
                        if (!unprotected && protector != null && !string.IsNullOrEmpty(xmlTextValue))
                        {
                            xmlTextValue = protector.Protect(xmlTextValue);
                        }
                        if (xmlTextValue != null)
                        {
                            xElement.SetAttributeValue(attributeName, xmlTextValue);
                        }
                    }
                }
            }
        }

        private static void DeserializeXmlToSettings(XElement xmlElement, ESettings eSettings, List<string> parents = null)
        {
            if (xmlElement == null)
                throw new ArgumentNullException("xmlElement");
            if (eSettings == null)
                throw new ArgumentNullException("eSettings");
            if (parents == null)
                parents = new List<string>();

            string parentPath = parents.Count > 0 ? string.Join(":", parents.ToArray()) + ":" : string.Empty;

            List<ESettingsException> ValidationExceptions = new List<ESettingsException>();

            foreach (PropertyInfo pi in eSettings.GetType().GetProperties())
            {
                ESettingsParser parser = pi.GetParser();
                ESettingsProtector protector = pi.GetProtector();

                string elementName = pi.GetXmlElementName();
                if (!string.IsNullOrEmpty(elementName) && pi.CanWrite)
                {
                    string elementPath = parentPath + elementName;
                    Type elementType;

                    XElement listElement = null;
                    List<XElement> childElements;
                    IList list = null;

                    if (pi.IsList())
                    {
                        elementType = pi.PropertyType.GetGenericArguments().Single(); // List element type
                        list = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(elementType));
                        string listElementName = pi.GetXmlListElementName();
                        if (listElementName != null)
                        {
                            listElement = xmlElement.Element(listElementName);
                            if (listElement != null)
                            {
                                childElements = listElement.Elements(elementName).ToList();
                            }
                            else
                            {
                                childElements = new List<XElement>(); // will be empty
                                list = null; // no parent element, null list
                            }
                        }
                        else
                        {
                            childElements = xmlElement.Elements(elementName).ToList();
                        }
                    }
                    else
                    {
                        elementType = pi.PropertyType;
                        list = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(elementType));
                        childElements = xmlElement.Elements(elementName).ToList();

                        if (childElements.Count == 0)
                            childElements.Add(null);
                    }

                    foreach (XElement childElement in childElements)
                    {
                        try
                        {
                            if (elementType.IsESettings())
                            {
                                ESettings subSettings = null;
                                if (childElement != null)
                                {
                                    subSettings = (ESettings)Activator.CreateInstance(elementType);
                                    List<string> subParents = new List<string>(parents);
                                    subParents.Add(elementName);
                                    try
                                    {
                                        DeserializeXmlToSettings(childElement, subSettings, subParents);
                                    }
                                    catch (ESettingsAggregateException aex)
                                    {
                                        if (aex.InnerExceptions.Any(p => p is ESettingsException))
                                        {
                                            ValidationExceptions.AddRange(aex.InnerExceptions.Where(p => p is ESettingsException).Cast<ESettingsException>());
                                        }
                                    }
                                }
                                list.Add(subSettings);

                                // Perform validation on the settings object
                                foreach (var a in pi.GetCustomAttributes(typeof(ESettingsValidationAttribute), false))
                                {
                                    ((ESettingsValidationAttribute)a).Validate(subSettings, pi);
                                }
                            }
                            else
                            {
                                string textValue = childElement != null ? childElement.Value : null; // could be null
                                if (protector != null && !string.IsNullOrEmpty(textValue))
                                {
                                    try
                                    {
                                        textValue = protector.Unprotect(textValue);
                                    }
                                    catch (Exception ex)
                                    {
                                        ValidationExceptions.Add(new ESettingsProtectionException(elementPath, pi.GetProtectedErrorMessage(), ex));
                                    }
                                }
                                try
                                {
                                    list.Add(parser.Parse(textValue, elementType));
                                }
                                catch (Exception ex)
                                {
                                    // Error parsing value, use Parse ErrorMessage if provided
                                    throw new ESettingsParseException(elementName, pi.GetParseErrorMessage(), ex);
                                }

                                // Perform validation on settings value
                                foreach (var a in pi.GetCustomAttributes(typeof(ESettingsValidationAttribute), false))
                                {
                                    ((ESettingsValidationAttribute)a).Validate(textValue, pi);
                                }
                            }
                        }
                        catch (ESettingsException evex)
                        {
                            evex.SettingName = elementPath;
                            ValidationExceptions.Add(evex);
                        }

                        if (!pi.IsList()) break;
                    }

                    if (pi.IsList())
                    {
                        try
                        {
                            // Perform validation on the list
                            foreach (var a in pi.GetCustomAttributes(typeof(ESettingsValidationAttribute), false))
                            {
                                ((ESettingsValidationAttribute)a).Validate(list, pi);
                            }
                        }
                        catch (ESettingsException evex)
                        {
                            evex.SettingName = elementPath;
                            ValidationExceptions.Add(evex);
                        }
                        pi.SetValue(eSettings, list, null);
                    }
                    else
                    {
                        if (list.Count > 0)
                        {
                            pi.SetValue(eSettings, list[0], null); // First element for non-lists
                        }
                    }
                }

                string attributeName = pi.GetXmlAttributeName();
                if (!string.IsNullOrEmpty(attributeName) && pi.CanWrite)
                {
                    string attributePath = parentPath + attributeName;
                    Type attributeType = pi.PropertyType;

                    XAttribute attribute = xmlElement.Attribute(attributeName);
                    string textValue = attribute != null ? attribute.Value : null; // could be null
                    if (protector != null && !string.IsNullOrEmpty(textValue))
                    {
                        try
                        {
                            textValue = protector.Unprotect(textValue);
                        }
                        catch (Exception ex)
                        {
                            ValidationExceptions.Add(new ESettingsProtectionException(attributePath, pi.GetProtectedErrorMessage(), ex));
                        }
                    }
                    try
                    {
                        if (textValue != null)
                        {
                            try
                            {
                                if (pi.CanWrite)
                                    pi.SetValue(eSettings, parser.Parse(textValue, attributeType), null);
                            }
                            catch (Exception ex)
                            {
                                // Error parsing value, use Parse ErrorMessage if provided
                                throw new ESettingsParseException(attributeName, pi.GetParseErrorMessage(), ex);
                            }

                            // Perform validation on settings value
                            foreach (var a in pi.GetCustomAttributes(typeof(ESettingsValidationAttribute), false))
                            {
                                ((ESettingsValidationAttribute)a).Validate(textValue, pi);
                            }
                        }
                    }
                    catch (ESettingsException evex)
                    {
                        evex.SettingName = attributePath;
                        ValidationExceptions.Add(evex);
                    }
                }
            }

            if (ValidationExceptions.Count > 0)
            {
                throw new ESettingsAggregateException("Settings failed validation", ValidationExceptions);
            }
        }

        private static void PrependCommentsToXml(XElement xElement, Type eSettingsType)
        {
            if (xElement == null)
                throw new ArgumentNullException("xElement");
            if (eSettingsType == null)
                throw new ArgumentNullException("eSettingsType");
            if (!eSettingsType.IsESettings())
                throw new ArgumentException(eSettingsType + " does not inherit ESettings");

            // Put comments for lists and objects before the first element
            foreach (PropertyInfo pi in eSettingsType.GetProperties())
            {
                Type valueType = pi.IsList() ? pi.PropertyType.GetGenericArguments().Single() : pi.PropertyType;

                string elementName = pi.GetXmlElementName();
                if (!string.IsNullOrEmpty(elementName))
                {
                    string comment = pi.IsXmlCommentInline() ? null : pi.GetXmlComment();

                    if (pi.IsESettings())
                    {
                        XElement firstChildElement = xElement.Element(elementName);
                        if (firstChildElement != null)
                        {
                            if (!string.IsNullOrEmpty(comment))
                                firstChildElement.AddBeforeSelf(new XComment(" " + comment + " "));
                            PrependCommentsToXml(firstChildElement, valueType);
                        }
                    }
                    else if (pi.IsList())
                    {
                        string listElementName = pi.GetXmlListElementName();
                        XElement listElement = string.IsNullOrEmpty(listElementName) ? xElement : xElement.Element(listElementName);
                        if (listElement != null)
                        {
                            bool first = true;
                            if (!string.IsNullOrEmpty(listElementName))
                            {
                                if (!string.IsNullOrEmpty(comment))
                                    listElement.AddBeforeSelf(new XComment(" " + comment + " "));
                                first = false;
                            }
                            foreach (XElement childElement in listElement.Elements(elementName))
                            {
                                if (first)
                                {
                                    if (!string.IsNullOrEmpty(comment))
                                        childElement.AddBeforeSelf(new XComment(" " + comment + " "));
                                    first = false;
                                }
                                // If list of ESettings recursively add comments
                                if (valueType.IsESettings())
                                    PrependCommentsToXml(childElement, valueType);
                                else
                                    break; // no need to loop through the rest if not ESettings
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(comment))
                        {
                            XElement firstChildElement = xElement.Element(elementName);
                            if (firstChildElement != null)
                            {
                                firstChildElement.AddBeforeSelf(new XComment(" " + comment + " "));
                            }
                        }
                    }
                }
            }
        }

        private static void AppendCommentsToXml(XElement xElement, Type eSettingsType)
        {
            if (xElement == null)
                throw new ArgumentNullException("xElement");
            if (eSettingsType == null)
                throw new ArgumentNullException("eSettingsType");
            if (!eSettingsType.IsESettings())
                throw new ArgumentException(eSettingsType + " does not inherit ESettings");

            // Put comments for values at the end of the line
            foreach (PropertyInfo pi in eSettingsType.GetProperties())
            {
                Type valueType = pi.IsList() ? pi.PropertyType.GetGenericArguments().Single() : pi.PropertyType;

                string elementName = pi.GetXmlElementName();
                if (!string.IsNullOrEmpty(elementName))
                {
                    string comment = pi.IsXmlCommentInline() ? pi.GetXmlComment() : null;
                    if (pi.IsESettings())
                    {
                        foreach (XElement childElement in xElement.Elements(elementName))
                        {
                            AppendCommentsToXml(childElement, valueType);
                        }
                    }
                    else if (pi.IsList())
                    {
                        if (valueType.IsESettings())
                        {
                            string listElementName = pi.GetXmlListElementName();
                            XElement listElement = string.IsNullOrEmpty(listElementName) ? xElement : xElement.Element(listElementName);
                            if (listElement != null)
                            {
                                foreach (XElement childElement in listElement.Elements(elementName))
                                {
                                    AppendCommentsToXml(childElement, valueType);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(comment))
                        {
                            XElement firstChildElement = xElement.Element(elementName);
                            if (firstChildElement != null)
                            {
                                XNode space = new XText(" ");
                                firstChildElement.AddAfterSelf(space);
                                space.AddAfterSelf(new XComment(" " + comment + " "));
                            }
                        }
                    }
                }
            }
        }




        #region File Monitor

        public void StartMonitoring(FileInfo file, TimeSpan interval)
        {
            lock (_fileLock)
            {
                StopMonitoring();
                _file = file;
                _fileTimerInterval = interval;
                _fileTimer = new System.Timers.Timer(interval.TotalMilliseconds); // verify interval is valid before setting it to 1 for initial load
                _fileTimer.Interval = 1;
                _fileTimer.AutoReset = false;
                _fileTimer.Elapsed += FileTimer_Elapsed;
                _fileTimer.Start();
            }
        }

        public void StopMonitoring()
        {
            lock (_fileLock)
            {
                _file = null;
                _fileTimerInterval = TimeSpan.Zero;
                if (_fileTimer != null)
                {
                    _fileTimer.Stop();
                    _fileTimer.Elapsed -= FileTimer_Elapsed;
                    _fileTimer.Dispose();
                    _fileTimer = null;
                }
            }
        }

        private void FileTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            FileInfo file = _file;
            XDocument oldSettingsXml = null;
            ESettings oldSettings = null;

            try
            {
                if (file != null)
                {
                    file.Refresh();
                    if (!_settingsMonitorLoadedTcs.Task.IsCompleted
                        || file.Exists && (file.LastWriteTimeUtc != _lastFileWriteTimeUtc || file.Length != _lastFileLength))
                    {
                        ESettingsAggregateException exception = null;
                        Exception error = null;

                        try { oldSettingsXml = ToComparableXDocument(); } catch { }

                        try
                        {
                            Load(file);
                        }
                        catch (ESettingsAggregateException esae)
                        {
                            exception = esae;
                        }
                        catch (Exception ex)
                        {
                            error = ex;
                        }

                        XDocument newSettingsXml = ToComparableXDocument();
                        if (error != null || !XNode.DeepEquals(oldSettingsXml, newSettingsXml))
                        {
                            try
                            {
                                oldSettings = (ESettings)Activator.CreateInstance(GetType());
                                oldSettings.Load(oldSettingsXml);
                            }
                            catch { }
                            OnSettingsChanged(oldSettings, exception, error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (oldSettings == null)
                {
                    if (oldSettingsXml == null)
                    {
                        try { oldSettingsXml = ToComparableXDocument(); } catch { }
                    }
                    try
                    {
                        oldSettings = (ESettings)Activator.CreateInstance(GetType());
                        oldSettings.Load(oldSettingsXml);
                    }
                    catch { }
                }
                OnSettingsChanged(oldSettings, null, ex);
            }
            finally
            {
                _settingsMonitorLoadedTcs.TrySetResult(true);
                lock (_fileLock)
                {
                    if (_fileTimer != null)
                    {
                        _fileTimer.Interval = _fileTimerInterval.TotalMilliseconds;
                        _fileTimer.Start();
                    }
                }
            }
        }

        public event EventHandler<ESettingsChangedEventArgs> SettingsChanged;
        protected virtual void OnSettingsChanged(ESettings oldSettings, ESettingsAggregateException eSettingsAggregateException, Exception error)
        {
            EventHandler<ESettingsChangedEventArgs> handler = SettingsChanged;
            if (handler != null) handler(this, new ESettingsChangedEventArgs(oldSettings, eSettingsAggregateException, error));
        }

        #endregion File Monitor

        #region Dispose

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any managed objects here.
                StopMonitoring();
            }

            // Free any unmanaged objects here
            disposed = true;
        }
        ~ESettings()
        {
            Dispose(false);
        }

        #endregion Dispose
    }

    public class ESettingsChangedEventArgs : EventArgs
    {
        public ESettings OldSettings { get; set; }
        public ESettingsAggregateException ESettingsAggregateException { get; set; }
        public Exception Error { get; set; }
        public ESettingsChangedEventArgs(ESettings oldSettings, ESettingsAggregateException eSettingsAggregateException, Exception error)
        {
            OldSettings = oldSettings;
            ESettingsAggregateException = eSettingsAggregateException;
            Error = error;
        }
    }

    public class ESettingsMonitorErrorEventArgs : EventArgs
    {
        public Exception Exception { get; set; }
        public ESettingsMonitorErrorEventArgs(Exception exception)
        {
            Exception = exception;
        }
    }

    public static partial class ESettingsExtensions
    {
        public static bool IsList(this Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>);
        }

        public static bool IsList(this PropertyInfo propertyInfo)
        {
            return propertyInfo.PropertyType.IsList();
        }

        public static bool IsESettings(this Type type)
        {
            return typeof(ESettings).IsAssignableFrom(type);
        }

        public static bool IsESettings(this PropertyInfo propertyInfo)
        {
            return propertyInfo.PropertyType.IsESettings();
        }

        public static bool IsESetting(this Type type)
        {
            return typeof(ESetting).IsAssignableFrom(type);
        }

        public static bool IsESetting(this PropertyInfo propertyInfo)
        {
            return propertyInfo.PropertyType.IsESetting();
        }

        public static bool GetAttributeExists(this PropertyInfo propertyInfo, Type type)
        {
            return typeof(Attribute).IsAssignableFrom(type) && propertyInfo.GetCustomAttributes(type, false).Any();
        }

        public static string GetXmlRootName(this Type type)
        {
            XmlRootAttribute xmlRootAttribute = type.GetCustomAttributes(typeof(XmlRootAttribute), false).FirstOrDefault() as XmlRootAttribute;
            return xmlRootAttribute != null && !string.IsNullOrEmpty(xmlRootAttribute.ElementName) ? xmlRootAttribute.ElementName : null;
        }

        public static string GetXmlAttributeName(this PropertyInfo propertyInfo)
        {
            XmlAttributeAttribute xmlAttributeAttribute = propertyInfo.GetCustomAttributes(typeof(XmlAttributeAttribute), false).FirstOrDefault() as XmlAttributeAttribute;
            return xmlAttributeAttribute != null && !string.IsNullOrEmpty(xmlAttributeAttribute.AttributeName) ? xmlAttributeAttribute.AttributeName : null;
        }

        public static string GetXmlElementName(this PropertyInfo propertyInfo)
        {
            XmlElementAttribute xmlElementAttribute = propertyInfo.GetCustomAttributes(typeof(XmlElementAttribute), false).FirstOrDefault() as XmlElementAttribute;
            return xmlElementAttribute != null && !string.IsNullOrEmpty(xmlElementAttribute.ElementName) ? xmlElementAttribute.ElementName : null;
        }

        public static string GetXmlListElementName(this PropertyInfo propertyInfo)
        {
            XmlListAttribute xmlListAttribute = propertyInfo.GetCustomAttributes(typeof(XmlListAttribute), false).FirstOrDefault() as XmlListAttribute;
            return xmlListAttribute != null && !string.IsNullOrEmpty(xmlListAttribute.ListName) ? xmlListAttribute.ListName : null;
        }

        public static string GetXmlComment(this PropertyInfo propertyInfo)
        {
            XmlCommentAttribute xmlCommentAttribute = propertyInfo.GetCustomAttributes(typeof(XmlCommentAttribute), false).FirstOrDefault() as XmlCommentAttribute;
            return xmlCommentAttribute != null && !string.IsNullOrEmpty(xmlCommentAttribute.Comment) ? xmlCommentAttribute.Comment : null;
        }

        public static bool IsXmlCommentInline(this PropertyInfo propertyInfo)
        {
            XmlCommentAttribute xmlCommentAttribute = propertyInfo.GetCustomAttributes(typeof(XmlCommentAttribute), false).FirstOrDefault() as XmlCommentAttribute;
            return xmlCommentAttribute != null ? xmlCommentAttribute.Inline : false;
        }

        public static object GetDefaultValue(this PropertyInfo pi)
        {
            Type elementType = pi.PropertyType;
            DefaultValueAttribute defaultValueAttribute = pi.GetCustomAttributes(typeof(DefaultValueAttribute), false).FirstOrDefault() as DefaultValueAttribute;
            if (defaultValueAttribute == null)
            {
                // No default specified, esettings and lists default to new instances
                if (elementType.IsESettings() || elementType.IsESetting())
                    return Activator.CreateInstance(elementType);
                else if (elementType.IsList())
                    return Activator.CreateInstance(typeof(List<>).MakeGenericType(elementType.GetGenericArguments().Single()));
                else if (elementType.IsValueType)
                    return Activator.CreateInstance(elementType);
                else
                    return null;
            }
            else if (defaultValueAttribute.Value == null)
            {
                // Default specified as null
                if (elementType.IsValueType)
                    return Activator.CreateInstance(elementType);
                else
                    return null;
            }
            else // defaultValueAttribute.Value != null
            {
                // Default value provided
                ESettingsParser parser = pi.GetParser();
                if (elementType.IsESettings())
                {
                    // Regardless of actual value create new default eSettings
                    return Activator.CreateInstance(elementType);
                }
                else if (elementType.IsList())
                {
                    // Defualt provided and not null, create a new list
                    elementType = elementType.GetGenericArguments().Single(); // List element type
                    IList list = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(elementType));
                    if (!elementType.IsList()) // List of Lists not supported
                    {
                        if (defaultValueAttribute.Value.GetType().IsArray)
                        {
                            // Defualt provided as array, parse each element and add them to the list
                            Array defaultArray = (Array)defaultValueAttribute.Value;
                            for (int i = 0; i < defaultArray.Length; i++)
                            {
                                if (defaultArray.GetValue(i) != null)
                                {
                                    if (elementType.IsESettings())
                                    {
                                        // for each non-null value add a new eSettings object...
                                        list.Add(Activator.CreateInstance(elementType));
                                    }
                                    else
                                    {
                                        // Else parse the element and add it
                                        list.Add(parser.UnparseParse(defaultArray.GetValue(i), elementType));
                                    }
                                }
                                else if (elementType.IsValueType)
                                {
                                    // null value in the defualt array: add default value of value type
                                    list.Add(Activator.CreateInstance(elementType));
                                }
                                else
                                {
                                    // null value in the defualt array: add null to the list...
                                    list.Add(null);
                                }
                            }
                        }
                        else if (elementType.IsESettings())
                        {
                            // Default provided and not null, add a new eSettings object...
                            list.Add(Activator.CreateInstance(elementType));
                        }
                        else
                        {
                            // Default provided and not null, parse it and add it to the list
                            list.Add(parser.UnparseParse(defaultValueAttribute.Value, elementType));
                        }
                    }
                    return list;
                }
                else
                {
                    // Default provided and not null, parse it
                    return parser.UnparseParse(defaultValueAttribute.Value, elementType);
                }
            }
        }

        public static bool GetHasDefaultValue(this PropertyInfo propertyInfo)
        {
            return propertyInfo.GetCustomAttributes(typeof(DefaultValueAttribute), false).Any();
        }

        public static bool IsRequired(this PropertyInfo propertyInfo)
        {
            return propertyInfo.GetCustomAttributes(typeof(RequiredAttribute), false).Any();
        }

        public static string GetParseErrorMessage(this PropertyInfo propertyInfo)
        {
            ParseAttribute parseAttribute = propertyInfo.GetCustomAttributes(typeof(ParseAttribute), false).FirstOrDefault() as ParseAttribute;
            return parseAttribute != null ? parseAttribute.ErrorMessage : new ParseAttribute().ErrorMessage;
        }

        public static ESettingsParser GetParser(this PropertyInfo propertyInfo)
        {
            Type type = propertyInfo.IsList() ? propertyInfo.PropertyType.GetGenericArguments().Single() : propertyInfo.PropertyType;
            if (type.IsESetting())
            {
                // ESetting classes parse themselves
                return new ParseAttribute(type).Parser;
            }
            else
            {
                ParseAttribute parseAttribute = propertyInfo.GetCustomAttributes(typeof(ParseAttribute), false).FirstOrDefault() as ParseAttribute;
                return parseAttribute != null ? parseAttribute.Parser : new ParseAttribute().Parser;
            }
        }

        public static string GetProtectedErrorMessage(this PropertyInfo propertyInfo)
        {
            ProtectedAttribute protectedAttribute = propertyInfo.GetCustomAttributes(typeof(ProtectedAttribute), false).FirstOrDefault() as ProtectedAttribute;
            return protectedAttribute != null ? protectedAttribute.ErrorMessage : new ProtectedAttribute().ErrorMessage;
        }

        public static ESettingsProtector GetProtector(this PropertyInfo propertyInfo)
        {
            ProtectedAttribute protectedAttribute = propertyInfo.GetCustomAttributes(typeof(ProtectedAttribute), false).FirstOrDefault() as ProtectedAttribute;
            return protectedAttribute != null ? protectedAttribute.Protector : null;
        }
    }

    public abstract class ESettingsAttribute : Attribute
    {
        public ESettingsAttribute() : base() { }
    }

    public abstract class ESettingsValidationAttribute : ESettingsAttribute
    {
        public ESettingsValidationAttribute() : base() { }
        public abstract void Validate(string value, PropertyInfo propertyInfo);
        public abstract void Validate(ESettings value, PropertyInfo propertyInfo);
        public abstract void Validate(IList value, PropertyInfo propertyInfo);
    }

    public abstract class ESettingsParser
    {
        public abstract object Parse(string value, Type type);
        public abstract string Unparse(object value, Type type);
        public object UnparseParse(object value, Type type)
        {
            return Parse(Unparse(value, type), type);
        }
        public string ParseUnparse(string value, Type type)
        {
            return Unparse(Parse(value, type), type);
        }
    }

    public abstract class ESettingsProtector
    {
        public abstract string Protect(string text);
        public abstract string Unprotect(string bytes);
    }

    /// <summary>
    /// Individual custom setting with its own parser
    /// </summary>
    public abstract class ESetting : ESettingsParser { }


}
