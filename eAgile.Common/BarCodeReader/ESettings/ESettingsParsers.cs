﻿using System;
using System.Collections.Generic;

namespace EAgile.SettingsXml
{

    public class DefaultParser : ESettingsParser
    {
        private static Dictionary<string, bool> BooleanValues = new Dictionary<string, bool>()
        {
            { "TRUE", true },
            { "YES", true },
            { "ON", true },
            { "1", true },
            { "ENABLED", true },
            { "ENABLE", true },
            { "POSITIVE", true },
            { "UP", true },
            { "HIGH", true },
            { "FALSE", false },
            { "NO", false },
            { "OFF", false },
            { "0", false },
            { "DISABLED", false },
            { "DISABLE", false },
            { "NEGATIVE", false },
            { "DOWN", false },
            { "LOW", false },
        };

        public override object Parse(string value, Type type)
        {
            if (type.IsArray)
            {
                if (string.IsNullOrEmpty(value))
                {
                    // Return empty array
                    return Array.CreateInstance(type.GetElementType(), 0);
                }
                string[] split = value.Split(',');
                object[] array = new object[split.Length];
                for (int i = 0; i < split.Length; i++)
                {
                    array[i] = Parse(split[i], type.GetElementType());
                }
                Array typeArray = Array.CreateInstance(type.GetElementType(), array.Length);
                Array.Copy(array, typeArray, array.Length);
                return typeArray;
            }
            else if (string.IsNullOrEmpty(value) && Nullable.GetUnderlyingType(type) != null)
            {
                // Hnadle null for nullable types like int?
                return null;
            }
            else if (string.IsNullOrEmpty(value) && type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            else
            {
                if (typeof(Enum).IsAssignableFrom(type))
                {
                    return Enum.Parse(type, value, true);
                }
                else if (typeof(bool).IsAssignableFrom(type))
                {
                    foreach (string s in BooleanValues.Keys)
                    {
                        if (string.Equals(value, s, StringComparison.OrdinalIgnoreCase))
                            return BooleanValues[s];
                    }
                    return bool.Parse(value);
                }
                else
                {
                    // GetUnderlyingType needed to handle nullable types like int?
                    Type safeType = Nullable.GetUnderlyingType(type) ?? type;
                    return Convert.ChangeType(value, safeType);
                }
            }
        }

        public override string Unparse(object value, Type type)
        {
            if (value == null)
            {
                return string.Empty;
            }
            else if (type.IsArray)
            {
                string[] csv = new string[((Array)value).Length];
                for (int i = 0; i < ((Array)value).Length; i++)
                {
                    csv[i] = ((Array)value).GetValue(i).ToString();
                }
                return string.Join(",", csv);
            }
            else
            {
                return value.ToString();
            }
        }
    }

}
