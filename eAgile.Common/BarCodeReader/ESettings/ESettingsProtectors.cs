﻿using System;

namespace EAgile.SettingsXml
{
    public class DefaultProtector : ESettingsProtector
    {
        public static string UniqueName { get; set; }

        // If protection is needed then uncomment one of the below methods and add required references

        #region NO PROTECTION
        public override string Protect(string text)
        {
            throw new NotImplementedException();
        }
        public override string Unprotect(string bytes)
        {
            throw new NotImplementedException();
        }
        #endregion NO PROTECTION


        #region DOT NET CORE
        //private Microsoft.AspNetCore.DataProtection.IDataProtector defaultProtector = Microsoft.AspNetCore.DataProtection.DataProtectionProvider.Create(UniqueName ?? "ESettingsProtector").CreateProtector("DefaultProtector");
        //public override string Protect(string plainText)
        //{
        //    byte[] data = System.Text.Encoding.Unicode.GetBytes(plainText);
        //    byte[] protectedData = defaultProtector.Protect(data);
        //    return Convert.ToBase64String(protectedData);
        //}
        //public override string Unprotect(string protectedText)
        //{
        //    byte[] protectedData = Convert.FromBase64String(protectedText);
        //    byte[] data = defaultProtector.Unprotect(protectedData);
        //    return System.Text.Encoding.Unicode.GetString(data);
        //}
        #endregion DOT NET CORE


        #region DOT NET FRAMEWORK
        //// Requires reference to System.Security
        //public override string Protect(string plainText)
        //{
        //    byte[] data = System.Text.Encoding.Unicode.GetBytes(plainText);
        //    byte[] entropy = System.Text.Encoding.Unicode.GetBytes(UniqueName);
        //    byte[] protectedData = System.Security.Cryptography.ProtectedData.Protect(data, entropy, System.Security.Cryptography.DataProtectionScope.LocalMachine);
        //    return Convert.ToBase64String(protectedData);
        //}
        //public override string Unprotect(string protectedText)
        //{
        //    byte[] protectedData = Convert.FromBase64String(protectedText);
        //    byte[] entropy = System.Text.Encoding.Unicode.GetBytes(UniqueName);
        //    byte[] data = System.Security.Cryptography.ProtectedData.Unprotect(protectedData, entropy, System.Security.Cryptography.DataProtectionScope.LocalMachine);
        //    return System.Text.Encoding.Unicode.GetString(data);
        //}
        #endregion DOT NET FRAMEWORK

    }

}
