﻿
using EAgile.SettingsXml;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFD
{
    [XmlRoot(ElementName = "Products")]
    public class DFDProducts : ESettings
    {
        [XmlElement(ElementName = "Product")]
        [XmlComment("Cross reference or products to EPCs")]
        public List<DFDProduct> Products { get; set; }
    }

    public class DFDProduct : ESettings
    {
        [XmlElement(ElementName = "ProductName")]
        public string ProductName { get; set; }

        [XmlElement(ElementName = "LOT")]
        public string LotNumber { get; set; }

        [XmlElement(ElementName = "EXP")]
        public DateTime ExpirationDate { get; set; }

        [XmlElement(ElementName = "EPC")]
        public List<string> EPC { get; set; }
    }
}
