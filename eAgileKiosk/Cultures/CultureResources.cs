﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace eAgileKiosk.Cultures
{
    /// <summary>
    /// Wraps up XAML access to instance of eAgileKiosk.Properties.Strings
    /// </summary>
    public class CultureResources
    {
        // https://www.codeproject.com/Articles/22967/WPF-Runtime-Localization
        // https://stackoverflow.com/questions/373388/best-way-to-implement-multi-language-globalization-in-large-net-project

        /// <summary>
        /// The Resources ObjectDataProvider uses this method to get an instance of the WPFLocalize.Properties.Resources class
        /// </summary>
        /// <returns></returns>
        public Properties.Strings GetStringsInstance()
        {
            return new Properties.Strings();
        }

        public static string GetString( string resourceString )
        {
            try
            {
                var rm = new System.Resources.ResourceManager("eAgileKiosk.Cultures.Strings", typeof( Properties.Strings ).Assembly );
                return rm.GetString( resourceString, CultureInfo.CurrentUICulture );
            }
            catch(Exception ex)
            {
                return Properties.Strings.event_2000;
            }
        }

        private static ObjectDataProvider m_provider;
        public static ObjectDataProvider ResourceProvider
        {
            get
            {
                if (m_provider == null)
                    m_provider = (ObjectDataProvider)App.Current.FindResource("Strings");
                return m_provider;
            }
        }

        /// <summary>
        /// Change the current culture used in the application.
        /// If the desired culture is available all localized elements are updated.
        /// </summary>
        /// <param name="culture">Culture to change to</param>
        public static void ChangeCulture(CultureInfo culture)
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            Properties.Strings.Culture = culture;
            ResourceProvider.Refresh();
        }
    }
}
