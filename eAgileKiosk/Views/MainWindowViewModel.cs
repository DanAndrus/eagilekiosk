﻿using eAgileKiosk.Messages;
using eAgileKiosk.Models;
using eAgileKiosk.Views.BaseModels;
using eAgileKiosk.WPF;
using GalaSoft.MvvmLight.Messaging;
using KioskManagerLib;
using KioskManagerLib.Models;
using RFIDReader;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace eAgileKiosk.Views
{
    public class MainWindowViewModel : ViewModel
    {

        public string ClockTime => _clockDateTime.ToString("t", Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        public string ClockDate => _clockDateTime.ToString("d MMM yyyy", Thread.CurrentThread.CurrentCulture.DateTimeFormat);

        private DateTime _clockDateTime = DateTime.Now;


        private readonly Task _clockTask;
        private readonly CancellationTokenSource _clockCts;

        private string _messageBarMessage = $"*** Last update time : {DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")} ***";
        public string MessageBarMessage { get => _messageBarMessage; set => SetProperty(ref _messageBarMessage, value); }
        

        public KioskController Controller { get; }

       // public BarCodeReader.BarCodeReader BarcodeReader = new BarCodeReader.BarCodeReader();

        public RfIDController ReaderRFID = new RfIDController(RfIDController.Mode.INVENTORY);

        private List<NavAction> NavActions { get; }
        public NavAction SKUEntryAction { get; }
        public NavAction AssociateAction { get; }
        public NavAction LogAction { get; }
        public NavAction ConfirmAction { get; }
        public SkuEntryViewModel SKUEntryView { get; }
        public AssociationViewModel AssociateView { get; }
        public LoggingViewModel LogView { get; }
        public NavAction CurrentContent
        {
            get => _currentContent;
            set
            {
                SetProperty(ref _currentContent, value);
                foreach (NavAction action in NavActions)
                {
                    action.IsChecked = (_currentContent == action);
                }
            }
        }
        private NavAction _currentContent;

        public RelayCommand<IClosable>CommandExit { get; private set; }


        public bool CanAssociate { get => _canAssociate; private set => SetProperty(ref _canAssociate, value); }
        private bool _canAssociate =false;

        public MainWindowViewModel()
        {

            _clockCts = new CancellationTokenSource();
            _clockTask = ClockUpdater(TimeSpan.FromSeconds(1), _clockCts.Token);

            Controller = new KioskController();
            //Create Views that will be used in the system
            SKUEntryView = new SkuEntryViewModel(Controller);
            AssociateView = new AssociationViewModel(Controller);
            LogView = new LoggingViewModel(Controller);

            NavActions = new List<NavAction>()
            {
                (SKUEntryAction = new NavAction(SKUEntryView) { GetLabel = ()=>Properties.Strings.main_SKUEntry }),
                (AssociateAction = new NavAction(AssociateView){ GetLabel = ()=>Properties.Strings.main_Association }),
                (LogAction = new NavAction(LogView){ GetLabel = ()=>Properties.Strings.main_Logging }),
                  
            };

            foreach (NavAction action in NavActions)
            {
                action.IsVisible = true;
                action.IsEnabled = true;
                action.Clicked += (sender, args) => Navigate(sender as NavAction);
            }

            CommandExit = new RelayCommand<IClosable>(ExecuteCommandExit);


            CurrentContent = SKUEntryAction;
            RegisterMessages();
            
            BarCodeReaderOnOff(true);
            BarcodeReader.BarCodeEvent += BarcodeReader_BarCodeEvent;
            ReaderRFID.RFIDEvent += ReaderRFID_RFIDEvent;
            Controller.KioskEvent += Controller_KioskEvent;
        }

        private void Controller_KioskEvent(object sender, string e)
        {

            MessageBarMessage = $"*** Last update time : {e} ***";

        }

        private async Task ClockUpdater(TimeSpan interval, CancellationToken ct)
        {
            try
            {
                while (!ct.IsCancellationRequested)
                {
                    _clockDateTime = DateTime.Now;
                    OnPropertyChanged(nameof(ClockTime), nameof(ClockDate));
                    await Task.Delay(interval, ct).ConfigureAwait(false);
                }
            }
            catch { }
        }


        private void ReaderRFID_RFIDEvent(object sender, List<EPCReport> e_report)
        {
            if (e_report.Count > 0)
            {
                var newthing = e_report[0];
                var founditem = Controller.CurrentAssociatedSkus.ScannedItems.Find(f => f.ThingID == newthing.EPC );

                if (founditem ==null)
                {
                    if (e_report.Count > 1)
                    {
                        Messenger.Default.Send(new ThingIdMessage() { NewRFIDInfo = new EPCReport("",DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture), "","") , Error = ThingError.Multiple});
                    }
                    else
                    {
                        Controller.WriteTotalSkuAssociationLog(newthing.EPC);
                        Controller.WriteSingleSkuAssociationLog(new SingleSkuAssociationModel()
                        {
                        EPC = newthing.RawEPC,
                        ThingID = newthing.EPC, // needs to change
                        TID= newthing.TID 
                        });
                        Messenger.Default.Send(new ThingIdMessage() { NewRFIDInfo = e_report[0], Error = ThingError.None });
                        Messenger.Default.Send(new LogThingIDMessage() { NewRFIDLogInfo = e_report[0] });
                    }
                }
                else
                {
                    Messenger.Default.Send(new ThingIdMessage() { NewRFIDInfo = new EPCReport(founditem.ThingID , DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture), "", ""), Error = ThingError.PreviousAssociated });
                }
            }
        }

        public void BarCodeReaderOnOff(bool isOpen)
        {
         
            if (isOpen)
            {
                BarcodeReader.OpenBarcodeReader();
            }

            else
            {
                BarcodeReader.CloseBarcodeReader();
            }

         

        }
        private void BarcodeReader_BarCodeEvent(object sender, string e)
        {

            

            if (!string.IsNullOrWhiteSpace (e))
            {
            

                bool founditem = false;
                foreach (var foundsku in Controller.CurrentSkusList.SkusList)
                {
                    if (foundsku.Name == e)
                    {
                        founditem = true;
                        break;
                    }
                }

                if(!founditem)
                {
            
                    Controller.WriteSKUFile(new SkusItem() { Name = e });
                }
            
                Messenger.Default.Send(new SkuMessage() { SKUNumber = e });
            }
            else
            {
                Messenger.Default.Send(new SkuMessage() { SKUNumber = "Bad SKU" });
            }


        }

        private void ExecuteCommandExit(IClosable window)
        {
            try
            {
                BarcodeReader.CloseBarcodeReader();
                ReaderRFID.CloseRFIDReader();

                UnRegisterMessages();
                Controller.ProcessPayload();
                Application.Current.MainWindow.Close();
            }
            finally
            {

            }
        }

        public void Navigate(NavAction navAction)
        {
            if (navAction.ViewModel != null)
            {
                    CurrentContent = navAction;
                    navAction?.ViewModel?.RefreshProperties();
                
                if (CurrentContent.TitleLabel == "ASSOCIATION")
                {
                    BarcodeReader.CloseBarcodeReader();
                    //BarcodeReader.BarcodeReader.Stop();
                    ReaderRFID.OpenRFIDReader();

                }

                if (CurrentContent.TitleLabel.Contains("SKU"))
                {
                    BarcodeReader.OpenBarcodeReader();
                    //BarcodeReader.BarcodeReader.Start();
                    //ReaderRFID.RfidReader.Disconnect();
                    ReaderRFID.CloseRFIDReader();
                }

                if (CurrentContent.TitleLabel.Contains("LOG"))
                {
                    BarcodeReader.CloseBarcodeReader();
                    //BarcodeReader.BarcodeReader.Stop();
                    //ReaderRFID.RfidReader.Disconnect();
                    ReaderRFID.CloseRFIDReader();
                }


            }
            if (navAction == ConfirmAction)
            {
                Confirm();
            }
        }
        public void Confirm()
        {
        
        }

        public override void RegisterMessages()
        {
            //Messages are used for passing information between viewmodels. This is a wrapper for events  
            Messenger.Default.Register<AssociationSkuMessage>(this, ExecuteCanAssociate);
        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Register<AssociationSkuMessage>(this, ExecuteCanAssociate);
        }

        private void ExecuteCanAssociate(AssociationSkuMessage msg)
        {
            
            if (!string.IsNullOrWhiteSpace(msg.SKUNumber))
            {
                CanAssociate = true;
            }
            else
            {
                CanAssociate = false;
            }

        }

    }


    public class NavAction : ObservableObject
    {
        public ViewModel ViewModel { get; }

        public ICommand Command => _command ?? (_command = new RelayCommand(OnClicked, () => IsEnabled));
        private ICommand _command;

        public Func<string> GetLabel = () => string.Empty;

        public string TitleLabel => GetLabel().ToUpper(Thread.CurrentThread.CurrentUICulture);
        public string ButtonLabel => GetLabel();
        public bool IsChecked { get => _isChecked; set => SetProperty(ref _isChecked, value); }
        private bool _isChecked;

        public bool IsVisible { get => _isVisible; set => SetProperty(ref _isVisible, value); }
        private bool _isVisible;

        public bool IsEnabled { get => _isEnabled; set => SetProperty(ref _isEnabled, value); }
        private bool _isEnabled;

        public NavAction(ViewModel viewModel)
        {
            ViewModel = viewModel;
        }

        public event EventHandler Clicked;
        protected virtual void OnClicked()
        {
            try { Clicked?.Invoke(this, EventArgs.Empty); } catch { }
        }

    }

}
