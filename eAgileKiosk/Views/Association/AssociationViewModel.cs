﻿using eAgileKiosk.Views.BaseModels;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using eAgileKiosk.Messages;
using System.Windows.Media;
using eAgileKiosk.WPF;
using KioskManagerLib.Configuration;
using System.Linq;
using KioskManagerLib;

namespace eAgileKiosk.Views
{
    public class AssociationViewModel : ViewModel
    {
        private AssociationModel _currentAssociation = new AssociationModel();
        public KioskController Controller { get; }
        public AssociationModel CurrentAssociation { get => _currentAssociation; set => SetProperty(ref _currentAssociation, value); }

        private string _displayMessage = Properties.Strings.Association_Default_Display_Message;
        public string DisplayMessage{ get => _displayMessage; set => SetProperty(ref _displayMessage, value); }

        public Brush _displayBackColor = DefaultBrush;
        public Brush DisplayBackColor { get => _displayBackColor; set => SetProperty(ref _displayBackColor, value); }

        public static Brush DefaultBrush { get; } = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);
        public static Brush ErrorBrush { get; } = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red);

        private string _rRFIDImage = string.Empty;
        public string RFIDImage { get => _rRFIDImage; set => SetProperty(ref _rRFIDImage, value); }

        private string _rFIDItem = string.Empty;
        public string RFIDItem { get => _rFIDItem; set => SetProperty(ref _rFIDItem, value); }

        private string _associateMessage = string.Empty;
        public string AssociateMessage { get => _associateMessage; set => SetProperty(ref _associateMessage, value); }
        

        private string _associationImage = string.Empty;
        public string AssociationImage { get => _associationImage; set => SetProperty(ref _associationImage, value); }

        

        public AssociationViewModel(KioskController controller)
        {

            Controller = controller;
            RegisterMessages();
            
        
        }

        public override void RegisterMessages()
        {
            Messenger.Default.Register<AssociationSkuMessage>(this, ExecuteDisplaySKU);
            Messenger.Default.Register<ThingIdMessage>(this, ExecuteThingIdMessage);
        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Unregister<AssociationSkuMessage>(this, ExecuteDisplaySKU);
            Messenger.Default.Unregister<ThingIdMessage>(this, ExecuteThingIdMessage);
        }


        private void ExecuteThingIdMessage(ThingIdMessage msg)
        {

            CurrentAssociation.RFIDNumber = msg.NewRFIDInfo.EPC;
            RFIDItem = msg.NewRFIDInfo.EPC;

            switch (msg.Error )
            {
                case ThingError.InvalidHeader:
                    DisplayBackColor = ErrorBrush;
                    RFIDImage = @"/Resources/Fail.png";
                    AssociationImage = @"/Resources/Fail.png";
                    DisplayMessage = Properties.Strings.Association_Tag_Error_Message;
                    CurrentAssociation.Associate = Properties.Strings.Association_Error_Previous;
                    AssociateMessage = Properties.Strings.Association_Error_Previous;
                    break;
                case ThingError.None:
                    DisplayBackColor = DefaultBrush;
                    RFIDImage = @"/Resources/check.png";
                    AssociationImage = @"/Resources/check.png";
                    DisplayMessage = Properties.Strings.Association_Reading_Display_Message;

                    CurrentAssociation.Associate = Properties.Strings.Association_Saved_Message;
                    AssociateMessage = Properties.Strings.Association_Saved_Message;

                    
                    break;
                case ThingError.PreviousAssociated:

                    DisplayBackColor = ErrorBrush;
                    RFIDImage = @"/Resources/Fail.png";
                    AssociationImage = @"/Resources/Fail.png";
                    DisplayMessage = Properties.Strings.Association_Error_Display_Message;
                    CurrentAssociation.Associate = Properties.Strings.Association_Error_Previous;
                    AssociateMessage = Properties.Strings.Association_Error_Previous;

                    break;
                case ThingError.Multiple:

                    DisplayBackColor = ErrorBrush;
                    RFIDImage = @"/Resources/Fail.png";
                    AssociationImage = @"/Resources/Fail.png";
                    DisplayMessage = Properties.Strings.Association_Tag_Error_Message;
                    CurrentAssociation.Associate = Properties.Strings.Association_Error_Multiple;
                    AssociateMessage = Properties.Strings.Association_Error_Multiple;
                    break;
            }


        }

        private void ExecuteDisplaySKU(AssociationSkuMessage msg)
        {
            if (!string.IsNullOrWhiteSpace(msg.SKUNumber))
            {
                CurrentAssociation.SkuNumber = msg.SKUNumber;

            }
            else
            {
                CurrentAssociation.SkuNumber = "";
            }

            RFIDItem = string.Empty;
            AssociateMessage = string.Empty;
            RFIDImage = string.Empty;
            AssociationImage = string.Empty;
        }
    }
}
