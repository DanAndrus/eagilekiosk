﻿using System;
using System.Collections.Generic;
using System.Text;
using eAgileKiosk.Views.BaseModels;
using System.Collections.ObjectModel;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using eAgileKiosk.WPF;
using GalaSoft.MvvmLight.Messaging;
using eAgileKiosk.Messages;
using KioskManagerLib.Models;
using KioskManagerLib;
using System.Windows;
using System.Text.RegularExpressions;

namespace eAgileKiosk.Views
{
    public class SkuEntryViewModel : ViewModel
    {

        public RelayCommand CommandReset { get; }
        public RelayCommand KeyCommand { get; }
        public RelayCommand ActivateCommand { get; }
        
        public KioskController Controller { get; }
        
        private string _activeSKU = "Current SKU" ;
        public string ActiveSKU { get => _activeSKU; set => SetProperty(ref _activeSKU, value); }

        private string _textitem;
        public string Textitem 
        { 
            get => _textitem;
            set
            {
                SetProperty(ref _textitem, value);
                if(_textitem.Length ==1 )
                {
                    SKUSelected = null;
                    CanAssociate = true;
                }
                
            }
        }

        public bool CanAssociate { get => _canAssociate; private set => SetProperty(ref _canAssociate, value); }
        private bool _canAssociate = false;

        private ObservableCollection<SkusItem> _skuData;
        public ObservableCollection<SkusItem> SkuData { get => _skuData; set => SetProperty(ref _skuData, value); }

        private SkusItem _skuSelected;
        public SkusItem SKUSelected
        {
            get => _skuSelected;

            set
            {

                SetProperty(ref _skuSelected, value);
                
                if (_skuSelected != null)
                {
                    CanAssociate = true;
                    Textitem = string.Empty;
                }
            }
        }

        private void NewSkuSelected(string actSKU)
        {
            if (!string.IsNullOrWhiteSpace (actSKU))
            {
                Messenger.Default.Send(new AssociationSkuMessage () { SKUNumber = actSKU });
                ActiveSKU = actSKU;
                Controller.ActiveSKU = ActiveSKU;
                Controller.ReadTotalSKUFile();
            }
            //else
            //{
            //    Messenger.Default.Send(new AssociationSkuMessage() { SKUNumber = "" });
            //    ActiveSKU = "Active SKU:";
            //    Controller.ActiveSKU = string.Empty;
            //}
        }

        
        public SkuEntryViewModel(KioskController controller)
        {
            Controller = controller;

            CommandReset = new RelayCommand(ExecuteCommandReset, CanReset);
            
            KeyCommand = new RelayCommand(ExecuteKeyCommand, CanKey);

            ActivateCommand = new RelayCommand(ExecuteActivateCommand, CanActivate);

            SkuData = new ObservableCollection<SkusItem>( controller.CurrentSkusList.SkusList);

            CanAssociate = false;

            RegisterMessages();

            ExecuteActivateCommand();
        }

        private void ExecuteNewBarCode(SkuMessage msg)
        {
            //var newitem = new SkusItem() { Name = msg.SKUNumber };

            SkuData = new ObservableCollection<SkusItem>(Controller.CurrentSkusList.SkusList);
            //var tmpsku = new ObservableCollection<SkusItem>( SkuData);

            //tmpsku.Add(newitem);

            //MessageBox.Show($"{tmpsku.Count}");

            //SkuData = tmpsku;

            //foreach ( var foundsku in SkuData)
            //{
            //       if(foundsku.Name  == msg.SKUNumber)
            //       {
            //           SKUSelected = foundsku;
            //           break;
            //       }
            //}


        }


        public static Boolean isAlphaNumeric(string strToCheck)
        {
            Regex rg = new Regex(@"^[a-zA-Z0-9]*$");
            return rg.IsMatch(strToCheck);
        }

        private bool CanActivate()
        {
            return true;
        }
        private void ExecuteActivateCommand()
        {
            try
            {

                string passSKU = string.Empty;
                if (SKUSelected !=null)
                {
                    passSKU = SKUSelected.Name;
                }
                else if (!string.IsNullOrWhiteSpace(Textitem))
                {

                    if (isAlphaNumeric(Textitem))
                    {
                        bool founditem = false;
                        foreach (var foundsku in SkuData)
                        {
                            if (foundsku.Name == Textitem)
                            {
                                founditem = true;
                                break;
                            }
                        }
                        passSKU = Textitem;
                        if (!founditem)
                        {

                            Controller.WriteSKUFile(new SkusItem() { Name = Textitem });
                            Messenger.Default.Send(new SkuMessage() { SKUNumber = Textitem });

                        }
                    }
                    else
                    {

                        return;
                    }
                    
                }

                if(!string.IsNullOrWhiteSpace(passSKU) )
                {
                    NewSkuSelected(passSKU);
                }
                
            }
            catch
            {

            }
        }

        private bool CanKey()
        {
            return true;
        }
        private void ExecuteKeyCommand()
        {
            try
            {

                //foreach (var foundsku in SkuData)
                //{
                //    if (foundsku.Name ==  Textitem )
                //    {
                //        SKUSelected = foundsku;
                //        break;
                //    }
                //}

                //if (!string.IsNullOrWhiteSpace(Textitem))
                //{
                //    Controller.WriteSKUFile(new SkusItem() { Name = Textitem });
                //    Messenger.Default.Send(new SkuMessage() { SKUNumber = Textitem });
                //}
            }
            finally
            {

            }
        }

        private bool CanReset()
        {
            return true;
        }
        private void ExecuteCommandReset()
        {
            try
            {
                SKUSelected = null;
                Textitem = string.Empty;
                ActiveSKU = string.Empty;
                Controller.ActiveSKU = string.Empty;
                CanAssociate = false;
                Messenger.Default.Send(new AssociationSkuMessage());
            }
            finally
            {

            }
        }

        public override void RegisterMessages()
        {
            Messenger.Default.Register<SkuMessage>(this, ExecuteNewBarCode);

        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Unregister<SkuMessage>(this, ExecuteNewBarCode);

        }




    }
}
