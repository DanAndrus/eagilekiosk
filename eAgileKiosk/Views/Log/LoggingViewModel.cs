﻿using eAgileKiosk.Messages;
using eAgileKiosk.Views.BaseModels;
using GalaSoft.MvvmLight.Messaging;
using KioskManagerLib;
using KioskManagerLib.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace eAgileKiosk.Views
{
    public class LoggingViewModel : ViewModel
    {
        public TotalSkuAssociationModel DisplayThing { get; set; } = new TotalSkuAssociationModel();
        private ObservableCollection<KioskLogModel> _scannedItems;
        public ObservableCollection<KioskLogModel> ScannedItems { get => _scannedItems; set => SetProperty(ref _scannedItems, value); }
            
        public KioskController Controller { get; }

        public LoggingViewModel(KioskController controller)
        {

            Controller = controller;
            RegisterMessages();
            DisplayThing = Controller.CurrentAssociatedSkus;

            ScannedItems = new ObservableCollection<KioskLogModel>();


            if (Controller.CurrentAssociatedSkus.ScannedItems != null)
            {
        
            }
            
        }

        public override void RegisterMessages()
        {
            Messenger.Default.Register<LogThingIDMessage>(this, ExecuteThingIdMessage);
            Messenger.Default.Register<LogGotFocusMessage>(this, ExecuteLogGotFocusMessage);
            // Messenger.Default.Register<AssociationSkuMessage>(this, ExecuteDisplaySKU);

        }

        public override void UnRegisterMessages()
        {
            Messenger.Default.Unregister<LogThingIDMessage>(this, ExecuteThingIdMessage);
            Messenger.Default.Unregister<LogGotFocusMessage>(this, ExecuteLogGotFocusMessage);
            // Messenger.Default.Unregister<AssociationSkuMessage>(this, ExecuteDisplaySKU);
        }
        private void ExecuteLogGotFocusMessage(LogGotFocusMessage msg)
        {
       //     Controller.ReadTotalSKUFile();
     //       ScannedItems = new ObservableCollection<KioskLogModel>();
        }
        private void ExecuteThingIdMessage(LogThingIDMessage msg)
        {
            // Controller.WriteTotalSkuAssociationLog(msg.NewRFIDLogInfo.EPC);

            List<KioskLogModel> tmplist =new List<KioskLogModel>() ;
            foreach (var stupid in ScannedItems)
            {
                tmplist.Add(stupid);
            }

            tmplist.Add(new KioskLogModel()
            {
                EPC = msg.NewRFIDLogInfo.EPC,
                Location = Controller.Kiosksettings.KioskID,
                RawEPC = msg.NewRFIDLogInfo.RawEPC,
                TID = msg.NewRFIDLogInfo.TID,
                TimeStamp = msg.NewRFIDLogInfo.TimeStamp
            });

            //new KioskLogModel()
            //{
            //    EPC = msg.NewRFIDLogInfo.EPC,
            //    Location = Controller.Kiosksettings.KioskID,
            //    RawEPC = msg.NewRFIDLogInfo.RawEPC,
            //    TID = msg.NewRFIDLogInfo.TID,
            //    TimeStamp = msg.NewRFIDLogInfo.TimeStamp
            //};


            //_scannedItems.Add (new KioskLogModel()
            //{
            //    EPC = msg.NewRFIDLogInfo.EPC,
            //    Location = Controller.Kiosksettings.KioskID,
            //    RawEPC = msg.NewRFIDLogInfo.RawEPC,
            //    TID = msg.NewRFIDLogInfo.TID,
            //    TimeStamp = msg.NewRFIDLogInfo.TimeStamp
            //});




            ScannedItems = new ObservableCollection<KioskLogModel>(tmplist) ; 


        }

        public class SkuItems
        {
            public string SKU { get; set; }
        }
    }
}
