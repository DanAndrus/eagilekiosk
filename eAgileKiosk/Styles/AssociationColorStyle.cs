﻿using KioskManagerLib.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace eAgileKiosk.Styles
{
    public class AssociationColorStyle : StyleSelector
    {
        public Style DefaultColor { get; set; }
        public Style ErrorColor { get; set; }
        
        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item.GetType() == typeof(AssociationModel))
            {
                var isERROR = ((AssociationModel)item).IsError;
                if(isERROR)
                {
                    return ErrorColor;
                }
            }

            return DefaultColor;
        }
    }

}
