﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eAgileKiosk.Models
{
    public interface IClosable
    {
        void Close();
    }
}
