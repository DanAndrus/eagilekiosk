﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eAgileLogging.Models
{
    public class SingleSkuAssociationModel
    {
        public enum ErrorLevel
        {
            Error,
            Warn,
            Info,
            Debug,
        }

            public string Message { get; set; }

            public DateTime Timestamp { get; set; } = DateTime.UtcNow;

          
            public int ErrorCode { get; set; }

            public ErrorLevel Level { get; set; }

            public Exception Error { get; set; }
    
            public DateTime LastErrorTime { get; set; } = DateTime.UtcNow;

            public int ErrorCount { get; set; } = 1;

            public int ResetFault_Index { get; set; } = -1;




    }
}
