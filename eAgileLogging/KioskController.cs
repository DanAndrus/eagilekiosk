﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace eAgileKioskController
{
    public class KioskController
    {
        public KioskController()
        {

            CreateDirectories();
        }

        private void CreateDirectories()
        {

            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\Association Kiosk\Settings");
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\Association Kiosk\TagLog");
            //System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\Association Kiosk\TagLog\Directory");
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\Association Kiosk\TagLog\Cache");
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\Association Kiosk\TagLog\Archive");
            System.IO.Directory.CreateDirectory(@"C:\ProgramData\eAgile\Association Kiosk\ErrorLog");
            if (!File.Exists(@"C:\ProgramData\eAgile\Association Kiosk\Settings\AK_Settings.json"))
            {
                WriteSettingsFile();
            }

            if (!File.Exists(@"C:\ProgramData\eAgile\Association Kiosk\Settings\AK_Connection.json"))
            {

            }
            if (!File.Exists(@"C:\ProgramData\eAgile\Association Kiosk\Settings\AK_SKU.json"))
            {
                WriteSKUFile();
            }

        }
        private static void WriteSettingsFile()
        {

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("KioskID");
                writer.WriteValue("eAgile01");
                writer.WritePropertyName("ReaderSettings");
                writer.WriteStartArray();
                writer.WriteStartObject();
                writer.WritePropertyName("ReaderAddress");
                writer.WriteValue("COM3");
                writer.WritePropertyName("ReaderPower");
                writer.WriteValue("15");
                writer.WriteEndObject();
                writer.WriteEndArray();

                writer.WritePropertyName("BarCodeSettings");
                writer.WriteStartArray();
                writer.WriteStartObject();
                writer.WritePropertyName("BarcodeAddress");
                writer.WriteValue("COM4");
                writer.WritePropertyName("BarcodeLength");
                writer.WriteValue("11");
                writer.WriteEndObject();
                writer.WriteEndArray();
                writer.WritePropertyName("ErrorSettings");
                writer.WriteStartArray();
                writer.WriteStartObject();
                writer.WritePropertyName("Level");
                writer.WriteValue("error");
                writer.WritePropertyName("PurgeDelay");
                writer.WriteValue("90");
                writer.WritePropertyName("submit");
                writer.WriteValue("Daily");
                writer.WriteEndObject();
                writer.WriteEndArray();

                writer.WritePropertyName("LogSettings");
                writer.WriteStartArray();
                writer.WriteStartObject();
                writer.WritePropertyName("Submit");
                writer.WriteValue("daily");
                writer.WritePropertyName("PurgeDelay");
                writer.WriteValue("90");
                writer.WriteEndObject();
                writer.WriteEndArray();

                writer.WritePropertyName("tagSettings");
                writer.WriteStartArray();
                writer.WriteStartObject();
                writer.WritePropertyName("TidHeader");
                writer.WriteValue("daily");
                writer.WritePropertyName("epcHeader");
                writer.WriteValue("01");
                writer.WriteEndObject();
                writer.WriteEndArray();



            }
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\ProgramData\eAgile\Association Kiosk\Settings\AK_Settings.json"))
            {
                file.WriteLine(sb.ToString()); // "sb" is the StringBuilder
            }
        }
        private static void WriteSKUFile()
        {

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.None;

                writer.WriteStartArray();
                for (int j = 1; j <= 3; ++j)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName("Name");
                    writer.WriteValue(RandomString(10));
                    writer.WriteEndObject();
                }
                writer.WriteEndArray();

            }
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\ProgramData\eAgile\Association Kiosk\Settings\AK_SKU.json"))
            {
                file.WriteLine(sb.ToString()); // "sb" is the StringBuilder
            }
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }



    }
}
